# Dow Advanced Map Editor  #
[AdvMissionEditor thread in RelicNews Forum](http://forums.relicnews.com/showthread.php?226355)
## Basic ##
The goal of this tool is to provide additional features for map editing to the vanilla map editor.
## Features ##
* copy/paste between maps (including heightmap, decals, ...)
* detailed table edition of tiles, decals, entities,... 
* resize map, turn map, ...
* detailed table Egroup and Sgroup management (assignement, names, ...)
* advanced deformation of objects that are present on the map
* minimap and installer generators
* ...

Details in [thread](http://forums.relicnews.com/showthread.php?226355)
### [Download section](https://bitbucket.org/dark40k/dowadvmapeditor/downloads) ###
![AME-v1.00-01.jpg](https://bitbucket.org/repo/KREXb4/images/258446399-AME-v1.00-01.jpg)