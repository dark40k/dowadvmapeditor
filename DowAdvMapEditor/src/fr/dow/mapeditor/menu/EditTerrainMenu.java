package fr.dow.mapeditor.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.CommandsEditTerrain;
import fr.dow.mapviewer.display.MapViewer;

public class EditTerrainMenu implements ActionListener {

	private static final String cmdEditDecals="cmdEditDecals";
	private static final String cmdEditTiles="cmdEditTiles";
	private static final String cmdEditMarkers="cmdEditMarkers";
	private static final String cmdEditGround="cmdEditGround";
	private static final String cmdEditSmoother="cmdEditSmoother";
	private static final String cmdImportPASM="cmdImportPASM";
	
	private MapEditor mapEditor;
	
	private JMenu menu;
	
	public EditTerrainMenu(MapEditor mapEditor) {
		
		this.mapEditor=mapEditor;
		
		JMenuItem menuItem;
		
        menu = new JMenu("Ground");
        menu.setMnemonic(KeyEvent.VK_D);

        menuItem = new JMenuItem("Edit Decals");
        menuItem.setActionCommand(cmdEditDecals);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Edit Tiles");
        menuItem.setActionCommand(cmdEditTiles);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Edit Markers");
        menuItem.setActionCommand(cmdEditMarkers);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menu.addSeparator();
        
        menuItem = new JMenuItem("Edit Ground");
        menuItem.setActionCommand(cmdEditGround);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Smooth Ground");
        menuItem.setActionCommand(cmdEditSmoother);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menu.addSeparator();
        
        menuItem = new JMenuItem("Import Path Sector");
        menuItem.setActionCommand(cmdImportPASM);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
	}
	
	public JMenu getMenu() {
		return menu;
	}
	
    public void actionPerformed(ActionEvent e) {
    	
    	String actionCommand=e.getActionCommand();
    	
        MapViewer curMapViewer = mapEditor.getSelectedMapViewer();
        if (curMapViewer==null) return;
    	CommandsEditTerrain editTerrainCommands=curMapViewer.commandsEditTerrain;
    	
        if (cmdEditDecals.equals(actionCommand)) editTerrainCommands.editDecals(mapEditor);
        else if (cmdEditTiles.equals(actionCommand)) editTerrainCommands.editTiles(mapEditor);
        else if (cmdEditMarkers.equals(actionCommand)) editTerrainCommands.editMarkers(mapEditor);
        else if (cmdEditGround.equals(actionCommand)) editTerrainCommands.editHeightMap(mapEditor);
        else if (cmdEditSmoother.equals(actionCommand)) editTerrainCommands.smoothHeightMap(mapEditor);
        else if (cmdImportPASM.equals(actionCommand)) editTerrainCommands.importPASM(mapEditor);

    }
	
	
}
