package fr.dow.mapeditor.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.AboutDialog;

public class AboutMenu implements ActionListener {
	
	private static final String cmdShowAbout="cmdShowAbout";
	
	private MapEditor mapEditor;
	
	private JMenu menu;
	
	public AboutMenu(MapEditor mapEditor) {
		
		this.mapEditor=mapEditor;
		
		JMenuItem menuItem;
		
        menu = new JMenu("About");

        menuItem = new JMenuItem("About AME");
        menuItem.setActionCommand(cmdShowAbout);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        
	}
	
	public JMenu getMenu() {
		return menu;
	}
	
    public void actionPerformed(ActionEvent e) {
    	
    	String actionCommand=e.getActionCommand();
    	
        if (cmdShowAbout.equals(actionCommand)) { AboutDialog.showDialog(mapEditor); return;}
    	
//    	CommandsAbout commandsAbout=mapEditor.getSelectedMapViewer().commandsAbout;
    	
    }

}
