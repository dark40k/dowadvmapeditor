package fr.dow.mapeditor.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.CommandsFile;
import fr.dow.mapviewer.display.MapViewer;

public class FileMenu implements ActionListener {

	private static final String cmdOpen="cmdOpen"; 
	private static final String cmdOpenMapInMod="cmdOpenMapInMod"; 
	private static final String cmdModLoadingParameters="cmdModLoadingParameters"; 
	private static final String cmdSave="cmdSave"; 
	private static final String cmdSaveAs="cmdSaveAs"; 
	private static final String cmdQuit="cmdQuit"; 
	private static final String cmdGenIsntaller="cmdGenIsntaller";

	private static final String cmdCreateMinimap="cmdCreateMinimap";
	
	private MapEditor mapEditor;
	
	private JMenu menu;
	
	private RecentSubMenu recentSubMenu;
	
	public FileMenu(MapEditor mapEditor) {
		
		this.mapEditor=mapEditor;
		
		JMenuItem menuItem;
		
        menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_D);

        menuItem = new JMenuItem("Open Map");
        menuItem.setActionCommand(cmdOpen);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Open Map in Mod");
        menuItem.setActionCommand(cmdOpenMapInMod);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        recentSubMenu=new RecentSubMenu("Open Recent",mapEditor);
        menu.add(recentSubMenu.getJMenu());
        
        menuItem = new JMenuItem("Mod Loading Parameters");
        menuItem.setActionCommand(cmdModLoadingParameters);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menu.addSeparator();

        menuItem = new JMenuItem("Create MiniMap");
        menuItem.setActionCommand(cmdCreateMinimap);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menu.addSeparator();

        menuItem = new JMenuItem("Generate NSIS Installer");
        menuItem.setActionCommand(cmdGenIsntaller);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menu.addSeparator();

        menuItem = new JMenuItem("Save Map");
        menuItem.setActionCommand(cmdSave);
        menuItem.addActionListener(this);
        menu.add(menuItem);

        menuItem = new JMenuItem("Save Map As.");
        menuItem.setActionCommand(cmdSaveAs);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menu.addSeparator();

        menuItem = new JMenuItem("Quit");
        menuItem.setActionCommand(cmdQuit);
        menuItem.addActionListener(this);
        menu.add(menuItem);
		
	}
	
	public JMenu getMenu() {
		return menu;
	}
	
    public void actionPerformed(ActionEvent e) {
    	
    	String actionCommand=e.getActionCommand();
    	
        if (cmdOpen.equals(actionCommand)) { mapEditor.openMapFileUsingDialog(); return; }
        else if (cmdOpenMapInMod.equals(actionCommand)) { mapEditor.openMapInModUsingDialog(); return; }
        else if (cmdQuit.equals(actionCommand)) { mapEditor.quit(); return; }
        else if (cmdModLoadingParameters.equals(actionCommand)) { mapEditor.EditModLoadingParameters(); return; }
        	
        MapViewer curMapViewer = mapEditor.getSelectedMapViewer();
        if (curMapViewer==null) return;
    	CommandsFile fileCommands=curMapViewer.commandsFile;
    	
        if (cmdCreateMinimap.equals(actionCommand)) fileCommands.createMinimap(mapEditor);
        else if (cmdGenIsntaller.equals(actionCommand)) fileCommands.genInstaller(mapEditor);
        else if (cmdSave.equals(actionCommand)) fileCommands.save(mapEditor);
        else if (cmdSaveAs.equals(actionCommand)) fileCommands.saveAs(mapEditor);
    	
    }
	
	
}
