package fr.dow.mapeditor.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.CommandsSelection;
import fr.dow.mapviewer.display.MapViewer;

public class SelectionMenu implements ActionListener {
	
	private static final String cmdClearSelection="cmdClearSelection";

	private static final String cmdFilterAll="cmdFilterAll";
	private static final String cmdFilterNone="cmdFilterNone";

	private MapEditor mapEditor;
	
	private JMenu menu;
	
	public SelectionMenu(MapEditor mapEditor) {
		
		this.mapEditor=mapEditor;
		
		JMenuItem menuItem;
		
        menu = new JMenu("Selection");

        menuItem = new JMenuItem("Clear Selection");
        menuItem.setActionCommand(cmdClearSelection);
        menuItem.addActionListener(this);
        menu.add(menuItem);

        menu.addSeparator();
        
        menuItem = new JMenuItem("Filter All");
        menuItem.setActionCommand(cmdFilterAll);
        menuItem.addActionListener(this);
        menu.add(menuItem);
       
        menuItem = new JMenuItem("Filter None");
        menuItem.setActionCommand(cmdFilterNone);
        menuItem.addActionListener(this);
        menu.add(menuItem);
       
        
	}
	
	public JMenu getMenu() {
		return menu;
	}
	
    public void actionPerformed(ActionEvent e) {
    	
    	String actionCommand=e.getActionCommand();
    	
        MapViewer curMapViewer = mapEditor.getSelectedMapViewer();
        if (curMapViewer==null) return;
    	CommandsSelection commandsSelection=curMapViewer.commandsSelection;
    	
        if (cmdClearSelection.equals(actionCommand)) { commandsSelection.clearSelection(); return;}
        else if (cmdFilterAll.equals(actionCommand)) { commandsSelection.setFilterAll(); return;}
        else if (cmdFilterNone.equals(actionCommand)) { commandsSelection.setFilterNone(); return;}
        
        
    }

}
