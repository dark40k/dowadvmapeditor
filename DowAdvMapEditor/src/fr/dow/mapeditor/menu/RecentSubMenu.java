package fr.dow.mapeditor.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.RecentMapsList;
import fr.dow.mapeditor.RecentMapsList.UpdateRecentMapListEvent;


public class RecentSubMenu implements RecentMapsList.UpdateRecentMapListListener, ActionListener {
	
	private static final String textNoMap="< Empty >"; 

	MapEditor mapEditor;
	
	JMenu jMenu;
	JMenuItem[] jRecentMapItems; 
	
	public RecentSubMenu(String title, MapEditor mapEditor) {
		
		this.mapEditor=mapEditor;
		
		jMenu=new JMenu(title);
		
		JMenuItem menuItem;
		jRecentMapItems=new JMenuItem[RecentMapsList.maxMaps];
		for (int i=0;i<jRecentMapItems.length;i++) {
			menuItem = new JMenuItem("");
	        menuItem.setActionCommand(textNoMap);
	        menuItem.addActionListener(this);
	        
	        jRecentMapItems[i]=menuItem;
	        jMenu.add(menuItem);
		}
		
		mapEditor.recentMapList.addUpdateListener(this);

		updateSubMenuTitles();
		
	}
	
	private void updateSubMenuTitles() {
		
		String mapName;
		
		for (int i=0;i<RecentMapsList.maxMaps;i++) {
			
			try {
				mapName=mapEditor.recentMapList.recentMapNames.get(i);
			}
			catch (ArrayIndexOutOfBoundsException e) {
				mapName=null;
			}
			
			if (mapName==null) {
				mapName=""; 
	        	jRecentMapItems[i].setActionCommand(textNoMap);
			} else {
		        jRecentMapItems[i].setActionCommand(mapName);
			}
			
	        jRecentMapItems[i].setText(Integer.toString(i)+" - "+mapName);
	        
		}
		
	}
	
	public JMenu getJMenu() {
		return jMenu;
	}

	@Override
	public void recentMapListUpdated(UpdateRecentMapListEvent evt) {
		updateSubMenuTitles();		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
    	String actionCommand=e.getActionCommand();
    	
        if (textNoMap.equals(actionCommand)) return; 
        	
       	mapEditor.openMap(new File(actionCommand)); 
       	
	}
	
}
