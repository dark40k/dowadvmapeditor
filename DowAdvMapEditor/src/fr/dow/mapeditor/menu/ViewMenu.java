package fr.dow.mapeditor.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.CommandsView;
import fr.dow.mapviewer.display.MapViewer;

public class ViewMenu implements ActionListener {

	private static final String cmdRedraw="cmdRedraw";
	
	private static final String cmdViewTop="cmdViewTop";
	private static final String cmdViewStd="cmdViewStd";
	
	
	private static final String cmdViewVisibily="cmdViewVisibily";
	private static final String cmdViewDisplay="cmdViewDisplay";
	private static final String cmdViewTerrainDisp="cmdViewTerrainDisp";
	private static final String cmdViewMarkersDisp="cmdViewMarkersDisp";
	private static final String cmdViewSelectionDisp="cmdViewSelectionDisp";
	
	private static final String cmdViewConsole="cmdViewConsole";
	
	private MapEditor mapEditor;
	
	private JMenu menu;
	
	public ViewMenu(MapEditor mapEditor) {
		
		this.mapEditor=mapEditor;
		
		JMenuItem menuItem;
		
        menu = new JMenu("View");
        menu.setMnemonic(KeyEvent.VK_D);

        menuItem = new JMenuItem("Redraw");
        menuItem.setActionCommand(cmdRedraw);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menu.addSeparator();

        menuItem = new JMenuItem("Top View");
        menuItem.setActionCommand(cmdViewTop);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Std View");
        menuItem.setActionCommand(cmdViewStd);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menu.addSeparator();
        
        menuItem = new JMenuItem("Entities Preferences");
        menuItem.setActionCommand(cmdViewVisibily);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Players Preferences");
        menuItem.setActionCommand(cmdViewDisplay);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Terrain Preferences");
        menuItem.setActionCommand(cmdViewTerrainDisp);
        menuItem.addActionListener(this);
        menu.add(menuItem);
                
        menuItem = new JMenuItem("Markers Preferences");
        menuItem.setActionCommand(cmdViewMarkersDisp);
        menuItem.addActionListener(this);
        menu.add(menuItem);
                
        menuItem = new JMenuItem("Selection Preferences");
        menuItem.setActionCommand(cmdViewSelectionDisp);
        menuItem.addActionListener(this);
        menu.add(menuItem);
		
        menu.addSeparator();
        
        menuItem = new JMenuItem("Console");
        menuItem.setActionCommand(cmdViewConsole);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
	}
	
	public JMenu getMenu() {
		return menu;
	}
	
    public void actionPerformed(ActionEvent e) {
    	
    	String actionCommand=e.getActionCommand();
    	
        if (cmdRedraw.equals(actionCommand)) { mapEditor.repaint(); return; }
        
        if (cmdViewConsole.equals(actionCommand)) { mapEditor.showConsole(); return;}
    	
        MapViewer curMapViewer = mapEditor.getSelectedMapViewer();
        if (curMapViewer==null) return;
    	CommandsView viewCommands=curMapViewer.commandsView;
    	
        if (cmdViewVisibily.equals(actionCommand)) { viewCommands.editVisibility(mapEditor); return;}
        else if (cmdViewDisplay.equals(actionCommand)) { viewCommands.editPlayersDisplay(mapEditor); return;}
        else if (cmdViewTerrainDisp.equals(actionCommand)) { viewCommands.editTerrainDisplay(mapEditor); return;}
        else if (cmdViewMarkersDisp.equals(actionCommand)) { viewCommands.editMarkersDisplay(mapEditor); return;}
        else if (cmdViewSelectionDisp.equals(actionCommand)) { viewCommands.editSelectionDisplay(mapEditor); return;}
        else if (cmdViewTop.equals(actionCommand)) { viewCommands.setViewTop(); return;}
        else if (cmdViewStd.equals(actionCommand)) { viewCommands.setViewStd(); return;}
    	
    }
	
	
}
