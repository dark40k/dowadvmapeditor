package fr.dow.mapeditor.menu;

import javax.swing.JMenuBar;

import fr.dow.mapeditor.MapEditor;

public class MapEditorMenuBar {
	
	private JMenuBar jMenuBar;

	public FileMenu fileMenu;
	public ViewMenu viewMenu;
	public SelectionMenu selectionMenu;
	public EditMapMenu editMapMenu;
	public EditTerrainMenu editTerrainMenu;
	public EntitiesMenu entitiesMenu;
	public AboutMenu aboutMenu;
	
	public MapEditorMenuBar(MapEditor mapEditor) {
		
		fileMenu=new FileMenu(mapEditor);
		viewMenu=new ViewMenu(mapEditor);
		selectionMenu=new SelectionMenu(mapEditor);
		editMapMenu=new EditMapMenu(mapEditor);
		editTerrainMenu=new EditTerrainMenu(mapEditor);
		entitiesMenu=new EntitiesMenu(mapEditor);
		aboutMenu=new AboutMenu(mapEditor);
		
        jMenuBar = new JMenuBar();
        jMenuBar.add(fileMenu.getMenu());
        jMenuBar.add(viewMenu.getMenu());
        jMenuBar.add(selectionMenu.getMenu());        
        jMenuBar.add(editMapMenu.getMenu());
        jMenuBar.add(editTerrainMenu.getMenu());
        jMenuBar.add(entitiesMenu.getMenu());
        jMenuBar.add(aboutMenu.getMenu());
        
        
	}
	
	public JMenuBar getJMenuBar() {
		return jMenuBar;
	}
	
}
