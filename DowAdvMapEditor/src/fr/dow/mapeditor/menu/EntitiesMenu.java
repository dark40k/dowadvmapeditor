package fr.dow.mapeditor.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.CommandsGroups;
import fr.dow.mapviewer.display.MapViewer;

public class EntitiesMenu implements ActionListener {
	
	private static final String cmdManageEntities="cmdManageEntities";
	private static final String cmdRotateEntities="cmdRotateEntities";

	private static final String cmdManageSquads="cmdManageSquads";
	
	private static final String cmdManageSGroups="cmdCopyArea";
	private static final String cmdManageEGroups="cmdRotateMap";
	
	private MapEditor mapEditor;
	
	private JMenu menu;
	
	public EntitiesMenu(MapEditor mapEditor) {
		
		this.mapEditor=mapEditor;
		
		JMenuItem menuItem;
		
        menu = new JMenu("Entities");

        menuItem = new JMenuItem("Edit Entities");
        menuItem.setActionCommand(cmdManageEntities);
        menuItem.addActionListener(this);
        menu.add(menuItem);

        menuItem = new JMenuItem("Rotate Entities");
        menuItem.setActionCommand(cmdRotateEntities);
        menuItem.addActionListener(this);
        menu.add(menuItem);

        menu.addSeparator();
        
        menuItem = new JMenuItem("Edit Squads");
        menuItem.setActionCommand(cmdManageSquads);
        menuItem.addActionListener(this);
        menu.add(menuItem);

        menu.addSeparator();
        
        menuItem = new JMenuItem("Entity Group List");
        menuItem.setActionCommand(cmdManageEGroups);
        menuItem.addActionListener(this);
        menu.add(menuItem);
		
        menuItem = new JMenuItem("Squad Group List");
        menuItem.setActionCommand(cmdManageSGroups);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        
	}
	
	public JMenu getMenu() {
		return menu;
	}
	
    public void actionPerformed(ActionEvent e) {
    	
    	String actionCommand=e.getActionCommand();
    	
        MapViewer curMapViewer = mapEditor.getSelectedMapViewer();
        if (curMapViewer==null) return;
    	CommandsGroups editMapCommands=curMapViewer.commandsGroups;
    	
        if (cmdManageEntities.equals(actionCommand)) { editMapCommands.editEntities(mapEditor); return;}
        else if (cmdRotateEntities.equals(actionCommand)) { editMapCommands.RotateEntities(mapEditor); return;}
        else if (cmdManageSquads.equals(actionCommand)) { editMapCommands.editSquads(mapEditor); return;}
        else if (cmdManageEGroups.equals(actionCommand)) { editMapCommands.editEGroups(mapEditor); return;}
        else if (cmdManageSGroups.equals(actionCommand)) { editMapCommands.editSGroups(mapEditor); return;}
    	
    }

}
