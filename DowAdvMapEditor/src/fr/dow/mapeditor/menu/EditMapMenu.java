package fr.dow.mapeditor.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.CommandsEditMap;
import fr.dow.mapviewer.display.MapViewer;

public class EditMapMenu implements ActionListener {

	private static final String cmdResizeMap="cmdResizeMap";
	private static final String cmdMoveMap="cmdMoveMap";
	private static final String cmdCopyArea="cmdCopyArea";
	private static final String cmdPasteArea="cmdPasteArea";
	private static final String cmdRotateMap="cmdRotateMap";
	
	private MapEditor mapEditor;
	
	private JMenu menu;
	
	public EditMapMenu(MapEditor mapEditor) {
		
		this.mapEditor=mapEditor;
		
		JMenuItem menuItem;
		
        menu = new JMenu("Map");
        menu.setMnemonic(KeyEvent.VK_D);

        menuItem = new JMenuItem("Resize Map");
        menuItem.setActionCommand(cmdResizeMap);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Move Map");
        menuItem.setActionCommand(cmdMoveMap);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Rotate Map");
        menuItem.setActionCommand(cmdRotateMap);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menu.addSeparator();
		
        menuItem = new JMenuItem("Copy Area");
        menuItem.setActionCommand(cmdCopyArea);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Paste Area");
        menuItem.setActionCommand(cmdPasteArea);
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
	}
	
	public JMenu getMenu() {
		return menu;
	}
	
    public void actionPerformed(ActionEvent e) {
    	
    	String actionCommand=e.getActionCommand();
    	
        MapViewer curMapViewer = mapEditor.getSelectedMapViewer();
        if (curMapViewer==null) return;
    	CommandsEditMap editMapCommands=curMapViewer.commandsEditMap;
    	
        if (cmdResizeMap.equals(actionCommand)) editMapCommands.resizeMap(mapEditor);
        else if (cmdMoveMap.equals(actionCommand)) editMapCommands.moveMap(mapEditor);
        else if (cmdRotateMap.equals(actionCommand)) editMapCommands.rotateMap(mapEditor);
        else if (cmdCopyArea.equals(actionCommand)) mapEditor.startCopyDialog(mapEditor.getSelectedMapViewer());
        else if (cmdPasteArea.equals(actionCommand)) mapEditor.startPasteDialog(mapEditor.getSelectedMapViewer());
    	
    }
	
	
}
