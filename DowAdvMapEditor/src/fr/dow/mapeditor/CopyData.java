package fr.dow.mapeditor;

import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.selection.Selection;

public class CopyData implements Cloneable {

	public SgbMap sgbMap;
	public Selection selection;

	public CopyData(Selection selection,SgbMap sgbMap) {
		this.selection=selection;
		this.sgbMap=sgbMap;
	}
	
	public CopyData(MapViewer mapViewer) {
		sgbMap=(SgbMap) mapViewer.getMap().clone();
		selection=(Selection) mapViewer.getRenderer().currentSelection.clone();
	}
	
	public Object clone() {
		
		CopyData newCopyData = null;
		try {
			newCopyData= (CopyData)super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		newCopyData.sgbMap=(SgbMap)sgbMap.clone();
		newCopyData.selection=(Selection)selection.clone();
		
		return newCopyData;
		
	}

}
