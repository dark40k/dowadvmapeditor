package fr.dow.mapeditor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.mapeditor.dialogs.CopyDialog;
import fr.dow.mapeditor.dialogs.PasteDialog;
import fr.dow.mapeditor.dialogs.openmapinmod.OpenMapInModDialog;
import fr.dow.mapeditor.dialogs.parameters.ParametersDialog;
import fr.dow.mapeditor.inimapeditor.IniMapEditor;
import fr.dow.mapeditor.menu.MapEditorMenuBar;
import fr.dow.mapeditor.toolbar.Toolbar;
import fr.dow.mapeditor.tools.FindValidMod;
import fr.dow.mapeditor.tools.console.MessageConsoleDialog;
import fr.dow.mapviewer.display.MapViewer;

public class MapEditor extends JFrame {

	private static final long serialVersionUID = 1L;

	private static final String version="1.04";

	public MessageConsoleDialog messageConsoleDialog = new MessageConsoleDialog(); 
	
	public JPanel basePanel;
	public JDesktopPane desktop;
    public MapEditorMenuBar menuBar;
    public Toolbar toolBar;

    public RecentMapsList recentMapList = new RecentMapsList();
    
	public static File currentDirectory;
	
	private CopyData copyData;

	public IniMapEditor iniMapEditor ;
	
    public MapEditor() {
        super("DoW Advanced Mission Editor - v"+version);

		System.out.println("======================================================");
		System.out.println("==                                                  ==");
		System.out.println("==           MAPEDITOR - CONSOLE LOG                ==");
		System.out.println("==                                                  ==");
		System.out.println("======================================================");
		System.out.println("Version = "+version);
        
        //Start ini data 
        iniMapEditor = new IniMapEditor();
        
        //Make the big window be indented 50 pixels from each edge
        //of the screen.
        int inset = 50;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int initSizeX=Math.min(screenSize.width  - inset*2,1000);
        int initSizeY=Math.min(screenSize.height - inset*2,750);
        setBounds((screenSize.width-initSizeX)/2, (screenSize.height-initSizeY)/2, initSizeX, initSizeY );

        //Assign base panel
        basePanel=new JPanel();
        basePanel.setLayout(new BorderLayout());
        setContentPane(basePanel);
        
        //Set up the panel for new windows.
        desktop = new JDesktopPane(); //a specialized layered pane
        basePanel.add(desktop, BorderLayout.CENTER);
        
        //Create and assign menubar
        menuBar=new MapEditorMenuBar(this);
        this.setJMenuBar(menuBar.getJMenuBar());
        
        //Create and position toolbar
        toolBar=new Toolbar();
        basePanel.add(toolBar, BorderLayout.NORTH);
        
        //Make dragging a little faster but perhaps uglier.
        desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);

        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter()   {
        	public void windowClosing(WindowEvent e)
        	{
        		quit();
        	}
        });
        
        
        // Apply Ini data
        iniMapEditor.apply(this);
    }

    /**
     * 
     * Dialogs
     * 
     */
    
	public void openMapInModUsingDialog() {
		OpenMapInModDialog.showDialog(this, currentDirectory);
	}
    
    public void openMapFileUsingDialog() { 
    	
        File mapFile = getMapFileUsingDialog();
        if (mapFile == null) return; 
        
    	openMap(mapFile);
    }
    
	public void startCopyDialog(MapViewer mapViewer) {
		CopyDialog.showDialog(this, mapViewer);
	}

	public void startPasteDialog(MapViewer mapViewer) {
		PasteDialog.showDialog(this, mapViewer,copyData);
	}


	public void showConsole() {
		messageConsoleDialog.setVisible(true);
	}
    
	public void EditModLoadingParameters() {
		ParametersDialog.showDialog(this, this);
	}

    /**
     * 
     * Open map
     * 
     */
    
    public void openMap(File mapFile) {

    	System.out.println("Opening Map : " + mapFile);
    	
    	// check tga and mod files
        File tgaFile = getTgaFile(mapFile);
        if (tgaFile == null) return;
        
    	// load mod
        File modFile = getModFile(mapFile);
        if (modFile == null) return;
    	System.out.println("Loading Mod : " + modFile);
    	DowMod dowMod=DowMod.buildFromModuleFile(modFile);
    	if (dowMod==null) return;

    	// save map name for future access
    	recentMapList.addMap(mapFile.getPath());

    	//load map
    	openMap(new DowFileVersion(mapFile,null),new DowFileVersion(tgaFile,null),dowMod);
    	
    }
    
	public void openMap(DowFileVersion dowSgbFile, DowFileVersion dowTgaFile, DowMod dowMod) {
		
    	// start mapview
        MapViewer subFrame=null;
		try {
			subFrame = new MapViewer(this,dowSgbFile, dowTgaFile, dowMod);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Could not load map","Error",JOptionPane.ERROR_MESSAGE);
			return;
		}
		
        desktop.add(subFrame);
        subFrame.setVisible(true);
        try {
            subFrame.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {}
		
	}
    
    
    /**
     * 
     * Utility functions
     * 
     */
	
	public File getMapFileUsingDialog() {

    	// select map file
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(currentDirectory);
        chooser.setFileFilter(new FileNameExtensionFilter("Dawn of War Map", "sgb"));
        
        int res = chooser.showOpenDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return null;
        
        File mapFile = chooser.getSelectedFile();
        if (mapFile == null) { System.err.println("No file selected"); return null; }
        	
        currentDirectory=mapFile.getParentFile();
    	
    	// check if map is fine
    	if (!mapFile.getName().endsWith(".sgb")) { System.err.println("File is not SGB type"); return null; }
    	if (!mapFile.exists()) { System.err.println("SGB File not found"); return null; }
		
		return mapFile;
	}
	
	public File getTgaFile(File mapFile) {

    	// get tga file
    	String mapName=mapFile.getName();
    	File tgaFile=new File(mapFile.getParent()+"/"+mapName.substring(0,mapName.length()-4)+".tga");
    	
    	// check if tga is fine
    	if (!tgaFile.isFile()) { System.err.println("TGA File not found"); return null; }
		
    	return tgaFile;
		
	}
    
	public File getModFile(File mapFile) {
		
    	// search for mod
    	String mapFileName=mapFile.getAbsolutePath().replace("\\","/"); // TODO find better way to get mod directory
    	File modFile=FindValidMod.showSelectionDialog(this, mapFileName, true);
    	
    	// check if modFile is fine
    	if (modFile==null) { System.err.println("Mod File not found"); return null; }
    	
    	return modFile;
		
	}
	
	    
    //Quit the application.
    public void quit() {
    	int rep=JOptionPane.showConfirmDialog(this, "Are your sure you want to quit?","Quit",JOptionPane.YES_NO_OPTION); 
    	if (rep!=JOptionPane.OK_OPTION) return;
    
    	iniMapEditor.save(this);
    	
    	System.exit(0);
    	
    }
    
	public MapViewer getSelectedMapViewer() {
		return (MapViewer) desktop.getSelectedFrame();
	}
 	
	public void copyArea(MapViewer mapViewer) {
		copyData=new CopyData(mapViewer);
	}
	

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {

    	//  JFrame.setDefaultLookAndFeelDecorated(true);
        
        try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		//trick to avoid problem of glCanvas showing over menus
		JPopupMenu.setDefaultLightWeightPopupEnabled(false);
		ToolTipManager.sharedInstance().setLightWeightPopupEnabled(false);

        //Create and set up the window.
        MapEditor frame = new MapEditor();
        
        //Display the window.
        frame.setVisible(true);
        
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
    	
        if (args.length>0) currentDirectory=new File(args[0]);
        
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }



}
