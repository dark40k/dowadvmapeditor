package fr.dow.mapeditor.dialogs.edittiles;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import javax.swing.JCheckBox;
import javax.swing.BoxLayout;


import java.awt.GridLayout;

public abstract class EditTilesDialogBase extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanelAction = null;
	private JPanel jPanel9 = null;
	protected JButton jButtonClose = null;
	protected JPanel jPanelMapList = null;
	/**
	 * @param owner
	 */
	public EditTilesDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(824, 605);
		this.setModal(false);
		this.setTitle("Edit Tiles");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanelAction(), BorderLayout.SOUTH);
			jContentPane.add(getJPanelMapList(), BorderLayout.CENTER);
			jContentPane.add(getJPanel(), BorderLayout.EAST);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanelAction	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelAction() {
		if (jPanelAction == null) {
			jPanelAction = new JPanel();
			jPanelAction.setLayout(new BorderLayout());
			jPanelAction.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanelAction;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonClose(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonClose	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonClose() {
		if (jButtonClose == null) {
			jButtonClose = new JButton();
			jButtonClose.setPreferredSize(new Dimension(100, 23));
			jButtonClose.setText("Close");
		}
		return jButtonClose;
	}

	/**
	 * This method initializes jPanelMapList	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMapList() {
		if (jPanelMapList == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Tiles Table");
			jPanelMapList = new JPanel();
			jPanelMapList.setLayout(new BorderLayout());
			jPanelMapList.setEnabled(true);
			jPanelMapList.setBorder(titledBorder);
			jPanelMapList.add(getJScrollPaneMapList(), BorderLayout.CENTER);
			jPanelMapList.add(getJCheckBoxMapSelectionFilter(), BorderLayout.NORTH);
		}
		return jPanelMapList;
	}

	protected JScrollPane jScrollPaneMapList = null;
	protected JTable jTableMapList = null;
	protected JCheckBox jCheckBoxMapSelectionFilter = null;
	private JPanel jPanel = null;
	protected JButton jButtonTableSelFromMap = null;
	private JPanel jPanelMod111 = null;
	protected JButton jButtonEditBlueprint = null;
	private JPanel jPanel1 = null;
	private JPanel jPanelMod112 = null;
	protected JButton jButtonTableSelClear = null;
	protected JButton jButtonTableSelAll = null;
	protected JButton jButtonTableSelToMap = null;
	private JPanel jPanelMod113 = null;
	protected JButton jButtonExportTextFile = null;
	protected JButton jButtonImportTextFile = null;
	/**
	 * This method initializes jScrollPaneMapList	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPaneMapList() {
		if (jScrollPaneMapList == null) {
			jScrollPaneMapList = new JScrollPane();
			jScrollPaneMapList.setName("jScrollPaneMapList");
			jScrollPaneMapList.setViewportView(getJTableMapList());
		}
		return jScrollPaneMapList;
	}

	/**
	 * This method initializes jTableMapList	
	 * 	
	 * @return javax.swing.JTable	
	 */
	private JTable getJTableMapList() {
		if (jTableMapList == null) {
			jTableMapList = new JTable();
			jTableMapList.setAutoCreateColumnsFromModel(true);
			jTableMapList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			jTableMapList.setModel(new EditTilesTableModel());
			jTableMapList.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
			jTableMapList.setShowGrid(true);
		}
		return jTableMapList;
	}

	/**
	 * This method initializes jCheckBoxMapSelectionFilter	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxMapSelectionFilter() {
		if (jCheckBoxMapSelectionFilter == null) {
			jCheckBoxMapSelectionFilter = new JCheckBox();
			jCheckBoxMapSelectionFilter.setName("jCheckBox");
			jCheckBoxMapSelectionFilter.setSelected(false);
			jCheckBoxMapSelectionFilter.setText("Show only decals selected in map");
		}
		return jCheckBoxMapSelectionFilter;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(new BoxLayout(getJPanel(), BoxLayout.Y_AXIS));
			jPanel.setPreferredSize(new Dimension(150, 63));
			jPanel.add(getJPanelMod112(), null);
			jPanel.add(getJPanelMod111(), null);
			jPanel.add(getJPanelMod113(), null);
			jPanel.add(getJPanel1(), null);
		}
		return jPanel;
	}

	/**
	 * This method initializes jButtonTableSelFromMap	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonTableSelFromMap() {
		if (jButtonTableSelFromMap == null) {
			jButtonTableSelFromMap = new JButton();
			jButtonTableSelFromMap.setText("Map => Table");
		}
		return jButtonTableSelFromMap;
	}

	/**
	 * This method initializes jPanelMod111	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMod111() {
		if (jPanelMod111 == null) {
			TitledBorder titledBorder1111 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder1111.setTitle("Tile Data");
			titledBorder1111.setBorder(null);
			GridLayout gridLayout11 = new GridLayout();
			gridLayout11.setHgap(5);
			gridLayout11.setRows(1);
			gridLayout11.setVgap(5);
			gridLayout11.setColumns(1);
			jPanelMod111 = new JPanel();
			jPanelMod111.setLayout(gridLayout11);
			jPanelMod111.setName("jPanelMod1");
			jPanelMod111.setBorder(titledBorder1111);
			jPanelMod111.add(getJButtonEditBlueprint(), null);
		}
		return jPanelMod111;
	}

	/**
	 * This method initializes jButtonEditBlueprint	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonEditBlueprint() {
		if (jButtonEditBlueprint == null) {
			jButtonEditBlueprint = new JButton();
			jButtonEditBlueprint.setText("Edit blueprint");
		}
		return jButtonEditBlueprint;
	}

	/**
	 * This method initializes jPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jPanel1 = new JPanel();
			jPanel1.setLayout(new BorderLayout());
		}
		return jPanel1;
	}

	/**
	 * This method initializes jPanelMod112	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMod112() {
		if (jPanelMod112 == null) {
			TitledBorder titledBorder1112 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder1112.setTitle("Table Selection");
			titledBorder1112.setBorder(null);
			GridLayout gridLayout12 = new GridLayout();
			gridLayout12.setRows(4);
			gridLayout12.setHgap(5);
			gridLayout12.setVgap(5);
			gridLayout12.setColumns(1);
			jPanelMod112 = new JPanel();
			jPanelMod112.setLayout(gridLayout12);
			jPanelMod112.setName("jPanelMod1");
			jPanelMod112.setBorder(titledBorder1112);
			jPanelMod112.add(getJButtonTableSelAll(), null);
			jPanelMod112.add(getJButtonTableSelClear(), null);
			jPanelMod112.add(getJButtonTableSelFromMap(), null);
			jPanelMod112.add(getJButtonTableSelToMap(), null);
		}
		return jPanelMod112;
	}

	/**
	 * This method initializes jButtonTableSelClear	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonTableSelClear() {
		if (jButtonTableSelClear == null) {
			jButtonTableSelClear = new JButton();
			jButtonTableSelClear.setText("Clear Selection");
		}
		return jButtonTableSelClear;
	}

	/**
	 * This method initializes jButtonTableSelAll	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonTableSelAll() {
		if (jButtonTableSelAll == null) {
			jButtonTableSelAll = new JButton();
			jButtonTableSelAll.setText("Select All");
		}
		return jButtonTableSelAll;
	}

	/**
	 * This method initializes jButtonTableSelToMap	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonTableSelToMap() {
		if (jButtonTableSelToMap == null) {
			jButtonTableSelToMap = new JButton();
			jButtonTableSelToMap.setText("Table => Map");
		}
		return jButtonTableSelToMap;
	}

	/**
	 * This method initializes jPanelMod113	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMod113() {
		if (jPanelMod113 == null) {
			TitledBorder titledBorder1113 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder1113.setTitle("Import / Export");
			titledBorder1113.setBorder(null);
			GridLayout gridLayout21 = new GridLayout();
			gridLayout21.setRows(2);
			gridLayout21.setVgap(5);
			gridLayout21.setColumns(1);
			jPanelMod113 = new JPanel();
			jPanelMod113.setLayout(gridLayout21);
			jPanelMod113.setName("jPanelMod1");
			jPanelMod113.setBorder(titledBorder1113);
			jPanelMod113.add(getJButtonExportTextFile(), null);
			jPanelMod113.add(getJButtonImportTextFile(), null);
		}
		return jPanelMod113;
	}

	/**
	 * This method initializes jButtonExportTextFile	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonExportTextFile() {
		if (jButtonExportTextFile == null) {
			jButtonExportTextFile = new JButton();
			jButtonExportTextFile.setText("Export text file");
		}
		return jButtonExportTextFile;
	}

	/**
	 * This method initializes jButtonImportTextFile	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonImportTextFile() {
		if (jButtonImportTextFile == null) {
			jButtonImportTextFile = new JButton();
			jButtonImportTextFile.setText("Import text file");
		}
		return jButtonImportTextFile;
	}
		
	//
	// End of custom part 
	//

}  //  @jve:decl-index=0:visual-constraint="10,10"
