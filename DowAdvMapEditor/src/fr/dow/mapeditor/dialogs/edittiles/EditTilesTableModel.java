package fr.dow.mapeditor.dialogs.edittiles;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.sgbmap.terrain.TexTileFileLinks;
import fr.dow.gamedata.sgbmap.terrain.TexTiles;
import fr.dow.mapviewer.display.MapViewer;

public class EditTilesTableModel extends AbstractTableModel {

	private static final String[] columnNames = { "Valid", "X","Z", "Blueprint" };
	
	private MapViewer mapViewer;
	
	class TileData {
		Integer id;
		Boolean isValid;
		Integer x;
		Integer z;
		String blueprint;
	}
	
	Vector<TileData> mapDataLines=new Vector<TileData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
	public Class<?> getColumnClass(int c) {
		switch (c) {
		case 0 : return Boolean.class;
		case 1 : return Integer.class; 
		case 2 : return Integer.class; 
		case 3 : return String.class; 
		}
		return null;
    }
    
    public String getColumnName(int col) { return columnNames[col]; }
    
	@Override
	public int getColumnCount() { return columnNames.length; }

	@Override
	public int getRowCount() { return mapDataLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (mapDataLines==null) return null;
		if (rowIndex>=mapDataLines.size()) return null;
		
		switch (columnIndex) {
		case 0 : return mapDataLines.get(rowIndex).isValid;
		case 1 : return mapDataLines.get(rowIndex).x; 
		case 2 : return mapDataLines.get(rowIndex).z; 
		case 3 : return mapDataLines.get(rowIndex).blueprint; 
		}
		
		return null;
		
	}
		
	public void updateTileTable(boolean onlySelected) {
		
		Vector<TileData> newDataLines=new Vector<TileData>();
		
		TexTiles texTiles=mapViewer.getMap().terrain.ground.texTiles;
		TexTileFileLinks texTileFileLinks=mapViewer.getMap().terrain.ground.texTileFileLinks;
		
		HashSet<Integer> selectedTiles = mapViewer.getRenderer().currentSelection.tiles;
		
		for (int nx=0;nx<texTiles.get_texTileNX();nx++)
			for (int nz=0;nz<texTiles.get_texTileNZ();nz++) {
		
				int tileID=texTiles.getId(nx, nz);
			
				if (onlySelected)
					if (!selectedTiles.contains(tileID))
						continue;
				
				TileData tileData=new TileData();
				tileData.id=tileID;
				tileData.x=nx;
				tileData.z=nz;
				tileData.blueprint=texTileFileLinks.get(texTiles.get_texTilePtr(tileID));
				
				if (mapViewer.getMod().isValidFile(tileData.blueprint+".dds")) tileData.isValid=true;
				else if (mapViewer.getMod().isValidFile(tileData.blueprint+".tga")) tileData.isValid=true;
				else tileData.isValid=false;
				
				newDataLines.add(tileData);
			
		}
		
		mapDataLines=newDataLines;
		fireTableDataChanged();
		
	}
	
	public void setMapViewer(MapViewer mapViewer, boolean onlySelected) {
		this.mapViewer=mapViewer;
		updateTileTable(onlySelected);
	}

	public void writeTable(File outFile) throws FileNotFoundException {
		
		PrintWriter out=new PrintWriter(outFile);
		
		for (int i=1;i<columnNames.length;i++) {
			out.print(columnNames[i]+"\t");
		}
		out.println();
		
		for (int j=0;j<mapDataLines.size();j++) {
			for (int i=1;i<columnNames.length;i++) {
				out.print(getValueAt(j, i).toString()+"\t");
			}
			out.println();
		}
		
		out.close();
		
	}
	
	
}
