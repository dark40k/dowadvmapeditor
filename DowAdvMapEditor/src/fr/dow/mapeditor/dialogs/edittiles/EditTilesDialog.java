package fr.dow.mapeditor.dialogs.edittiles;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.dow.gamedata.sgbmap.terrain.TexTiles;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.selecttileblueprint.SelectTileBP;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeEvent;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeListener;

public class EditTilesDialog extends EditTilesDialogBase implements ActionListener, SelectionChangeListener {

	MapViewer mapViewer;
	
	EditTilesTableModel tileTable;
	
	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		EditTilesDialog dialog = new EditTilesDialog(mapEditor, mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		mapViewer.getRenderer().curFilter.lockAllExcept(FilterType.TILE);
		dialog.setVisible(true);
		return;
	}	
	
	public void close() {
		mapViewer.getRenderer().curFilter.unlockAll();
		this.setVisible(false);
	}
	
	public EditTilesDialog(MapEditor mapEditor, MapViewer mapViewer) {
		super(mapEditor);
		
		this.mapViewer=mapViewer;
		
		tileTable=(EditTilesTableModel)jTableMapList.getModel();
		tileTable.setMapViewer(mapViewer,jCheckBoxMapSelectionFilter.isSelected());
				
		jTableMapList.setAutoCreateRowSorter(true);
		jTableMapList.getColumnModel().getColumn(0).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(1).setPreferredWidth(20);
		jTableMapList.getColumnModel().getColumn(2).setPreferredWidth(20);
		jTableMapList.getColumnModel().getColumn(3).setPreferredWidth(250);
	
		jCheckBoxMapSelectionFilter.addActionListener(this);
		
		jButtonClose.addActionListener(this);
		
		jButtonTableSelAll.addActionListener(this);
		jButtonTableSelClear.addActionListener(this);
		jButtonTableSelFromMap.addActionListener(this);
		jButtonTableSelToMap.addActionListener(this);
		
		jButtonEditBlueprint.addActionListener(this);
		
		jButtonImportTextFile.addActionListener(this); 
		jButtonExportTextFile.addActionListener(this); 
		
		mapViewer.getRenderer().currentSelection.addSelectionChangeListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource()==jCheckBoxMapSelectionFilter) { updateTable(); return; }
		
		if (e.getSource()==jButtonClose) { close(); return; }
		
		if (e.getSource()==jButtonTableSelAll) { tableSelectAll(); return; }
		if (e.getSource()==jButtonTableSelClear){ tableSelClear(); return; }
		if (e.getSource()==jButtonTableSelFromMap) { tableSelFromMap(); return; }
		if (e.getSource()==jButtonTableSelToMap) { tableSelToMap(); return; }
		
		if (e.getSource()==jButtonEditBlueprint) { tileBlueprint(); return; }
		
		if (e.getSource()==jButtonImportTextFile) { importTextFile(); return; }
		if (e.getSource()==jButtonExportTextFile) { exportTextFile(); return; }
		
	}

	private void updateTable() {
		
		HashSet<Integer> tableSelTiles = new HashSet<Integer>();

		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelTiles.add(tileTable.mapDataLines.get(i).id);
		}

		jTableMapList.clearSelection();
		
		tileTable.updateTileTable(jCheckBoxMapSelectionFilter.isSelected());
		
		for (int i=0;i<tileTable.mapDataLines.size();i++) 
			if (tableSelTiles.contains(tileTable.mapDataLines.get(i).id)){
				int row=jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}
	
	@Override
	public void selectionChange(SelectionChangeEvent evt) {
		// TODO Auto-generated method stub
		if (jCheckBoxMapSelectionFilter.isSelected()) {
			updateTable();
		}
	}
	
	private void tileBlueprint() {
		
		int[] selRows = jTableMapList.getSelectedRows();
		
		if (selRows.length==0) return;
		
		int selRow=jTableMapList.getRowSorter().convertRowIndexToModel(selRows[0]);
		
		String newTileBP=SelectTileBP.showDialog(this, mapViewer.getMap(), mapViewer.getMod(), tileTable.mapDataLines.get(selRow).blueprint);
		if (newTileBP==null) return;
		
		
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			mapViewer.commandsEditTerrain.doChangeTileBP(tileTable.mapDataLines.get(i).id,newTileBP);
		}
		
		updateTable();
		
	}

	private void tableSelectAll() { 
		jTableMapList.selectAll(); 
	}
	
	private void tableSelClear() {
		jTableMapList.clearSelection(); 
	}

	private void tableSelFromMap() {
		
		HashSet<Integer> selectedTiles = mapViewer.getRenderer().currentSelection.tiles;
		jTableMapList.clearSelection();
				
		for (int i=0;i<tileTable.mapDataLines.size();i++)
			if (selectedTiles.contains(tileTable.mapDataLines.get(i).id)) {
				int row=jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}

	private void tableSelToMap() {
		
		HashSet<Integer> selectedTiles = mapViewer.getRenderer().currentSelection.tiles;
		selectedTiles.clear();
		
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			selectedTiles.add(tileTable.mapDataLines.get(i).id);
		}
		
		updateTable();
		
	}

	private void exportTextFile() {
		
        JFileChooser chooser = new JFileChooser();
        // chooser.setCurrentDirectory(currentDirectory);
        chooser.setFileFilter(new FileNameExtensionFilter("Text file (*.txt)", "txt"));
        
        int res = chooser.showSaveDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        File outFile = chooser.getSelectedFile();
        if (outFile == null) { System.err.println("No file selected"); return; }
        
        if (!outFile.getName().endsWith(".txt")) outFile=new File(outFile.getPath()+".txt");

        if (outFile.exists()) {
        	int rep=JOptionPane.showConfirmDialog(this, "Overwrite existing file?","File already exist",JOptionPane.YES_NO_OPTION); 
        	if (rep!=JOptionPane.OK_OPTION) return; 
        }
        
        try {
			tileTable.writeTable(outFile);
			JOptionPane.showMessageDialog(this, "Export Success","Export Tiles Text file",JOptionPane.INFORMATION_MESSAGE);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Export Failed","Export Tiles Text file",JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	public void importTextFile() {
		
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("Text file (*.txt)", "txt"));
        
        int res = chooser.showOpenDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        File inFile = chooser.getSelectedFile();
        
        if (inFile == null) { 
        	System.err.println("No file selected"); 
        	return; 
        }
        
        if (!inFile.exists()) {
        	JOptionPane.showMessageDialog(this, "File does not exist./nImport Aborted.","Import Tiles Text file",JOptionPane.ERROR_MESSAGE);
        	return;
        }
                
		readTable(inFile);
		
	}

	private void readTable(File inFile) {

		int            iLine      = -1;
		String         strLine    = null;
		BufferedReader objBRdr    = null;
		FileReader     objFRdr    = null;

		TexTiles texTiles=mapViewer.getMap().terrain.ground.texTiles;
		
		try {
			
			objFRdr = new FileReader(inFile);
			
			if (objFRdr != null) {
				
				objBRdr = new BufferedReader(objFRdr);
				
				if (objBRdr != null) {
					
					while (objBRdr.ready()) {
						strLine  = null;
						strLine = objBRdr.readLine().trim();
						iLine++;

						if (iLine==0) continue; // skip 1st line
						
						String[] strColumns=strLine.split("\t");

						Integer x = new Integer(strColumns[0]);
						Integer z = new Integer(strColumns[1]);
						String blueprint=strColumns[2];

						int tileID=texTiles.getId(x,z);
						
						mapViewer.commandsEditTerrain.doChangeTileBP(tileID,blueprint);

					}
				}
			}
			objBRdr.close();
		}
		catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(this, "File not found","Import Tiles Text file failed",JOptionPane.ERROR_MESSAGE);
		}
		catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Format error,\nLine number="+iLine,"Import Tiles Text file failed",JOptionPane.ERROR_MESSAGE);
		}
		catch (IOException e) {
			JOptionPane.showMessageDialog(this, "I/O error,\nLine number="+iLine,"Import Tiles Text file failed",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
		updateTable();
				
	}
	
	
}
