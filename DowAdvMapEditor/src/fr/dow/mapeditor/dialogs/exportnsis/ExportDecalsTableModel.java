package fr.dow.mapeditor.dialogs.exportnsis;

import java.io.File;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.filesystem.DowFile;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.terrain.Decal;
import fr.dow.gamedata.sgbmap.terrain.DecalFileLinks;
import fr.dow.gamedata.sgbmap.terrain.Decals;
import fr.dow.mapviewer.display.MapViewer;

public class ExportDecalsTableModel extends AbstractTableModel {

	private static final String[] columnNames = { "Export", "Blueprint", "Decal File" };

	protected class DecalData {
		Boolean export;
		String blueprint;
		File file;
	}
	
	protected Vector<DecalData> mapDataLines=new Vector<DecalData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
    public String getColumnName(int col) { return columnNames[col]; }
    
	@Override
	public int getColumnCount() { return 3; }
	
	public Class<?> getColumnClass(int c) {
		switch (c) {
		case 0 : return Boolean.class;
		case 1 : return String.class; 
		case 2 : return String.class; 
		}
		return null;
    }

	@Override
	public int getRowCount() { return mapDataLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (mapDataLines==null) return null;
		if (rowIndex>mapDataLines.size()) return null;
		
		switch (columnIndex) {
		case 0 : return mapDataLines.get(rowIndex).export; 
		case 1 : return mapDataLines.get(rowIndex).blueprint; 
		case 2 : return mapDataLines.get(rowIndex).file.getAbsolutePath(); 
		}
		
		return null;
		
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex>=mapDataLines.size()) return false;
		switch (columnIndex) {
			case 0 : return true;
			default : return false;
		}
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (rowIndex>=mapDataLines.size()) return;
		DecalData decalData=mapDataLines.get(rowIndex);
		switch (columnIndex) {
			case 0 : decalData.export=(Boolean)aValue; break;
			default : return;
		}
		fireTableDataChanged();
	}
	
	//
	// Methods for managing table datas
	//
	
	public void clear() {
		mapDataLines.clear();
		fireTableDataChanged();
	}

	public void setMapViewer(MapViewer mapViewer) {
		
		DowMod dowMod=mapViewer.getMod();
		
		HashSet<Integer> decalBpIds= new HashSet<Integer>();
		
		Vector<DecalData> newDataLines=new Vector<DecalData>();
		
		Decals decals=mapViewer.getMap().terrain.decals;
		DecalFileLinks decalFileLinks=mapViewer.getMap().terrain.decalFileLinks;
		
		for (Decal decal : decals.values())	{
			
			Integer ptr=decal.get_decalPtr();
			
			if (!decalBpIds.contains(ptr)) {
				
				DecalData decalData=new DecalData();
				decalData.export=true;
				decalData.blueprint=decalFileLinks.get(ptr);
				
				DowFile dowFile=dowMod.getDowFile(decalData.blueprint+".tga");
				if (dowFile==null) dowFile=dowMod.getDowFile(decalData.blueprint+".dds");
				if (dowFile==null) continue;
				
				File file=dowFile.getLastVersion().getFile();
				if (file==null) continue;
				
				decalData.file=file;
				
				newDataLines.add(decalData);
				
				decalBpIds.add(ptr);
				
			}
			
		}
		
		mapDataLines=newDataLines;
		fireTableDataChanged();
		
	}
	
	
	
}
