package fr.dow.mapeditor.dialogs.exportnsis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.exportnsis.ExportDecalsTableModel.DecalData;
import fr.dow.mapeditor.dialogs.exportnsis.ExportTexturesTableModel.TextureData;
import fr.dow.mapviewer.display.MapViewer;


public class ExportNsisDialog extends ExportNsisDialogBase {

	private static File currentDirectory;
	
	private ExportDecalsTableModel mapListDecalsTable;	
	
	private ExportTexturesTableModel mapListTexturesTable;	

	private SgbMap exportMap;
	
	private File licenseFile;
	private File mainFile;
	private File groundPictureFile;
	private File minimapFile;
	private File iconFile;
	private File loadingPictureFile;
	private File scarFile;
	
	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		ExportNsisDialog dialog = new ExportNsisDialog(mapEditor,mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		dialog.setVisible(true);
		return;
	}
	
	public ExportNsisDialog(MapEditor mapEditor, MapViewer mapViewer) {
		
		super(mapEditor);
		
		exportMap=mapViewer.getMap();
		
		if (currentDirectory==null) currentDirectory=MapEditor.currentDirectory;
		
		/*
		 * Initialize text fields
		 */
		
		String mainFileBaseName=removeExtension(exportMap.mapFile.getAbsolutePath());
		
		jTextFieldInstallerTitle.setText(exportMap.mapData.get_mapName());
		jCheckBoxInstallerTitle.setSelected(true);
		jCheckBoxInstallerTitle.setEnabled(false);
		
		licenseFile=null;
		updateLicenseFile();
		
		jTextFieldModFolder.setText(mapViewer.getMod().getModFolder().getName());
		jCheckBoxDestModFolder.setEnabled(false);
		jCheckBoxDestModFolder.setSelected(true);
		
		mainFile=exportMap.mapFile;
		jLabelMainFile.setText(mainFile.getAbsolutePath());
		jCheckBoxMainFile.setSelected(true);
		jCheckBoxMainFile.setEnabled(true);
		
		groundPictureFile=new File(mainFileBaseName+".tga");
		jLabelGroundPicture.setText(groundPictureFile.getAbsolutePath());
		jCheckBoxGroundPicture.setEnabled(groundPictureFile.exists());
		jCheckBoxGroundPicture.setSelected(groundPictureFile.exists());
		
		minimapFile=new File(mainFileBaseName+"_mm.tga");
		jLabelMinimap.setText(minimapFile.getAbsolutePath());
		jCheckBoxMinimap.setEnabled(minimapFile.exists());
		jCheckBoxMinimap.setSelected(minimapFile.exists());
		
		iconFile=new File(mainFileBaseName+"_icon.tga");
		jLabelIcon.setText(iconFile.getAbsolutePath());
		jCheckBoxIcon.setEnabled(iconFile.exists());
		jCheckBoxIcon.setSelected(iconFile.exists());
		
		loadingPictureFile=new File( mainFile.getParentFile().getAbsolutePath() + File.separatorChar + 
				"Loading" + File.separatorChar + groundPictureFile.getName());
		jLabelLoadingPicture.setText(loadingPictureFile.getAbsolutePath());
		jCheckBoxLoadingPicture.setEnabled(loadingPictureFile.exists());
		jCheckBoxLoadingPicture.setSelected(loadingPictureFile.exists());
		
		scarFile=new File(mainFileBaseName+".scar");
		jLabelScarFile.setText(scarFile.getAbsolutePath());
		jCheckBoxScarFile.setEnabled(scarFile.exists());
		jCheckBoxScarFile.setSelected(scarFile.exists());
		
		jTextFieldInstallerFile.setText(removeExtension(exportMap.mapFile.getName())+".exe");
		jCheckBoxInstallerFile.setSelected(true);
		jCheckBoxInstallerFile.setEnabled(false);
		
		/*
		 * Initialize jTable
		 */
		jTableMapDecals.setAutoCreateRowSorter(true);
		jTableMapDecals.getColumnModel().getColumn(0).setPreferredWidth(20);
		jTableMapDecals.getColumnModel().getColumn(1).setPreferredWidth(200);
		jTableMapDecals.getColumnModel().getColumn(2).setPreferredWidth(400);
		
		mapListDecalsTable=(ExportDecalsTableModel) jTableMapDecals.getModel();
		mapListDecalsTable.setMapViewer(mapViewer);
		
		jTableMapTextures.setAutoCreateRowSorter(true);
		jTableMapTextures.getColumnModel().getColumn(0).setPreferredWidth(20);
		jTableMapTextures.getColumnModel().getColumn(1).setPreferredWidth(200);
		jTableMapTextures.getColumnModel().getColumn(2).setPreferredWidth(400);
		
		mapListTexturesTable=(ExportTexturesTableModel) jTableMapTextures.getModel();
		mapListTexturesTable.setMapViewer(mapViewer);
		
		/*
		 * Initialize buttons
		 */
		jButtonExport.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) { doExport(); }
		});
		jButtonClose.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) { doClose(); }
		});
		jButtonChangeModInstallerFile.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) { doChangeInstallerFile(); }
		});
		jButtonChangeLicenseFile.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) { doChangeLicenseFile(); }
		});
				
	}
	
	private void doExport() {
		
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(currentDirectory);
        chooser.setFileFilter(new FileNameExtensionFilter("NSIS Script (*.nsi)", "nsi"));
        
        int res = chooser.showOpenDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        File nsisFile = chooser.getSelectedFile();
        if (nsisFile == null) { System.err.println("No file selected"); return; }
        
        if (!nsisFile.getName().endsWith(".nsi")) nsisFile=new File(nsisFile.getPath()+".nsi");

        if (nsisFile.exists()) {
        	int rep=JOptionPane.showConfirmDialog(this, "Overwrite existing file?","File already exist",JOptionPane.YES_NO_OPTION); 
        	if (rep!=JOptionPane.OK_OPTION) return; 
        }
        
        try {
			writeNsisFile(nsisFile);
			JOptionPane.showMessageDialog(this, "Export Done","Generate NSIS",JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		JOptionPane.showMessageDialog(this, "Export Failed","Generate NSIS",JOptionPane.ERROR_MESSAGE);
        
	}     
     
	private void writeNsisFile(File nsisFile) throws FileNotFoundException {
   
		PrintWriter out=new PrintWriter(nsisFile);
		
		out.println("!include \"MUI.nsh\"");
        out.println("!define MUI_ABORTWARNING");
        out.println("!define MUI_ICON \"${NSISDIR}\\Contrib\\Graphics\\Icons\\modern-install.ico\"");
        out.println("!insertmacro MUI_PAGE_WELCOME");
        
        if (licenseFile!=null) {
	        out.println("!define MUI_LICENSEPAGE_CHECKBOX");
	        out.println("!insertmacro MUI_PAGE_LICENSE \""+ licenseFile.getAbsolutePath() +"\"");
        }
        
        out.println("!insertmacro MUI_PAGE_DIRECTORY");
        out.println("!insertmacro MUI_PAGE_INSTFILES");
        out.println("!insertmacro MUI_PAGE_FINISH");
        out.println("!insertmacro MUI_LANGUAGE \"English\"");
        
        out.println("Name \"" + jTextFieldInstallerTitle.getText() +"\"");
        
        out.println("OutFile \""+ jTextFieldInstallerFile.getText() +"\"");
        
        out.println("InstallDir \"C:\"");
        out.println("Function .onInit");
        out.println(" ReadRegStr $INSTDIR HKLM \"SOFTWARE\\THQ\\Dawn of War - Soulstorm\" \"InstallLocation\"");
        out.println(" IfErrors 0 NoAbort");
        out.println(" ReadRegStr $INSTDIR HKLM \"SOFTWARE\\THQ\\Dawn of War - Dark Crusade\" \"InstallLocation\"");
        out.println(" IfErrors 0 NoAbort");
        out.println(" MessageBox MB_OK \"Dawn of War DC or SS not found. Set install path manually.\"");
        out.println(" StrCpy $INSTDIR \"\"");
        out.println("    NoAbort:");
        out.println("FunctionEnd");
        out.println("Function .onVerifyInstDir");
        
        out.println(" IfFileExists $INSTDIR\\"+ jTextFieldModFolder.getText() +"\\*.* PathGood");
        
        out.println(" Abort");
        out.println(" PathGood:");
        out.println("FunctionEnd");
        out.println("");
        out.println("ShowInstDetails show");
        out.println("");
        out.println("Section \"Hauptgruppe\" SEC01");
        out.println("  SetOverwrite ifnewer");
        out.println("  SetOverwrite try");
        out.println("");
        

        out.println("  SetOutPath \"$INSTDIR\\"+ jTextFieldModFolder.getText() +"\\Data\\scenarios\\mp\"");
        
        if (jCheckBoxMainFile.isSelected()) out.println("  File \"" + mainFile.getAbsolutePath() + "\"");
        if (jCheckBoxGroundPicture.isSelected()) out.println("  File \"" + groundPictureFile.getAbsolutePath() + "\"");
        if (jCheckBoxMinimap.isSelected()) out.println("  File \"" + minimapFile.getAbsolutePath() + "\"");
        if (jCheckBoxIcon.isSelected()) out.println("  File \"" + iconFile.getAbsolutePath() + "\"");
        if (jCheckBoxLoadingPicture.isSelected()) out.println("  File \"" + loadingPictureFile.getAbsolutePath() + "\"");
        if (jCheckBoxScarFile.isSelected()) out.println("  File \"" + scarFile.getAbsolutePath() + "\"");
        out.println("");
        
        for ( DecalData decalData : mapListDecalsTable.mapDataLines) {
        	
        	if (decalData.export) {
        		File destFolder=new File(decalData.blueprint);
        		out.println("  SetOutPath \"$INSTDIR\\"+ jTextFieldModFolder.getText() +"\\Data\\" + destFolder.getParent() +"\"" );
        		out.println("  File \"" + decalData.file.getAbsolutePath() + "\"");
        	}
        	
        }
        
        for ( TextureData textureData : mapListTexturesTable.texturesLines) {
        	
        	if (textureData.export) {
        		File destFolder=new File(textureData.blueprint);
        		out.println("  SetOutPath \"$INSTDIR\\"+ jTextFieldModFolder.getText() +"\\Data\\" + destFolder.getParent() +"\"" );
        		out.println("  File \"" + textureData.file.getAbsolutePath() + "\"");
        	}
        	
        }
        
        
        out.println("SectionEnd");
        out.println("Section -Post");
        out.println("SectionEnd          ");
        out.println("");
        
        out.close();
        
	}
	
	private void doClose() {
		this.setVisible(false);
	}
	
	private void doChangeLicenseFile() {
		
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(currentDirectory);
        chooser.setFileFilter(new FileNameExtensionFilter("Rich Text Format (*.rtf)", "rtf"));
        
        int res = chooser.showOpenDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        licenseFile = chooser.getSelectedFile();
        
		updateLicenseFile();
	}
	
	
	private void doChangeInstallerFile() {
        JFileChooser chooser = new JFileChooser();

        chooser.setFileFilter(new FileNameExtensionFilter("Installer Exec. (*.exe)", "exe"));
        
        int res = chooser.showOpenDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        if (chooser.getSelectedFile()==null) return;
        
        jTextFieldInstallerFile.setText(chooser.getSelectedFile().getPath());
        
        if (!jTextFieldInstallerFile.getText().endsWith(".exe")) jTextFieldInstallerFile.setText(jTextFieldInstallerFile.getText()+".exe");
        
	}
	
	private void updateLicenseFile() {
		
		if (licenseFile==null) {
			jLabelLicenseFile.setText("< No File Selected >");
			jCheckBoxLicenseFile.setSelected(false);
			jCheckBoxLicenseFile.setEnabled(false);
		} else {
			jLabelLicenseFile.setText(licenseFile.getPath());
			jCheckBoxLicenseFile.setSelected( (!jCheckBoxLicenseFile.isEnabled()) || jCheckBoxLicenseFile.isSelected());
			jCheckBoxLicenseFile.setEnabled(true);
		}
			
	}
		
	
	private String removeExtension(String filename) {
		int i=filename.lastIndexOf('.');
		if (i>0) return filename.substring(0,i);
		return filename;
	}
	
}
