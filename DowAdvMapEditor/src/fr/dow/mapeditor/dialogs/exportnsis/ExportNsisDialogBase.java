package fr.dow.mapeditor.dialogs.exportnsis;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import javax.swing.JCheckBox;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;

public abstract class ExportNsisDialogBase extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanelAction = null;
	private JPanel jPanel9 = null;
	protected JButton jButtonExport = null;
	protected JButton jButtonClose = null;
	protected JPanel jPanelMapDecals = null;
	/**
	 * @param owner
	 */
	public ExportNsisDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(762, 559);
		this.setModal(true);
		this.setTitle("Generate NSIS installer file");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanelAction(), BorderLayout.SOUTH);
			jContentPane.add(getJPanelBase(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanelAction	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelAction() {
		if (jPanelAction == null) {
			jPanelAction = new JPanel();
			jPanelAction.setLayout(new BorderLayout());
			jPanelAction.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanelAction;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonExport(), null);
			jPanel9.add(getJButtonClose(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonExport	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonExport() {
		if (jButtonExport == null) {
			jButtonExport = new JButton();
			jButtonExport.setPreferredSize(new Dimension(100, 23));
			jButtonExport.setActionCommand("Export NSIS");
			jButtonExport.setText("Export NSIS");
		}
		return jButtonExport;
	}

	/**
	 * This method initializes jButtonClose	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonClose() {
		if (jButtonClose == null) {
			jButtonClose = new JButton();
			jButtonClose.setPreferredSize(new Dimension(100, 23));
			jButtonClose.setActionCommand("Close");
			jButtonClose.setText("Close");
		}
		return jButtonClose;
	}

	/**
	 * This method initializes jPanelMapDecals	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMapDecals() {
		if (jPanelMapDecals == null) {
			jPanelMapDecals = new JPanel();
			jPanelMapDecals.setLayout(new BorderLayout());
			jPanelMapDecals.setEnabled(true);
			jPanelMapDecals.setBorder(BorderFactory.createTitledBorder(null, "Map Decals ( WARNING:  all selected decals will be installed under Dest. Mod. folder )", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213)));
			jPanelMapDecals.add(getJScrollPaneMapDecals(), BorderLayout.CENTER);
		}
		return jPanelMapDecals;
	}

	protected JScrollPane jScrollPaneMapDecals = null;
	protected JTable jTableMapDecals = null;
	protected JPanel jPanelBase = null;
	protected JPanel jPanel411 = null;
	protected JCheckBox jCheckBoxMainFile = null;
	protected JLabel jLabelMainFile = null;
	protected JCheckBox jCheckBoxGroundPicture = null;
	protected JLabel jLabelGroundPicture = null;
	protected JCheckBox jCheckBoxMinimap = null;
	protected JLabel jLabelMinimap = null;
	protected JCheckBox jCheckBoxIcon = null;
	protected JLabel jLabelIcon = null;
	protected JCheckBox jCheckBoxLoadingPicture = null;
	protected JLabel jLabelLoadingPicture = null;
	protected JCheckBox jCheckBoxScarFile = null;
	protected JLabel jLabelScarFile = null;
	protected JPanel jPaneInstallerData = null;
	protected JLabel jLabelModName111 = null;
	protected JTextField jTextFieldInstallerTitle = null;
	protected JLabel jLabelModName13 = null;
	protected JLabel jLabelModName3 = null;
	protected JButton jButtonChangeLicenseFile = null;
	protected JLabel jLabelModName121 = null;
	protected JLabel jLabelModName21 = null;
	protected JLabel jLabelLicenseFile = null;
	protected JPanel jPaneInstallerFile = null;
	protected JButton jButtonChangeModInstallerFile = null;
	protected JCheckBox jCheckBoxInstallerTitle = null;
	protected JCheckBox jCheckBoxLicenseFile = null;
	protected JCheckBox jCheckBoxInstallerFile = null;
	protected JCheckBox jCheckBoxDestModFolder = null;
	protected JTextField jTextFieldModFolder = null;
	protected JTextField jTextFieldInstallerFile = null;
	protected JPanel jPanelMapTextures = null;
	protected JScrollPane jScrollPaneMapTextures = null;
	protected JTable jTableMapTextures = null;
	/**
	 * This method initializes jScrollPaneMapDecals	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPaneMapDecals() {
		if (jScrollPaneMapDecals == null) {
			jScrollPaneMapDecals = new JScrollPane();
			jScrollPaneMapDecals.setName("jScrollPaneMapList");
			jScrollPaneMapDecals.setViewportView(getJTableMapDecals());
		}
		return jScrollPaneMapDecals;
	}

	/**
	 * This method initializes jTableMapDecals	
	 * 	
	 * @return javax.swing.JTable	
	 */
	private JTable getJTableMapDecals() {
		if (jTableMapDecals == null) {
			jTableMapDecals = new JTable();
			jTableMapDecals.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			jTableMapDecals.setModel(new ExportDecalsTableModel());
			jTableMapDecals.setShowGrid(true);
		}
		return jTableMapDecals;
	}

	/**
	 * This method initializes jPanelBase	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelBase() {
		if (jPanelBase == null) {
			GridBagConstraints gridBagConstraints15 = new GridBagConstraints();
			gridBagConstraints15.gridx = 0;
			gridBagConstraints15.fill = GridBagConstraints.BOTH;
			gridBagConstraints15.weighty = 1.0D;
			gridBagConstraints15.gridy = 3;
			GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
			gridBagConstraints13.gridx = 0;
			gridBagConstraints13.fill = GridBagConstraints.BOTH;
			gridBagConstraints13.gridy = 4;
			GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
			gridBagConstraints12.gridx = 0;
			gridBagConstraints12.weighty = 1.0D;
			gridBagConstraints12.fill = GridBagConstraints.BOTH;
			gridBagConstraints12.weightx = 1.0D;
			gridBagConstraints12.gridy = 2;
			GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
			gridBagConstraints10.gridx = 0;
			gridBagConstraints10.fill = GridBagConstraints.BOTH;
			gridBagConstraints10.gridy = 1;
			GridBagConstraints gridBagConstraints9 = new GridBagConstraints();
			gridBagConstraints9.gridx = 0;
			gridBagConstraints9.fill = GridBagConstraints.BOTH;
			gridBagConstraints9.gridy = 0;
			jPanelBase = new JPanel();
			jPanelBase.setLayout(new GridBagLayout());
			jPanelBase.add(getJPaneInstallerData(), gridBagConstraints9);
			jPanelBase.add(getJPanel411(), gridBagConstraints10);
			jPanelBase.add(getJPanelMapDecals(), gridBagConstraints12);
			jPanelBase.add(getJPanelMapTextures(), gridBagConstraints15);
			jPanelBase.add(getJPaneInstallerFile(), gridBagConstraints13);
		}
		return jPanelBase;
	}

	/**
	 * This method initializes jPanel411	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel411() {
		if (jPanel411 == null) {
			GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
			gridBagConstraints21.fill = GridBagConstraints.BOTH;
			gridBagConstraints21.gridy = 6;
			gridBagConstraints21.weightx = 1.0D;
			gridBagConstraints21.gridx = 1;
			jLabelScarFile = new JLabel();
			jLabelScarFile.setText("< No File >");
			GridBagConstraints gridBagConstraints151 = new GridBagConstraints();
			gridBagConstraints151.fill = GridBagConstraints.BOTH;
			gridBagConstraints151.gridy = 6;
			gridBagConstraints151.gridx = 0;
			GridBagConstraints gridBagConstraints16 = new GridBagConstraints();
			gridBagConstraints16.fill = GridBagConstraints.BOTH;
			gridBagConstraints16.gridy = 4;
			gridBagConstraints16.weightx = 1.0D;
			gridBagConstraints16.gridx = 1;
			jLabelLoadingPicture = new JLabel();
			jLabelLoadingPicture.setText("< No File >");
			GridBagConstraints gridBagConstraints141 = new GridBagConstraints();
			gridBagConstraints141.fill = GridBagConstraints.BOTH;
			gridBagConstraints141.gridy = 4;
			gridBagConstraints141.gridx = 0;
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.fill = GridBagConstraints.BOTH;
			gridBagConstraints3.gridy = 3;
			gridBagConstraints3.weightx = 1.0D;
			gridBagConstraints3.gridx = 1;
			jLabelIcon = new JLabel();
			jLabelIcon.setHorizontalAlignment(SwingConstants.LEFT);
			jLabelIcon.setText("< No File >");
			GridBagConstraints gridBagConstraints131 = new GridBagConstraints();
			gridBagConstraints131.fill = GridBagConstraints.BOTH;
			gridBagConstraints131.gridy = 3;
			gridBagConstraints131.gridx = 0;
			GridBagConstraints gridBagConstraints411 = new GridBagConstraints();
			gridBagConstraints411.fill = GridBagConstraints.BOTH;
			gridBagConstraints411.gridy = 2;
			gridBagConstraints411.weightx = 1.0D;
			gridBagConstraints411.gridx = 1;
			jLabelMinimap = new JLabel();
			jLabelMinimap.setHorizontalAlignment(SwingConstants.LEFT);
			jLabelMinimap.setText("< No File >");
			GridBagConstraints gridBagConstraints121 = new GridBagConstraints();
			gridBagConstraints121.fill = GridBagConstraints.BOTH;
			gridBagConstraints121.gridy = 2;
			gridBagConstraints121.gridx = 0;
			GridBagConstraints gridBagConstraints511 = new GridBagConstraints();
			gridBagConstraints511.fill = GridBagConstraints.BOTH;
			gridBagConstraints511.gridy = 1;
			gridBagConstraints511.weightx = 1.0D;
			gridBagConstraints511.gridx = 1;
			jLabelGroundPicture = new JLabel();
			jLabelGroundPicture.setHorizontalAlignment(SwingConstants.LEFT);
			jLabelGroundPicture.setText("< No File >");
			GridBagConstraints gridBagConstraints101 = new GridBagConstraints();
			gridBagConstraints101.fill = GridBagConstraints.BOTH;
			gridBagConstraints101.gridy = 1;
			gridBagConstraints101.gridx = 0;
			GridBagConstraints gridBagConstraints111 = new GridBagConstraints();
			gridBagConstraints111.fill = GridBagConstraints.BOTH;
			gridBagConstraints111.gridx = 1;
			gridBagConstraints111.gridy = 0;
			gridBagConstraints111.weightx = 1.0D;
			jLabelMainFile = new JLabel();
			jLabelMainFile.setHorizontalAlignment(SwingConstants.LEFT);
			jLabelMainFile.setText("< No File >");
			GridBagConstraints gridBagConstraints91 = new GridBagConstraints();
			gridBagConstraints91.fill = GridBagConstraints.BOTH;
			gridBagConstraints91.gridy = 0;
			gridBagConstraints91.gridx = 0;
			jPanel411 = new JPanel();
			jPanel411.setLayout(new GridBagLayout());
			jPanel411.setBorder(BorderFactory.createTitledBorder(null, "Map Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213)));
			jPanel411.add(getJCheckBoxMainFile(), gridBagConstraints91);
			jPanel411.add(jLabelMainFile, gridBagConstraints111);
			jPanel411.add(getJCheckBoxGroundPicture(), gridBagConstraints101);
			jPanel411.add(jLabelGroundPicture, gridBagConstraints511);
			jPanel411.add(getJCheckBoxMinimap(), gridBagConstraints121);
			jPanel411.add(jLabelMinimap, gridBagConstraints411);
			jPanel411.add(getJCheckBoxIcon(), gridBagConstraints131);
			jPanel411.add(jLabelIcon, gridBagConstraints3);
			jPanel411.add(getJCheckBoxLoadingPicture(), gridBagConstraints141);
			jPanel411.add(jLabelLoadingPicture, gridBagConstraints16);
			jPanel411.add(getJCheckBoxScarFile(), gridBagConstraints151);
			jPanel411.add(jLabelScarFile, gridBagConstraints21);
		}
		return jPanel411;
	}

	/**
	 * This method initializes jCheckBoxMainFile	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxMainFile() {
		if (jCheckBoxMainFile == null) {
			jCheckBoxMainFile = new JCheckBox();
			jCheckBoxMainFile.setText("Main file : ");
		}
		return jCheckBoxMainFile;
	}

	/**
	 * This method initializes jCheckBoxGroundPicture	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxGroundPicture() {
		if (jCheckBoxGroundPicture == null) {
			jCheckBoxGroundPicture = new JCheckBox();
			jCheckBoxGroundPicture.setText("Ground picture : ");
		}
		return jCheckBoxGroundPicture;
	}

	/**
	 * This method initializes jCheckBoxMinimap	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxMinimap() {
		if (jCheckBoxMinimap == null) {
			jCheckBoxMinimap = new JCheckBox();
			jCheckBoxMinimap.setText("Minimap : ");
		}
		return jCheckBoxMinimap;
	}

	/**
	 * This method initializes jCheckBoxIcon	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxIcon() {
		if (jCheckBoxIcon == null) {
			jCheckBoxIcon = new JCheckBox();
			jCheckBoxIcon.setText("Icon : ");
		}
		return jCheckBoxIcon;
	}

	/**
	 * This method initializes jCheckBoxLoadingPicture	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxLoadingPicture() {
		if (jCheckBoxLoadingPicture == null) {
			jCheckBoxLoadingPicture = new JCheckBox();
			jCheckBoxLoadingPicture.setText("Loading picture : ");
		}
		return jCheckBoxLoadingPicture;
	}

	/**
	 * This method initializes jCheckBoxScarFile	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxScarFile() {
		if (jCheckBoxScarFile == null) {
			jCheckBoxScarFile = new JCheckBox();
			jCheckBoxScarFile.setText("SCAR File : ");
		}
		return jCheckBoxScarFile;
	}

	/**
	 * This method initializes jPaneInstallerData	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPaneInstallerData() {
		if (jPaneInstallerData == null) {
			GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
			gridBagConstraints11.fill = GridBagConstraints.BOTH;
			gridBagConstraints11.gridx = 1;
			gridBagConstraints11.gridy = 2;
			gridBagConstraints11.gridwidth = 2;
			gridBagConstraints11.weightx = 1.0;
			GridBagConstraints gridBagConstraints81 = new GridBagConstraints();
			gridBagConstraints81.gridx = 0;
			gridBagConstraints81.fill = GridBagConstraints.BOTH;
			gridBagConstraints81.gridy = 2;
			GridBagConstraints gridBagConstraints71 = new GridBagConstraints();
			gridBagConstraints71.gridx = 0;
			gridBagConstraints71.fill = GridBagConstraints.BOTH;
			gridBagConstraints71.gridy = 1;
			GridBagConstraints gridBagConstraints61 = new GridBagConstraints();
			gridBagConstraints61.gridx = 0;
			gridBagConstraints61.fill = GridBagConstraints.BOTH;
			gridBagConstraints61.gridy = 0;
			GridBagConstraints gridBagConstraints41 = new GridBagConstraints();
			gridBagConstraints41.gridx = 1;
			gridBagConstraints41.weightx = 1.0D;
			gridBagConstraints41.fill = GridBagConstraints.BOTH;
			gridBagConstraints41.gridy = 1;
			jLabelLicenseFile = new JLabel();
			jLabelLicenseFile.setText("< No License file selected >");
			GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
			gridBagConstraints8.gridy = 1;
			gridBagConstraints8.fill = GridBagConstraints.BOTH;
			gridBagConstraints8.gridx = 2;
			GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
			gridBagConstraints7.gridy = 2;
			gridBagConstraints7.weightx = 1.0D;
			gridBagConstraints7.fill = GridBagConstraints.BOTH;
			gridBagConstraints7.gridx = 1;
			GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
			gridBagConstraints6.gridy = 1;
			gridBagConstraints6.fill = GridBagConstraints.BOTH;
			gridBagConstraints6.gridx = 0;
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			gridBagConstraints4.gridy = 1;
			gridBagConstraints4.fill = GridBagConstraints.BOTH;
			gridBagConstraints4.weightx = 1.0D;
			gridBagConstraints4.gridx = 1;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.gridy = 2;
			gridBagConstraints2.fill = GridBagConstraints.BOTH;
			gridBagConstraints2.gridx = 0;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.fill = GridBagConstraints.BOTH;
			gridBagConstraints1.gridy = 0;
			gridBagConstraints1.weightx = 1.0D;
			gridBagConstraints1.gridwidth = 2;
			gridBagConstraints1.gridx = 1;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridy = 0;
			gridBagConstraints.fill = GridBagConstraints.BOTH;
			gridBagConstraints.gridx = 0;
			TitledBorder titledBorder11 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder11.setTitle("Installer data");
			titledBorder11.setBorder(null);
			jPaneInstallerData = new JPanel();
			jPaneInstallerData.setLayout(new GridBagLayout());
			jPaneInstallerData.setBorder(titledBorder11);
			jPaneInstallerData.add(getJCheckBoxInstallerTitle(), gridBagConstraints61);
			jPaneInstallerData.add(getJTextFieldInstallerTitle(), gridBagConstraints1);
			jPaneInstallerData.add(getJCheckBoxLicenseFile(), gridBagConstraints71);
			jPaneInstallerData.add(jLabelLicenseFile, gridBagConstraints41);
			jPaneInstallerData.add(getJButtonChangeLicenseFile(), gridBagConstraints8);
			jPaneInstallerData.add(getJCheckBoxDestModFolder(), gridBagConstraints81);
			jPaneInstallerData.add(getJTextFieldModFolder(), gridBagConstraints11);
		}
		return jPaneInstallerData;
	}


	/**
	 * This method initializes jTextFieldInstallerTitle	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldInstallerTitle() {
		if (jTextFieldInstallerTitle == null) {
			jTextFieldInstallerTitle = new JTextField();
		}
		return jTextFieldInstallerTitle;
	}


	/**
	 * This method initializes jButtonChangeLicenseFile	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonChangeLicenseFile() {
		if (jButtonChangeLicenseFile == null) {
			jButtonChangeLicenseFile = new JButton();
			jButtonChangeLicenseFile.setPreferredSize(new Dimension(50, 23));
			jButtonChangeLicenseFile.setText("...");
		}
		return jButtonChangeLicenseFile;
	}

	/**
	 * This method initializes jPaneInstallerFile	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPaneInstallerFile() {
		if (jPaneInstallerFile == null) {
			GridBagConstraints gridBagConstraints14 = new GridBagConstraints();
			gridBagConstraints14.fill = GridBagConstraints.BOTH;
			gridBagConstraints14.gridx = 1;
			gridBagConstraints14.gridy = 0;
			gridBagConstraints14.weightx = 1.0;
			GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
			gridBagConstraints5.gridx = 0;
			gridBagConstraints5.fill = GridBagConstraints.BOTH;
			gridBagConstraints5.gridy = 0;
			TitledBorder titledBorder111 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder111.setTitle("Installer file");
			titledBorder111.setBorder(null);
			GridBagConstraints gridBagConstraints52 = new GridBagConstraints();
			gridBagConstraints52.fill = GridBagConstraints.BOTH;
			gridBagConstraints52.gridy = 0;
			gridBagConstraints52.gridx = 2;
			jPaneInstallerFile = new JPanel();
			jPaneInstallerFile.setLayout(new GridBagLayout());
			jPaneInstallerFile.setBorder(titledBorder111);
			jPaneInstallerFile.add(getJCheckBoxInstallerFile(), gridBagConstraints5);
			jPaneInstallerFile.add(getJTextFieldInstallerFile(), gridBagConstraints14);
			jPaneInstallerFile.add(getJButtonChangeModInstallerFile(), gridBagConstraints52);
		}
		return jPaneInstallerFile;
	}

	/**
	 * This method initializes jButtonChangeModInstallerFile	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonChangeModInstallerFile() {
		if (jButtonChangeModInstallerFile == null) {
			jButtonChangeModInstallerFile = new JButton();
			jButtonChangeModInstallerFile.setPreferredSize(new Dimension(50, 23));
			jButtonChangeModInstallerFile.setText("...");
		}
		return jButtonChangeModInstallerFile;
	}

	/**
	 * This method initializes jCheckBoxInstallerTitle	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxInstallerTitle() {
		if (jCheckBoxInstallerTitle == null) {
			jCheckBoxInstallerTitle = new JCheckBox();
			jCheckBoxInstallerTitle.setText("Installer Title : ");
		}
		return jCheckBoxInstallerTitle;
	}

	/**
	 * This method initializes jCheckBoxLicenseFile	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxLicenseFile() {
		if (jCheckBoxLicenseFile == null) {
			jCheckBoxLicenseFile = new JCheckBox();
			jCheckBoxLicenseFile.setText("License File : ");
		}
		return jCheckBoxLicenseFile;
	}

	/**
	 * This method initializes jCheckBoxInstallerFile	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxInstallerFile() {
		if (jCheckBoxInstallerFile == null) {
			jCheckBoxInstallerFile = new JCheckBox();
			jCheckBoxInstallerFile.setText("Installer Output File : ");
		}
		return jCheckBoxInstallerFile;
	}

	/**
	 * This method initializes jCheckBoxDestModFolder	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxDestModFolder() {
		if (jCheckBoxDestModFolder == null) {
			jCheckBoxDestModFolder = new JCheckBox();
			jCheckBoxDestModFolder.setText("Dest. Mod Folder : ");
		}
		return jCheckBoxDestModFolder;
	}

	/**
	 * This method initializes jTextFieldModFolder	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldModFolder() {
		if (jTextFieldModFolder == null) {
			jTextFieldModFolder = new JTextField();
		}
		return jTextFieldModFolder;
	}

	/**
	 * This method initializes jTextFieldInstallerFile	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldInstallerFile() {
		if (jTextFieldInstallerFile == null) {
			jTextFieldInstallerFile = new JTextField();
		}
		return jTextFieldInstallerFile;
	}

	/**
	 * This method initializes jPanelMapTextures	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMapTextures() {
		if (jPanelMapTextures == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Map Decals ( WARNING:  all selected decals will be installed under Dest. Mod. folder )", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setBorder(null);
			titledBorder.setTitle("Map Textures ( WARNING:  all selected textures will be installed under Dest. Mod. folder )");
			jPanelMapTextures = new JPanel();
			jPanelMapTextures.setLayout(new BorderLayout());
			jPanelMapTextures.setEnabled(true);
			jPanelMapTextures.setBorder(titledBorder);
			jPanelMapTextures.add(getJScrollPaneMapTextures(), java.awt.BorderLayout.CENTER);
		}
		return jPanelMapTextures;
	}

	/**
	 * This method initializes jScrollPaneMapTextures	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPaneMapTextures() {
		if (jScrollPaneMapTextures == null) {
			jScrollPaneMapTextures = new JScrollPane();
			jScrollPaneMapTextures.setName("jScrollPaneMapList");
			jScrollPaneMapTextures.setViewportView(getJTableMapTextures());
		}
		return jScrollPaneMapTextures;
	}

	/**
	 * This method initializes jTableMapTextures	
	 * 	
	 * @return javax.swing.JTable	
	 */
	private JTable getJTableMapTextures() {
		if (jTableMapTextures == null) {
			jTableMapTextures = new JTable();
			jTableMapTextures.setShowGrid(true);
			jTableMapTextures.setModel(new ExportTexturesTableModel());
			jTableMapTextures.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return jTableMapTextures;
	}
		
	//
	// End of custom part 
	//

}  //  @jve:decl-index=0:visual-constraint="10,10"
