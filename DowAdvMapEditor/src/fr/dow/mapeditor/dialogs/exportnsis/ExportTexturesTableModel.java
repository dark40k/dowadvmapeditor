package fr.dow.mapeditor.dialogs.exportnsis;

import java.io.File;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.filesystem.DowFile;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.terrain.TexTileFileLinks;
import fr.dow.gamedata.sgbmap.terrain.TexTiles;
import fr.dow.mapviewer.display.MapViewer;

public class ExportTexturesTableModel extends AbstractTableModel {

	private static final String[] columnNames = { "Export", "Blueprint", "Texture File" };

	protected class TextureData {
		Boolean export;
		String blueprint;
		File file;
	}
	
	protected Vector<TextureData> texturesLines=new Vector<TextureData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
	@Override
    public String getColumnName(int col) { return columnNames[col]; }
    
	@Override
	public int getColumnCount() { return 3; }
	
	@Override
    public Class<?> getColumnClass(int c) {
		switch (c) {
		case 0 : return Boolean.class;
		case 1 : return String.class; 
		case 2 : return String.class; 
		}
		return null;
    }

	@Override
	public int getRowCount() { return texturesLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (texturesLines==null) return null;
		if (rowIndex>texturesLines.size()) return null;
		
		switch (columnIndex) {
		case 0 : return texturesLines.get(rowIndex).export; 
		case 1 : return texturesLines.get(rowIndex).blueprint; 
		case 2 : return texturesLines.get(rowIndex).file.getAbsolutePath(); 
		}
		
		return null;
		
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex>=texturesLines.size()) return false;
		switch (columnIndex) {
			case 0 : return true;
			default : return false;
		}
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (rowIndex>=texturesLines.size()) return;
		TextureData decalData=texturesLines.get(rowIndex);
		switch (columnIndex) {
			case 0 : decalData.export=(Boolean)aValue; break;
			default : return;
		}
		fireTableDataChanged();
	}
	
	//
	// Methods for managing table datas
	//
	
	public void clear() {
		texturesLines.clear();
		fireTableDataChanged();
	}

	public void setMapViewer(MapViewer mapViewer) {
		
		DowMod dowMod=mapViewer.getMod();
		
		HashSet<Integer> texturesBpIds= new HashSet<Integer>();
		
		Vector<TextureData> newDataLines=new Vector<TextureData>();
		
		TexTiles textTiles=mapViewer.getMap().terrain.ground.texTiles;
		TexTileFileLinks texFileLinks=mapViewer.getMap().terrain.ground.texTileFileLinks;
		
		for (int i=0; i<textTiles.get_texTileNX(); i++) 
			for (int j=0; j<textTiles.get_texTileNZ(); j++)
			{
			
				Integer ptr=textTiles.get_texTilePtr(i,j);
				
				if (!texturesBpIds.contains(ptr)) {
					
					TextureData textureData=new TextureData();
					textureData.export=true;
					textureData.blueprint=texFileLinks.get(ptr);
					
					DowFile dowFile=dowMod.getDowFile(textureData.blueprint+".tga");
					if (dowFile==null) dowFile=dowMod.getDowFile(textureData.blueprint+".dds");
					if (dowFile==null) continue;
					
					File file=dowFile.getLastVersion().getFile();
					if (file==null) continue;
					
					textureData.file=file;
					
					newDataLines.add(textureData);
					
					texturesBpIds.add(ptr);
					
			}
			
		}
		
		texturesLines=newDataLines;
		fireTableDataChanged();
		
	}
	
	
	
}
