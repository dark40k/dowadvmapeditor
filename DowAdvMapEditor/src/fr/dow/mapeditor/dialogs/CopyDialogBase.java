package fr.dow.mapeditor.dialogs;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.BoxLayout;

public class CopyDialogBase extends JDialog {


	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanel7 = null;
	private JPanel jPanel9 = null;
	protected JButton jButton11 = null;
	protected JButton jButton21 = null;
	private JPanel jPanel = null;
	protected JCheckBox jCheckBoxDisplayGroundArea = null;
	protected JCheckBox jCheckBoxDisplayTiles = null;
	protected JCheckBox jCheckBoxDisplayDecals = null;
	protected JCheckBox jCheckBoxDisplayEntities = null;
	protected JCheckBox jCheckBoxDisplayMarkers = null;

	/**
	 * @param owner
	 */
	public CopyDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(292, 220);
		this.setTitle("Copy Area");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel7(), BorderLayout.SOUTH);
			jContentPane.add(getJPanel(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel7	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel7() {
		if (jPanel7 == null) {
			jPanel7 = new JPanel();
			jPanel7.setLayout(new BorderLayout());
			jPanel7.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanel7;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButton11(), null);
			jPanel9.add(getJButton21(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButton11	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton11() {
		if (jButton11 == null) {
			jButton11 = new JButton();
			jButton11.setPreferredSize(new Dimension(100, 23));
			jButton11.setText("Copy");
		}
		return jButton11;
	}

	/**
	 * This method initializes jButton21	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton21() {
		if (jButton21 == null) {
			jButton21 = new JButton();
			jButton21.setPreferredSize(new Dimension(100, 23));
			jButton21.setText("Close");
		}
		return jButton21;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Selection Filter");
			jPanel = new JPanel();
			jPanel.setLayout(new BoxLayout(getJPanel(), BoxLayout.Y_AXIS));
			jPanel.setBorder(titledBorder);
			jPanel.add(getJCheckBoxDisplayGroundArea(), null);
			jPanel.add(getJCheckBoxDisplayTiles(), null);
			jPanel.add(getJCheckBoxDisplayDecals(), null);
			jPanel.add(getJCheckBoxDisplayEntities(), null);
			jPanel.add(getJCheckBoxDisplayMarkers(), null);
		}
		return jPanel;
	}

	/**
	 * This method initializes jCheckBoxDisplayGroundArea	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxDisplayGroundArea() {
		if (jCheckBoxDisplayGroundArea == null) {
			jCheckBoxDisplayGroundArea = new JCheckBox();
			jCheckBoxDisplayGroundArea.setText("Ground Area");
			jCheckBoxDisplayGroundArea.setSelected(true);
		}
		return jCheckBoxDisplayGroundArea;
	}

	/**
	 * This method initializes jCheckBoxDisplayTiles	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxDisplayTiles() {
		if (jCheckBoxDisplayTiles == null) {
			jCheckBoxDisplayTiles = new JCheckBox();
			jCheckBoxDisplayTiles.setText("Tiles");
			jCheckBoxDisplayTiles.setSelected(true);
		}
		return jCheckBoxDisplayTiles;
	}

	/**
	 * This method initializes jCheckBoxDisplayDecals	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxDisplayDecals() {
		if (jCheckBoxDisplayDecals == null) {
			jCheckBoxDisplayDecals = new JCheckBox();
			jCheckBoxDisplayDecals.setText("Decals");
			jCheckBoxDisplayDecals.setSelected(true);
		}
		return jCheckBoxDisplayDecals;
	}

	/**
	 * This method initializes jCheckBoxDisplayEntities	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxDisplayEntities() {
		if (jCheckBoxDisplayEntities == null) {
			jCheckBoxDisplayEntities = new JCheckBox();
			jCheckBoxDisplayEntities.setText("Entities");
			jCheckBoxDisplayEntities.setSelected(true);
		}
		return jCheckBoxDisplayEntities;
	}

	/**
	 * This method initializes jCheckBoxDisplayMarkers	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxDisplayMarkers() {
		if (jCheckBoxDisplayMarkers == null) {
			jCheckBoxDisplayMarkers = new JCheckBox();
			jCheckBoxDisplayMarkers.setText("Markers");
			jCheckBoxDisplayMarkers.setSelected(true);
		}
		return jCheckBoxDisplayMarkers;
	}
	
}  //  @jve:decl-index=0:visual-constraint="10,10"
