package fr.dow.mapeditor.dialogs.editsquads;

import java.util.HashSet;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.sgbmap.units.Entities;
import fr.dow.gamedata.sgbmap.units.Entity;
import fr.dow.gamedata.sgbmap.units.EntityBlueprints;
import fr.dow.gamedata.sgbmap.units.Squad;
import fr.dow.gamedata.sgbmap.units.SquadBlueprints;
import fr.dow.gamedata.sgbmap.units.Squads;
import fr.dow.mapviewer.display.MapViewer;

public class EditSquadsTableModel extends AbstractTableModel {

	private static final String[] columnNames = { "Valid", "Squad ID", "Squad BP", "Entity ID", "Entity BP", "X", "Y", "Z","Head"  };

	private MapViewer mapViewer;
	
	class EntityData {
		Integer entityID,squadID;
		Boolean isValid;
		Float x,y,z;
		Double heading;
		String entityBP;
		String squadBP;
	}
	
	Vector<EntityData> mapDataLines=new Vector<EntityData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
	public Class<?> getColumnClass(int c) {
		switch (c) {
		case 0 : return Boolean.class;
		case 1 : return Integer.class; 
		case 2 : return String.class; 
		case 3 : return Integer.class; 
		case 4 : return String.class; 
		case 5 : return Float.class; 
		case 6 : return Float.class; 
		case 7 : return Float.class; 
		case 8 : return Double.class; 
		}
		return null;
    }
    
    public String getColumnName(int col) { return columnNames[col]; }
    
	@Override
	public int getColumnCount() { return columnNames.length; }

	@Override
	public int getRowCount() { return mapDataLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (mapDataLines==null) return null;
		if (rowIndex>=mapDataLines.size()) return null;
		
		switch (columnIndex) {
		case 0 : return mapDataLines.get(rowIndex).isValid;
		case 1 : return mapDataLines.get(rowIndex).squadID; 
		case 2 : return mapDataLines.get(rowIndex).squadBP; 
		case 3 : return mapDataLines.get(rowIndex).entityID; 
		case 4 : return mapDataLines.get(rowIndex).entityBP; 
		case 5 : return mapDataLines.get(rowIndex).x; 
		case 6 : return mapDataLines.get(rowIndex).y; 
		case 7 : return mapDataLines.get(rowIndex).z; 
		case 8 : return mapDataLines.get(rowIndex).heading; 
		}
		
		return null;
		
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex>=mapDataLines.size()) return false;
		switch (columnIndex) {
			case 5 : return true;
			case 6 : return true;
			case 7 : return true;
			case 8 : return true;
			default : return false;
		}
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (rowIndex>=mapDataLines.size()) return;
		EntityData entityData=mapDataLines.get(rowIndex);
		Entity entity=mapViewer.getMap().entities.get(entityData.entityID);
		
		switch (columnIndex) {
			case 5 : entityData.x=(Float)aValue; entity.setPosition(entityData.x, entityData.y, entityData.z); break;
			case 6 : entityData.y=(Float)aValue; entity.setPosition(entityData.x, entityData.y, entityData.z); break;
			case 7 : entityData.z=(Float)aValue; entity.setPosition(entityData.x, entityData.y, entityData.z); break;
			
			case 8 : entityData.heading=(Double)aValue; entity.setAnglesRad(entityData.heading, 0, 0); break;
			
			default : return;
		}
		
		mapViewer.getRenderer().mapDisplayParams.playerDisplayParams.fireUpdateEvent(1);
		
		fireTableDataChanged();
		
	}
	
	public void updateEntityTable(boolean onlySelected) {
		
		Vector<EntityData> newDataLines=new Vector<EntityData>();
		
		EntityBlueprints entityBlueprints=mapViewer.getMap().entityBlueprints;
		SquadBlueprints squadBlueprints=mapViewer.getMap().squadBlueprints;
		
		Entities entities=mapViewer.getMap().entities;
		
		Squads squads=mapViewer.getMap().squads;
		
		HashSet<Integer> selectedEntities = mapViewer.getRenderer().currentSelection.entities;
		
		for (Integer entityID : entities.keySet())	{
			
			if (onlySelected)
				if (!selectedEntities.contains(entityID))
					continue;
			
			Integer squadID=squads.getSquadID(entityID);
			if (squadID==null) continue;
			
			Entity entity=entities.get(entityID);
			Squad squad=squads.squadList.get(squadID);
			
			EntityData entityData=new EntityData();
			entityData.entityID=entityID;
			entityData.squadID=squadID;
			entityData.entityBP=entityBlueprints.get(entity.getBlueprintIndex()).replace("\\", "/").replace("data:attrib/ebps/","");
			entityData.squadBP=squadBlueprints.get(squad.blueprintIndex).replace("\\", "/").replace("data:attrib/ebps/","");
			entityData.x=entity.getX();
			entityData.y=entity.getY();
			entityData.z=entity.getZ();
			entityData.heading=Math.toDegrees(entity.getHeadingRad());
			
			entityData.isValid=mapViewer.getMod().isValidFile(entityBlueprints.get(entity.getBlueprintIndex()).substring(5).replace("\\", "/"));
			
			newDataLines.add(entityData);
			
		}
		
		mapDataLines=newDataLines;
		fireTableDataChanged();
		
	}
	
	public void setMapViewer(MapViewer mapViewer, boolean onlySelected) {
		this.mapViewer=mapViewer;
		updateEntityTable(onlySelected);
	}	
	
	
}
