package fr.dow.mapeditor.dialogs.editsquads;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeEvent;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeListener;

public class EditSquadsDialog extends EditSquadsDialogBase implements ActionListener, SelectionChangeListener {

	MapViewer mapViewer;

	EditSquadsTableModel squadsTable;

	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		EditSquadsDialog dialog = new EditSquadsDialog(mapEditor, mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		mapViewer.getRenderer().curFilter.lockAllExcept(FilterType.ENTITY);
		dialog.setVisible(true);
		return;
	}

	public void close() {
		mapViewer.getRenderer().curFilter.unlockAll();
		this.setVisible(false);
	}

	public EditSquadsDialog(MapEditor mapEditor, MapViewer mapViewer) {
		super(mapEditor);

		this.mapViewer = mapViewer;

		squadsTable = (EditSquadsTableModel) jTableMapList.getModel();
		squadsTable.setMapViewer(mapViewer, jCheckBoxMapSelectionFilter.isSelected());

		jTableMapList.setAutoCreateRowSorter(true);
		jTableMapList.getColumnModel().getColumn(0).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(1).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(2).setPreferredWidth(150);
		jTableMapList.getColumnModel().getColumn(3).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(4).setPreferredWidth(150);
		jTableMapList.getColumnModel().getColumn(5).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(6).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(7).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(8).setPreferredWidth(10);
		
		jCheckBoxMapSelectionFilter.addActionListener(this);

		jButtonClose.addActionListener(this);

		jButtonTableSelAll.setEnabled(false); // TODO EditSquads : add entity size
		jButtonTableSelClear.setEnabled(false); // TODO EditSquads : add entity size
		jButtonTableSelFromMap.setEnabled(false); // TODO EditSquads : add entity size
		jButtonTableSelToMap.setEnabled(false); // TODO EditSquads : add entity size

		jButtonSquadSize.setEnabled(false); // TODO EditSquads : add entity size
		jButtonSquadAngle.setEnabled(false); // TODO EditSquads : add entity angle
		
		jButtonSquadDelete.setEnabled(false); // TODO EditSquads : add entity angle

		jButtonImportTextFile.setEnabled(false); // TODO EditSquads : add ImportTextFile
		jButtonExportTextFile.setEnabled(false); // TODO EditSquads : add ExportTextFile

		mapViewer.getRenderer().currentSelection.addSelectionChangeListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == jCheckBoxMapSelectionFilter) {
			updateTableRows();
			return;
		}

		if (e.getSource() == jButtonClose) {
			close();
			return;
		}

//		if (e.getSource() == jButtonTableSelAll) {
//			tableSelectAll();
//			return;
//		}

//		if (e.getSource() == jButtonTableSelClear) {
//			tableSelClear();
//			return;
//		}
		
//		if (e.getSource() == jButtonTableSelFromMap) {
//			tableSelFromMap();
//			return;
//		}
		
//		if (e.getSource() == jButtonTableSelToMap) {
//			tableSelToMap();
//			return;
//		}
		

	}

	@Override
	public void selectionChange(SelectionChangeEvent evt) {
		if (jCheckBoxMapSelectionFilter.isSelected()) updateTableRows();
	}

	private void updateTableRows() {
		
		HashSet<Integer> tableSelDecals = new HashSet<Integer>();

		for (int row : jTableMapList.getSelectedRows()) {
			int i = jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelDecals.add(squadsTable.mapDataLines.get(i).entityID);
		}

		jTableMapList.clearSelection();

		squadsTable.updateEntityTable(jCheckBoxMapSelectionFilter.isSelected());

		for (int i = 0; i < squadsTable.mapDataLines.size(); i++)
			if (tableSelDecals.contains(squadsTable.mapDataLines.get(i).entityID)) {
				int row = jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}

}
