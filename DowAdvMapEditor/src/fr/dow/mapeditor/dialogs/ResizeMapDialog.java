package fr.dow.mapeditor.dialogs;

import java.awt.Frame;

import fr.dow.mapviewer.display.MapViewer;

public class ResizeMapDialog extends ResizeMapDialogBase {
	
	private MapViewer mapViewer;
		
	public void restoreDefault() {

		int cellX=mapViewer.getMap().terrain.ground.groundData.get_cellnbrX();
		if (cellX==128) jRadioButtonX128.setSelected(true);
		else if (cellX==256) jRadioButtonX256.setSelected(true);
		else if (cellX==512) jRadioButtonX512.setSelected(true);
		else if (cellX==1024) jRadioButtonX1024.setSelected(true);
		else if (cellX==2048) jRadioButtonX2048.setSelected(true);
		
		int cellZ=mapViewer.getMap().terrain.ground.groundData.get_cellnbrZ();
		if (cellZ==128) jRadioButtonZ128.setSelected(true);
		else if (cellZ==256) jRadioButtonZ256.setSelected(true);
		else if (cellZ==512) jRadioButtonZ512.setSelected(true);
		else if (cellZ==1024) jRadioButtonZ1024.setSelected(true);
		else if (cellZ==2048) jRadioButtonZ2048.setSelected(true);
		
		repaint();
	}
	
	public void apply() {
		
		int newCellX=0;
		if (jRadioButtonX128.isSelected()) newCellX=128;
		else if (jRadioButtonX256.isSelected()) newCellX=256;
		else if (jRadioButtonX512.isSelected()) newCellX=512;
		else if (jRadioButtonX1024.isSelected()) newCellX=1024;
		else if (jRadioButtonX2048.isSelected()) newCellX=2048;
		
		int newCellZ=0;
		if (jRadioButtonZ128.isSelected()) newCellZ=128;
		else if (jRadioButtonZ256.isSelected()) newCellZ=256;
		else if (jRadioButtonZ512.isSelected()) newCellZ=512;
		else if (jRadioButtonZ1024.isSelected()) newCellZ=1024;
		else if (jRadioButtonZ2048.isSelected()) newCellZ=2048;
		
		mapViewer.commandsEditMap.doResizeMap(newCellX,newCellZ);
		
		close();
	}
	
	public static void showDialog(Frame frame, MapViewer mapViewer) {
		ResizeMapDialog dialog = new ResizeMapDialog(frame);
		dialog.setMapViewer(mapViewer);
		dialog.setLocationRelativeTo(mapViewer);
		dialog.restoreDefault();
		dialog.setVisible(true);
		return;
	}

	public void setMapViewer(MapViewer mapViewer) {
		this.mapViewer=mapViewer;
	}
	
	public void close() {
		this.setVisible(false);
	}

	public ResizeMapDialog(Frame owner) {
		super(owner);
		jButtonDefault.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				restoreDefault();
			}
		});
		jButtonApply.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				apply();
			}
		});
		jButtonClose.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				close();
			}
		});
	}

}
