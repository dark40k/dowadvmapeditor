package fr.dow.mapeditor.dialogs.selecttileblueprint;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.media.opengl.awt.GLCanvas;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.jogamp.opengl.util.FPSAnimator;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.mapeditor.tools.ColorizedButton;

public class SelectTileBP extends SelectTileBPBase implements ListSelectionListener {

	SelectTileRenderer tileRenderer;
	
	DowMod mod;
	SgbMap map;
	
	SelectTileBPTableModel tileTable;
	
	private String defaultTexture=null;
	
	private String currentTexture=null;
	
	public void close() {
		currentTexture=defaultTexture;
		this.setVisible(false);
	}
	
	public void apply() {
		this.setVisible(false);
	}
	
	private SelectTileBP(Frame owner,SgbMap map, DowMod mod) {
		super(owner);
		
		this.mod=mod;
		this.map=map;
		
		tileTable=(SelectTileBPTableModel)jTableBPList.getModel();
		
		tileTable.setData(map,mod);
				
		jTableBPList.setAutoCreateRowSorter(true);
		jTableBPList.getColumnModel().getColumn(0).setPreferredWidth(10);
		jTableBPList.getColumnModel().getColumn(1).setPreferredWidth(200);
		
		jTableBPList.getRowSorter().toggleSortOrder(1);
		
		jTableBPList.getSelectionModel().addListSelectionListener(this);
		
		GLCanvas canvas = new GLCanvas();
		
		tileRenderer=new SelectTileRenderer();
	    canvas.addGLEventListener(tileRenderer);
	    
		jPanelPreview.add(canvas,BorderLayout.CENTER);
		
		//Init animator
		System.out.println("Starting animation");	
		FPSAnimator animator = new FPSAnimator( canvas, 25 );
		//animator.setRunAsFastAsPossible(false);
		animator.start(); 
		
		jButtonApply.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						apply();
					}
				}
			);
		
		jButtonCancel.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						close();
					}
				}
			);
		
		ColorizedButton colorButton=new ColorizedButton();
		colorButton.setPreferredSize(new Dimension(100,23));
		colorButton.setText("Back. Color");
		colorButton.setColor(new Color(1f,1f,1f,0f));
		jPanelLeft.add(colorButton);
		
		
		colorButton.addPropertyChangeListener( 
				new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent evt) {
						if (evt.getPropertyName().equals("Color")) {
							tileRenderer.setNewBackgroundColor((Color) evt.getNewValue());
						}
					}
				}
		);
		
	}

	
	private void setDefaultBP(String dowBPPath) {
		
		System.out.println("Start with default "+dowBPPath);
		
		int selRow=tileTable.findPathRowIndex(dowBPPath);
		
		if (jTableBPList.getSelectedRow()>=0) jTableBPList.clearSelection();
		
		if (selRow>=0) {
			selRow=jTableBPList.getRowSorter().convertRowIndexToView(selRow);
			jTableBPList.addRowSelectionInterval(selRow, selRow);
			defaultTexture=dowBPPath;
		}
		
	}
	
	private String getSelectedBP() {
		return currentTexture;
	}
	
	public static String showDialog(Component owner, SgbMap map, DowMod mod, String defaultBP) {
		
		SelectTileBP dialog = new SelectTileBP(null,map, mod);
		dialog.setLocationRelativeTo(owner);
		if (defaultBP!=null) {
			dialog.setDefaultBP(defaultBP);
		} 
		
		dialog.setVisible(true);
		return dialog.getSelectedBP();
		
	}
	
	private void updateTextureDisplay() {
		
		int selRow=jTableBPList.getSelectedRow();
		
		String newTexture=null;
		
		if (selRow<0) {
			if (currentTexture!=null) {
	        	tileRenderer.clearTexture();
	        	currentTexture=null;				
			}
			return;
		}
		
		selRow=jTableBPList.getRowSorter().convertRowIndexToModel(selRow);
		newTexture=tileTable.getPath(selRow);
		
		if (newTexture.equalsIgnoreCase(currentTexture)) return;
		
		currentTexture=newTexture;		
		tileRenderer.displayNewTexure(newTexture, mod);
        
	}

	
	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		updateTextureDisplay();
	}

	
}
