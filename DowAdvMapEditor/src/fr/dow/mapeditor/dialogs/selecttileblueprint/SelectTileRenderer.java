package fr.dow.mapeditor.dialogs.selecttileblueprint;

import java.awt.Color;

//import javax.media.opengl.DebugGL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.gl2.GLUgl2;

import com.jogamp.opengl.util.texture.Texture;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.texture.DdsTexture;
import fr.dow.gamedata.texture.TgaTexture;

public class SelectTileRenderer extends GLCanvas implements GLEventListener {

	private GLUgl2 glu = new GLUgl2();

	private Texture texture;
	
	private boolean flushTexture;
	private boolean newTexture;
	
	private String newTexFilename;
	private DowMod newTexModData;
	
	private Color newBackGroundColor=new Color(1f,1f,1f,0f);
	
	public void displayNewTexure(String texFilename, DowMod modData) {
		this.newTexFilename=texFilename;
		this.newTexModData=modData;
		this.newTexture=true;
	}
	
	public void clearTexture() {
		this.flushTexture=true;
	}
	
	public void init(GLAutoDrawable drawable) {
		//drawable.setGL(new DebugGL(drawable.getGL()));

		GL2 gl = drawable.getGL().getGL2();
		gl.glClearColor(0, 0, 0, 0);
		gl.glEnable(GL2.GL_DEPTH_TEST);
		
        gl.glEnable(GL2.GL_LIGHTING);
        float pos[] = { 5.0f, 5.0f, 10.0f, 0.0f };
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, pos, 0);
        gl.glEnable(GL2.GL_LIGHT0);
        
        //
        // Set default material and background color
        //
        gl.glColorMaterial ( GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE ) ;
		
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		
		GL2 gl = drawable.getGL().getGL2();
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		
		double ratio=((double)width)/((double)height);
		if (ratio>1.0) 
			glu.gluOrtho2D(0.5-ratio/2.0, 0.5+ratio/2.0, 0, 1);
		else 
			glu.gluOrtho2D(0, 1, 0.5-1.0/ratio/2.0, 0.5+1.0/ratio/2.0);
		
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

        if (newBackGroundColor!=null) {
        	float r=(float)newBackGroundColor.getRed() / 255f;
        	float g=(float)newBackGroundColor.getGreen() / 255f;
        	float b=(float)newBackGroundColor.getBlue() / 255f;
        	float a=(float)newBackGroundColor.getAlpha() / 255f;
        	gl.glClearColor(r,g,b,a);              
        	newBackGroundColor=null;
        }
		
		if (flushTexture) {
			flushTexture = false;
			if (texture != null) {
				texture.destroy(gl);
				texture = null;
			}
		}

		if (newTexture) {
			newTexture = false;

			if (texture != null) {
				texture.destroy(gl);
				texture = null;
			}

			System.out.println("Loading texture:"+newTexFilename);
			texture = loadTexture(gl,newTexFilename, newTexModData);
			
			if (texture==null) { System.err.println("Loading failed for texture:"+newTexFilename);}
			
		}

		if (texture != null) {
			texture.enable(gl);
			texture.bind(gl);
			gl.glEnable(GL2.GL_BLEND);
	        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
			gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);

			float ratio=((float) texture.getImageWidth())/((float)texture.getImageHeight());
			float r1=Math.min(ratio, 1);
			float r2=Math.min(1/ratio, 1);
			
			gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0, 0);
			gl.glVertex3f(0.5f-r1/2.0f, 0.5f-r2/2.0f, 0);
			gl.glTexCoord2f(1, 0);
			gl.glVertex3f(0.5f+r1/2.0f, 0.5f-r2/2.0f, 0);
			gl.glTexCoord2f(1, 1);
			gl.glVertex3f(0.5f+r1/2.0f, 0.5f+r2/2.0f, 0);
			gl.glTexCoord2f(0, 1);
			gl.glVertex3f(0.5f-r1/2.0f, 0.5f+r2/2.0f, 0);
			gl.glEnd();
			texture.disable(gl);
			
		}
	}

    public void dispose(GLAutoDrawable drawable) { }
    
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {}	

	private Texture loadTexture(GL2 gl, String texFilename, DowMod modData) {
		
		Texture newTex=null;
		
		newTex=TgaTexture.load(texFilename+".tga", modData);
		if (newTex!=null) return newTex;
		
		newTex=DdsTexture.load(texFilename+".dds", modData);
		if (newTex!=null) return newTex;
		
		return null;
	}

	public void setNewBackgroundColor(Color newColor) {
		newBackGroundColor=newColor;
	}

}
