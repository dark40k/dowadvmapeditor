package fr.dow.mapeditor.dialogs.selecttileblueprint;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.filesystem.DowFile;
import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.gamedata.sgbmap.terrain.TexTileFileLinks;

public class SelectTileBPTableModel  extends AbstractTableModel {
	
	private static final String[] columnNames = { "Loaded", "Blueprint" };

	private Vector<LineData> tableData=new Vector<LineData>();
	
	public Class<? extends Object> getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }
	
	@Override
	public int getColumnCount() { return columnNames.length; }

	@Override
	public int getRowCount() {
		return tableData.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (tableData==null) return null;
		if (tableData.get(rowIndex)==null) return null;
		
		switch (columnIndex) {
			case 0 : return tableData.get(rowIndex).isLoaded;
			case 1 : return tableData.get(rowIndex).tileName;
		}
		
		return null;
	}
	
	@Override
    public String getColumnName(int col) { return columnNames[col]; }

	public void setData(SgbMap map, DowMod mod) {
		tableData.clear();
		addData(mod.getFolder("art/scenarios/textures/detail"),map.terrain.ground.texTileFileLinks);
		fireTableDataChanged();
	}

	public String getPath(int rowIndex) {
		return tableData.get(rowIndex).tileName;
	}
	
	public int findPathRowIndex(String dowPath) {
		for (int i=0;i<tableData.size();i++) 
			if (dowPath.equals(tableData.get(i).tileName))
				return i;
		return -1;
	}
	
	private void addData(DowFolder dowFolder, TexTileFileLinks tileFileLinks) {
		
		for (DowFolder subFolder : dowFolder.getSubFoldersList().values()) addData(subFolder,tileFileLinks); 
		
		for (DowFile subFile : dowFolder.getSubFilesList().values() ) {
			String tilePath=subFile.getDowPath();
			int dotPos=tilePath.lastIndexOf('.');
			if (dotPos<0) {
				System.out.println("Skipping tile filename (no extension) : "+tilePath);
				continue;
			}
			tilePath=tilePath.substring(0, dotPos);
			tableData.add(new LineData(tileFileLinks.containsValue(tilePath),tilePath));
		}
	}
	
	
	private class LineData {
		Boolean isLoaded;
		String tileName;
		public LineData(boolean isLoaded,String tileName) {
			this.isLoaded=isLoaded;
			this.tileName=tileName;
		}
	}
	
}
