package fr.dow.mapeditor.dialogs;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.border.TitledBorder;
import javax.swing.BorderFactory;
import java.awt.Font;
import java.awt.Color;

public class PasteDialogBase extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanel5 = null;
	private JPanel jPanel7 = null;
	private JPanel jPanel9 = null;
	
	protected JButton jButtonClose = null;
	protected JButton jButtonZPTile = null;
	protected JButton jButtonZNCell = null;
	protected JButton jButtonXNTile = null;
	protected JButton jButtonXPCell = null;
	protected JButton jButtonXPTile = null;
	protected JButton jButtonZNTile = null;
	protected JButton jButtonZPCell = null;
	protected JButton jButtonXNCell = null;
	
	private JPanel jPanel1 = null;
	
	protected JCheckBox jCheckBoxPasteHeightMap = null;
	protected JCheckBox jCheckBoxPasteTiles = null;
	protected JCheckBox jCheckBoxPasteDecals = null;
	protected JCheckBox jCheckBoxPasteEntitiesSquads = null;
	protected JCheckBox jCheckBoxPasteMarkers = null;
	protected JCheckBox jCheckBoxPasteGroundTexture = null;
	
	protected JButton jButtonPaste = null;
	private JPanel jPanel91 = null;
	protected JButton jButtonReset = null;
	
	/**
	 * @param owner
	 */
	public PasteDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(587, 380);
		this.setTitle("Paste Data");
		this.setResizable(false);
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel5(), BorderLayout.CENTER);
			jContentPane.add(getJPanel7(), BorderLayout.SOUTH);
			jContentPane.add(getJPanel1(), BorderLayout.WEST);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel5	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel5() {
		if (jPanel5 == null) {
			TitledBorder titledBorder1 = BorderFactory.createTitledBorder(null, "Posistion", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null);
			titledBorder1.setTitle("Paste Position");
			jPanel5 = new JPanel();
			jPanel5.setLayout(null);
			jPanel5.setBorder(titledBorder1);
			jPanel5.add(getJButtonZPTile(), null);
			jPanel5.add(getJButtonZNCell(), null);
			jPanel5.add(getJButtonXNTile(), null);
			jPanel5.add(getJButtonXPCell(), null);
			jPanel5.add(getJButtonXPTile(), null);
			jPanel5.add(getJButtonZNTile(), null);
			jPanel5.add(getJButtonZPCell(), null);
			jPanel5.add(getJButtonXNCell(), null);
		}
		return jPanel5;
	}

	/**
	 * This method initializes jPanel7	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel7() {
		if (jPanel7 == null) {
			jPanel7 = new JPanel();
			jPanel7.setLayout(new BorderLayout());
			jPanel7.add(getJPanel91(), BorderLayout.WEST);
			jPanel7.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanel7;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonPaste(), null);
			jPanel9.add(getJButtonClose(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonClose	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonClose() {
		if (jButtonClose == null) {
			jButtonClose = new JButton();
			jButtonClose.setPreferredSize(new Dimension(100, 23));
			jButtonClose.setText("Close");
		}
		return jButtonClose;
	}

	/**
	 * This method initializes jButtonZPTile	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonZPTile() {
		if (jButtonZPTile == null) {
			jButtonZPTile = new JButton();
			jButtonZPTile.setBounds(new Rectangle(195, 15, 76, 46));
			jButtonZPTile.setText("Z + 1Tile");
		}
		return jButtonZPTile;
	}

	/**
	 * This method initializes jButtonZNCell	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonZNCell() {
		if (jButtonZNCell == null) {
			jButtonZNCell = new JButton();
			jButtonZNCell.setBounds(new Rectangle(195, 195, 76, 46));
			jButtonZNCell.setText("Z - 1Cell");
		}
		return jButtonZNCell;
	}

	/**
	 * This method initializes jButtonXNTile	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonXNTile() {
		if (jButtonXNTile == null) {
			jButtonXNTile = new JButton();
			jButtonXNTile.setBounds(new Rectangle(15, 135, 76, 46));
			jButtonXNTile.setText("X - 1Tile");
		}
		return jButtonXNTile;
	}

	/**
	 * This method initializes jButtonXPCell	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonXPCell() {
		if (jButtonXPCell == null) {
			jButtonXPCell = new JButton();
			jButtonXPCell.setBounds(new Rectangle(285, 135, 76, 46));
			jButtonXPCell.setText("X + 1Cell");
		}
		return jButtonXPCell;
	}

	/**
	 * This method initializes jButtonXPTile	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonXPTile() {
		if (jButtonXPTile == null) {
			jButtonXPTile = new JButton();
			jButtonXPTile.setBounds(new Rectangle(375, 135, 75, 46));
			jButtonXPTile.setText("X + 1Tile");
		}
		return jButtonXPTile;
	}

	/**
	 * This method initializes jButtonZNTile	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonZNTile() {
		if (jButtonZNTile == null) {
			jButtonZNTile = new JButton();
			jButtonZNTile.setBounds(new Rectangle(195, 255, 76, 46));
			jButtonZNTile.setText("Z - 1Tile");
		}
		return jButtonZNTile;
	}

	/**
	 * This method initializes jButtonZPCell	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonZPCell() {
		if (jButtonZPCell == null) {
			jButtonZPCell = new JButton();
			jButtonZPCell.setBounds(new Rectangle(195, 75, 76, 46));
			jButtonZPCell.setText("Z + 1Cell");
		}
		return jButtonZPCell;
	}

	/**
	 * This method initializes jButtonXNCell	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonXNCell() {
		if (jButtonXNCell == null) {
			jButtonXNCell = new JButton();
			jButtonXNCell.setBounds(new Rectangle(105, 135, 76, 46));
			jButtonXNCell.setText("X - 1Cell");
		}
		return jButtonXNCell;
	}

	

	/**
	 * This method initializes jPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Paste Data");
			jPanel1 = new JPanel();
			jPanel1.setLayout(new BoxLayout(getJPanel1(), BoxLayout.Y_AXIS));
			jPanel1.setBorder(titledBorder);
			jPanel1.add(getJCheckBoxPasteHeightMap(), null);
			jPanel1.add(getJCheckBoxPasteGroundTexture(), null);
			jPanel1.add(getJCheckBoxPasteTiles(), null);
			jPanel1.add(getJCheckBoxPasteDecals(), null);
			jPanel1.add(getJCheckBoxPasteEntitiesSquads(), null);
			jPanel1.add(getJCheckBoxPasteMarkers(), null);
		}
		return jPanel1;
	}

	/**
	 * This method initializes jCheckBoxPasteHeightMap	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxPasteHeightMap() {
		if (jCheckBoxPasteHeightMap == null) {
			jCheckBoxPasteHeightMap = new JCheckBox();
			jCheckBoxPasteHeightMap.setSelected(true);
			jCheckBoxPasteHeightMap.setText("Height Map");
		}
		return jCheckBoxPasteHeightMap;
	}

	/**
	 * This method initializes jCheckBoxPasteTiles	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxPasteTiles() {
		if (jCheckBoxPasteTiles == null) {
			jCheckBoxPasteTiles = new JCheckBox();
			jCheckBoxPasteTiles.setSelected(true);
			jCheckBoxPasteTiles.setText("Tiles");
		}
		return jCheckBoxPasteTiles;
	}

	/**
	 * This method initializes jCheckBoxPasteDecals	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxPasteDecals() {
		if (jCheckBoxPasteDecals == null) {
			jCheckBoxPasteDecals = new JCheckBox();
			jCheckBoxPasteDecals.setSelected(true);
			jCheckBoxPasteDecals.setText("Decals");
		}
		return jCheckBoxPasteDecals;
	}

	/**
	 * This method initializes jCheckBoxPasteEntitiesSquads	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxPasteEntitiesSquads() {
		if (jCheckBoxPasteEntitiesSquads == null) {
			jCheckBoxPasteEntitiesSquads = new JCheckBox();
			jCheckBoxPasteEntitiesSquads.setSelected(true);
			jCheckBoxPasteEntitiesSquads.setText("Entities/Squads");
		}
		return jCheckBoxPasteEntitiesSquads;
	}

	/**
	 * This method initializes jCheckBoxPasteMarkers	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxPasteMarkers() {
		if (jCheckBoxPasteMarkers == null) {
			jCheckBoxPasteMarkers = new JCheckBox();
			jCheckBoxPasteMarkers.setSelected(true);
			jCheckBoxPasteMarkers.setText("Markers");
		}
		return jCheckBoxPasteMarkers;
	}

	/**
	 * This method initializes jButtonPaste	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonPaste() {
		if (jButtonPaste == null) {
			jButtonPaste = new JButton();
			jButtonPaste.setPreferredSize(new Dimension(100, 23));
			jButtonPaste.setText("Paste");
		}
		return jButtonPaste;
	}

	/**
	 * This method initializes jPanel91	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel91() {
		if (jPanel91 == null) {
			FlowLayout flowLayout71 = new FlowLayout();
			flowLayout71.setHgap(10);
			jPanel91 = new JPanel();
			jPanel91.setLayout(flowLayout71);
			jPanel91.add(getJButtonReset(), null);
		}
		return jPanel91;
	}

	/**
	 * This method initializes jButtonReset	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonReset() {
		if (jButtonReset == null) {
			jButtonReset = new JButton();
			jButtonReset.setPreferredSize(new Dimension(100, 23));
			jButtonReset.setText("Reset");
		}
		return jButtonReset;
	}

	//  @jve:decl-index=0:
	
	/**
	 * This method initializes jCheckBoxPasteGroundTexture	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxPasteGroundTexture() {
		if (jCheckBoxPasteGroundTexture == null) {
			jCheckBoxPasteGroundTexture = new JCheckBox();
			jCheckBoxPasteGroundTexture.setSelected(true);
			jCheckBoxPasteGroundTexture.setText("Ground texture");
		}
		return jCheckBoxPasteGroundTexture;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
