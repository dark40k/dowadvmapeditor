package fr.dow.mapeditor.dialogs.heightmap;


import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.border.TitledBorder;
import javax.swing.BorderFactory;
import java.awt.Font;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class EditHeightMapDialogBase extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	protected JPanel jPanelCorners = null;
	private JPanel jPanel7 = null;
	private JPanel jPanel9 = null;
	
	protected JButton jButtonCancel = null;
	private JPanel jPanel1 = null;
	
	protected JButton jButtonApply = null;
	private JPanel jPanel91 = null;
	protected JButton jButtonReset = null;
	protected JButton jButtonAdd = null;
	protected JButton jButtonRemove = null;
	protected JButton jButtonMin = null;
	protected JButton jButtonMax = null;
	protected JButton jButtonSet = null;
	private JPanel jPanel = null;
	protected JButton jButtonWN = null;
	protected JButton jButtonWP = null;
	protected JButton jButtonNP = null;
	protected JButton jButtonNN = null;
	protected JButton jButtonSN = null;
	protected JButton jButtonSP = null;
	protected JButton jButtonEN = null;
	protected JButton jButtonEP = null;
	private JPanel jPanel2 = null;
	private JPanel jPanel8 = null;
	private JPanel jPanel3 = null;
	private JPanel jPanel4 = null;
	/**
	 * @param owner
	 */
	public EditHeightMapDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(532, 446);
		this.setTitle("Edit Height Map");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel7(), BorderLayout.SOUTH);
			jContentPane.add(getJPanelCorners(), java.awt.BorderLayout.CENTER);
			jContentPane.add(getJPanel1(), BorderLayout.WEST);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanelCorners	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelCorners() {
		if (jPanelCorners == null) {
			GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
			gridBagConstraints7.gridx = 3;
			gridBagConstraints7.weighty = 1.0D;
			gridBagConstraints7.fill = GridBagConstraints.BOTH;
			gridBagConstraints7.gridy = 4;
			GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
			gridBagConstraints6.gridx = 3;
			gridBagConstraints6.fill = GridBagConstraints.BOTH;
			gridBagConstraints6.weighty = 1.0D;
			gridBagConstraints6.gridy = 2;
			GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
			gridBagConstraints5.gridx = 4;
			gridBagConstraints5.fill = GridBagConstraints.BOTH;
			gridBagConstraints5.weightx = 1.0;
			gridBagConstraints5.gridy = 3;
			GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
			gridBagConstraints12.gridx = 2;
			gridBagConstraints12.fill = GridBagConstraints.BOTH;
			gridBagConstraints12.weightx = 1.0;
			gridBagConstraints12.weighty = 0.0;
			gridBagConstraints12.gridy = 3;
			GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
			gridBagConstraints11.gridx = 6;
			gridBagConstraints11.fill = GridBagConstraints.NONE;
			gridBagConstraints11.weightx = 0.0;
			gridBagConstraints11.gridy = 3;
			GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
			gridBagConstraints10.gridx = 5;
			gridBagConstraints10.gridy = 3;
			GridBagConstraints gridBagConstraints9 = new GridBagConstraints();
			gridBagConstraints9.gridx = 3;
			gridBagConstraints9.ipadx = 0;
			gridBagConstraints9.ipady = 0;
			gridBagConstraints9.gridy = 5;
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			gridBagConstraints4.gridy = 6;
			gridBagConstraints4.anchor = GridBagConstraints.CENTER;
			gridBagConstraints4.gridx = 3;
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.gridx = 3;
			gridBagConstraints3.gridy = 1;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.gridx = 3;
			gridBagConstraints2.gridy = 0;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 1;
			gridBagConstraints1.gridy = 3;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.weighty = 0.0;
			gridBagConstraints.fill = GridBagConstraints.NONE;
			gridBagConstraints.gridy = 3;
			TitledBorder titledBorder1 = BorderFactory.createTitledBorder(null, "Posistion", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null);
			titledBorder1.setTitle("HeightMap positions");
			jPanelCorners = new JPanel();
			jPanelCorners.setLayout(new GridBagLayout());
			jPanelCorners.setBorder(titledBorder1);
			jPanelCorners.add(getJButtonWN(), gridBagConstraints);
			jPanelCorners.add(getJButtonWP(), gridBagConstraints1);
			jPanelCorners.add(getJButtonNP(), gridBagConstraints2);
			jPanelCorners.add(getJButtonNN(), gridBagConstraints3);
			jPanelCorners.add(getJButtonSN(), gridBagConstraints4);
			jPanelCorners.add(getJButtonSP(), gridBagConstraints9);
			jPanelCorners.add(getJButtonEN(), gridBagConstraints10);
			jPanelCorners.add(getJButtonEP(), gridBagConstraints11);
			jPanelCorners.add(getJPanel2(), gridBagConstraints12);
			jPanelCorners.add(getJPanel8(), gridBagConstraints5);
			jPanelCorners.add(getJPanel3(), gridBagConstraints6);
			jPanelCorners.add(getJPanel4(), gridBagConstraints7);
		}
		return jPanelCorners;
	}

	/**
	 * This method initializes jPanel7	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel7() {
		if (jPanel7 == null) {
			jPanel7 = new JPanel();
			jPanel7.setLayout(new BorderLayout());
			jPanel7.add(getJPanel91(), BorderLayout.WEST);
			jPanel7.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanel7;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonApply(), null);
			jPanel9.add(getJButtonCancel(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonCancel() {
		if (jButtonCancel == null) {
			jButtonCancel = new JButton();
			jButtonCancel.setPreferredSize(new Dimension(100, 23));
			jButtonCancel.setText("Cancel");
		}
		return jButtonCancel;
	}

	/**
	 * This method initializes jPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Height function");
			jPanel1 = new JPanel();
			jPanel1.setLayout(new BorderLayout());
			jPanel1.setBorder(titledBorder);
			jPanel1.add(getJPanel(), BorderLayout.NORTH);
		}
		return jPanel1;
	}

	/**
	 * This method initializes jButtonApply	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonApply() {
		if (jButtonApply == null) {
			jButtonApply = new JButton();
			jButtonApply.setPreferredSize(new Dimension(100, 23));
			jButtonApply.setText("Apply");
		}
		return jButtonApply;
	}

	/**
	 * This method initializes jPanel91	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel91() {
		if (jPanel91 == null) {
			FlowLayout flowLayout71 = new FlowLayout();
			flowLayout71.setHgap(10);
			jPanel91 = new JPanel();
			jPanel91.setLayout(flowLayout71);
			jPanel91.add(getJButtonReset(), null);
		}
		return jPanel91;
	}

	/**
	 * This method initializes jButtonReset	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonReset() {
		if (jButtonReset == null) {
			jButtonReset = new JButton();
			jButtonReset.setPreferredSize(new Dimension(100, 23));
			jButtonReset.setText("Reset");
		}
		return jButtonReset;
	}

	/**
	 * This method initializes jButtonAdd	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonAdd() {
		if (jButtonAdd == null) {
			jButtonAdd = new JButton();
			jButtonAdd.setText("Add");
		}
		return jButtonAdd;
	}

	/**
	 * This method initializes jButtonRemove	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonRemove() {
		if (jButtonRemove == null) {
			jButtonRemove = new JButton();
			jButtonRemove.setText("Remove");
			jButtonRemove.setPreferredSize(new Dimension(80, 23));
		}
		return jButtonRemove;
	}

	/**
	 * This method initializes jButtonMin	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonMin() {
		if (jButtonMin == null) {
			jButtonMin = new JButton();
			jButtonMin.setText("Min");
		}
		return jButtonMin;
	}

	/**
	 * This method initializes jButtonMax	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonMax() {
		if (jButtonMax == null) {
			jButtonMax = new JButton();
			jButtonMax.setText("Max");
		}
		return jButtonMax;
	}

	/**
	 * This method initializes jButtonSet	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonSet() {
		if (jButtonSet == null) {
			jButtonSet = new JButton();
			jButtonSet.setText("Set");
			jButtonSet.setPreferredSize(new Dimension(80, 23));
		}
		return jButtonSet;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			GridLayout gridLayout1 = new GridLayout();
			gridLayout1.setRows(5);
			gridLayout1.setHgap(2);
			gridLayout1.setVgap(2);
			gridLayout1.setColumns(1);
			jPanel = new JPanel();
			jPanel.setLayout(gridLayout1);
			jPanel.add(getJButtonSet(), null);
			jPanel.add(getJButtonAdd(), null);
			jPanel.add(getJButtonRemove(), null);
			jPanel.add(getJButtonMin(), null);
			jPanel.add(getJButtonMax(), null);
		}
		return jPanel;
	}

	/**
	 * This method initializes jButtonWN	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonWN() {
		if (jButtonWN == null) {
			jButtonWN = new JButton();
			jButtonWN.setMinimumSize(new Dimension(40, 40));
			jButtonWN.setPreferredSize(new Dimension(40, 40));
			jButtonWN.setFont(new Font("Dialog", Font.BOLD, 12));
			jButtonWN.setMargin(new Insets(1,1,1,1));
			jButtonWN.setText("W-");
		}
		return jButtonWN;
	}

	/**
	 * This method initializes jButtonWP	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonWP() {
		if (jButtonWP == null) {
			jButtonWP = new JButton();
			jButtonWP.setMinimumSize(new Dimension(40, 40));
			jButtonWP.setPreferredSize(new Dimension(40, 40));
			jButtonWP.setFont(new Font("Dialog", Font.BOLD, 12));
			jButtonWP.setMargin(new Insets(1,1,1,1));
			jButtonWP.setText("W+");
		}
		return jButtonWP;
	}

	/**
	 * This method initializes jButtonNP	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonNP() {
		if (jButtonNP == null) {
			jButtonNP = new JButton();
			jButtonNP.setMinimumSize(new Dimension(40, 40));
			jButtonNP.setPreferredSize(new Dimension(40, 40));
			jButtonNP.setFont(new Font("Dialog", Font.BOLD, 12));
			jButtonNP.setMargin(new Insets(1,1,1,1));
			jButtonNP.setText("N+");
		}
		return jButtonNP;
	}

	/**
	 * This method initializes jButtonNN	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonNN() {
		if (jButtonNN == null) {
			jButtonNN = new JButton();
			jButtonNN.setMinimumSize(new Dimension(40, 40));
			jButtonNN.setPreferredSize(new Dimension(40, 40));
			jButtonNN.setFont(new Font("Dialog", Font.BOLD, 12));
			jButtonNN.setMargin(new Insets(1,1,1,1));
			jButtonNN.setText("N-");
		}
		return jButtonNN;
	}

	/**
	 * This method initializes jButtonSN	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonSN() {
		if (jButtonSN == null) {
			jButtonSN = new JButton();
			jButtonSN.setMinimumSize(new Dimension(40, 40));
			jButtonSN.setPreferredSize(new Dimension(40, 40));
			jButtonSN.setFont(new Font("Dialog", Font.BOLD, 12));
			jButtonSN.setMargin(new Insets(1,1,1,1));
			jButtonSN.setText("S-");
		}
		return jButtonSN;
	}

	/**
	 * This method initializes jButtonSP	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonSP() {
		if (jButtonSP == null) {
			jButtonSP = new JButton();
			jButtonSP.setMinimumSize(new Dimension(40, 40));
			jButtonSP.setPreferredSize(new Dimension(40, 40));
			jButtonSP.setFont(new Font("Dialog", Font.BOLD, 12));
			jButtonSP.setMargin(new Insets(1,1,1,1));
			jButtonSP.setText("S+");
		}
		return jButtonSP;
	}

	/**
	 * This method initializes jButtonEN	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonEN() {
		if (jButtonEN == null) {
			jButtonEN = new JButton();
			jButtonEN.setMinimumSize(new Dimension(40, 40));
			jButtonEN.setPreferredSize(new Dimension(40, 40));
			jButtonEN.setFont(new Font("Dialog", Font.BOLD, 12));
			jButtonEN.setMargin(new Insets(1,1,1,1));
			jButtonEN.setText("E-");
		}
		return jButtonEN;
	}

	/**
	 * This method initializes jButtonEP	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonEP() {
		if (jButtonEP == null) {
			jButtonEP = new JButton();
			jButtonEP.setMinimumSize(new Dimension(40, 40));
			jButtonEP.setPreferredSize(new Dimension(40, 40));
			jButtonEP.setFont(new Font("Dialog", Font.BOLD, 12));
			jButtonEP.setMargin(new Insets(1,1,1,1));
			jButtonEP.setText("E+");
		}
		return jButtonEP;
	}

	/**
	 * This method initializes jPanel2	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel2() {
		if (jPanel2 == null) {
			jPanel2 = new JPanel();
			jPanel2.setLayout(new GridBagLayout());
		}
		return jPanel2;
	}

	/**
	 * This method initializes jPanel8	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel8() {
		if (jPanel8 == null) {
			jPanel8 = new JPanel();
			jPanel8.setLayout(new GridBagLayout());
		}
		return jPanel8;
	}

	/**
	 * This method initializes jPanel3	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel3() {
		if (jPanel3 == null) {
			jPanel3 = new JPanel();
			jPanel3.setLayout(new GridBagLayout());
		}
		return jPanel3;
	}

	/**
	 * This method initializes jPanel4	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel4() {
		if (jPanel4 == null) {
			jPanel4 = new JPanel();
			jPanel4.setLayout(new GridBagLayout());
		}
		return jPanel4;
	}

}  //  @jve:decl-index=0:visual-constraint="96,9"
