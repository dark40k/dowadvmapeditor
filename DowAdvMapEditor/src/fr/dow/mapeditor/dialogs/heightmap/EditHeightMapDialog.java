package fr.dow.mapeditor.dialogs.heightmap;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.dow.gamedata.sgbmap.terrain.Ground;
import fr.dow.gamedata.sgbmap.terrain.HeightMap;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.heightmap.PositionJPanel.PositionChangeEvent;
import fr.dow.mapeditor.dialogs.heightmap.PositionJPanel.PositionChangeListener;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.joglobject.JoglWireGround;
import fr.dow.mapviewer.display.selection.SelArea;
import fr.dow.mapviewer.display.selection.SelArea.ChangeSelAreaEvent;
import fr.dow.mapviewer.display.selection.SelArea.ChangeSelAreaListener;
import fr.dow.mapviewer.display.selection.SelArea.CornerDef;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;
import fr.dow.mapviewer.display.selection.Selection;

public class EditHeightMapDialog extends EditHeightMapDialogBase implements ActionListener,PositionChangeListener, ChangeSelAreaListener {

	private enum ApplyMode { SET, ADD, REMOVE, MAX, MIN; }
	
	private TerrainTool terrainTool=new TerrainTool();
	
	private HeightMap heightMap;
	private float[][] newHeightGrid;
	private float[][] mapHeightGrid;
	private float heightScale;
	
	private SelArea.CornerDef curDeactivated;
	
	private PositionJPanel jPanelCornerNW;
	private PositionJPanel jPanelCornerSW;
	private PositionJPanel jPanelCornerNE;
	private PositionJPanel jPanelCornerSE;

	private Selection mapSel;

	private MapViewer mapViewer;
	
	private JoglWireGround joglWireGround;
	
	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		EditHeightMapDialog dialog = new EditHeightMapDialog(mapEditor, mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		mapViewer.getRenderer().curFilter.lockAllExcept(FilterType.GROUNDAREA);
		dialog.setVisible(true);
		return;
	}	
	
	public EditHeightMapDialog(MapEditor mapEditor, MapViewer mapViewer) {
		
		super(mapEditor);
		
		this.mapViewer=mapViewer;
		
		Ground ground=mapViewer.getMap().terrain.ground;
		
		heightScale=ground.groundData.get_heightscale();
		heightMap=ground.heightMap;
		
		mapHeightGrid=heightMap.get_heightGrid();
		
		newHeightGrid=new float[mapHeightGrid.length][];
		for (int i=0;i<mapHeightGrid.length;i++) newHeightGrid[i]=mapHeightGrid[i].clone();
		
		joglWireGround=new JoglWireGround(newHeightGrid,heightMap.get_heightGrid_Xaxis(),heightMap.get_heightGrid_Zaxis(),ground.groundData.get_heightscale());
		mapViewer.getRenderer().addDisplayListener(joglWireGround);
		
		mapSel = mapViewer.getRenderer().currentSelection;
		
		jPanelCornerNW=new PositionJPanel("NW Corner", CornerDef.NORTHWEST, mapSel,ground);
		jPanelCornerSW=new PositionJPanel("SW Corner", CornerDef.SOUTHWEST, mapSel,ground);
		jPanelCornerNE=new PositionJPanel("NE Corner", CornerDef.NORTHEAST, mapSel,ground);
		jPanelCornerSE=new PositionJPanel("SE Corner", CornerDef.SOUTHEAST, mapSel,ground);
		
		jPanelCornerNW.addPositionChangeListener(this);
		jPanelCornerSW.addPositionChangeListener(this);
		jPanelCornerNE.addPositionChangeListener(this);
		jPanelCornerSE.addPositionChangeListener(this);
		
		GridBagConstraints gridBagConstraintsSE = new GridBagConstraints();
		gridBagConstraintsSE.gridx = 4;
		gridBagConstraintsSE.gridwidth = 3;
		gridBagConstraintsSE.gridheight = 3;
		gridBagConstraintsSE.fill = GridBagConstraints.BOTH;
		gridBagConstraintsSE.gridy = 4;
		GridBagConstraints gridBagConstraintsNE = new GridBagConstraints();
		gridBagConstraintsNE.gridx = 4;
		gridBagConstraintsNE.fill = GridBagConstraints.BOTH;
		gridBagConstraintsNE.gridwidth = 3;
		gridBagConstraintsNE.gridheight = 3;
		gridBagConstraintsNE.gridy = 0;
		GridBagConstraints gridBagConstraintsSW = new GridBagConstraints();
		gridBagConstraintsSW.gridx = 0;
		gridBagConstraintsSW.gridheight = 3;
		gridBagConstraintsSW.gridwidth = 3;
		gridBagConstraintsSW.fill = GridBagConstraints.BOTH;
		gridBagConstraintsSW.gridy = 4;
		GridBagConstraints gridBagConstraintsNW = new GridBagConstraints();
		gridBagConstraintsNW.gridx = 0;
		gridBagConstraintsNW.gridwidth = 3;
		gridBagConstraintsNW.gridheight = 3;
		gridBagConstraintsNW.fill = GridBagConstraints.BOTH;
		gridBagConstraintsNW.gridy = 0;
		jPanelCorners.add(jPanelCornerNW, gridBagConstraintsNW);
		jPanelCorners.add(jPanelCornerSW, gridBagConstraintsSW);
		jPanelCorners.add(jPanelCornerNE, gridBagConstraintsNE);
		jPanelCorners.add(jPanelCornerSE, gridBagConstraintsSE);
		
		setMode(ApplyMode.SET);
		jButtonSet.addActionListener(this);
		jButtonAdd.addActionListener(this);
		jButtonRemove.addActionListener(this);
		jButtonMax.addActionListener(this);
		jButtonMin.addActionListener(this);
		
		setDeactivated(CornerDef.NORTHEAST);
		jPanelCornerNW.jButtonDeActivate.addActionListener(this);
		jPanelCornerSW.jButtonDeActivate.addActionListener(this);
		jPanelCornerNE.jButtonDeActivate.addActionListener(this);
		jPanelCornerSE.jButtonDeActivate.addActionListener(this);
		
		mapSel.area.addChangeSelAreaListener(this);
		
		jButtonNP.addActionListener(this);
		jButtonNN.addActionListener(this);
		jButtonSP.addActionListener(this);
		jButtonSN.addActionListener(this);
		jButtonEP.addActionListener(this);
		jButtonEN.addActionListener(this);
		jButtonWP.addActionListener(this);
		jButtonWN.addActionListener(this);
		
		jButtonReset.addActionListener(this);
		jButtonApply.addActionListener(this);
		jButtonCancel.addActionListener(this);
		
		updateTerrain();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource()==jButtonSet) setMode(ApplyMode.SET) ;  
		if (e.getSource()==jButtonAdd) setMode(ApplyMode.ADD) ;  
		if (e.getSource()==jButtonRemove) setMode(ApplyMode.REMOVE) ;  
		if (e.getSource()==jButtonMax) setMode(ApplyMode.MAX) ;  
		if (e.getSource()==jButtonMin) setMode(ApplyMode.MIN) ;  
		
		if (e.getSource()==jPanelCornerNW.jButtonDeActivate) setDeactivated(CornerDef.NORTHWEST); 
		if (e.getSource()==jPanelCornerSW.jButtonDeActivate) setDeactivated(CornerDef.SOUTHWEST); 
		if (e.getSource()==jPanelCornerNE.jButtonDeActivate) setDeactivated(CornerDef.NORTHEAST); 
		if (e.getSource()==jPanelCornerSE.jButtonDeActivate) setDeactivated(CornerDef.SOUTHEAST); 
		
		if (e.getSource()==jButtonNP) mapSel.area.moveCorner(CornerDef.NORTHEAST, 0, 1, heightMap);
		if (e.getSource()==jButtonNN) mapSel.area.moveCorner(CornerDef.NORTHEAST, 0, -1, heightMap);
		if (e.getSource()==jButtonEP) mapSel.area.moveCorner(CornerDef.NORTHEAST, 1, 0, heightMap);
		if (e.getSource()==jButtonEN) mapSel.area.moveCorner(CornerDef.NORTHEAST, -1, 0, heightMap);
		if (e.getSource()==jButtonSP) mapSel.area.moveCorner(CornerDef.SOUTHWEST, 0, 1, heightMap);
		if (e.getSource()==jButtonSN) mapSel.area.moveCorner(CornerDef.SOUTHWEST, 0, -1, heightMap);
		if (e.getSource()==jButtonWP) mapSel.area.moveCorner(CornerDef.SOUTHWEST, 1, 0, heightMap);
		if (e.getSource()==jButtonWN) mapSel.area.moveCorner(CornerDef.SOUTHWEST, -1, 0, heightMap);
		
		if (e.getSource()==jButtonReset) reset();
		if (e.getSource()==jButtonApply) apply();
		if (e.getSource()==jButtonCancel) { cancel(); return; }
		updateTerrain();
	}
	
	private void apply() {
		terrainTool.applyHeight();
		updateTerrain();
	}
	
	private void cancel() {
		mapViewer.getRenderer().removeDisplayListener(joglWireGround);
		mapSel.area.removeChangeSelAreaListener(this);
		this.setVisible(false);
	}
	
	private void setMode(ApplyMode newMode) {
		jButtonSet.setSelected(newMode==ApplyMode.SET);
		jButtonAdd.setSelected(newMode==ApplyMode.ADD);
		jButtonRemove.setSelected(newMode==ApplyMode.REMOVE);
		jButtonMax.setSelected(newMode==ApplyMode.MAX);
		jButtonMin.setSelected(newMode==ApplyMode.MIN);
	}
	
	private void setDeactivated(CornerDef newDeactivated) {
		curDeactivated=newDeactivated;
		jPanelCornerNW.setEditable(newDeactivated!=CornerDef.NORTHWEST);
		jPanelCornerSW.setEditable(newDeactivated!=CornerDef.SOUTHWEST);
		jPanelCornerNE.setEditable(newDeactivated!=CornerDef.NORTHEAST);
		jPanelCornerSE.setEditable(newDeactivated!=CornerDef.SOUTHEAST);
	}
	
	private void updateTerrain() {
		if (jButtonSet.isSelected()) { terrainTool.updateTerrainSet(); }
		if (jButtonMin.isSelected()) { terrainTool.updateTerrainMin(); }
		if (jButtonMax.isSelected()) { terrainTool.updateTerrainMax(); }
		if (jButtonAdd.isSelected()) { terrainTool.updateTerrainAdd(); }
		if (jButtonRemove.isSelected()) { terrainTool.updateTerrainRemove(); }
		
		switch (curDeactivated) {
			case NORTHEAST : { jPanelCornerNE.initCornerHeight(); break; }
			case NORTHWEST : { jPanelCornerNW.initCornerHeight(); break; }
			case SOUTHWEST : { jPanelCornerSW.initCornerHeight(); break; }
			case SOUTHEAST : { jPanelCornerSE.initCornerHeight(); break; }
		}

		jPanelCornerNW.updateDisplay();
		jPanelCornerSW.updateDisplay();
		jPanelCornerNE.updateDisplay();
		jPanelCornerSE.updateDisplay();
		
	}
	
	@Override
	public void positionChange(PositionChangeEvent evt) {
		updateTerrain();
	}
	
	@Override
	public void changeSelArea(ChangeSelAreaEvent evt) {
		updateTerrain();
	}
	
	public void reset() {
		terrainTool.restoreHeight();
		if ((jButtonAdd.isSelected())|(jButtonRemove.isSelected())) {
			jPanelCornerNW.setCornerHeight(0);
			jPanelCornerSW.setCornerHeight(0);
			jPanelCornerNE.setCornerHeight(0);
			jPanelCornerSE.setCornerHeight(0);	
		} else {
			jPanelCornerNW.initCornerHeight();
			jPanelCornerSW.initCornerHeight();
			jPanelCornerNE.initCornerHeight();
			jPanelCornerSE.initCornerHeight();
		}
		updateTerrain();
	}
	
	private class TerrainTool {
		
		int i1,i2,j1,j2;
		float h1,dhi,dhj;
		
		private void update() {

			restoreHeight();
			
			i1=mapSel.area.i1;
			i2=mapSel.area.i2;
			j1=mapSel.area.j1;
			j2=mapSel.area.j2;

			joglWireGround.setDisplayWindow(i1-1, j1-1, i2+1, j2+1);
			
			switch (curDeactivated) {
				case NORTHEAST : {
					h1=jPanelCornerSW.getCornerHeight();
					dhi=(jPanelCornerSE.getCornerHeight() - jPanelCornerSW.getCornerHeight()) / (float)(i2-i1);
					dhj=(jPanelCornerNW.getCornerHeight() - jPanelCornerSW.getCornerHeight()) / (float)(j2-j1);
					break;
				}
				case NORTHWEST : {
					h1=jPanelCornerSW.getCornerHeight();
					dhi=(jPanelCornerSE.getCornerHeight() - jPanelCornerSW.getCornerHeight()) / (float)(i2-i1);
					dhj=(jPanelCornerNE.getCornerHeight() - jPanelCornerSE.getCornerHeight()) / (float)(j2-j1);
					break;
				}
				case SOUTHWEST : {
					h1=jPanelCornerSE.getCornerHeight()-jPanelCornerNE.getCornerHeight()+jPanelCornerNW.getCornerHeight();
					dhi=(jPanelCornerNE.getCornerHeight() - jPanelCornerNW.getCornerHeight()) / (float)(i2-i1);
					dhj=(jPanelCornerNE.getCornerHeight() - jPanelCornerSE.getCornerHeight()) / (float)(j2-j1);
					break;
				}
				case SOUTHEAST :{
					h1=jPanelCornerSW.getCornerHeight();
					dhi=(jPanelCornerNE.getCornerHeight() - jPanelCornerNW.getCornerHeight()) / (float)(i2-i1);
					dhj=(jPanelCornerNW.getCornerHeight() - jPanelCornerSW.getCornerHeight()) / (float)(j2-j1);
					break;
				}
			}
		}
		
		private void applyHeight() {

			for (int i = i1; i <= i2; i++)
				for (int j = j1; j <= j2; j++)
					mapHeightGrid[i][j] = roundHeight(newHeightGrid[i][j]);

			mapViewer.getRenderer().mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);

		}

		private void restoreHeight() {
			for (int i = i1; i <= i2; i++)
				for (int j = j1; j <= j2; j++)
					newHeightGrid[i][j] = mapHeightGrid[i][j];
		}

		private void updateTerrainSet() {
			update();
			for (int i = i1; i <= i2; i++)
				for (int j = j1; j <= j2; j++)
					newHeightGrid[i][j] = roundHeight(h1 + ((float) (i - i1)) * dhi + ((float) (j - j1)) * dhj);
		}

		private void updateTerrainMin() {
			update();
			for (int i = i1; i <= i2; i++)
				for (int j = j1; j <= j2; j++)
					newHeightGrid[i][j] = roundHeight(Math.min(newHeightGrid[i][j], h1 + ((float) (i - i1)) * dhi + ((float) (j - j1)) * dhj));
		}

		private void updateTerrainMax() {
			update();
			for (int i = i1; i <= i2; i++)
				for (int j = j1; j <= j2; j++)
					newHeightGrid[i][j] = roundHeight(Math.max(newHeightGrid[i][j], h1 + ((float) (i - i1)) * dhi + ((float) (j - j1)) * dhj));
		}

		private void updateTerrainAdd() {
			update();
			for (int i = i1; i <= i2; i++)
				for (int j = j1; j <= j2; j++)
					newHeightGrid[i][j] += roundHeight(h1 + ((float) (i - i1)) * dhi + ((float) (j - j1)) * dhj);
		}

		private void updateTerrainRemove() {
			update();
			for (int i = i1; i <= i2; i++)
				for (int j = j1; j <= j2; j++)
					newHeightGrid[i][j] -= roundHeight(h1 + ((float) (i - i1)) * dhi + ((float) (j - j1)) * dhj);
		}

		private float roundHeight(double height) {
			double newHeight = Math.floor(height / heightScale);
			if (newHeight<0) return 0;
			if (newHeight>255) return 255 * heightScale;
			return (float) (newHeight * heightScale);
		}
		
	}

	
}
