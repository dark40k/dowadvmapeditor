package fr.dow.mapeditor.dialogs.heightmap;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JSlider;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Dimension;
import javax.swing.SwingConstants;
import java.awt.Insets;
import java.awt.Font;
import javax.swing.JButton;

public class PositionJPanelBase extends JPanel {

	private static final long serialVersionUID = 1L;
	protected JSlider jSlider = null;
	private JLabel jLabel = null;
	protected JTextField jTextFieldX = null;
	private JLabel jLabel1 = null;
	protected JTextField jTextFieldY = null;
	private JLabel jLabel11 = null;
	protected JTextField jTextFieldZ = null;
	protected JButton jButtonDeActivate = null;
	/**
	 * This is the default constructor
	 */
	public PositionJPanelBase() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
		gridBagConstraints7.gridx = 0;
		gridBagConstraints7.gridwidth = 3;
		gridBagConstraints7.anchor = GridBagConstraints.WEST;
		gridBagConstraints7.fill = GridBagConstraints.NONE;
		gridBagConstraints7.weighty = 1.0;
		gridBagConstraints7.gridy = 0;
		GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
		gridBagConstraints6.gridx = 0;
		gridBagConstraints6.weightx = 0.2;
		gridBagConstraints6.weighty = 0.0;
		gridBagConstraints6.anchor = GridBagConstraints.EAST;
		gridBagConstraints6.gridy = 3;
		GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
		gridBagConstraints5.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints5.gridy = 3;
		gridBagConstraints5.gridx = 1;
		gridBagConstraints5.weighty = 1.0;
		gridBagConstraints5.insets = new Insets(5, 5, 5, 5);
		gridBagConstraints5.weightx = 1.0;
		jLabel11 = new JLabel();
		jLabel11.setPreferredSize(new Dimension(50, 14));
		jLabel11.setText("Z=");
		jLabel11.setFont(new Font("Dialog", Font.BOLD, 14));
		jLabel11.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
		gridBagConstraints4.gridx = 0;
		gridBagConstraints4.weightx = 0.2;
		gridBagConstraints4.weighty = 0.0;
		gridBagConstraints4.anchor = GridBagConstraints.EAST;
		gridBagConstraints4.gridy = 2;
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints3.gridx = 1;
		gridBagConstraints3.gridy = 2;
		gridBagConstraints3.weighty = 1.0;
		gridBagConstraints3.insets = new Insets(5, 5, 5, 5);
		gridBagConstraints3.weightx = 1.0;
		jLabel1 = new JLabel();
		jLabel1.setPreferredSize(new Dimension(50, 14));
		jLabel1.setText("Y=");
		jLabel1.setFont(new Font("Dialog", Font.BOLD, 14));
		jLabel1.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints2.gridy = 1;
		gridBagConstraints2.weightx = 1.0;
		gridBagConstraints2.weighty = 1.0;
		gridBagConstraints2.insets = new Insets(5, 5, 5, 5);
		gridBagConstraints2.gridx = 1;
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.gridx = 0;
		gridBagConstraints1.weightx = 0.2;
		gridBagConstraints1.weighty = 0.0;
		gridBagConstraints1.anchor = GridBagConstraints.EAST;
		gridBagConstraints1.gridy = 1;
		jLabel = new JLabel();
		jLabel.setText("X=");
		jLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		jLabel.setFont(new Font("Dialog", Font.BOLD, 14));
		jLabel.setPreferredSize(new Dimension(50, 14));
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.weightx = 0.1;
		gridBagConstraints.gridheight = 3;
		gridBagConstraints.weighty = 0.0D;
		gridBagConstraints.insets = new Insets(0, 0, 0, 0);
		gridBagConstraints.gridx = 2;
		this.setSize(201, 157);
		this.setLayout(new GridBagLayout());
		this.add(getJSlider(), gridBagConstraints);
		this.add(jLabel, gridBagConstraints1);
		this.add(getJTextFieldX(), gridBagConstraints2);
		this.add(jLabel1, gridBagConstraints4);
		this.add(getJTextFieldY(), gridBagConstraints3);
		this.add(jLabel11, gridBagConstraints6);
		this.add(getJTextFieldZ(), gridBagConstraints5);
		this.add(getJButtonDeActivate(), gridBagConstraints7);
	}

	/**
	 * This method initializes jSlider	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getJSlider() {
		if (jSlider == null) {
			jSlider = new JSlider();
			jSlider.setOrientation(JSlider.VERTICAL);
		}
		return jSlider;
	}

	/**
	 * This method initializes jTextFieldX	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldX() {
		if (jTextFieldX == null) {
			jTextFieldX = new JTextField();
			jTextFieldX.setEnabled(false);
		}
		return jTextFieldX;
	}

	/**
	 * This method initializes jTextFieldY	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldY() {
		if (jTextFieldY == null) {
			jTextFieldY = new JTextField();
		}
		return jTextFieldY;
	}

	/**
	 * This method initializes jTextFieldZ	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldZ() {
		if (jTextFieldZ == null) {
			jTextFieldZ = new JTextField();
			jTextFieldZ.setEnabled(false);
		}
		return jTextFieldZ;
	}

	/**
	 * This method initializes jButtonDeActivate	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonDeActivate() {
		if (jButtonDeActivate == null) {
			jButtonDeActivate = new JButton();
			jButtonDeActivate.setText("Deactivate Corner");
		}
		return jButtonDeActivate;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
