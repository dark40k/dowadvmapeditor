package fr.dow.mapeditor.dialogs.heightmap;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventListener;
import java.util.EventObject;

import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

import fr.dow.gamedata.sgbmap.terrain.Ground;
import fr.dow.gamedata.sgbmap.terrain.HeightMap;
import fr.dow.mapviewer.display.selection.SelArea.CornerDef;
import fr.dow.mapviewer.display.selection.Selection;

public class PositionJPanel extends PositionJPanelBase implements ActionListener, MouseListener, ChangeListener {

	private CornerDef corner;

	private Selection mapSel;
	private float height;
	private float height0;
	
	private HeightMap heightMap;
	private float heightScale;
		
	public PositionJPanel(String title, CornerDef corner, Selection mapSel, Ground ground) {
		super();
		
		this.setBorder(BorderFactory.createTitledBorder(null, title, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213)));
		
		this.heightMap=ground.heightMap;
		this.heightScale=ground.groundData.get_heightscale();
		
		this.corner=corner;
		this.mapSel=mapSel;
		
		jSlider.setMinimum(-50);
		jSlider.setValue(0);
		//jSlider.setPaintTicks(true);
		jSlider.setMajorTickSpacing(1);
		jSlider.setMaximum(50);
		
		jSlider.addMouseListener(this);
		jSlider.addChangeListener(this);
		
		jTextFieldY.addActionListener(this);
		
		jTextFieldY.addFocusListener( new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				 updateHeightWithText();
			}
			
			@Override
			public void focusGained(FocusEvent e) {
			}
		});
		
		initCornerHeight();
		updateDisplay();		
		
	}

	public float getCornerHeight() {
		return height;
	}
	
	public void setCornerHeight(float newHeight) {
		height=newHeight;
		height0=newHeight;
		updateDisplay();
	}
	
	public void initCornerHeight() {
		height=heightMap.get_height(mapSel.area.getCornerI(corner), mapSel.area.getCornerJ(corner));
		height0=height;
		updateDisplay();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==jTextFieldY) {
			updateHeightWithText();
		}
//		if (e.getSource()==jButtonHP) { height+=heightScale; updateDisplay(); firePositionChangeEvent(); return; }
//		if (e.getSource()==jButtonHN) { height-=heightScale; updateDisplay(); firePositionChangeEvent(); return; }
	}

	public void updateDisplay() {
		jTextFieldX.setText(Float.toString(heightMap.get_heightGrid_Xaxis()[mapSel.area.getCornerI(corner)]));
		jTextFieldZ.setText(Float.toString(heightMap.get_heightGrid_Zaxis()[mapSel.area.getCornerJ(corner)]));
		jTextFieldY.setText(Float.toString(height));
	}
	
	public void setEditable(boolean newStatus) {
//		jButtonDeActivate.setVisible(newStatus);
//		jSlider.setVisible(newStatus);
//		jTextFieldX.setEditable(false);
//		jTextFieldZ.setEditable(false);
//		jTextFieldY.setEditable(false);
		this.setVisible(newStatus);
	}
	
	protected EventListenerList listenerList = new EventListenerList();

	class PositionChangeEvent extends EventObject {
		public PositionChangeEvent(Object source) {
			super(source);
		}
	}

	interface PositionChangeListener extends EventListener {
		public void positionChange(PositionChangeEvent evt);
	}

	public void addPositionChangeListener(PositionChangeListener listener) {
		listenerList.add(PositionChangeListener.class, listener);
	}
	
	public void removePositionChangeListener(PositionChangeListener listener) {
		listenerList.remove(PositionChangeListener.class, listener);
	}
	
	void firePositionChangeEvent() {
		PositionChangeEvent evt=new PositionChangeEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == PositionChangeListener.class) {
				((PositionChangeListener) listeners[i+1]).positionChange(evt);
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		height0=height;
		jSlider.setValue(0);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		height=height0+((float)jSlider.getValue())*heightScale;
		firePositionChangeEvent();
	}
		
	private void updateHeightWithText() {
		  try  
		  {  
			  height = Float.parseFloat(jTextFieldY.getText());  
			  firePositionChangeEvent();
		  }  
		  catch(NumberFormatException nfe)  
		  {  
			  jTextFieldY.setText(Float.toString(height));
		  }  		
	}
	
}
