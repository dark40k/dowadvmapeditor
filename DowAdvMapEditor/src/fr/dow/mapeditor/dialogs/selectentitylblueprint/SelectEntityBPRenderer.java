package fr.dow.mapeditor.dialogs.selectentitylblueprint;

import java.awt.Color;

//import javax.media.opengl.DebugGL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.gl2.GLUgl2;


import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.units.Entity;
import fr.dow.gamedata.sgbmap.units.PlayerID;
import fr.dow.mapviewer.display.joglobject.joglentity.JoglEntity;
import fr.dow.mapviewer.display.renderer.Mouse3DMoveHandler;

public class SelectEntityBPRenderer implements GLEventListener {

	private GLUgl2 glu = new GLUgl2();

	private JoglEntity joglEntity;
	private Entity defaultEntity=new Entity(0,0,PlayerID.World); // create default entity for transformation
	
	private int gridGenList=-1;
	
	private boolean flushEntity;
	private boolean newEntity;
	
	private String newBlueprint;
	private DowMod newModData;
	
	private Color newBackGroundColor=new Color(1f,1f,1f,0f);

	private Mouse3DMoveHandler mouse3DMoveHandler;
	
	public SelectEntityBPRenderer(GLCanvas glCanvas) {
		mouse3DMoveHandler=new Mouse3DMoveHandler(glCanvas);
		mouse3DMoveHandler.setView(10, 10, 10, -45, -45);
	}
	
	public void displayNewEntity(String blueprint, DowMod modData) {
		this.newBlueprint=blueprint;
		this.newModData=modData;
		this.newEntity=true;
	}
	
	public void clearEntity() {
		this.flushEntity=true;
	}
	
	public void init(GLAutoDrawable drawable) {
		//drawable.setGL(new DebugGL(drawable.getGL()));

		GL2 gl = drawable.getGL().getGL2();
		gl.glClearColor(0, 0, 0, 0);
		
        //
        // Init OpenGL parameters
        //
        gl.glEnable(GL2.GL_CULL_FACE);
        gl.glEnable(GL2.GL_COLOR_MATERIAL) ;
        gl.glFrontFace(GL2.GL_CW);
        
        gl.glPointSize(4.0f);
        
        gl.glShadeModel(GL2.GL_FLAT); //Enables Smooth Color Shading

		gl.glEnable(GL2.GL_DEPTH_TEST);
        gl.glClearDepth(1.0); //Enables Clearing Of The Depth Buffer
        gl.glDepthFunc(GL2.GL_LEQUAL); //The Type Of Depth Test To Do
		
        gl.glEnable(GL2.GL_LIGHTING);
        float pos[] = { 5.0f, 5.0f, 10.0f, 0.0f };
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, pos, 0);
        gl.glEnable(GL2.GL_LIGHT0);
        
        //
        // Set default material and background color
        //
        gl.glColorMaterial ( GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE ) ;
		
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		
        GL2 gl = drawable.getGL().getGL2();

        height = (height == 0) ? 1 : height;

        gl.glViewport(0, 0, width, height);
        
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45, (float) width / height, 0.1, 100000);
        
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
	}

	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT); //Clear The Screen And The Depth Buffer

        if (gridGenList<=0) {
        	
        	gridGenList = gl.glGenLists(1);
        	
    		gl.glNewList( gridGenList, GL2.GL_COMPILE );
        	
			for (float x=-10f;x<=10f;x+=1f) {
				   gl.glBegin (GL2.GL_LINES);
				     gl.glVertex3f (x, 0f, -10f);
				     gl.glVertex3f (x, 0f, 10f);
				   gl.glEnd();
			}

			for (float z=-10f;z<=10f;z+=1f) {
				   gl.glBegin (GL2.GL_LINES);
				     gl.glVertex3f (-10f, 0f, z);
				     gl.glVertex3f (10f, 0f, z);
				   gl.glEnd();
			}
			
			gl.glEndList();
			
        }
        
        if (newBackGroundColor!=null) {
        	float r=(float)newBackGroundColor.getRed() / 255f;
        	float g=(float)newBackGroundColor.getGreen() / 255f;
        	float b=(float)newBackGroundColor.getBlue() / 255f;
        	float a=(float)newBackGroundColor.getAlpha() / 255f;
        	gl.glClearColor(r,g,b,a);              
        	newBackGroundColor=null;
        }
		
		if (flushEntity) {
			flushEntity = false;
			if (joglEntity != null) {
				joglEntity=null; // TODO : IMPORTANT - clear video memory
			}
		}

		if (newEntity) {
			newEntity = false;

			if (joglEntity != null) {
				joglEntity = null; // TODO : IMPORTANT - clear video memory
			}

			try {
				joglEntity=JoglEntity.loadRgd(gl, newModData, newBlueprint);
			} catch (Exception e) {
				System.err.println("Error - could not load "+newBlueprint);
			}
			
			if (joglEntity==null) { System.err.println("Loading failed for entity "+newBlueprint);}
			
		}

		if (joglEntity != null) {

	        // Set view direction
	        mouse3DMoveHandler.setViewDirection(gl);
	        
			// Draw scene
			gl.glPushMatrix();
			gl.glMultMatrixf(defaultEntity.getTransf(), 0);
			joglEntity.displayTextured(gl);
			gl.glPopMatrix();
			
			// Draw grid
			gl.glColor4f(0f,0f,0f,1f);
			gl.glCallList(gridGenList);
			
		}
	}
	
    public void dispose(GLAutoDrawable drawable) { }

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {}	

	public void setNewBackgroundColor(Color newColor) {
		newBackGroundColor=newColor;
	}

}
