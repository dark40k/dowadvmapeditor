package fr.dow.mapeditor.dialogs.selectentitylblueprint;

import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.tree.TreeNode;

import fr.dow.gamedata.filesystem.DowFile;
import fr.dow.gamedata.filesystem.DowFolder;

public class BlueprintTreeNode implements TreeNode {

	private Object dowObject; // can be DowFolder or DowFile

	private BlueprintTreeNode father;
	private Vector<BlueprintTreeNode> children;
	
	private class BlueprintTreeNodeComparator implements Comparator<BlueprintTreeNode> {
		@Override
		public int compare(BlueprintTreeNode arg0, BlueprintTreeNode arg1) {
			BlueprintTreeNode bptn0 = (BlueprintTreeNode)arg0;
			BlueprintTreeNode bptn1 = (BlueprintTreeNode)arg1;
			return bptn0.toString().toLowerCase().compareTo(bptn1.toString().toLowerCase());
		}
	}
	
	public BlueprintTreeNode(DowFolder dowFolder, BlueprintTreeNode father) {
		
		dowObject=dowFolder;
		this.father=father;
		
		children=new Vector<BlueprintTreeNode>();
		
		for (DowFolder subFolder : dowFolder.getSubFoldersList().values())
			children.add(new BlueprintTreeNode(subFolder,this));
		
		for (DowFile subFile : dowFolder.getSubFilesList().values() ) 
			children.add(new BlueprintTreeNode(subFile,this));
		
		Collections.sort(children, new BlueprintTreeNodeComparator());
		
	}
	
	public BlueprintTreeNode(DowFile dowFile, BlueprintTreeNode father) {
		dowObject=dowFile;
		this.father=father;
	}
	
	
	@Override
	public String toString() {
		if (dowObject.getClass()==DowFolder.class) return ((DowFolder) dowObject).getName();
		else return ((DowFile) dowObject).getName();
	}

	public String fullPath() {
		if (dowObject.getClass()==DowFolder.class) return ((DowFolder) dowObject).getDowPath();
		else return ((DowFile) dowObject).getDowPath();
	}
	
	public BlueprintTreeNode findChild(String childName) {
		for (BlueprintTreeNode child : children )
			if (childName.equals(child.toString()))
				return child;
		return null;
	}
	
	//
	// TreeNode interface
	//
	@Override
	public Enumeration<BlueprintTreeNode> children() {
		return children.elements();
	}

	@Override
	public boolean getAllowsChildren() {
		return (dowObject.getClass()==DowFolder.class);
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		return children.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return children.size();
	}

	@Override
	public int getIndex(TreeNode node) {
		String dowPath=node.toString();
		for (int i=0; i<children.size();i++)   
			if ( dowPath.equals(children.get(i).toString()) ) return i;
		return -1;
	}

	@Override
	public TreeNode getParent() {
		return father;
	}

	@Override
	public boolean isLeaf() {
		return (dowObject.getClass()==DowFile.class);
	}
	


}
