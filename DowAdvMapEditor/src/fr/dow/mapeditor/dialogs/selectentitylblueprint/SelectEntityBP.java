package fr.dow.mapeditor.dialogs.selectentitylblueprint;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.media.opengl.awt.GLCanvas;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import com.jogamp.opengl.util.FPSAnimator;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.mapeditor.tools.ColorizedButton;

public class SelectEntityBP extends SelectEntityBPBase implements TreeSelectionListener {

	SelectEntityBPRenderer entityRenderer;
	
	DowMod mod;
	SgbMap map;
	
	SelectEntityBPTableModel entityTable;
	
	BlueprintTreeNode rootNode;
	
	public static String showDialog(Component owner, SgbMap map, DowMod mod, String defaultBP) {
		
		SelectEntityBP dialog = new SelectEntityBP(null,map, mod);
		dialog.setTitle(dialog.getTitle()+mod.getModFile().getName());
		dialog.setLocationRelativeTo(owner);
		dialog.setDefaultBP(defaultBP);
		dialog.setVisible(true);
		return dialog.getSelectedBP();
		
	}
	
	private String defaultEntity=null;
	
	private String currentEntity=null;
	
	public void close() {
		currentEntity=defaultEntity;
		this.setVisible(false);
	}
	
	public void apply() {
		this.setVisible(false);
	}
	
	private SelectEntityBP(Frame owner,SgbMap map, DowMod mod) {
		super(owner);
		
		this.mod=mod;
		this.map=map;
		
		
		//
		// Treelist
		//
		
		rootNode = new BlueprintTreeNode(mod.getFolder("attrib/ebps"),null);
		jTreeBlueprints.setModel(new DefaultTreeModel(rootNode));
//		jTreeBlueprints.setCellRenderer(new BlueprintTreeCellRenderer());
		jTreeBlueprints.setRootVisible(false);
		jTreeBlueprints.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		jTreeBlueprints.addTreeSelectionListener(this);
		
		//
		// OpenGL display
		//
		
		GLCanvas canvas = new GLCanvas();
		
		entityRenderer=new SelectEntityBPRenderer(canvas);
	    canvas.addGLEventListener(entityRenderer);
	    
		jPanelEntityPreview.add(canvas,BorderLayout.CENTER);
		
		//Init animator
		System.out.println("Starting animation");	
		FPSAnimator animator = new FPSAnimator( canvas, 25 );
		//animator.setRunAsFastAsPossible(false);
		animator.start(); 
		
		//
		// Interface
		//
		
		jButtonApply.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						apply();
					}
				}
			);
		
		jButtonCancel.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						close();
					}
				}
			);
		
		ColorizedButton colorButton=new ColorizedButton();
		colorButton.setPreferredSize(new Dimension(100,23));
		colorButton.setText("Back. Color");
		colorButton.setColor(new Color(1f,1f,1f,0f));
		jPanelLeft.add(colorButton);
		
		colorButton.addPropertyChangeListener( 
				new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent evt) {
						if (evt.getPropertyName().equals("Color")) {
							entityRenderer.setNewBackgroundColor((Color) evt.getNewValue());
						}
					}
				}
		);
		
	}

	
	private void setDefaultBP(String dowBPPath) {
		
		System.out.println("Start with default "+dowBPPath);
		
		TreePath selPath = findTreePath(dowBPPath.substring(17).replace('\\', '/'));

		if (selPath==null) return;
		
		jTreeBlueprints.setSelectionPath(selPath);
		jTreeBlueprints.setExpandsSelectedPaths(true);
		
		defaultEntity=dowBPPath;
		
	}
	
	private TreePath findTreePath(String path) {
		
		BlueprintTreeNode node = rootNode;
		TreePath treePath=new TreePath(rootNode);
		
		for (String subPath : path.split("/")) {
			node=node.findChild(subPath);
			if (node==null) return null;
			treePath=treePath.pathByAddingChild(node);
		}
		
		return treePath;
	}
	
	private String getSelectedBP() {
		return currentEntity;
	}
	
	private void updateEntityDisplay() {
		
		BlueprintTreeNode node = (BlueprintTreeNode) jTreeBlueprints.getLastSelectedPathComponent();
		
		String newEntity=null;
		
		if (node!=null) 
			if (node.isLeaf())
				newEntity=node.fullPath();
		
		jButtonApply.setEnabled(newEntity!=null);
		
		if (newEntity==null) {
			if (currentEntity != null) {
				entityRenderer.clearEntity();
				currentEntity = null;
			}
			return;
		}
		
		if (newEntity.equalsIgnoreCase(currentEntity)) return;
		
		currentEntity="data:"+newEntity;		
		entityRenderer.displayNewEntity("data:"+newEntity, mod);
        
	}

	@Override
	public void valueChanged(TreeSelectionEvent arg0) {
		updateEntityDisplay();
	}

	
}
