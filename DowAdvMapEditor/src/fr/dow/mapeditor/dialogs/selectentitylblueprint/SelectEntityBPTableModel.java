package fr.dow.mapeditor.dialogs.selectentitylblueprint;


import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.filesystem.DowFile;
import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.gamedata.sgbmap.units.EntityBlueprints;

public class SelectEntityBPTableModel  extends AbstractTableModel {
	
	private static final String[] columnNames = { "Loaded", "Blueprint" };

	private Vector<LineData> tableData=new Vector<LineData>();
	
	public Class<? extends Object> getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }
	
	@Override
	public int getColumnCount() { return columnNames.length; }

	@Override
	public int getRowCount() {
		return tableData.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (tableData==null) return null;
		if (tableData.get(rowIndex)==null) return null;
		
		switch (columnIndex) {
			case 0 : return tableData.get(rowIndex).isLoaded;
			case 1 : return tableData.get(rowIndex).entityBlueprint.substring(5);
		}
		
		return null;
	}
	
	@Override
    public String getColumnName(int col) { return columnNames[col]; }

	public void setData(SgbMap map, DowMod mod) {
		tableData.clear();
		addData(mod.getFolder("attrib/ebps/environment"),map.entityBlueprints);
		fireTableDataChanged();
	}

	public String getBlueprint(int rowIndex) {
		return tableData.get(rowIndex).entityBlueprint;
	}
	
	public int findBlueprintRowIndex(String dowPath) {
		for (int i=0;i<tableData.size();i++) 
			if (dowPath.equals(tableData.get(i).entityBlueprint))
				return i;
		return -1;
	}
	
	private void addData(DowFolder dowFolder, EntityBlueprints entityBlueprints) {
		
		for (DowFolder subFolder : dowFolder.getSubFoldersList().values()) addData(subFolder,entityBlueprints); 
		
		for (DowFile subFile : dowFolder.getSubFilesList().values() ) {
			String decalPath="data:"+subFile.getDowPath().replace('/','\\');
			tableData.add(new LineData(entityBlueprints.contains(decalPath),decalPath));
		}
	}
	
	
	private class LineData {
		Boolean isLoaded;
		String entityBlueprint;
		public LineData(boolean isLoaded,String decalName) {
			this.isLoaded=isLoaded;
			this.entityBlueprint=decalName;
		}
	}
	
}
