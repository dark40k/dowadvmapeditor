package fr.dow.mapeditor.dialogs.selectentitylblueprint;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;

public abstract class SelectEntityBPBase extends JDialog {

	private static final long serialVersionUID = 1L;
	protected JPanel jContentPane = null;
	protected JPanel jPanelAction = null;
	protected JPanel jPanel9 = null;
	protected JButton jButtonApply = null;
	protected JPanel jPanelEntityPreview = null;
	/**
	 * @param owner
	 */
	public SelectEntityBPBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(1028, 663);
		this.setModal(true);
		this.setTitle("Select Entity Blueprint - Mod=");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanelAction(), BorderLayout.SOUTH);
			jContentPane.add(getJSplitPane(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanelAction	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelAction() {
		if (jPanelAction == null) {
			jPanelAction = new JPanel();
			jPanelAction.setLayout(new BorderLayout());
			jPanelAction.add(getJPanel9(), java.awt.BorderLayout.EAST);
			jPanelAction.add(getJPanelLeft(), BorderLayout.WEST);
		}
		return jPanelAction;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonApply(), null);
			jPanel9.add(getJButtonCancel(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonApply	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonApply() {
		if (jButtonApply == null) {
			jButtonApply = new JButton();
			jButtonApply.setPreferredSize(new Dimension(100, 23));
			jButtonApply.setText("Apply");
		}
		return jButtonApply;
	}

	/**
	 * This method initializes jPanelEntityPreview	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelEntityPreview() {
		if (jPanelEntityPreview == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Entity preview");
			jPanelEntityPreview = new JPanel();
			jPanelEntityPreview.setLayout(new BorderLayout());
			jPanelEntityPreview.setEnabled(true);
			jPanelEntityPreview.setBorder(titledBorder);
			jPanelEntityPreview.addComponentListener(new java.awt.event.ComponentAdapter() {
				public void componentResized(java.awt.event.ComponentEvent e) {
					jPanelEntityPreview.setMinimumSize(new Dimension(10,10));
				}
			});
		}
		return jPanelEntityPreview;
	}

	protected JPanel jPanelBPSelection = null;
	protected JButton jButtonCancel = null;
	protected JScrollPane jScrollPane = null;
	protected JSplitPane jSplitPane = null;
	protected JPanel jPanelLeft = null;
	protected JTree jTreeBlueprints = null;
	/**
	 * This method initializes jPanelBPSelection	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelBPSelection() {
		if (jPanelBPSelection == null) {
			TitledBorder titledBorder1112 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder1112.setTitle("Blueprint Selection");
			titledBorder1112.setBorder(null);
			jPanelBPSelection = new JPanel();
			jPanelBPSelection.setLayout(new BorderLayout());
			jPanelBPSelection.setPreferredSize(new Dimension(200, 453));
			jPanelBPSelection.setBorder(titledBorder1112);
			jPanelBPSelection.add(getJScrollPane(), BorderLayout.CENTER);
		}
		return jPanelBPSelection;
	}

	/**
	 * This method initializes jButtonCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonCancel() {
		if (jButtonCancel == null) {
			jButtonCancel = new JButton();
			jButtonCancel.setPreferredSize(new Dimension(100, 23));
			jButtonCancel.setText("Cancel");
		}
		return jButtonCancel;
	}

	/**
	 * This method initializes jScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getJTreeBlueprints());
		}
		return jScrollPane;
	}

	/**
	 * This method initializes jSplitPane	
	 * 	
	 * @return javax.swing.JSplitPane	
	 */
	private JSplitPane getJSplitPane() {
		if (jSplitPane == null) {
			jSplitPane = new JSplitPane();
			jSplitPane.setDividerLocation(400);
			jSplitPane.setContinuousLayout(true);
			jSplitPane.setLeftComponent(getJPanelBPSelection());
			jSplitPane.setRightComponent(getJPanelEntityPreview());
		}
		return jSplitPane;
	}

	/**
	 * This method initializes jPanelLeft	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelLeft() {
		if (jPanelLeft == null) {
			FlowLayout flowLayout71 = new FlowLayout();
			flowLayout71.setHgap(10);
			jPanelLeft = new JPanel();
			jPanelLeft.setLayout(flowLayout71);
		}
		return jPanelLeft;
	}

	/**
	 * This method initializes jTreeBlueprints	
	 * 	
	 * @return javax.swing.JTree	
	 */
	private JTree getJTreeBlueprints() {
		if (jTreeBlueprints == null) {
			jTreeBlueprints = new JTree();
		}
		return jTreeBlueprints;
	}
		
	//
	// End of custom part 
	//

}  //  @jve:decl-index=0:visual-constraint="10,10"
