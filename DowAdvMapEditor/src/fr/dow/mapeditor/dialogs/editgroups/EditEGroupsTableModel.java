package fr.dow.mapeditor.dialogs.editgroups;

import java.util.HashSet;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.sgbmap.units.Egroup;
import fr.dow.gamedata.sgbmap.units.Egroups;
import fr.dow.mapviewer.display.MapViewer;

public class EditEGroupsTableModel extends AbstractTableModel {

	private static final String[] columnNames = { "ID", "Count", "Name" };

	private MapViewer mapViewer;
	
	class EgroupData {
		Integer id;
		Integer count;
		String name;
	}
	
	Vector<EgroupData> egroupDataLines=new Vector<EgroupData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
    public Class<?> getColumnClass(int c) {
		switch (c) {
		case 0 : return Integer.class;
		case 1 : return Integer.class; 
		case 2 : return String.class; 
		}
		return null;
    }
    
    public String getColumnName(int col) { return columnNames[col]; }
    
	@Override
	public int getColumnCount() { return columnNames.length; }

	@Override
	public int getRowCount() { return egroupDataLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (egroupDataLines==null) return null;
		if (rowIndex>=egroupDataLines.size()) return null;
		
		switch (columnIndex) {
		case 0 : return egroupDataLines.get(rowIndex).id;
		case 1 : return egroupDataLines.get(rowIndex).count; 
		case 2 : return egroupDataLines.get(rowIndex).name; 
		}
		
		return null;
		
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex>=egroupDataLines.size()) return false;
		switch (columnIndex) {
			case 0 : return true;
			case 2 : return true;
			default : return false;
		}
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (rowIndex>=egroupDataLines.size()) return;
		EgroupData egroupData=egroupDataLines.get(rowIndex);
		switch (columnIndex) {
			case 0 : {
				int newID=(Integer)aValue;
				if (newID<=0) {
					JOptionPane.showMessageDialog(mapViewer, "Invalid ID, Cancelling", "Set Egroup ID", JOptionPane.ERROR_MESSAGE);
					return;
				}
				if (mapViewer.getMap().egroups.containsKey(newID)) {
					JOptionPane.showMessageDialog(mapViewer, "ID already exist, Cancelling", "Set Egroup ID", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				Egroup egroup=mapViewer.getMap().egroups.remove(egroupData.id);
				egroup.set_id(newID);
				mapViewer.getMap().egroups.put(egroup.get_id(),egroup);
				egroupData.id=newID;
				fireTableDataChanged();
				return;
			}
			case 2 : {
				String newName=(String)aValue;
				for (EgroupData tmpSgroupData : egroupDataLines) 
					if (tmpSgroupData.name.equals(newName)){
						JOptionPane.showMessageDialog(mapViewer, "Name already exist, Cancelling", "Set Egroup Name", JOptionPane.ERROR_MESSAGE);
						return;
					}
				egroupData.name=newName; 
				mapViewer.getMap().egroups.get(egroupData.id).set_name(egroupData.name);
				fireTableDataChanged();
				return;
			}
			default : return;
		}
		
		
	}
	
	public void updateEGroupTable(boolean onlySelected) {
		
		Vector<EgroupData> newDataLines=new Vector<EgroupData>();
		
		Egroups egroups=mapViewer.getMap().egroups;
		
		HashSet<Integer> entities=mapViewer.getRenderer().currentSelection.entities;
		
		for (Integer egroupID : egroups.keySet())	{
			
			Egroup egroup=egroups.get(egroupID);
			
			if (onlySelected)
				if (!egroup.containsAny(entities))
					continue;
						
			EgroupData egroupData=new EgroupData();
			egroupData.id=egroup.get_id();
			egroupData.name=egroup.get_name();
			egroupData.count=egroup.getNumberOfEntities();
			
			newDataLines.add(egroupData);
			
		}
		
		egroupDataLines=newDataLines;
		fireTableDataChanged();
		
	}
	
	public void setMapViewer(MapViewer mapViewer, boolean onlySelected) {
		this.mapViewer=mapViewer;
		updateEGroupTable(onlySelected);
	}

	public int findName(String selName) {
		for (int i=0;i<egroupDataLines.size();i++) 
			if (selName.equals(egroupDataLines.get(i).name)) 
				return i;
		return -1;
	}

}
