package fr.dow.mapeditor.dialogs.editgroups;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import javax.media.opengl.GL2;
import javax.swing.JOptionPane;

import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.gamedata.sgbmap.units.Sgroup;
import fr.dow.gamedata.sgbmap.units.Sgroups;
import fr.dow.gamedata.sgbmap.units.Squad;
import fr.dow.gamedata.sgbmap.units.Squads;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.joglobject.JoglWorld;
import fr.dow.mapviewer.display.renderer.MapViewerRenderer.DisplayEvent;
import fr.dow.mapviewer.display.renderer.MapViewerRenderer.DisplayListener;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeEvent;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeListener;;

public class EditSgroupsDialog extends EditSGroupsDialogBase implements ActionListener,SelectionChangeListener,DisplayListener {

	MapViewer mapViewer;
	
	EditSGroupsTableModel sgroupsTable;
	
	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		EditSgroupsDialog dialog = new EditSgroupsDialog(mapEditor, mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		mapViewer.getRenderer().curFilter.lockAllExcept(FilterType.ENTITY);
		dialog.setVisible(true);
		return;
	}	
	
	public void close() {
		mapViewer.getRenderer().curFilter.unlockAll();
		mapViewer.getRenderer().currentSelection.removeSelectionChangeListener(this);
		this.setVisible(false);
	}
	
	public EditSgroupsDialog(MapEditor mapEditor, MapViewer mapViewer) {
		super(mapEditor);
		
		this.mapViewer=mapViewer;
		
		sgroupsTable=(EditSGroupsTableModel)jTableMapList.getModel();
		sgroupsTable.setMapViewer(mapViewer,jCheckBoxMapSelectionFilter.isSelected());
		
		jTableMapList.setAutoCreateRowSorter(true);
		jTableMapList.getColumnModel().getColumn(0).setWidth(10);
		jTableMapList.getColumnModel().getColumn(1).setWidth(10);
		jTableMapList.getColumnModel().getColumn(2).setPreferredWidth(100);
		
		jTableMapList.getRowSorter().toggleSortOrder(0);
		
		jCheckBoxMapSelectionFilter.addActionListener(this);
		jCheckBoxMapSelectionHighLight.addActionListener(this);
		
		jButtonClose.addActionListener(this);
		
		jButtonMapSelAll.addActionListener(this);
		jButtonMapSelClear.addActionListener(this);
		jButtonMapSelFromTable.addActionListener(this);
		
		jButtonTableSelAll.addActionListener(this);
		jButtonTableSelClear.addActionListener(this);
		jButtonTableSelFromMap.addActionListener(this);
		
		jButtonGroupRegroup.addActionListener(this);
		jButtonGroupClear.addActionListener(this);
		jButtonGroupAdd.addActionListener(this);
		jButtonGroupRemove.addActionListener(this);
		jButtonGroupIntersect.addActionListener(this);
		
		jButtonNew.addActionListener(this);
		jButtonRenumber.addActionListener(this);
		jButtonMove.addActionListener(this);
		
		mapViewer.getRenderer().currentSelection.addSelectionChangeListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource()==jCheckBoxMapSelectionFilter) { updateTable(); return; }
		
		if (e.getSource()==jCheckBoxMapSelectionHighLight) { updateHightLight(); return; }
		
		if (e.getSource()==jButtonClose) { close(); return; }
		
		if (e.getSource()==jButtonMapSelAll) { mapSelAll(); return; }
		if (e.getSource()==jButtonMapSelClear) { mapSelClear(); return; }
		if (e.getSource()==jButtonMapSelFromTable) { tableSelToMap(); return; }
		
		if (e.getSource()==jButtonTableSelAll) { tableSelectAll(); return; }
		if (e.getSource()==jButtonTableSelClear){ tableSelClear(); return; }
		if (e.getSource()==jButtonTableSelFromMap) { tableSelFromMap(); return; }
		
		if (e.getSource()==jButtonGroupRegroup) { groupRegroup(); return; }
		if (e.getSource()==jButtonGroupClear) { groupClear(); return; }
		if (e.getSource()==jButtonGroupAdd) { groupAdd(); return; }
		if (e.getSource()==jButtonGroupRemove) { groupRemove(); return; }
		if (e.getSource()==jButtonGroupIntersect) { groupIntersect(); return; }
		
		if (e.getSource()==jButtonNew) { createNewGroup(); return; }
		if (e.getSource()==jButtonRenumber) { tableRenumber(); return; }
		if (e.getSource()==jButtonMove) { tableMove(); return; }
	}
	
	private void createNewGroup() {

		HashSet<Integer> selSquads = getMapSelSquadIDs();
		
		String newGroupName=JOptionPane.showInputDialog(mapViewer, "Enter new group Name", "Create Squad Group", JOptionPane.QUESTION_MESSAGE);
		if (newGroupName==null) return;
		if (newGroupName.isEmpty()) return;
	
		if (mapViewer.getMap().sgroups.getByName(newGroupName)!=null) {
			JOptionPane.showMessageDialog(mapViewer, "Name already exists, Cancelling", "Create Squad Group", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Sgroup sgroup=mapViewer.getMap().sgroups.newSgroup(newGroupName);
		sgroup.add(selSquads);
		
		sgroupsTable.updateSGroupTable(jCheckBoxMapSelectionFilter.isSelected());
		
	}

	private void updateHightLight() {
		if (jCheckBoxMapSelectionHighLight.isSelected()) {
			mapViewer.getRenderer().removeDisplayListener(this);
			mapViewer.getRenderer().addDisplayListener(this); 
		} else
			mapViewer.getRenderer().removeDisplayListener(this);
		
	}

	private void groupIntersect() {
		
		if (jTableMapList.getSelectedRowCount()==0) {
			JOptionPane.showMessageDialog(mapViewer, "No Sgroup are selected", "Sgroup Intersect", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		HashSet<Integer> selectedSquadIDs = getMapSelSquadIDs();
		
		if (selectedSquadIDs.isEmpty()) {
			JOptionPane.showMessageDialog(mapViewer, "No squads are selected", "Sgroup Intersect", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (jTableMapList.getSelectedRowCount()>1) {
			int rep=JOptionPane.showOptionDialog(mapViewer, "More than one Sgroup is selected.\n Are you sure you want to intersect map selection from several Sgroups?", "Sgroup Intersect", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);
			if (rep==JOptionPane.NO_OPTION) return;
		}
		
		
		Sgroups sgroups=mapViewer.getMap().sgroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			sgroups.get(sgroupsTable.sgroupDataLines.get(i).id).intersect(selectedSquadIDs);
		}	
		
		updateTable();
		
		
	}

	private void groupRemove() {
		
		if (jTableMapList.getSelectedRowCount()==0) {
			JOptionPane.showMessageDialog(mapViewer, "No Sgroup are selected", "Sgroup Remove", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		HashSet<Integer> selectedSquadIDs = getMapSelSquadIDs();
		
		if (selectedSquadIDs.isEmpty()) {
			JOptionPane.showMessageDialog(mapViewer, "No squads are selected", "Sgroup Remove", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (jTableMapList.getSelectedRowCount()>1) {
			int rep=JOptionPane.showOptionDialog(mapViewer, "More than one Sgroup is selected.\n Are you sure you want to remove map selection from several Sgroups?", "Sgroup Regroup", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);
			if (rep==JOptionPane.NO_OPTION) return;
		}
		
		Sgroups sgroups=mapViewer.getMap().sgroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			sgroups.get(sgroupsTable.sgroupDataLines.get(i).id).remove(selectedSquadIDs);
		}	
		
		updateTable();
		
		
	}

	private void groupAdd() {
		if (jTableMapList.getSelectedRowCount()==0) {
			JOptionPane.showMessageDialog(mapViewer, "No Sgroup are selected", "Sgroup Add", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		HashSet<Integer> selectedSquadIDs = getMapSelSquadIDs();
		
		if (selectedSquadIDs.isEmpty()) {
			JOptionPane.showMessageDialog(mapViewer, "No squads are selected", "Sgroup Add", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Sgroups sgroups=mapViewer.getMap().sgroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			sgroups.get(sgroupsTable.sgroupDataLines.get(i).id).add(selectedSquadIDs);
		}	
		
		updateTable();
		
	}

	private void groupClear() {
		
		if (jTableMapList.getSelectedRowCount()==0) {
			JOptionPane.showMessageDialog(mapViewer, "No Sgroup are selected", "Sgroup Clear", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Sgroups sgroups=mapViewer.getMap().sgroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			sgroups.get(sgroupsTable.sgroupDataLines.get(i).id).clear();
		}	
		
		updateTable();		
		
	}

	private void groupRegroup() {
		if (jTableMapList.getSelectedRowCount()==0) {
			JOptionPane.showMessageDialog(mapViewer, "No Sgroup are selected", "Sgroup Regroup", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		HashSet<Integer> selectedSquadIDs = getMapSelSquadIDs();
		
		if (selectedSquadIDs.isEmpty()) {
			JOptionPane.showMessageDialog(mapViewer, "No squads are selected", "Sgroup Regroup", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (jTableMapList.getSelectedRowCount()>1) {
			int rep=JOptionPane.showOptionDialog(mapViewer, "More than one Sgroup is selected.\n Are you sure you want to overwrite several Sgroups with same selection?", "Sgroup Regroup", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);
			if (rep==JOptionPane.NO_OPTION) return;
		}
		
		Sgroups sgroups=mapViewer.getMap().sgroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			sgroups.get(sgroupsTable.sgroupDataLines.get(i).id).clear();
			sgroups.get(sgroupsTable.sgroupDataLines.get(i).id).add(selectedSquadIDs);
		}	
		
		updateTable();
	}

	private void mapSelClear() {
		mapViewer.getRenderer().currentSelection.entities.clear();
		mapViewer.getRenderer().currentSelection.fireSelectionChangeEvent();
	}

	private void mapSelAll() {
		HashSet<Integer> selEntities = mapViewer.getRenderer().currentSelection.entities;
		selEntities.clear();
		for (Squad squad : mapViewer.getMap().squads.squadList.values())
			for (int entityID : squad.entitiesID)
				selEntities.add(entityID);
		
		mapViewer.getRenderer().currentSelection.fireSelectionChangeEvent();
	}

	private void tableRenumber() {
		
		int rep=JOptionPane.showOptionDialog(mapViewer, "Are you sure you want to renumber SGroups?", "Sgroup renumber", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);
		if (rep==JOptionPane.NO_OPTION) return;
		
		HashSet<String> tableSelSGroups = new HashSet<String>();

		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelSGroups.add(sgroupsTable.sgroupDataLines.get(i).name);
		}

		jTableMapList.clearSelection();

		mapViewer.getMap().sgroups.renumberSortedByName();
		
		sgroupsTable.updateSGroupTable(jCheckBoxMapSelectionFilter.isSelected());
		
		for (String selName : tableSelSGroups) {
			int tableRow=sgroupsTable.findName(selName);
			if (tableRow<0) continue;
			int row=jTableMapList.getRowSorter().convertRowIndexToView(tableRow);
			jTableMapList.addRowSelectionInterval(row, row);
		}
		
	}

	private void tableMove() {
		
		int startID;
		try { 
			startID=Integer.parseInt(
					JOptionPane.showInputDialog(mapViewer, "Enter new start ID\n In case of conflict existing IDs are pushed higher", "Move Selection", JOptionPane.QUESTION_MESSAGE)
					); 
		}
		catch (NumberFormatException e) { return; }
		if (startID<=0) return;
		
		HashSet<String> tableSelSGroups = new HashSet<String>();
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelSGroups.add(sgroupsTable.sgroupDataLines.get(i).name);
		}
		
		HashSet<Integer> movePack = getTableSelSgroupIDs();
		
		jTableMapList.clearSelection();
		
		mapViewer.getMap().sgroups.move(movePack,startID);
		
		
		sgroupsTable.updateSGroupTable(jCheckBoxMapSelectionFilter.isSelected());
		
		for (String selName : tableSelSGroups) {
			int tableRow=sgroupsTable.findName(selName);
			if (tableRow<0) continue;
			int row=jTableMapList.getRowSorter().convertRowIndexToView(tableRow);
			jTableMapList.addRowSelectionInterval(row, row);
		}
		
		
	}
	
	private void updateTable() {
		
		HashSet<Integer> tableSelSGroups = new HashSet<Integer>();

		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelSGroups.add(sgroupsTable.sgroupDataLines.get(i).id);
		}

		jTableMapList.clearSelection();
		
		sgroupsTable.updateSGroupTable(jCheckBoxMapSelectionFilter.isSelected());
		
		for (int i=0;i<sgroupsTable.sgroupDataLines.size();i++) 
			if (tableSelSGroups.contains(sgroupsTable.sgroupDataLines.get(i).id)){
				int row=jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void selectionChange(SelectionChangeEvent evt) {
		
		HashSet<Integer> mapSelEntities = mapViewer.getRenderer().currentSelection.entities;
		
		boolean requireSelUpdate=false;
		Squads squads=mapViewer.getMap().squads;
		
		// check that entities belong to a squad
		for (Integer entityID : (HashSet<Integer>) mapSelEntities.clone())
			if (squads.getSquadID(entityID)==null) {
				mapSelEntities.remove(entityID);
				requireSelUpdate=true;
			}
				
		// squad completion
		for (Integer squadID : getMapSelSquadIDs() )
			for (Integer entityID : squads.getSquad(squadID).getEntitiesID())
				if (!mapSelEntities.contains(entityID)) {
					mapSelEntities.add(entityID);
					requireSelUpdate=true;
				}
		
		if (requireSelUpdate) {
			mapViewer.getRenderer().currentSelection.removeSelectionChangeListener(this);
			mapViewer.getRenderer().currentSelection.fireSelectionChangeEvent();
			mapViewer.getRenderer().currentSelection.addSelectionChangeListener(this);
		}
		
		updateTable();
	}
	
	private void tableSelectAll() { 
		jTableMapList.selectAll(); 
	}
	
	private void tableSelClear() {
		jTableMapList.clearSelection(); 
	}

	private void tableSelFromMap() {
		
		Sgroups sgroups=mapViewer.getMap().sgroups;
		
		HashSet<Integer> selectedSquadIDs=getMapSelSquadIDs();
		
		jTableMapList.clearSelection();
		for (int i=0;i<sgroupsTable.sgroupDataLines.size();i++)
			if (sgroups.get(sgroupsTable.sgroupDataLines.get(i).id).contains(selectedSquadIDs)) {
				int row=jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}

	private void tableSelToMap() {
		
		HashSet<Integer> selectedEntities = mapViewer.getRenderer().currentSelection.entities;
		selectedEntities.clear();
		
		Squads squads=mapViewer.getMap().squads;
		for (int squadID : getTableSelSquadIDs())
			for (int entityID : squads.getSquad(squadID).getEntitiesID())
				selectedEntities.add(entityID);
		
		updateTable();
		
	}
	
	private HashSet<Integer> getMapSelSquadIDs() {
		HashSet<Integer> selectedSquadIDs=new HashSet<Integer>();
		Squads squads = mapViewer.getMap().squads;
		for (int entityID : mapViewer.getRenderer().currentSelection.entities) {
			Integer squadID=squads.getSquadID(entityID);
			if (squadID!=null) selectedSquadIDs.add(squadID);
		}
		return selectedSquadIDs;
	}
	
	private HashSet<Integer> getTableSelSquadIDs() {
		HashSet<Integer> selectedSquadIDs=new HashSet<Integer>();
		Sgroups sgroups=mapViewer.getMap().sgroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			for (int squadID : sgroups.get(sgroupsTable.sgroupDataLines.get(i).id).get_squadIDs())
				selectedSquadIDs.add(squadID);
		}
		return selectedSquadIDs;
	}
	
	private HashSet<Integer> getTableSelSgroupIDs() {
		HashSet<Integer> selectedEgroupIDs=new HashSet<Integer>();
		for (int row : jTableMapList.getSelectedRows())
			selectedEgroupIDs.add(
					sgroupsTable.sgroupDataLines.get(
							jTableMapList.getRowSorter().convertRowIndexToModel(row)
						).id);

		return selectedEgroupIDs;
	}

	@Override
	public void display(DisplayEvent evt) {
		GL2 gl=evt.gl;
		
		HashSet<Integer> selectedEntities = new HashSet<Integer>();
		Squads squads=mapViewer.getMap().squads;
		for (int squadID : getTableSelSquadIDs())
			for (int entityID : squads.getSquad(squadID).getEntitiesID())
				selectedEntities.add(entityID);
		
		Color color = colButtonHighLight.getColor();
		
		JoglWorld joglWorld = mapViewer.getRenderer().joglWorld;
		SgbMap map = mapViewer.getMap();
		
		gl.glColor4f(color.getRed()/255f, color.getGreen()/255f, color.getBlue()/255f,1.0f);
		for (Object id : selectedEntities) 
			joglWorld.joglMapEntities.drawPicked(gl,(Integer) id,map.entities,map.terrain.ground, 0.5f);
		
	}
	
}
