package fr.dow.mapeditor.dialogs.editgroups;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import javax.swing.JCheckBox;
import javax.swing.BoxLayout;


import java.awt.GridLayout;
import fr.dow.mapeditor.tools.ColorizedButton;

public abstract class EditSGroupsDialogBase extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanelAction = null;
	private JPanel jPanel9 = null;
	protected JButton jButtonClose = null;
	protected JPanel jPanelMapList = null;
	/**
	 * @param owner
	 */
	public EditSGroupsDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(511, 605);
		this.setModal(false);
		this.setTitle("Edit Squad Groups");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanelAction(), BorderLayout.SOUTH);
			jContentPane.add(getJPanelMapList(), BorderLayout.CENTER);
			jContentPane.add(getJPanel(), BorderLayout.EAST);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanelAction	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelAction() {
		if (jPanelAction == null) {
			jPanelAction = new JPanel();
			jPanelAction.setLayout(new BorderLayout());
			jPanelAction.add(getJPanel9(), java.awt.BorderLayout.EAST);
			jPanelAction.add(getJPanel2(), BorderLayout.WEST);
		}
		return jPanelAction;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonClose(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonClose	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonClose() {
		if (jButtonClose == null) {
			jButtonClose = new JButton();
			jButtonClose.setPreferredSize(new Dimension(100, 23));
			jButtonClose.setText("Close");
		}
		return jButtonClose;
	}

	/**
	 * This method initializes jPanelMapList	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMapList() {
		if (jPanelMapList == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Sgroups Table");
			jPanelMapList = new JPanel();
			jPanelMapList.setLayout(new BorderLayout());
			jPanelMapList.setEnabled(true);
			jPanelMapList.setBorder(titledBorder);
			jPanelMapList.add(getJScrollPaneMapList(), BorderLayout.CENTER);
			jPanelMapList.add(getJPanel3(), BorderLayout.NORTH);
		}
		return jPanelMapList;
	}

	protected JScrollPane jScrollPaneMapList = null;
	protected JTable jTableMapList = null;
	protected JCheckBox jCheckBoxMapSelectionFilter = null;
	private JPanel jPanel = null;
	private JPanel jPanel1 = null;
	private JPanel jPanelMod112 = null;
	protected JButton jButtonTableSelClear = null;
	protected JButton jButtonTableSelAll = null;
	protected JButton jButtonTableSelFromMap = null;
	private JPanel jPanelMod1121 = null;
	private JPanel jPanelMod1122 = null;
	protected JButton jButtonMapSelAll = null;
	protected JButton jButtonMapSelClear = null;
	protected JButton jButtonMapSelFromTable = null;
	protected JButton jButtonGroupAdd = null;
	protected JButton jButtonGroupRemove = null;
	protected JButton jButtonGroupIntersect = null;
	private JPanel jPanelMod11211 = null;
	protected JButton jButtonRenumber = null;
	protected JButton jButtonGroupRegroup = null;
	protected JButton jButtonGroupClear = null;
	private JPanel jPanel2 = null;
	protected ColorizedButton colButtonHighLight = null;
	protected JCheckBox jCheckBoxMapSelectionHighLight = null;
	private JPanel jPanel3 = null;
	protected JButton jButtonMove = null;
	protected JButton jButtonNew = null;
	/**
	 * This method initializes jScrollPaneMapList	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPaneMapList() {
		if (jScrollPaneMapList == null) {
			jScrollPaneMapList = new JScrollPane();
			jScrollPaneMapList.setName("jScrollPaneMapList");
			jScrollPaneMapList.setViewportView(getJTableMapList());
		}
		return jScrollPaneMapList;
	}

	/**
	 * This method initializes jTableMapList	
	 * 	
	 * @return javax.swing.JTable	
	 */
	private JTable getJTableMapList() {
		if (jTableMapList == null) {
			jTableMapList = new JTable();
			jTableMapList.setAutoCreateColumnsFromModel(true);
			jTableMapList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			jTableMapList.setModel(new EditSGroupsTableModel());
			jTableMapList.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
			jTableMapList.setShowGrid(true);
		}
		return jTableMapList;
	}

	/**
	 * This method initializes jCheckBoxMapSelectionFilter	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxMapSelectionFilter() {
		if (jCheckBoxMapSelectionFilter == null) {
			jCheckBoxMapSelectionFilter = new JCheckBox();
			jCheckBoxMapSelectionFilter.setName("jCheckBox");
			jCheckBoxMapSelectionFilter.setSelected(false);
			jCheckBoxMapSelectionFilter.setText("Show only SGroups with entities selected in map");
		}
		return jCheckBoxMapSelectionFilter;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(new BoxLayout(getJPanel(), BoxLayout.Y_AXIS));
			jPanel.setPreferredSize(new Dimension(150, 63));
			jPanel.add(getJPanelMod1122(), null);
			jPanel.add(getJPanelMod112(), null);
			jPanel.add(getJPanelMod11211(), null);
			jPanel.add(getJPanelMod1121(), null);
			jPanel.add(getJPanel1(), null);
		}
		return jPanel;
	}

	/**
	 * This method initializes jPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jPanel1 = new JPanel();
			jPanel1.setLayout(new BorderLayout());
		}
		return jPanel1;
	}

	/**
	 * This method initializes jPanelMod112	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMod112() {
		if (jPanelMod112 == null) {
			TitledBorder titledBorder1112 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder1112.setTitle("Table Sgroup Selection");
			titledBorder1112.setBorder(null);
			GridLayout gridLayout12 = new GridLayout();
			gridLayout12.setRows(3);
			gridLayout12.setHgap(5);
			gridLayout12.setVgap(5);
			gridLayout12.setColumns(1);
			jPanelMod112 = new JPanel();
			jPanelMod112.setLayout(gridLayout12);
			jPanelMod112.setName("jPanelMod1");
			jPanelMod112.setBorder(titledBorder1112);
			jPanelMod112.add(getJButtonTableSelAll(), null);
			jPanelMod112.add(getJButtonTableSelClear(), null);
			jPanelMod112.add(getJButtonTableSelFromMap(), null);
		}
		return jPanelMod112;
	}

	/**
	 * This method initializes jButtonTableSelClear	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonTableSelClear() {
		if (jButtonTableSelClear == null) {
			jButtonTableSelClear = new JButton();
			jButtonTableSelClear.setText("Clear Selection");
		}
		return jButtonTableSelClear;
	}

	/**
	 * This method initializes jButtonTableSelAll	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonTableSelAll() {
		if (jButtonTableSelAll == null) {
			jButtonTableSelAll = new JButton();
			jButtonTableSelAll.setText("Select All Sgroups");
		}
		return jButtonTableSelAll;
	}

	/**
	 * This method initializes jButtonTableSelFromMap	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonTableSelFromMap() {
		if (jButtonTableSelFromMap == null) {
			jButtonTableSelFromMap = new JButton();
			jButtonTableSelFromMap.setText("Select from map");
		}
		return jButtonTableSelFromMap;
	}

	/**
	 * This method initializes jPanelMod1121	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMod1121() {
		if (jPanelMod1121 == null) {
			TitledBorder titledBorder11121 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder11121.setTitle("Sgroup Content");
			titledBorder11121.setBorder(null);
			GridLayout gridLayout121 = new GridLayout();
			gridLayout121.setRows(5);
			gridLayout121.setHgap(5);
			gridLayout121.setVgap(5);
			gridLayout121.setColumns(1);
			jPanelMod1121 = new JPanel();
			jPanelMod1121.setLayout(gridLayout121);
			jPanelMod1121.setName("jPanelMod1");
			jPanelMod1121.setBorder(titledBorder11121);
			jPanelMod1121.add(getJButtonGroupRegroup(), null);
			jPanelMod1121.add(getJButtonGroupClear(), null);
			jPanelMod1121.add(getJButtonGroupAdd(), null);
			jPanelMod1121.add(getJButtonGroupRemove(), null);
			jPanelMod1121.add(getJButtonGroupIntersect(), null);
		}
		return jPanelMod1121;
	}

	/**
	 * This method initializes jPanelMod1122	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMod1122() {
		if (jPanelMod1122 == null) {
			TitledBorder titledBorder11122 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder11122.setTitle("Map Squads Selection");
			titledBorder11122.setBorder(null);
			GridLayout gridLayout122 = new GridLayout();
			gridLayout122.setRows(3);
			gridLayout122.setHgap(5);
			gridLayout122.setVgap(5);
			gridLayout122.setColumns(1);
			jPanelMod1122 = new JPanel();
			jPanelMod1122.setLayout(gridLayout122);
			jPanelMod1122.setName("jPanelMod1");
			jPanelMod1122.setBorder(titledBorder11122);
			jPanelMod1122.add(getJButtonMapSelAll(), null);
			jPanelMod1122.add(getJButtonMapSelClear(), null);
			jPanelMod1122.add(getJButtonMapSelFromTable(), null);
		}
		return jPanelMod1122;
	}

	/**
	 * This method initializes jButtonMapSelAll	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonMapSelAll() {
		if (jButtonMapSelAll == null) {
			jButtonMapSelAll = new JButton();
			jButtonMapSelAll.setText("Select All Squads");
		}
		return jButtonMapSelAll;
	}

	/**
	 * This method initializes jButtonMapSelClear	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonMapSelClear() {
		if (jButtonMapSelClear == null) {
			jButtonMapSelClear = new JButton();
			jButtonMapSelClear.setText("Clear Selection");
		}
		return jButtonMapSelClear;
	}

	/**
	 * This method initializes jButtonMapSelFromTable	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonMapSelFromTable() {
		if (jButtonMapSelFromTable == null) {
			jButtonMapSelFromTable = new JButton();
			jButtonMapSelFromTable.setText("Select from Table");
		}
		return jButtonMapSelFromTable;
	}

	/**
	 * This method initializes jButtonGroupAdd	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonGroupAdd() {
		if (jButtonGroupAdd == null) {
			jButtonGroupAdd = new JButton();
			jButtonGroupAdd.setText("Add");
		}
		return jButtonGroupAdd;
	}

	/**
	 * This method initializes jButtonGroupRemove	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonGroupRemove() {
		if (jButtonGroupRemove == null) {
			jButtonGroupRemove = new JButton();
			jButtonGroupRemove.setText("Remove");
		}
		return jButtonGroupRemove;
	}

	/**
	 * This method initializes jButtonGroupIntersect	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonGroupIntersect() {
		if (jButtonGroupIntersect == null) {
			jButtonGroupIntersect = new JButton();
			jButtonGroupIntersect.setText("Intersect");
		}
		return jButtonGroupIntersect;
	}

	/**
	 * This method initializes jPanelMod11211	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMod11211() {
		if (jPanelMod11211 == null) {
			TitledBorder titledBorder111211 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder111211.setTitle("Sgroup");
			titledBorder111211.setBorder(null);
			GridLayout gridLayout1211 = new GridLayout();
			gridLayout1211.setRows(3);
			gridLayout1211.setHgap(5);
			gridLayout1211.setVgap(5);
			gridLayout1211.setColumns(1);
			jPanelMod11211 = new JPanel();
			jPanelMod11211.setLayout(gridLayout1211);
			jPanelMod11211.setName("jPanelMod1");
			jPanelMod11211.setBorder(titledBorder111211);
			jPanelMod11211.add(getJButtonNew(), null);
			jPanelMod11211.add(getJButtonRenumber(), null);
			jPanelMod11211.add(getJButtonMove(), null);
		}
		return jPanelMod11211;
	}

	/**
	 * This method initializes jButtonRenumber	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonRenumber() {
		if (jButtonRenumber == null) {
			jButtonRenumber = new JButton();
			jButtonRenumber.setText("Name Sort All IDs");
		}
		return jButtonRenumber;
	}

	/**
	 * This method initializes jButtonGroupRegroup	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonGroupRegroup() {
		if (jButtonGroupRegroup == null) {
			jButtonGroupRegroup = new JButton();
			jButtonGroupRegroup.setText("Regroup");
		}
		return jButtonGroupRegroup;
	}

	/**
	 * This method initializes jButtonGroupClear	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonGroupClear() {
		if (jButtonGroupClear == null) {
			jButtonGroupClear = new JButton();
			jButtonGroupClear.setText("Clear");
		}
		return jButtonGroupClear;
	}

	/**
	 * This method initializes jPanel2	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel2() {
		if (jPanel2 == null) {
			jPanel2 = new JPanel();
			jPanel2.setLayout(new FlowLayout());
			jPanel2.add(getColButtonHighLight(), null);
		}
		return jPanel2;
	}

	/**
	 * This method initializes colButtonHighLight	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonHighLight() {
		if (colButtonHighLight == null) {
			colButtonHighLight = new ColorizedButton();
			colButtonHighLight.setPreferredSize(new Dimension(100, 25));
			colButtonHighLight.setColor(Color.cyan);
			colButtonHighLight.setText("HighLight");
		}
		return colButtonHighLight;
	}

	/**
	 * This method initializes jCheckBoxMapSelectionHighLight	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxMapSelectionHighLight() {
		if (jCheckBoxMapSelectionHighLight == null) {
			jCheckBoxMapSelectionHighLight = new JCheckBox();
			jCheckBoxMapSelectionHighLight.setName("jCheckBox");
			jCheckBoxMapSelectionHighLight.setText("Highlight Sgroups in map");
			jCheckBoxMapSelectionHighLight.setSelected(false);
		}
		return jCheckBoxMapSelectionHighLight;
	}

	/**
	 * This method initializes jPanel3	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel3() {
		if (jPanel3 == null) {
			jPanel3 = new JPanel();
			jPanel3.setLayout(new BoxLayout(getJPanel3(), BoxLayout.Y_AXIS));
			jPanel3.add(getJCheckBoxMapSelectionHighLight(), null);
			jPanel3.add(getJCheckBoxMapSelectionFilter(), null);
		}
		return jPanel3;
	}

	/**
	 * This method initializes jButtonMove	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonMove() {
		if (jButtonMove == null) {
			jButtonMove = new JButton();
			jButtonMove.setText("Move IDs + Sort");
		}
		return jButtonMove;
	}

	/**
	 * This method initializes jButtonNew	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonNew() {
		if (jButtonNew == null) {
			jButtonNew = new JButton();
			jButtonNew.setText("Create New");
		}
		return jButtonNew;
	}
		
	//
	// End of custom part 
	//

}  //  @jve:decl-index=0:visual-constraint="10,10"
