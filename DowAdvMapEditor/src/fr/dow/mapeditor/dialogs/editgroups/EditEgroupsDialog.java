package fr.dow.mapeditor.dialogs.editgroups;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import javax.media.opengl.GL2;
import javax.swing.JOptionPane;

import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.gamedata.sgbmap.units.Egroup;
import fr.dow.gamedata.sgbmap.units.Egroups;
import fr.dow.gamedata.sgbmap.units.Squads;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.joglobject.JoglWorld;
import fr.dow.mapviewer.display.renderer.MapViewerRenderer.DisplayEvent;
import fr.dow.mapviewer.display.renderer.MapViewerRenderer.DisplayListener;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeEvent;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeListener;;

public class EditEgroupsDialog extends EditEGroupsDialogBase implements ActionListener,SelectionChangeListener,DisplayListener {

	MapViewer mapViewer;
	
	EditEGroupsTableModel egroupsTable;
	
	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		EditEgroupsDialog dialog = new EditEgroupsDialog(mapEditor, mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		mapViewer.getRenderer().curFilter.lockAllExcept(FilterType.ENTITY);
		dialog.setVisible(true);
		return;
	}	
	
	public void close() {
		mapViewer.getRenderer().curFilter.unlockAll();
		mapViewer.getRenderer().currentSelection.removeSelectionChangeListener(this);
		this.setVisible(false);
	}
	
	public EditEgroupsDialog(MapEditor mapEditor, MapViewer mapViewer) {
		super(mapEditor);
		
		this.mapViewer=mapViewer;
		
		egroupsTable=(EditEGroupsTableModel)jTableMapList.getModel();
		egroupsTable.setMapViewer(mapViewer,jCheckBoxMapSelectionFilter.isSelected());
		
		jTableMapList.setAutoCreateRowSorter(true);
		jTableMapList.getColumnModel().getColumn(0).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(1).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(2).setPreferredWidth(100);
		
		jTableMapList.getRowSorter().toggleSortOrder(0);
		
		jCheckBoxMapSelectionFilter.addActionListener(this);
		jCheckBoxMapSelectionHighLight.addActionListener(this);
		
		jButtonClose.addActionListener(this);
		
		jButtonMapSelAll.addActionListener(this);
		jButtonMapSelClear.addActionListener(this);
		jButtonMapSelFromTable.addActionListener(this);
		
		jButtonTableSelAll.addActionListener(this);
		jButtonTableSelClear.addActionListener(this);
		jButtonTableSelFromMap.addActionListener(this);
		
		jButtonGroupRegroup.addActionListener(this);
		jButtonGroupClear.addActionListener(this);
		jButtonGroupAdd.addActionListener(this);
		jButtonGroupRemove.addActionListener(this);
		jButtonGroupIntersect.addActionListener(this);
		
		jButtonNew.addActionListener(this);
		jButtonRenumber.addActionListener(this);
		jButtonMove.addActionListener(this);
		
		mapViewer.getRenderer().currentSelection.addSelectionChangeListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource()==jCheckBoxMapSelectionFilter) { updateTable(); return; }
		
		if (e.getSource()==jCheckBoxMapSelectionHighLight) { updateHightLight(); return; }
		
		if (e.getSource()==jButtonClose) { close(); return; }
		
		if (e.getSource()==jButtonMapSelAll) { mapSelAll(); return; }
		if (e.getSource()==jButtonMapSelClear) { mapSelClear(); return; }
		if (e.getSource()==jButtonMapSelFromTable) { tableSelToMap(); return; }
		
		if (e.getSource()==jButtonTableSelAll) { tableSelectAll(); return; }
		if (e.getSource()==jButtonTableSelClear){ tableSelClear(); return; }
		if (e.getSource()==jButtonTableSelFromMap) { tableSelFromMap(); return; }
		
		if (e.getSource()==jButtonGroupRegroup) { groupRegroup(); return; }
		if (e.getSource()==jButtonGroupClear) { groupClear(); return; }
		if (e.getSource()==jButtonGroupAdd) { groupAdd(); return; }
		if (e.getSource()==jButtonGroupRemove) { groupRemove(); return; }
		if (e.getSource()==jButtonGroupIntersect) { groupIntersect(); return; }
		
		if (e.getSource()==jButtonNew) { createNewGroup(); return; }
		if (e.getSource()==jButtonRenumber) { tableRenumber(); return; }
		if (e.getSource()==jButtonMove) { tableMove(); return; }
	}
	
	private void createNewGroup() {

		HashSet<Integer> selEntities = mapViewer.getRenderer().currentSelection.entities;
		
		String newGroupName=JOptionPane.showInputDialog(mapViewer, "Enter new group Name", "Create Entity Group", JOptionPane.QUESTION_MESSAGE);
		if (newGroupName==null) return;
		if (newGroupName.isEmpty()) return;
	
		if (mapViewer.getMap().egroups.getByName(newGroupName)!=null) {
			JOptionPane.showMessageDialog(mapViewer, "Name already exists, Cancelling", "Create Entity Group", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Egroup egroup=mapViewer.getMap().egroups.newEgroup(newGroupName);
		egroup.add(selEntities);
		
		egroupsTable.updateEGroupTable(jCheckBoxMapSelectionFilter.isSelected());
		
	}
	
	private void updateHightLight() {
		if (jCheckBoxMapSelectionHighLight.isSelected()) {
			mapViewer.getRenderer().removeDisplayListener(this);
			mapViewer.getRenderer().addDisplayListener(this); 
		} else
			mapViewer.getRenderer().removeDisplayListener(this);
		
	}

	private void groupIntersect() {
		
		if (jTableMapList.getSelectedRowCount()==0) {
			JOptionPane.showMessageDialog(mapViewer, "No Egroup selected", "Egroup Intersect", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		HashSet<Integer> selectedEntitiesIDs = mapViewer.getRenderer().currentSelection.entities;
		
		if (selectedEntitiesIDs.isEmpty()) {
			JOptionPane.showMessageDialog(mapViewer, "No entities are selected", "Egroup Intersect", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (jTableMapList.getSelectedRowCount()>1) {
			int rep=JOptionPane.showOptionDialog(mapViewer, "More than one Egroup is selected.\n Are you sure you want to intersect map selection from several Egroups?", "Egroup Intersect", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);
			if (rep==JOptionPane.NO_OPTION) return;
		}
		
		
		Egroups egroups=mapViewer.getMap().egroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			egroups.get(egroupsTable.egroupDataLines.get(i).id).intersect(selectedEntitiesIDs);
		}	
		
		updateTable();
		
		
	}

	private void groupRemove() {
		
		if (jTableMapList.getSelectedRowCount()==0) {
			JOptionPane.showMessageDialog(mapViewer, "No Egroup are selected", "Egroup Remove", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		HashSet<Integer> selectedSquadIDs = mapViewer.getRenderer().currentSelection.entities;
		
		if (selectedSquadIDs.isEmpty()) {
			JOptionPane.showMessageDialog(mapViewer, "No squads are selected", "Egroup Remove", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (jTableMapList.getSelectedRowCount()>1) {
			int rep=JOptionPane.showOptionDialog(mapViewer, "More than one Egroup is selected.\n Are you sure you want to remove map selection from several Egroups?", "Egroup Regroup", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);
			if (rep==JOptionPane.NO_OPTION) return;
		}
		
		Egroups egroups=mapViewer.getMap().egroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			egroups.get(egroupsTable.egroupDataLines.get(i).id).remove(selectedSquadIDs);
		}	
		
		updateTable();
		
		
	}

	private void groupAdd() {
		if (jTableMapList.getSelectedRowCount()==0) {
			JOptionPane.showMessageDialog(mapViewer, "No Egroup are selected", "Egroup Add", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		HashSet<Integer> selectedEntityIDs = mapViewer.getRenderer().currentSelection.entities;
		
		if (selectedEntityIDs.isEmpty()) {
			JOptionPane.showMessageDialog(mapViewer, "No squads are selected", "Egroup Add", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Egroups egroups=mapViewer.getMap().egroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			egroups.get(egroupsTable.egroupDataLines.get(i).id).add(selectedEntityIDs);
		}	
		
		updateTable();
		
	}

	private void groupClear() {
		
		if (jTableMapList.getSelectedRowCount()==0) {
			JOptionPane.showMessageDialog(mapViewer, "No Egroup are selected", "Egroup Clear", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Egroups egroups=mapViewer.getMap().egroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			egroups.get(egroupsTable.egroupDataLines.get(i).id).clear();
		}	
		
		updateTable();		
		
	}

	private void groupRegroup() {
		if (jTableMapList.getSelectedRowCount()==0) {
			JOptionPane.showMessageDialog(mapViewer, "No Egroup are selected", "Egroup Regroup", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		HashSet<Integer> selectedEntityIDs = mapViewer.getRenderer().currentSelection.entities;
				
		if (selectedEntityIDs.isEmpty()) {
			JOptionPane.showMessageDialog(mapViewer, "No squads are selected", "Egroup Regroup", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (jTableMapList.getSelectedRowCount()>1) {
			int rep=JOptionPane.showOptionDialog(mapViewer, "More than one Egroup is selected.\n Are you sure you want to overwrite several Egroups with same selection?", "Egroup Regroup", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);
			if (rep==JOptionPane.NO_OPTION) return;
		}
		
		Egroups egroups=mapViewer.getMap().egroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			egroups.get(egroupsTable.egroupDataLines.get(i).id).clear();
			egroups.get(egroupsTable.egroupDataLines.get(i).id).add(selectedEntityIDs);
		}	
		
		updateTable();
	}

	private void mapSelClear() {
		mapViewer.getRenderer().currentSelection.entities.clear();
		mapViewer.getRenderer().currentSelection.fireSelectionChangeEvent();
	}

	private void mapSelAll() {
		HashSet<Integer> selEntities = mapViewer.getRenderer().currentSelection.entities;
		Squads squads=mapViewer.getMap().squads;
		selEntities.clear();
		for (int entityID : mapViewer.getMap().entities.keySet())
			if (squads.getSquadID(entityID)==null)
				selEntities.add(entityID);
		mapViewer.getRenderer().currentSelection.fireSelectionChangeEvent();
	}

	private void tableRenumber() {
		
		int rep=JOptionPane.showOptionDialog(mapViewer, "Are you sure you want to renumber Egroups?", "Egroup renumber", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);
		if (rep==JOptionPane.NO_OPTION) return;
		
		HashSet<String> tableSelEGroups = new HashSet<String>();

		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelEGroups.add(egroupsTable.egroupDataLines.get(i).name);
		}

		jTableMapList.clearSelection();

		mapViewer.getMap().egroups.renumberSortedByName();
		
		egroupsTable.updateEGroupTable(jCheckBoxMapSelectionFilter.isSelected());
		
		for (String selName : tableSelEGroups) {
			int tableRow=egroupsTable.findName(selName);
			if (tableRow<0) continue;
			int row=jTableMapList.getRowSorter().convertRowIndexToView(tableRow);
			jTableMapList.addRowSelectionInterval(row, row);
		}
		
	}

	private void tableMove() {
		
		int startID;
		try { 
			startID=Integer.parseInt(
					JOptionPane.showInputDialog(mapViewer, "Enter new start ID\n In case of conflict existing IDs are pushed higher", "Move Selection", JOptionPane.QUESTION_MESSAGE)
					); 
		}
		catch (NumberFormatException e) { return; }
		if (startID<=0) return;
		
		HashSet<String> tableSelEGroups = new HashSet<String>();
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelEGroups.add(egroupsTable.egroupDataLines.get(i).name);
		}
		
		HashSet<Integer> movePack = getTableSelEgroupIDs();
		
		jTableMapList.clearSelection();
		
		mapViewer.getMap().egroups.move(movePack,startID);
		
		
		egroupsTable.updateEGroupTable(jCheckBoxMapSelectionFilter.isSelected());
		
		for (String selName : tableSelEGroups) {
			int tableRow=egroupsTable.findName(selName);
			if (tableRow<0) continue;
			int row=jTableMapList.getRowSorter().convertRowIndexToView(tableRow);
			jTableMapList.addRowSelectionInterval(row, row);
		}
		
		
	}
	
	private void updateTable() {
		
		HashSet<Integer> tableSelEGroups = new HashSet<Integer>();

		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelEGroups.add(egroupsTable.egroupDataLines.get(i).id);
		}

		jTableMapList.clearSelection();
		
		egroupsTable.updateEGroupTable(jCheckBoxMapSelectionFilter.isSelected());
		
		for (int i=0;i<egroupsTable.egroupDataLines.size();i++) 
			if (tableSelEGroups.contains(egroupsTable.egroupDataLines.get(i).id)){
				int row=jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void selectionChange(SelectionChangeEvent evt) {
		
		HashSet<Integer> mapSelEntities = mapViewer.getRenderer().currentSelection.entities;
		
		boolean requireSelUpdate=false;
		Squads squads=mapViewer.getMap().squads;
		
		// check that entities do not belong to a squad
		for (Integer entityID : (HashSet<Integer>) mapSelEntities.clone())
			if (squads.getSquadID(entityID)!=null) {
				mapSelEntities.remove(entityID);
				requireSelUpdate=true;
			}
						
		if (requireSelUpdate) {
			mapViewer.getRenderer().currentSelection.removeSelectionChangeListener(this);
			mapViewer.getRenderer().currentSelection.fireSelectionChangeEvent();
			mapViewer.getRenderer().currentSelection.addSelectionChangeListener(this);
		}
		
		updateTable();
	}
	
	private void tableSelectAll() { 
		jTableMapList.selectAll(); 
	}
	
	private void tableSelClear() {
		jTableMapList.clearSelection(); 
	}

	private void tableSelFromMap() {
		
		Egroups egroups=mapViewer.getMap().egroups;
		
		HashSet<Integer> selectedEntityIDs=mapViewer.getRenderer().currentSelection.entities;
		
		jTableMapList.clearSelection();
		for (int i=0;i<egroupsTable.egroupDataLines.size();i++)
			if (egroups.get(egroupsTable.egroupDataLines.get(i).id).containsAny(selectedEntityIDs)) {
				int row=jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}

	private void tableSelToMap() {
		HashSet<Integer> selectedEntities = mapViewer.getRenderer().currentSelection.entities;
		selectedEntities.clear();
		for (int entityID : getTableSelEntityIDs()) selectedEntities.add(entityID);
		updateTable();
	}
	
	private HashSet<Integer> getTableSelEntityIDs() {
		HashSet<Integer> selectedEntityIDs=new HashSet<Integer>();
		Egroups egroups=mapViewer.getMap().egroups;
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			for (int entityID : egroups.get(egroupsTable.egroupDataLines.get(i).id).get_EntityIDs())
				selectedEntityIDs.add(entityID);
		}
		return selectedEntityIDs;
	}

	private HashSet<Integer> getTableSelEgroupIDs() {
		HashSet<Integer> selectedEgroupIDs=new HashSet<Integer>();
		for (int row : jTableMapList.getSelectedRows())
			selectedEgroupIDs.add(
					egroupsTable.egroupDataLines.get(
							jTableMapList.getRowSorter().convertRowIndexToModel(row)
						).id);

		return selectedEgroupIDs;
	}

	@Override
	public void display(DisplayEvent evt) {
		GL2 gl=evt.gl;
		
		Color color = colButtonHighLight.getColor();
		
		JoglWorld joglWorld = mapViewer.getRenderer().joglWorld;
		SgbMap map = mapViewer.getMap();
		
		gl.glColor4f(color.getRed()/255f, color.getGreen()/255f, color.getBlue()/255f,1.0f);
		for (Integer id : getTableSelEntityIDs()) 
			joglWorld.joglMapEntities.drawPicked(gl,id,map.entities,map.terrain.ground, 0.5f);
		
	}
	
}
