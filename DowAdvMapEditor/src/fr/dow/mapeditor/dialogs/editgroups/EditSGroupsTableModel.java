package fr.dow.mapeditor.dialogs.editgroups;

import java.util.HashSet;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.sgbmap.units.Sgroup;
import fr.dow.gamedata.sgbmap.units.Sgroups;
import fr.dow.mapviewer.display.MapViewer;

public class EditSGroupsTableModel extends AbstractTableModel {

	private static final String[] columnNames = { "ID", "Count", "Name" };

	private MapViewer mapViewer;
	
	class SgroupData {
		Integer id;
		Integer count;
		String name;
	}
	
	Vector<SgroupData> sgroupDataLines=new Vector<SgroupData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
	public Class<?> getColumnClass(int c) {
		switch (c) {
		case 0 : return Integer.class;
		case 1 : return Integer.class; 
		case 2 : return String.class; 
		}
		return null;
    }
    
    public String getColumnName(int col) { return columnNames[col]; }
    
	@Override
	public int getColumnCount() { return columnNames.length; }

	@Override
	public int getRowCount() { return sgroupDataLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (sgroupDataLines==null) return null;
		if (rowIndex>=sgroupDataLines.size()) return null;
		
		switch (columnIndex) {
		case 0 : return sgroupDataLines.get(rowIndex).id;
		case 1 : return sgroupDataLines.get(rowIndex).count; 
		case 2 : return sgroupDataLines.get(rowIndex).name; 
		}
		
		return null;
		
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex>=sgroupDataLines.size()) return false;
		switch (columnIndex) {
			case 0 : return true;
			case 2 : return true;
			default : return false;
		}
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (rowIndex>=sgroupDataLines.size()) return;
		SgroupData sgroupData=sgroupDataLines.get(rowIndex);
		switch (columnIndex) {
			case 0 : {
				int newID=(Integer)aValue;
				if (newID<=0) {
					JOptionPane.showMessageDialog(mapViewer, "Invalid ID, Cancelling", "Set Sgroup ID", JOptionPane.ERROR_MESSAGE);
					return;
				}
				if (mapViewer.getMap().sgroups.containsKey(newID)) {
					JOptionPane.showMessageDialog(mapViewer, "ID already exist, Cancelling", "Set Sgroup ID", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				Sgroup sgroup=mapViewer.getMap().sgroups.remove(sgroupData.id);
				sgroup.set_id(newID);
				mapViewer.getMap().sgroups.put(sgroup.get_id(),sgroup);
				sgroupData.id=newID;
				fireTableDataChanged();
				return;
			}
			case 2 : {
				String newName=(String)aValue;
				for (Sgroup sgroup : mapViewer.getMap().sgroups.values()) 
					if (sgroup.get_name().equals(newName)) {
						JOptionPane.showMessageDialog(mapViewer, "Name already exist, Cancelling", "Set Sgroup Name", JOptionPane.ERROR_MESSAGE);
						return;
					}
				sgroupData.name=newName; 
				mapViewer.getMap().sgroups.get(sgroupData.id).set_name(sgroupData.name);
				fireTableDataChanged();
				return;
			}
			default : return;
		}
		
		
	}
	
	public void updateSGroupTable(boolean onlySelected) {
		
		Vector<SgroupData> newDataLines=new Vector<SgroupData>();
		
		Sgroups sgroups=mapViewer.getMap().sgroups;
		
		HashSet<Integer> selectedSquadIDs=new HashSet<Integer>();
		
		if (onlySelected)
			for (int entityID : mapViewer.getRenderer().currentSelection.entities) {
				Integer squadID=mapViewer.getMap().squads.getSquadID(entityID);
				if (squadID!=null) selectedSquadIDs.add(squadID);
			}
		
		
		for (Integer sgroupID : sgroups.keySet())	{
			
			Sgroup sgroup=sgroups.get(sgroupID);
			
			if (onlySelected)
				if (!sgroup.contains(selectedSquadIDs))
					continue;
			
			SgroupData sgroupData=new SgroupData();
			sgroupData.id=sgroup.get_id();
			sgroupData.name=sgroup.get_name();
			sgroupData.count=sgroup.get_numberofsquads();
			
			newDataLines.add(sgroupData);
			
		}
		
		sgroupDataLines=newDataLines;
		fireTableDataChanged();
		
	}
	
	public void setMapViewer(MapViewer mapViewer, boolean onlySelected) {
		this.mapViewer=mapViewer;
		updateSGroupTable(onlySelected);
	}

	public int findName(String selName) {
		for (int i=0;i<sgroupDataLines.size();i++) 
			if (selName.equals(sgroupDataLines.get(i).name)) 
				return i;
		return -1;
	}

}
