package fr.dow.mapeditor.dialogs.setdisplay;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import fr.dow.mapeditor.tools.ColorizedButton;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.params.MapSelDisPars;

import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.BorderFactory;
import java.awt.Font;

public class SetSelectionDisplay extends JDialog {

	//
	// Beginning of custom part 
	//
	
	MapSelDisPars selDisPars;
	
	public static void showDialog(Frame frame, MapViewer mapViewer) {
		SetSelectionDisplay dialog = new SetSelectionDisplay(frame);
		dialog.selDisPars=mapViewer.getRenderer().mapDisplayParams.selDisPars;
		dialog.setLocationRelativeTo(mapViewer);
		dialog.restoreDefault();
		dialog.setVisible(true);
		return;
	}	
	
	public void restoreDefault() {
		colButtonMouseArea.setColor(selDisPars.mouseArea);
		
		colButtonGroundArea.setColor(selDisPars.selected.groundArea);
		colButtonTiles.setColor(selDisPars.selected.tiles);
		colButtonDecals.setColor(selDisPars.selected.decals);
		colButtonEntities.setColor(selDisPars.selected.entities);
		colButtonMarkers.setColor(selDisPars.selected.markers);
		
		colButtonGroundAreaOver.setColor(selDisPars.over.groundArea);
		colButtonTilesOver.setColor(selDisPars.over.tiles);
		colButtonDecalsOver.setColor(selDisPars.over.decals);
		colButtonEntitiesOver.setColor(selDisPars.over.entities);
		colButtonMarkersOver.setColor(selDisPars.over.markers);
		
		repaint();
		
	}
	
	public void apply () {
		selDisPars.mouseArea=colButtonMouseArea.getColor();
		
		selDisPars.selected.groundArea=colButtonGroundArea.getColor();
		selDisPars.selected.tiles=colButtonTiles.getColor();
		selDisPars.selected.decals=colButtonDecals.getColor();
		selDisPars.selected.entities=colButtonEntities.getColor();
		selDisPars.selected.markers=colButtonMarkers.getColor();
		
		selDisPars.over.groundArea=colButtonGroundAreaOver.getColor();
		selDisPars.over.tiles=colButtonTilesOver.getColor();
		selDisPars.over.decals=colButtonDecalsOver.getColor();
		selDisPars.over.entities=colButtonEntitiesOver.getColor();
		selDisPars.over.markers=colButtonMarkersOver.getColor();
		
	}
	
	public void close(){
		this.setVisible(false);
	}
	
	//
	// End of custom part 
	//
	
	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanel7 = null;
	private JPanel jPanel8 = null;
	private JButton jButton3 = null;
	private JPanel jPanel9 = null;
	private JButton jButton11 = null;
	private JButton jButton21 = null;
	private JPanel jPanel42 = null;
	private ColorizedButton colButtonMouseArea = null;
	private JPanel jPanel421 = null;
	private JPanel jPanel422 = null;
	private ColorizedButton colButtonGroundArea = null;
	private JPanel jPanel4221 = null;
	private ColorizedButton colButtonDecals = null;
	private JPanel jPanel4222 = null;
	private ColorizedButton colButtonEntities = null;
	private JPanel jPanel4223 = null;
	private ColorizedButton colButtonMarkers = null;
	private ColorizedButton colButtonGroundAreaOver = null;
	private ColorizedButton colButtonDecalsOver = null;
	private ColorizedButton colButtonEntitiesOver = null;
	private ColorizedButton colButtonMarkersOver = null;
	private JPanel jPanel42211 = null;
	private ColorizedButton colButtonTiles = null;
	private ColorizedButton colButtonTilesOver = null;
	/**
	 * @param owner
	 */
	public SetSelectionDisplay(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(457, 505);
		this.setTitle("Selection colors");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel7(), BorderLayout.SOUTH);
			jContentPane.add(getJPanel42(), BorderLayout.NORTH);
			jContentPane.add(getJPanel421(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel7	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel7() {
		if (jPanel7 == null) {
			jPanel7 = new JPanel();
			jPanel7.setLayout(new BorderLayout());
			jPanel7.add(getJPanel8(), java.awt.BorderLayout.WEST);
			jPanel7.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanel7;
	}

	/**
	 * This method initializes jPanel8	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel8() {
		if (jPanel8 == null) {
			FlowLayout flowLayout6 = new FlowLayout();
			flowLayout6.setHgap(10);
			jPanel8 = new JPanel();
			jPanel8.setLayout(flowLayout6);
			jPanel8.add(getJButton3(), null);
		}
		return jPanel8;
	}

	/**
	 * This method initializes jButton3	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton3() {
		if (jButton3 == null) {
			jButton3 = new JButton();
			jButton3.setPreferredSize(new Dimension(100, 23));
			jButton3.setText("Default");
			jButton3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					restoreDefault();
				}
			});
		}
		return jButton3;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButton11(), null);
			jPanel9.add(getJButton21(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButton11	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton11() {
		if (jButton11 == null) {
			jButton11 = new JButton();
			jButton11.setPreferredSize(new Dimension(100, 23));
			jButton11.setText("Apply");
			jButton11.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					apply();
				}
			});
		}
		return jButton11;
	}

	/**
	 * This method initializes jButton21	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton21() {
		if (jButton21 == null) {
			jButton21 = new JButton();
			jButton21.setPreferredSize(new Dimension(100, 23));
			jButton21.setText("Close");
			jButton21.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					close();
				}
			});
		}
		return jButton21;
	}

	/**
	 * This method initializes jPanel42	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel42() {
		if (jPanel42 == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setAlignment(FlowLayout.LEFT);
			jPanel42 = new JPanel();
			jPanel42.setPreferredSize(new Dimension(110, 60));
			jPanel42.setLayout(flowLayout);
			jPanel42.setBorder(BorderFactory.createTitledBorder(null, "Mouse Over", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel42.add(getColButtonMouseArea(), null);
		}
		return jPanel42;
	}

	/**
	 * This method initializes colButtonMouseArea	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonMouseArea() {
		if (colButtonMouseArea == null) {
			colButtonMouseArea = new ColorizedButton();
			colButtonMouseArea.setPreferredSize(new Dimension(100, 25));
			colButtonMouseArea.setColor(Color.white);
			colButtonMouseArea.setText("Color");
		}
		return colButtonMouseArea;
	}

	/**
	 * This method initializes jPanel421	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel421() {
		if (jPanel421 == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Mouse Over", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder.setTitle("Selection");
			jPanel421 = new JPanel();
			jPanel421.setLayout(new BoxLayout(getJPanel421(), BoxLayout.Y_AXIS));
			jPanel421.setBorder(titledBorder);
			jPanel421.add(getJPanel422(), null);
			jPanel421.add(getJPanel42211(), null);
			jPanel421.add(getJPanel4221(), null);
			jPanel421.add(getJPanel4222(), null);
			jPanel421.add(getJPanel4223(), null);
		}
		return jPanel421;
	}

	/**
	 * This method initializes jPanel422	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel422() {
		if (jPanel422 == null) {
			TitledBorder titledBorder1 = BorderFactory.createTitledBorder(null, "Mouse Over", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder1.setTitle("Ground Area");
			FlowLayout flowLayout1 = new FlowLayout();
			flowLayout1.setAlignment(FlowLayout.LEFT);
			jPanel422 = new JPanel();
			jPanel422.setPreferredSize(new Dimension(110, 60));
			jPanel422.setLayout(flowLayout1);
			jPanel422.setBorder(titledBorder1);
			jPanel422.add(getColButtonGroundArea(), null);
			jPanel422.add(getColButtonGroundAreaOver(), null);
		}
		return jPanel422;
	}

	/**
	 * This method initializes colButtonGroundArea	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonGroundArea() {
		if (colButtonGroundArea == null) {
			colButtonGroundArea = new ColorizedButton();
			colButtonGroundArea.setPreferredSize(new Dimension(100, 25));
			colButtonGroundArea.setColor(Color.white);
			colButtonGroundArea.setText("Selection");
		}
		return colButtonGroundArea;
	}

	/**
	 * This method initializes jPanel4221	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel4221() {
		if (jPanel4221 == null) {
			TitledBorder titledBorder11 = BorderFactory.createTitledBorder(null, "Mouse Over", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder11.setTitle("Decals");
			FlowLayout flowLayout11 = new FlowLayout();
			flowLayout11.setAlignment(FlowLayout.LEFT);
			jPanel4221 = new JPanel();
			jPanel4221.setPreferredSize(new Dimension(110, 60));
			jPanel4221.setLayout(flowLayout11);
			jPanel4221.setBorder(titledBorder11);
			jPanel4221.add(getColButtonDecals(), null);
			jPanel4221.add(getColButtonDecalsOver(), null);
		}
		return jPanel4221;
	}

	/**
	 * This method initializes colButtonDecals	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonDecals() {
		if (colButtonDecals == null) {
			colButtonDecals = new ColorizedButton();
			colButtonDecals.setPreferredSize(new Dimension(100, 25));
			colButtonDecals.setColor(Color.white);
			colButtonDecals.setText("Selection");
		}
		return colButtonDecals;
	}

	/**
	 * This method initializes jPanel4222	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel4222() {
		if (jPanel4222 == null) {
			TitledBorder titledBorder12 = BorderFactory.createTitledBorder(null, "Mouse Over", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder12.setTitle("Entities");
			FlowLayout flowLayout12 = new FlowLayout();
			flowLayout12.setAlignment(FlowLayout.LEFT);
			jPanel4222 = new JPanel();
			jPanel4222.setPreferredSize(new Dimension(110, 60));
			jPanel4222.setLayout(flowLayout12);
			jPanel4222.setBorder(titledBorder12);
			jPanel4222.add(getColButtonEntities(), null);
			jPanel4222.add(getColButtonEntitiesOver(), null);
		}
		return jPanel4222;
	}

	/**
	 * This method initializes colButtonEntities	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonEntities() {
		if (colButtonEntities == null) {
			colButtonEntities = new ColorizedButton();
			colButtonEntities.setPreferredSize(new Dimension(100, 25));
			colButtonEntities.setColor(Color.white);
			colButtonEntities.setText("Selection");
		}
		return colButtonEntities;
	}

	/**
	 * This method initializes jPanel4223	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel4223() {
		if (jPanel4223 == null) {
			TitledBorder titledBorder13 = BorderFactory.createTitledBorder(null, "Mouse Over", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder13.setTitle("Markers");
			FlowLayout flowLayout13 = new FlowLayout();
			flowLayout13.setAlignment(FlowLayout.LEFT);
			jPanel4223 = new JPanel();
			jPanel4223.setPreferredSize(new Dimension(110, 60));
			jPanel4223.setLayout(flowLayout13);
			jPanel4223.setBorder(titledBorder13);
			jPanel4223.add(getColButtonMarkers(), null);
			jPanel4223.add(getColButtonMarkersOver(), null);
		}
		return jPanel4223;
	}

	/**
	 * This method initializes colButtonMarkers	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonMarkers() {
		if (colButtonMarkers == null) {
			colButtonMarkers = new ColorizedButton();
			colButtonMarkers.setPreferredSize(new Dimension(100, 25));
			colButtonMarkers.setColor(Color.white);
			colButtonMarkers.setText("Selection");
		}
		return colButtonMarkers;
	}

	/**
	 * This method initializes colButtonGroundAreaOver	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonGroundAreaOver() {
		if (colButtonGroundAreaOver == null) {
			colButtonGroundAreaOver = new ColorizedButton();
			colButtonGroundAreaOver.setPreferredSize(new Dimension(100, 25));
			colButtonGroundAreaOver.setColor(Color.white);
			colButtonGroundAreaOver.setText("Over");
		}
		return colButtonGroundAreaOver;
	}

	/**
	 * This method initializes colButtonDecalsOver	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonDecalsOver() {
		if (colButtonDecalsOver == null) {
			colButtonDecalsOver = new ColorizedButton();
			colButtonDecalsOver.setPreferredSize(new Dimension(100, 25));
			colButtonDecalsOver.setColor(Color.white);
			colButtonDecalsOver.setText("Over");
		}
		return colButtonDecalsOver;
	}

	/**
	 * This method initializes colButtonEntitiesOver	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonEntitiesOver() {
		if (colButtonEntitiesOver == null) {
			colButtonEntitiesOver = new ColorizedButton();
			colButtonEntitiesOver.setPreferredSize(new Dimension(100, 25));
			colButtonEntitiesOver.setColor(Color.white);
			colButtonEntitiesOver.setText("Over");
		}
		return colButtonEntitiesOver;
	}

	/**
	 * This method initializes colButtonMarkersOver	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonMarkersOver() {
		if (colButtonMarkersOver == null) {
			colButtonMarkersOver = new ColorizedButton();
			colButtonMarkersOver.setPreferredSize(new Dimension(100, 25));
			colButtonMarkersOver.setColor(Color.white);
			colButtonMarkersOver.setText("Over");
		}
		return colButtonMarkersOver;
	}

	/**
	 * This method initializes jPanel42211	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel42211() {
		if (jPanel42211 == null) {
			TitledBorder titledBorder112 = BorderFactory.createTitledBorder(null, "Mouse Over", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder112.setTitle("Tiles");
			FlowLayout flowLayout112 = new FlowLayout();
			flowLayout112.setAlignment(FlowLayout.LEFT);
			jPanel42211 = new JPanel();
			jPanel42211.setPreferredSize(new Dimension(110, 60));
			jPanel42211.setLayout(flowLayout112);
			jPanel42211.setBorder(titledBorder112);
			jPanel42211.add(getColButtonTiles(), null);
			jPanel42211.add(getColButtonTilesOver(), null);
		}
		return jPanel42211;
	}

	/**
	 * This method initializes colButtonTiles	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonTiles() {
		if (colButtonTiles == null) {
			colButtonTiles = new ColorizedButton();
			colButtonTiles.setPreferredSize(new Dimension(100, 25));
			colButtonTiles.setColor(Color.white);
			colButtonTiles.setText("Selection");
		}
		return colButtonTiles;
	}

	/**
	 * This method initializes colButtonTilesOver	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonTilesOver() {
		if (colButtonTilesOver == null) {
			colButtonTilesOver = new ColorizedButton();
			colButtonTilesOver.setPreferredSize(new Dimension(100, 25));
			colButtonTilesOver.setColor(Color.white);
			colButtonTilesOver.setText("Over");
		}
		return colButtonTilesOver;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
