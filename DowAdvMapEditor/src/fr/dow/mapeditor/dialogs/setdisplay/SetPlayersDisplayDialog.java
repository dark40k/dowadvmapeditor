package fr.dow.mapeditor.dialogs.setdisplay;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.dow.mapeditor.tools.ColorizedButton;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.params.PlayersDisPars;

public class SetPlayersDisplayDialog extends JDialog implements ActionListener {
	
	private static final long serialVersionUID = 1L;

	private MapViewer _mapViewer;
	
	private JPanel playersVisibilityPane;
	
	private JButton closeButton;
	
	private PlayersDisPars playerDisplayParams;
	
	public static void showDialog(Frame frame, MapViewer mapViewer) {
    		SetPlayersDisplayDialog dialog = new SetPlayersDisplayDialog(frame, mapViewer);
			dialog.setVisible(true);
			return;
    }
	
    public SetPlayersDisplayDialog(Frame frame, MapViewer mapViewer) {
    	
    	super(frame, "Set players display", true);
    	
    	_mapViewer=mapViewer;
    	
    	playerDisplayParams=mapViewer.getRenderer().mapDisplayParams.playerDisplayParams;
    	
    	Container contentPane = getContentPane();
    	
    	contentPane.add(buildUIComponent_ControlBar(), BorderLayout.PAGE_END);
    	
    	contentPane.add(buildUIComponent_PlayersVisibility(), BorderLayout.CENTER);
    	
        pack();
        setLocationRelativeTo(_mapViewer);	
		getRootPane().setDefaultButton(closeButton);
	}
    
   
    private class PlayerDisplayPanel extends JPanel{
    	
		private static final long serialVersionUID = 1L;
		
		public JCheckBox visibleCheckBox;
		public JCheckBox textureCheckBox;
		public ColorizedButton colorButton;
    	
		private PlayersDisPars.PlayerData _playerData;
		
    	public PlayerDisplayPanel(PlayersDisPars.PlayerData playerData) {
    		
    		super();
    		
    		_playerData=playerData;
    		
    		this.setLayout(new FlowLayout(FlowLayout.LEFT,5,5));
    		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), playerData.name));
    		
    		visibleCheckBox = new JCheckBox("Show", playerData.isVisible);
    		textureCheckBox = new JCheckBox("Textured", playerData.isTextured);
    		colorButton = new ColorizedButton();
    		colorButton.setColor(new Color(playerData.red,playerData.green,playerData.blue));
    		
    		colorButton.setAction(
    			new AbstractAction(){
    				ColorizedButton actionColorButton=colorButton;
					public void actionPerformed(ActionEvent e) {
						Color res = JColorChooser.showDialog(actionColorButton, "Change color", actionColorButton.getColor());
						if (res != null) {
							actionColorButton.setColor(res);
							repaint();
						}						
					}
				}
    		);
    		colorButton.setPreferredSize(new Dimension(50,25));
    		
    		this.add(visibleCheckBox);
    		this.add(Box.createRigidArea(new Dimension(25, 0)));
    		this.add(textureCheckBox);
    		this.add(Box.createRigidArea(new Dimension(50, 0)));
    		this.add(colorButton);
    		this.add(new JLabel("Color"));
    	}
    	
    	public void Apply() {
    		_playerData.isVisible=visibleCheckBox.isSelected();
    		_playerData.isTextured=textureCheckBox.isSelected();
    		
    		Color color=colorButton.getColor();
    		_playerData.red=color.getRed()/255f;
    		_playerData.green=color.getGreen()/255f;
    		_playerData.blue=color.getBlue()/255f;
    		
    	}
    	
    }
    
    private JPanel buildUIComponent_PlayersVisibility() {
    	
		playersVisibilityPane=new JPanel();
		playersVisibilityPane.setLayout(new GridLayout(5,2));
    	playersVisibilityPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    	
    	for (PlayersDisPars.PlayerData playerData : playerDisplayParams.players.values())
    		playersVisibilityPane.add(new PlayerDisplayPanel(playerData));
    	
    	return playersVisibilityPane;
    }
    
    
    private JPanel buildUIComponent_ControlBar() {
    	
    	JButton showAllButton = new JButton("Show All");
		showAllButton.setActionCommand("ShowAll");
		showAllButton.addActionListener(this);
		
		JButton hideAllButton = new JButton("Hide All");
		hideAllButton.setActionCommand("HideAll");
		hideAllButton.addActionListener(this);
		
		JButton texAllButton = new JButton("Tex All");
		texAllButton.setActionCommand("TexAll");
		texAllButton.addActionListener(this);
		
		JButton unTexAllButton = new JButton("unTex All");
		unTexAllButton.setActionCommand("unTexAll");
		unTexAllButton.addActionListener(this);
		
		closeButton = new JButton("Close");
		closeButton.setActionCommand("Close");
		closeButton.addActionListener(this);
		
		JButton applyButton = new JButton("Apply");
		applyButton.setActionCommand("Apply");
		applyButton.addActionListener(this);
		
		JPanel controlBarPane=new JPanel();
    	controlBarPane.setLayout(new BoxLayout(controlBarPane, BoxLayout.LINE_AXIS));
    	controlBarPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    	
    	controlBarPane.add(showAllButton);
    	controlBarPane.add(hideAllButton);
    	controlBarPane.add(Box.createRigidArea(new Dimension(10, 0)));
    	controlBarPane.add(texAllButton);
    	controlBarPane.add(unTexAllButton);
    	controlBarPane.add(Box.createHorizontalGlue());
    	controlBarPane.add(applyButton);
    	controlBarPane.add(Box.createRigidArea(new Dimension(10, 0)));
    	controlBarPane.add(closeButton);
    	
		return controlBarPane;
    }

    //Handle clicks on the Set and Cancel buttons.
    public void actionPerformed(ActionEvent e) {
        if ("Close".equals(e.getActionCommand())) this.setVisible(false);
        else if ("Apply".equals(e.getActionCommand())) applyParams();
        else if ("ShowAll".equals(e.getActionCommand())) showAll();
        else if ("HideAll".equals(e.getActionCommand())) hideAll();
        else if ("TexAll".equals(e.getActionCommand())) texAll();
        else if ("unTexAll".equals(e.getActionCommand())) unTexAll();
        
    }
    
    private void showAll() {
    	for (Component component : playersVisibilityPane.getComponents()) {
    		PlayerDisplayPanel playerDisplayPanel = (PlayerDisplayPanel) component;
    		playerDisplayPanel.visibleCheckBox.setSelected(true);
    	}    	
    }
    private void hideAll() {
    	for (Component component : playersVisibilityPane.getComponents()) {
    		PlayerDisplayPanel playerDisplayPanel = (PlayerDisplayPanel) component;
    		playerDisplayPanel.visibleCheckBox.setSelected(false);
    	}    	
    }
    
    private void texAll() {
    	for (Component component : playersVisibilityPane.getComponents()) {
    		PlayerDisplayPanel playerDisplayPanel = (PlayerDisplayPanel) component;
    		playerDisplayPanel.textureCheckBox.setSelected(true);
    	}    	
    }
    private void unTexAll() {
    	for (Component component : playersVisibilityPane.getComponents()) {
    		PlayerDisplayPanel playerDisplayPanel = (PlayerDisplayPanel) component;
    		playerDisplayPanel.textureCheckBox.setSelected(false);
    	}    	
    }
    
    
    private void applyParams() {
    	for (Component component : playersVisibilityPane.getComponents()) {
    		PlayerDisplayPanel playerDisplayPanel = (PlayerDisplayPanel) component;
    		playerDisplayPanel.Apply();
    	}
    	playerDisplayParams.fireUpdateEvent(1);
    }
    
}
