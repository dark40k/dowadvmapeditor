package fr.dow.mapeditor.dialogs.setdisplay;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Vector;

import javax.media.opengl.awt.GLCanvas;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.jogamp.opengl.util.FPSAnimator;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.mapeditor.tools.ColorizedButton;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.joglobject.joglentity.JoglEntity;
import fr.dow.mapviewer.display.params.EntitiesDisPars;

public class SetEntitiesDisplayDialog extends SetEntitiesDisplayBase implements ListSelectionListener {

	private SetEntitiesDisplayRenderer entityRenderer;
	
	private DowMod mod;
	
	private JoglEntity selJoglEntity;
	
	private Vector<JoglEntity> joglEntities;
	private EntitiesDisPars entitiesDisplayParams;
	
	private DefaultListModel<String> listEntities;
	private JList<String> jListEntities;
	
	private DefaultListModel<String> listModelParts;
	private JList<String> jListParts;
	
	HashSet<String> hiddenMeshes;
	
	public static void showDialog(Component owner, MapViewer mapViewer) {
		
		SetEntitiesDisplayDialog dialog = new SetEntitiesDisplayDialog(null,mapViewer);
		dialog.setTitle(dialog.getTitle()+mapViewer.getMod().getModFile().getName());
		dialog.setLocationRelativeTo(owner);
		dialog.setVisible(true);
	}
	
	
	public void close() {
		this.setVisible(false);
	}
	
	public void apply() {
	}
	
	private SetEntitiesDisplayDialog(Frame owner,MapViewer mapViewer) {
		super(owner);
		
		mod=mapViewer.getMod();
		
		joglEntities=mapViewer.getRenderer().joglWorld.joglMapEntities.joglEntities;

		entitiesDisplayParams=mapViewer.getRenderer().mapDisplayParams.entitiesDisplayParams;

		//
		// Entities
		//
		
		listEntities=new DefaultListModel<String>();
		jListEntities=new JList<String>(listEntities);
		jListEntities.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jListEntities.setLayoutOrientation(JList.VERTICAL);
		
		jScrollPaneEntities.setViewportView(jListEntities);
		
		//
		// Parts
		//
		
		listModelParts=new DefaultListModel<String>();
		jListParts = new JList<String>(listModelParts);
		jListParts.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		jListParts.setLayoutOrientation(JList.VERTICAL);
		
		jScrollPaneParts.setViewportView(jListParts);
		
		//
		// OpenGL display
		//
		
		GLCanvas canvas = new GLCanvas();
		
		entityRenderer=new SetEntitiesDisplayRenderer(canvas);
	    canvas.addGLEventListener(entityRenderer);
	    
		jPanelEntityPreview.add(canvas,BorderLayout.CENTER);
		
		//Init animator
		System.out.println("Starting animation");	
		FPSAnimator animator = new FPSAnimator( canvas, 25 );
		//animator.setRunAsFastAsPossible(false);
		animator.start(); 
		
		//
		// Interface
		//
		
		jButtonApply.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						applyPartSelection();
					}
				}
			);
		
		jButtonClose.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						close();
					}
				}
			);
		
		ColorizedButton colorButton=new ColorizedButton();
		colorButton.setPreferredSize(new Dimension(100,23));
		colorButton.setText("Back. Color");
		colorButton.setColor(new Color(1f,1f,1f,0f));
		jPanelLeft.add(colorButton);
		
		colorButton.addPropertyChangeListener( 
				new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent evt) {
						if (evt.getPropertyName().equals("Color")) {
							entityRenderer.setNewBackgroundColor((Color) evt.getNewValue());
						}
					}
				}
		);

		JButton resetButton = new JButton("Reset");
		resetButton.setPreferredSize(new Dimension(100,23));
		jPanelLeft.add(resetButton);
		
		jCheckBoxFilterSingle.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						updateListEntities();
					}
				}
			);		
		
		resetButton.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						resetPartSelection();
					}
				}
			);
		
		
		//
		// Start
		//
		updateListEntities();
		
	}


	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource()==jListEntities) { if (!e.getValueIsAdjusting()) updateSelectedEntity(); }
		else if (e.getSource()==jListParts) { if (!e.getValueIsAdjusting()) updateSelectedParts(); }
			
	}
	
	private void updateListEntities() {
		
		jListEntities.removeListSelectionListener(this);
		jListParts.removeListSelectionListener(this);
		
//		String[] entityNames=new String[joglEntities.size()];
//		for (int i=0;i<joglEntities.size();i++) 
//			if (joglEntities.get(i)!=null) 
//				entityNames[i]=joglEntities.get(i).baseName; 
//			else 
//				entityNames[i]="<Entity not found>";
		
		boolean selectAllEntities=!jCheckBoxFilterSingle.isSelected();
		
		listEntities.clear();
		for (JoglEntity joglEntity : joglEntities) 
			if (joglEntity!=null) 
				if ( (joglEntity.joglSubModels.length>1) || selectAllEntities)
					listEntities.addElement(joglEntity.baseName);
				
		if (jListEntities.getModel().getSize()>0) jListEntities.setSelectedIndex(0);

		jListEntities.addListSelectionListener(this);
		
		updateSelectedEntity();
		
	}
	
	private void updateSelectedEntity() {
	
		jListParts.removeListSelectionListener(this);

		listModelParts.clear();		
		entityRenderer.clearEntity();
		
		selJoglEntity=null;
		if (jListEntities.isSelectionEmpty()) return;
		
		String selEntityName= (String) jListEntities.getSelectedValue();
		for (JoglEntity joglEntity : joglEntities)
			if (joglEntity!=null) 
				if (selEntityName.equals(joglEntity.baseName))
				{
					selJoglEntity=joglEntity;
					break;
				}
		
		for (int i=0;i<selJoglEntity.joglSubModels.length;i++) listModelParts.addElement(selJoglEntity.joglSubModels[i].name);
		resetPartSelection();
		
		entityRenderer.displayNewEntity(selJoglEntity.getBlueprint(), mod);
		updateSelectedParts();
	}
	
	@SuppressWarnings("deprecation")
	private void updateSelectedParts() {
		hiddenMeshes=new HashSet<String>();
		for (Object hiddenPart : jListParts.getSelectedValues()) hiddenMeshes.add((String)hiddenPart);
		entityRenderer.updateHiddenMeshes(hiddenMeshes);
	}
	
	private void resetPartSelection() {
		
		if (selJoglEntity==null) return;
		
		jListParts.removeListSelectionListener(this);
		
		jListParts.clearSelection();
		for (int i=0;i<selJoglEntity.joglSubModels.length;i++) {
			if (selJoglEntity.hiddenMeshes.contains(selJoglEntity.joglSubModels[i].name)) 
				jListParts.addSelectionInterval(i, i);
		}
		
		updateSelectedParts();
		
		jListParts.addListSelectionListener(this);
	}
	
	private void applyPartSelection() {
		if (selJoglEntity==null) return;
		selJoglEntity.hiddenMeshes=hiddenMeshes;
		entitiesDisplayParams.fireUpdateEvent(1);
	}
	

	
}
