package fr.dow.mapeditor.dialogs.setdisplay;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JCheckBox;
import fr.dow.mapeditor.tools.ColorizedButton;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.params.terrain.TerrainDisPars;

import javax.swing.SwingConstants;

public class SetGroundDisplayDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanel5 = null;
	private JPanel jPanel4 = null;
	private JCheckBox jCheckBoxGroundShow = null;
	private JCheckBox jCheckBoxGroundWireframe = null;
	private JPanel jPanel11 = null;
	private JCheckBox jCheckBoxDecalsShow = null;
	private JCheckBox jCheckBoxDecalsWireframe = null;
	private JPanel jPanel111 = null;
	private JCheckBox jCheckBoxWaterShow = null;
	private JCheckBox jCheckBoxWaterWireframe = null;
	private JPanel jPanel6 = null;
	private JPanel jPanel31 = null;
	private JCheckBox jCheckBoxCoverShow = null;
	private ColorizedButton colButtonCoverNone = null;
	private ColorizedButton colButtonCoverLight = null;
	private ColorizedButton colButtonCoverHeavy = null;
	private ColorizedButton colButtonCoverNegative = null;
	private ColorizedButton colButtonCoverBlocking = null;
	private ColorizedButton colButtonCoverStealth = null;
	private JPanel jPanel21 = null;
	private JCheckBox jCheckBoxImpassShow = null;
	private ColorizedButton colButtonImpassPassable = null;
	private ColorizedButton colButtonImpassImpassable = null;
	private JPanel jPanel41 = null;
	private JCheckBox jCheckBoxFootfallShow = null;
	private ColorizedButton colButtonFootfallUnknown = null;
	private ColorizedButton colButtonFootfallDirtsand = null;
	private ColorizedButton colButtonFootfallRock = null;
	private ColorizedButton colButtonFootfallGrass = null;
	private ColorizedButton colButtonFootfallWater = null;
	private JPanel jPanel7 = null;
	private JPanel jPanel8 = null;
	private JButton jButton3 = null;
	private JPanel jPanel9 = null;
	private JButton jButton11 = null;
	private JButton jButton21 = null;
	/**
	 * @param owner
	 */
	public SetGroundDisplayDialog(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(800, 564);
		this.setTitle("Ground display parameters");
		this.setModal(false);
		this.setContentPane(getJContentPane());
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				close();
			}
		});
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel5(), BorderLayout.CENTER);
			jContentPane.add(getJPanel7(), BorderLayout.SOUTH);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel5	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel5() {
		if (jPanel5 == null) {
			jPanel5 = new JPanel();
			jPanel5.setLayout(new BoxLayout(getJPanel5(), BoxLayout.Y_AXIS));
			jPanel5.add(getJPanel(), null);
			jPanel5.add(getJPanel6(), null);
		}
		return jPanel5;
	}

	/**
	 * This method initializes jPanel4	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel4() {
		if (jPanel4 == null) {
			FlowLayout flowLayout11 = new FlowLayout();
			flowLayout11.setHgap(10);
			flowLayout11.setAlignment(FlowLayout.LEFT);
			jPanel4 = new JPanel();
			jPanel4.setPreferredSize(new Dimension(0, 20));
			jPanel4.setBorder(BorderFactory.createTitledBorder(null, "Ground texture", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel4.setLayout(flowLayout11);
			jPanel4.add(getJCheckBoxGroundShow(), null);
			jPanel4.add(getJCheckBoxGroundWireframe(), null);
		}
		return jPanel4;
	}

	/**
	 * This method initializes jCheckBoxGroundShow	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxGroundShow() {
		if (jCheckBoxGroundShow == null) {
			jCheckBoxGroundShow = new JCheckBox();
			jCheckBoxGroundShow.setSelected(true);
			jCheckBoxGroundShow.setVerticalAlignment(SwingConstants.CENTER);
			jCheckBoxGroundShow.setText("Show");
		}
		return jCheckBoxGroundShow;
	}

	/**
	 * This method initializes jCheckBoxGroundWireframe	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxGroundWireframe() {
		if (jCheckBoxGroundWireframe == null) {
			jCheckBoxGroundWireframe = new JCheckBox();
			jCheckBoxGroundWireframe.setText("Wireframe");
			jCheckBoxGroundWireframe.setVerticalAlignment(SwingConstants.CENTER);
		}
		return jCheckBoxGroundWireframe;
	}

	/**
	 * This method initializes jPanel11	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel11() {
		if (jPanel11 == null) {
			FlowLayout flowLayout2 = new FlowLayout();
			flowLayout2.setHgap(10);
			flowLayout2.setAlignment(FlowLayout.LEFT);
			jPanel11 = new JPanel();
			jPanel11.setPreferredSize(new Dimension(0, 20));
			jPanel11.setBorder(BorderFactory.createTitledBorder(null, "Decals", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel11.setLayout(flowLayout2);
			jPanel11.add(getJCheckBoxDecalsShow(), null);
			jPanel11.add(getJCheckBoxDecalsWireframe(), null);
		}
		return jPanel11;
	}

	/**
	 * This method initializes jCheckBoxDecalsShow	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxDecalsShow() {
		if (jCheckBoxDecalsShow == null) {
			jCheckBoxDecalsShow = new JCheckBox();
			jCheckBoxDecalsShow.setSelected(true);
			jCheckBoxDecalsShow.setVerticalAlignment(SwingConstants.CENTER);
			jCheckBoxDecalsShow.setVerticalTextPosition(SwingConstants.CENTER);
			jCheckBoxDecalsShow.setText("Show");
		}
		return jCheckBoxDecalsShow;
	}

	/**
	 * This method initializes jCheckBoxDecalsWireframe	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxDecalsWireframe() {
		if (jCheckBoxDecalsWireframe == null) {
			jCheckBoxDecalsWireframe = new JCheckBox();
			jCheckBoxDecalsWireframe.setText("Wireframe");
			jCheckBoxDecalsWireframe.setVerticalAlignment(SwingConstants.CENTER);
		}
		return jCheckBoxDecalsWireframe;
	}

	/**
	 * This method initializes jPanel111	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel111() {
		if (jPanel111 == null) {
			TitledBorder titledBorder4 = BorderFactory.createTitledBorder(null, "Decals", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder4.setTitle("Water");
			FlowLayout flowLayout21 = new FlowLayout();
			flowLayout21.setHgap(10);
			flowLayout21.setAlignment(FlowLayout.LEFT);
			jPanel111 = new JPanel();
			jPanel111.setPreferredSize(new Dimension(0, 20));
			jPanel111.setLayout(flowLayout21);
			jPanel111.setBorder(titledBorder4);
			jPanel111.add(getJCheckBoxWaterShow(), null);
			jPanel111.add(getJCheckBoxWaterWireframe(), null);
		}
		return jPanel111;
	}

	/**
	 * This method initializes jCheckBoxWaterShow	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxWaterShow() {
		if (jCheckBoxWaterShow == null) {
			jCheckBoxWaterShow = new JCheckBox();
			jCheckBoxWaterShow.setSelected(true);
			jCheckBoxWaterShow.setVerticalAlignment(SwingConstants.CENTER);
			jCheckBoxWaterShow.setVerticalTextPosition(SwingConstants.CENTER);
			jCheckBoxWaterShow.setText("Show");
		}
		return jCheckBoxWaterShow;
	}

	/**
	 * This method initializes jCheckBoxWaterWireframe	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxWaterWireframe() {
		if (jCheckBoxWaterWireframe == null) {
			jCheckBoxWaterWireframe = new JCheckBox();
			jCheckBoxWaterWireframe.setText("Wireframe");
			jCheckBoxWaterWireframe.setVerticalAlignment(SwingConstants.CENTER);
		}
		return jCheckBoxWaterWireframe;
	}

	/**
	 * This method initializes jPanel6	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel6() {
		if (jPanel6 == null) {
			jPanel6 = new JPanel();
			jPanel6.setLayout(new BoxLayout(getJPanel6(), BoxLayout.Y_AXIS));
			jPanel6.setBorder(BorderFactory.createTitledBorder(null, "Overlays", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel6.add(getJPanel31(), null);
			jPanel6.add(getJPanel21(), null);
			jPanel6.add(getJPanel41(), null);
			jPanel6.add(getJPanel211(), null);
			jPanel6.add(getJPanel311(), null);
			jPanel6.add(getJPanel3111(), null);
		}
		return jPanel6;
	}

	/**
	 * This method initializes jPanel31	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel31() {
		if (jPanel31 == null) {
			TitledBorder titledBorder1 = BorderFactory.createTitledBorder(null, "Cover overlay", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder1.setTitle("Cover");
			FlowLayout flowLayout3 = new FlowLayout();
			flowLayout3.setHgap(10);
			flowLayout3.setAlignment(FlowLayout.LEFT);
			jPanel31 = new JPanel();
			jPanel31.setPreferredSize(new Dimension(0, 20));
			jPanel31.setLayout(flowLayout3);
			jPanel31.setBorder(titledBorder1);
			jPanel31.add(getJCheckBoxCoverShow(), null);
			jPanel31.add(getColButtonCoverNone(), null);
			jPanel31.add(getColButtonCoverLight(), null);
			jPanel31.add(getColButtonCoverHeavy(), null);
			jPanel31.add(getColButtonCoverNegative(), null);
			jPanel31.add(getColButtonCoverBlocking(), null);
			jPanel31.add(getColButtonCoverStealth(), null);
		}
		return jPanel31;
	}

	/**
	 * This method initializes jCheckBoxCoverShow	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxCoverShow() {
		if (jCheckBoxCoverShow == null) {
			jCheckBoxCoverShow = new JCheckBox();
			jCheckBoxCoverShow.setText("Show");
			jCheckBoxCoverShow.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jCheckBoxCoverShow.isSelected()) {
						jCheckBoxImpassShow.setSelected(false);
						jCheckBoxFootfallShow.setSelected(false);
						jCheckBoxPassabilityMapShow.setSelected(false);
						jCheckBoxPreciseTerrainMapShow.setSelected(false);
						jCheckBoxPathSectorMapShow.setSelected(false);
					}
				}
			});
		}
		return jCheckBoxCoverShow;
	}

	/**
	 * This method initializes colButtonCoverNone	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonCoverNone() {
		if (colButtonCoverNone == null) {
			colButtonCoverNone = new ColorizedButton();
			colButtonCoverNone.setPreferredSize(new Dimension(100, 25));
			colButtonCoverNone.setColor(Color.lightGray);
			colButtonCoverNone.setText("None");
		}
		return colButtonCoverNone;
	}

	/**
	 * This method initializes colButtonCoverLight	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonCoverLight() {
		if (colButtonCoverLight == null) {
			colButtonCoverLight = new ColorizedButton();
			colButtonCoverLight.setPreferredSize(new Dimension(100, 25));
			colButtonCoverLight.setColor(Color.orange);
			colButtonCoverLight.setText("Light");
		}
		return colButtonCoverLight;
	}

	/**
	 * This method initializes colButtonCoverHeavy	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonCoverHeavy() {
		if (colButtonCoverHeavy == null) {
			colButtonCoverHeavy = new ColorizedButton();
			colButtonCoverHeavy.setPreferredSize(new Dimension(100, 25));
			colButtonCoverHeavy.setColor(Color.red);
			colButtonCoverHeavy.setText("Heavy");
		}
		return colButtonCoverHeavy;
	}

	/**
	 * This method initializes colButtonCoverNegative	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonCoverNegative() {
		if (colButtonCoverNegative == null) {
			colButtonCoverNegative = new ColorizedButton();
			colButtonCoverNegative.setPreferredSize(new Dimension(100, 25));
			colButtonCoverNegative.setColor(Color.green);
			colButtonCoverNegative.setText("Negative");
		}
		return colButtonCoverNegative;
	}

	/**
	 * This method initializes colButtonCoverBlocking	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonCoverBlocking() {
		if (colButtonCoverBlocking == null) {
			colButtonCoverBlocking = new ColorizedButton();
			colButtonCoverBlocking.setPreferredSize(new Dimension(100, 25));
			colButtonCoverBlocking.setColor(Color.pink);
			colButtonCoverBlocking.setText("Blocking");
		}
		return colButtonCoverBlocking;
	}

	/**
	 * This method initializes colButtonCoverStealth	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonCoverStealth() {
		if (colButtonCoverStealth == null) {
			colButtonCoverStealth = new ColorizedButton();
			colButtonCoverStealth.setPreferredSize(new Dimension(100, 25));
			colButtonCoverStealth.setColor(Color.cyan);
			colButtonCoverStealth.setText("Stealth");
		}
		return colButtonCoverStealth;
	}

	/**
	 * This method initializes jPanel21	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel21() {
		if (jPanel21 == null) {
			TitledBorder titledBorder2 = BorderFactory.createTitledBorder(null, "Impass overlay", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder2.setTitle("Impass (Forced + Generated)");
			FlowLayout flowLayout4 = new FlowLayout();
			flowLayout4.setHgap(10);
			flowLayout4.setAlignment(FlowLayout.LEFT);
			jPanel21 = new JPanel();
			jPanel21.setPreferredSize(new Dimension(0, 20));
			jPanel21.setLayout(flowLayout4);
			jPanel21.setBorder(titledBorder2);
			jPanel21.add(getJCheckBoxImpassShow(), null);
			jPanel21.add(getColButtonImpassPassable(), null);
			jPanel21.add(getColButtonImpassImpassable(), null);
			jPanel21.add(getColButtonImpassGenPass(), null);
			jPanel21.add(getColButtonImpassGenImpass(), null);
		}
		return jPanel21;
	}

	/**
	 * This method initializes jCheckBoxImpassShow	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxImpassShow() {
		if (jCheckBoxImpassShow == null) {
			jCheckBoxImpassShow = new JCheckBox();
			jCheckBoxImpassShow.setText("Show");
			jCheckBoxImpassShow.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jCheckBoxImpassShow.isSelected()) {
						jCheckBoxCoverShow.setSelected(false);
						jCheckBoxFootfallShow.setSelected(false);
						jCheckBoxPassabilityMapShow.setSelected(false);
						jCheckBoxPreciseTerrainMapShow.setSelected(false);
						jCheckBoxPathSectorMapShow.setSelected(false);
					}
				}
			});
		}
		return jCheckBoxImpassShow;
	}

	/**
	 * This method initializes colButtonImpassPassable	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonImpassPassable() {
		if (colButtonImpassPassable == null) {
			colButtonImpassPassable = new ColorizedButton();
			colButtonImpassPassable.setPreferredSize(new Dimension(100, 25));
			colButtonImpassPassable.setColor(new Color(0, 150, 0));
			colButtonImpassPassable.setText("Forced Pass");
		}
		return colButtonImpassPassable;
	}

	/**
	 * This method initializes colButtonImpassImpassable	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonImpassImpassable() {
		if (colButtonImpassImpassable == null) {
			colButtonImpassImpassable = new ColorizedButton();
			colButtonImpassImpassable.setPreferredSize(new Dimension(100, 25));
			colButtonImpassImpassable.setColor(new Color(150, 0, 0));
			colButtonImpassImpassable.setText("Forced Impa.");
		}
		return colButtonImpassImpassable;
	}

	/**
	 * This method initializes jPanel41	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel41() {
		if (jPanel41 == null) {
			TitledBorder titledBorder3 = BorderFactory.createTitledBorder(null, "Footfall overlay", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder3.setTitle("Footfall");
			FlowLayout flowLayout5 = new FlowLayout();
			flowLayout5.setHgap(10);
			flowLayout5.setAlignment(FlowLayout.LEFT);
			jPanel41 = new JPanel();
			jPanel41.setPreferredSize(new Dimension(0, 20));
			jPanel41.setLayout(flowLayout5);
			jPanel41.setBorder(titledBorder3);
			jPanel41.add(getJCheckBoxFootfallShow(), null);
			jPanel41.add(getColButtonFootfallUnknown(), null);
			jPanel41.add(getColButtonFootfallDirtsand(), null);
			jPanel41.add(getColButtonFootfallRock(), null);
			jPanel41.add(getColButtonFootfallGrass(), null);
			jPanel41.add(getColButtonFootfallWater(), null);
		}
		return jPanel41;
	}

	/**
	 * This method initializes jCheckBoxFootfallShow	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxFootfallShow() {
		if (jCheckBoxFootfallShow == null) {
			jCheckBoxFootfallShow = new JCheckBox();
			jCheckBoxFootfallShow.setText("Show");
			jCheckBoxFootfallShow.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jCheckBoxFootfallShow.isSelected()) {
						jCheckBoxCoverShow.setSelected(false);
						jCheckBoxImpassShow.setSelected(false);
						jCheckBoxPassabilityMapShow.setSelected(false);
						jCheckBoxPreciseTerrainMapShow.setSelected(false);
						jCheckBoxPathSectorMapShow.setSelected(false);
					}
				}
			});
		}
		return jCheckBoxFootfallShow;
	}

	/**
	 * This method initializes colButtonFootfallUnknown	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonFootfallUnknown() {
		if (colButtonFootfallUnknown == null) {
			colButtonFootfallUnknown = new ColorizedButton();
			colButtonFootfallUnknown.setPreferredSize(new Dimension(100, 25));
			colButtonFootfallUnknown.setColor(Color.lightGray);
			colButtonFootfallUnknown.setText("Unknown");
		}
		return colButtonFootfallUnknown;
	}

	/**
	 * This method initializes colButtonFootfallDirtsand	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonFootfallDirtsand() {
		if (colButtonFootfallDirtsand == null) {
			colButtonFootfallDirtsand = new ColorizedButton();
			colButtonFootfallDirtsand.setPreferredSize(new Dimension(100, 25));
			colButtonFootfallDirtsand.setColor(Color.yellow);
			colButtonFootfallDirtsand.setText("Dirtsand");
		}
		return colButtonFootfallDirtsand;
	}

	/**
	 * This method initializes colButtonFootfallRock	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonFootfallRock() {
		if (colButtonFootfallRock == null) {
			colButtonFootfallRock = new ColorizedButton();
			colButtonFootfallRock.setPreferredSize(new Dimension(100, 25));
			colButtonFootfallRock.setColor(Color.orange);
			colButtonFootfallRock.setText("Rock");
		}
		return colButtonFootfallRock;
	}

	/**
	 * This method initializes colButtonFootfallGrass	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonFootfallGrass() {
		if (colButtonFootfallGrass == null) {
			colButtonFootfallGrass = new ColorizedButton();
			colButtonFootfallGrass.setPreferredSize(new Dimension(100, 25));
			colButtonFootfallGrass.setColor(Color.green);
			colButtonFootfallGrass.setText("Grass");
		}
		return colButtonFootfallGrass;
	}

	/**
	 * This method initializes colButtonFootfallWater	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonFootfallWater() {
		if (colButtonFootfallWater == null) {
			colButtonFootfallWater = new ColorizedButton();
			colButtonFootfallWater.setPreferredSize(new Dimension(100, 25));
			colButtonFootfallWater.setColor(Color.cyan);
			colButtonFootfallWater.setText("Water");
		}
		return colButtonFootfallWater;
	}

	/**
	 * This method initializes jPanel7	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel7() {
		if (jPanel7 == null) {
			jPanel7 = new JPanel();
			jPanel7.setLayout(new BorderLayout());
			jPanel7.add(getJPanel8(), java.awt.BorderLayout.WEST);
			jPanel7.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanel7;
	}

	/**
	 * This method initializes jPanel8	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel8() {
		if (jPanel8 == null) {
			FlowLayout flowLayout6 = new FlowLayout();
			flowLayout6.setHgap(10);
			jPanel8 = new JPanel();
			jPanel8.setLayout(flowLayout6);
			jPanel8.add(getJButton3(), null);
		}
		return jPanel8;
	}

	/**
	 * This method initializes jButton3	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton3() {
		if (jButton3 == null) {
			jButton3 = new JButton();
			jButton3.setPreferredSize(new Dimension(100, 23));
			jButton3.setText("Default");
			jButton3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					restoreDefault();
				}
			});
		}
		return jButton3;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButton11(), null);
			jPanel9.add(getJButton21(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButton11	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton11() {
		if (jButton11 == null) {
			jButton11 = new JButton();
			jButton11.setPreferredSize(new Dimension(100, 23));
			jButton11.setText("Apply");
			jButton11.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					apply();
				}
			});
		}
		return jButton11;
	}

	/**
	 * This method initializes jButton21	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton21() {
		if (jButton21 == null) {
			jButton21 = new JButton();
			jButton21.setPreferredSize(new Dimension(100, 23));
			jButton21.setText("Close");
			jButton21.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					close();
				}
			});
		}
		return jButton21;
	}
	//
	// Custom part - used to connect to MapViewer
	//
	private TerrainDisPars terrainDisPars;
	private JPanel jPanel = null;
	private JPanel jPanel211 = null;
	private JCheckBox jCheckBoxPassabilityMapShow = null;
	private JPanel jPanel311 = null;
	private JCheckBox jCheckBoxPreciseTerrainMapShow = null;
	private JPanel jPanel3111 = null;
	private JCheckBox jCheckBoxPathSectorMapShow = null;
	private ColorizedButton colButtonPathSizeMap7 = null;
	private ColorizedButton colButtonPathSectorMapTextColor = null;
	private ColorizedButton colButtonPassabilityMapPassable = null;
	private ColorizedButton colButtonPassabilityMapImpassable = null;
	private ColorizedButton colButtonPassabilityMapBorderWater = null;
	private ColorizedButton colButtonPassabilityMapDeepWater = null;
	private ColorizedButton colButtonPathSizeMap5 = null;
	private ColorizedButton colButtonPathSizeMap3 = null;
	private ColorizedButton colButtonPathSizeMap1 = null;
	private ColorizedButton colButtonPathSizeMap0 = null;
	private ColorizedButton colButtonImpassGenPass = null;
	private ColorizedButton colButtonImpassGenImpass = null;
	private ColorizedButton colButtonPathSectorMapSector1Color;
	private JCheckBox jCheckBoxPathSectorMapShowText;
	private ColorizedButton colButtonPathSectorMapDefaultSectorColor;
	private ColorizedButton colButtonPathSectorMapBordersColor;
	public void close() {
		this.setVisible(false);
	}
	
	public void restoreDefault() {
		jCheckBoxGroundShow.setSelected(terrainDisPars.ground.isVisible);
		jCheckBoxGroundWireframe.setSelected(terrainDisPars.ground.wireframe);
		
		jCheckBoxDecalsShow.setSelected(terrainDisPars.decals.isVisible);
		jCheckBoxDecalsWireframe.setSelected(terrainDisPars.decals.wireframe);
		
		jCheckBoxWaterShow.setSelected(terrainDisPars.water.isVisible);
		jCheckBoxWaterWireframe.setSelected(terrainDisPars.water.wireframe);
		
		jCheckBoxCoverShow.setSelected(terrainDisPars.coverOverlay.isVisible);
		colButtonCoverNone.setColor(terrainDisPars.coverOverlay.noneColor);
		colButtonCoverLight.setColor(terrainDisPars.coverOverlay.lightColor);
		colButtonCoverHeavy.setColor(terrainDisPars.coverOverlay.heavyColor);
		colButtonCoverNegative.setColor(terrainDisPars.coverOverlay.negativeColor);
		colButtonCoverBlocking.setColor(terrainDisPars.coverOverlay.blockingColor);
		colButtonCoverStealth.setColor(terrainDisPars.coverOverlay.stealthColor);
		
		jCheckBoxImpassShow.setSelected(terrainDisPars.impassOverlay.isVisible);
		colButtonImpassPassable.setColor(terrainDisPars.impassOverlay.passableColor);
		colButtonImpassImpassable.setColor(terrainDisPars.impassOverlay.impassableColor);
		colButtonImpassGenPass.setColor(terrainDisPars.impassOverlay.genPassableColor);
		colButtonImpassGenImpass.setColor(terrainDisPars.impassOverlay.genImpassableColor);
		
		jCheckBoxFootfallShow.setSelected(terrainDisPars.footfallOverlay.isVisible);
		colButtonFootfallUnknown.setColor(terrainDisPars.footfallOverlay.unknownColor);
		colButtonFootfallDirtsand.setColor(terrainDisPars.footfallOverlay.dirtsandColor);
		colButtonFootfallRock.setColor(terrainDisPars.footfallOverlay.rockColor);
		colButtonFootfallGrass.setColor(terrainDisPars.footfallOverlay.grassColor);
		colButtonFootfallWater.setColor(terrainDisPars.footfallOverlay.waterColor);
		
		jCheckBoxPassabilityMapShow.setSelected(terrainDisPars.passabilityDisPars.isVisible);
		colButtonPassabilityMapPassable.setColor(terrainDisPars.passabilityDisPars.passableColor);
		colButtonPassabilityMapImpassable.setColor(terrainDisPars.passabilityDisPars.impassableColor);
		colButtonPassabilityMapBorderWater.setColor(terrainDisPars.passabilityDisPars.borderWaterColor);
		colButtonPassabilityMapDeepWater.setColor(terrainDisPars.passabilityDisPars.deepWaterColor);

		jCheckBoxPreciseTerrainMapShow.setSelected(terrainDisPars.preciseDisPars.isVisible);
		colButtonPathSizeMap7.setColor(terrainDisPars.preciseDisPars.path7Color);
		colButtonPathSizeMap5.setColor(terrainDisPars.preciseDisPars.path5Color);
		colButtonPathSizeMap3.setColor(terrainDisPars.preciseDisPars.path3Color);
		colButtonPathSizeMap1.setColor(terrainDisPars.preciseDisPars.path1Color);
		colButtonPathSizeMap0.setColor(terrainDisPars.preciseDisPars.path0Color);

		jCheckBoxPathSectorMapShow.setSelected(terrainDisPars.pathDisPars.isVisible);
		colButtonPathSectorMapDefaultSectorColor.setColor(terrainDisPars.pathDisPars.defaultSectorColor);
		colButtonPathSectorMapSector1Color.setColor(terrainDisPars.pathDisPars.sector1Color);
		colButtonPathSectorMapBordersColor.setColor(terrainDisPars.pathDisPars.borderColor);
		jCheckBoxPathSectorMapShowText.setSelected(terrainDisPars.pathDisPars.showText);
		colButtonPathSectorMapTextColor.setColor(terrainDisPars.pathDisPars.textColor);
		
		repaint();
	}
	
	public void apply() {
		
		terrainDisPars.ground.isVisible=jCheckBoxGroundShow.isSelected();
		terrainDisPars.ground.wireframe=jCheckBoxGroundWireframe.isSelected();
		
		terrainDisPars.decals.isVisible=jCheckBoxDecalsShow.isSelected();
		terrainDisPars.decals.wireframe=jCheckBoxDecalsWireframe.isSelected();
		
		terrainDisPars.water.isVisible=jCheckBoxWaterShow.isSelected();
		terrainDisPars.water.wireframe=jCheckBoxWaterWireframe.isSelected();
		
		terrainDisPars.coverOverlay.isVisible=jCheckBoxCoverShow.isSelected();
		terrainDisPars.coverOverlay.noneColor=colButtonCoverNone.getColor();
		terrainDisPars.coverOverlay.lightColor=colButtonCoverLight.getColor();
		terrainDisPars.coverOverlay.heavyColor=colButtonCoverHeavy.getColor();
		terrainDisPars.coverOverlay.negativeColor=colButtonCoverNegative.getColor();
		terrainDisPars.coverOverlay.blockingColor=colButtonCoverBlocking.getColor();
		terrainDisPars.coverOverlay.stealthColor=colButtonCoverStealth.getColor();
		
		terrainDisPars.impassOverlay.isVisible=jCheckBoxImpassShow.isSelected();
		terrainDisPars.impassOverlay.passableColor=colButtonImpassPassable.getColor();
		terrainDisPars.impassOverlay.impassableColor=colButtonImpassImpassable.getColor();
		terrainDisPars.impassOverlay.genPassableColor=colButtonImpassGenPass.getColor();
		terrainDisPars.impassOverlay.genImpassableColor=colButtonImpassGenImpass.getColor();
		
		terrainDisPars.footfallOverlay.isVisible=jCheckBoxFootfallShow.isSelected();
		terrainDisPars.footfallOverlay.unknownColor=colButtonFootfallUnknown.getColor();
		terrainDisPars.footfallOverlay.dirtsandColor=colButtonFootfallDirtsand.getColor();
		terrainDisPars.footfallOverlay.rockColor=colButtonFootfallRock.getColor();
		terrainDisPars.footfallOverlay.grassColor=colButtonFootfallGrass.getColor();
		terrainDisPars.footfallOverlay.waterColor=colButtonFootfallWater.getColor();	

		terrainDisPars.passabilityDisPars.isVisible=jCheckBoxPassabilityMapShow.isSelected();
		terrainDisPars.passabilityDisPars.passableColor=colButtonPassabilityMapPassable.getColor();	
		terrainDisPars.passabilityDisPars.impassableColor=colButtonPassabilityMapImpassable.getColor();	
		terrainDisPars.passabilityDisPars.borderWaterColor=colButtonPassabilityMapBorderWater.getColor();	
		terrainDisPars.passabilityDisPars.deepWaterColor=colButtonPassabilityMapDeepWater.getColor();	

		terrainDisPars.preciseDisPars.isVisible=jCheckBoxPreciseTerrainMapShow.isSelected();
		terrainDisPars.preciseDisPars.path7Color=colButtonPathSizeMap7.getColor();	
		terrainDisPars.preciseDisPars.path5Color=colButtonPathSizeMap5.getColor();	
		terrainDisPars.preciseDisPars.path3Color=colButtonPathSizeMap3.getColor();	
		terrainDisPars.preciseDisPars.path1Color=colButtonPathSizeMap1.getColor();	
		terrainDisPars.preciseDisPars.path0Color=colButtonPathSizeMap0.getColor();	

		terrainDisPars.pathDisPars.isVisible=jCheckBoxPathSectorMapShow.isSelected();
		terrainDisPars.pathDisPars.defaultSectorColor=colButtonPathSectorMapDefaultSectorColor.getColor();
		terrainDisPars.pathDisPars.sector1Color=colButtonPathSectorMapSector1Color.getColor();
		terrainDisPars.pathDisPars.borderColor=colButtonPathSectorMapBordersColor.getColor();
		terrainDisPars.pathDisPars.showText=jCheckBoxPathSectorMapShowText.isSelected();
		terrainDisPars.pathDisPars.textColor=colButtonPathSectorMapTextColor.getColor();	
		
		terrainDisPars.fireUpdateEvent(1);
	}
	
	public static void showDialog(Frame frame, MapViewer mapViewer) {
		SetGroundDisplayDialog dialog = new SetGroundDisplayDialog(frame);
		dialog.terrainDisPars=mapViewer.getRenderer().mapDisplayParams.terrainDisplayParams;
		dialog.setLocationRelativeTo(mapViewer);
		dialog.restoreDefault();
		dialog.setVisible(true);
		return;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(new BoxLayout(getJPanel(), BoxLayout.X_AXIS));
			jPanel.add(getJPanel4(), null);
			jPanel.add(getJPanel11(), null);
			jPanel.add(getJPanel111(), null);
		}
		return jPanel;
	}

	/**
	 * This method initializes jPanel211	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel211() {
		if (jPanel211 == null) {
			TitledBorder titledBorder21 = BorderFactory.createTitledBorder(null, "Impass overlay", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder21.setTitle("Passability Map");
			FlowLayout flowLayout41 = new FlowLayout();
			flowLayout41.setHgap(10);
			flowLayout41.setAlignment(FlowLayout.LEFT);
			jPanel211 = new JPanel();
			jPanel211.setPreferredSize(new Dimension(0, 20));
			jPanel211.setLayout(flowLayout41);
			jPanel211.setBorder(titledBorder21);
			jPanel211.add(getJCheckBoxPassabilityMapShow(), null);
			jPanel211.add(getColButtonPassabilityMapPassable(), null);
			jPanel211.add(getColButtonPassabilityMapImpassable(), null);
			jPanel211.add(getColButtonPassabilityMapBorderWater(), null);
			jPanel211.add(getColButtonPassabilityMapDeepWater(), null);
		}
		return jPanel211;
	}

	/**
	 * This method initializes jCheckBoxPassabilityMapShow	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxPassabilityMapShow() {
		if (jCheckBoxPassabilityMapShow == null) {
			jCheckBoxPassabilityMapShow = new JCheckBox();
			jCheckBoxPassabilityMapShow.setText("Show");
			jCheckBoxPassabilityMapShow
					.addChangeListener(new javax.swing.event.ChangeListener() {
						public void stateChanged(javax.swing.event.ChangeEvent e) {
							if (jCheckBoxPassabilityMapShow.isSelected()) {
								jCheckBoxCoverShow.setSelected(false);
								jCheckBoxImpassShow.setSelected(false);
								jCheckBoxFootfallShow.setSelected(false);
								jCheckBoxPreciseTerrainMapShow.setSelected(false);
								jCheckBoxPathSectorMapShow.setSelected(false);
							}
						}
					});
		}
		return jCheckBoxPassabilityMapShow;
	}

	/**
	 * This method initializes jPanel311	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel311() {
		if (jPanel311 == null) {
			TitledBorder titledBorder11 = BorderFactory.createTitledBorder(null, "Cover overlay", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder11.setTitle("PathSize Map (Maximum size of unit allowed to access position)");
			FlowLayout flowLayout31 = new FlowLayout();
			flowLayout31.setHgap(10);
			flowLayout31.setAlignment(FlowLayout.LEFT);
			jPanel311 = new JPanel();
			jPanel311.setPreferredSize(new Dimension(0, 20));
			jPanel311.setLayout(flowLayout31);
			jPanel311.setBorder(titledBorder11);
			jPanel311.add(getJCheckBoxPreciseTerrainMapShow(), null);
			jPanel311.add(getColButtonPathSizeMap7(), null);
			jPanel311.add(getColButtonPathSizeMap5(), null);
			jPanel311.add(getColButtonPathSizeMap3(), null);
			jPanel311.add(getColButtonPathSizeMap1(), null);
			jPanel311.add(getColButtonPathSizeMap0(), null);
		}
		return jPanel311;
	}

	/**
	 * This method initializes jCheckBoxPreciseTerrainMapShow	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxPreciseTerrainMapShow() {
		if (jCheckBoxPreciseTerrainMapShow == null) {
			jCheckBoxPreciseTerrainMapShow = new JCheckBox();
			jCheckBoxPreciseTerrainMapShow.setText("Show");
			jCheckBoxPreciseTerrainMapShow
					.addChangeListener(new javax.swing.event.ChangeListener() {
						public void stateChanged(javax.swing.event.ChangeEvent e) {
							if (jCheckBoxPreciseTerrainMapShow.isSelected()) {
								jCheckBoxCoverShow.setSelected(false);
								jCheckBoxImpassShow.setSelected(false);
								jCheckBoxFootfallShow.setSelected(false);
								jCheckBoxPassabilityMapShow.setSelected(false);
								jCheckBoxPathSectorMapShow.setSelected(false);
							}
						}
					});
		}
		return jCheckBoxPreciseTerrainMapShow;
	}

	/**
	 * This method initializes jPanel3111	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel3111() {
		if (jPanel3111 == null) {
			TitledBorder titledBorder111 = BorderFactory.createTitledBorder(null, "Cover overlay", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder111.setTitle("Path Sector Map");
			FlowLayout flowLayout311 = new FlowLayout();
			flowLayout311.setHgap(10);
			flowLayout311.setAlignment(FlowLayout.LEFT);
			jPanel3111 = new JPanel();
			jPanel3111.setPreferredSize(new Dimension(0, 20));
			jPanel3111.setLayout(flowLayout311);
			jPanel3111.setBorder(new TitledBorder(null, "Path sector overlay", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			jPanel3111.add(getJCheckBoxPathSectorMapShow(), null);
			jPanel3111.add(getColButtonPathSectorMapDefaultSectorColor());
			jPanel3111.add(getColButtonPathSectorMapInaccessible());
			jPanel3111.add(getColButtonPathSectorMapBordersColor());
			jPanel3111.add(getJCheckBoxPathSectorMapShowText());
			jPanel3111.add(getColButtonPathSectorMapTextColor(), null);
		}
		return jPanel3111;
	}

	/**
	 * This method initializes jCheckBoxPathSectorMapShow	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxPathSectorMapShow() {
		if (jCheckBoxPathSectorMapShow == null) {
			jCheckBoxPathSectorMapShow = new JCheckBox();
			jCheckBoxPathSectorMapShow.setText("Show");
			jCheckBoxPathSectorMapShow
					.addChangeListener(new javax.swing.event.ChangeListener() {
						public void stateChanged(javax.swing.event.ChangeEvent e) {
							if (jCheckBoxPathSectorMapShow.isSelected()) {
								jCheckBoxCoverShow.setSelected(false);
								jCheckBoxImpassShow.setSelected(false);
								jCheckBoxFootfallShow.setSelected(false);
								jCheckBoxPassabilityMapShow.setSelected(false);
								jCheckBoxPreciseTerrainMapShow.setSelected(false);
							}
						}
					});
		}
		return jCheckBoxPathSectorMapShow;
	}

	/**
	 * This method initializes colButtonPathSizeMap7	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonPathSizeMap7() {
		if (colButtonPathSizeMap7 == null) {
			colButtonPathSizeMap7 = new ColorizedButton();
			colButtonPathSizeMap7.setPreferredSize(new Dimension(100, 25));
			colButtonPathSizeMap7.setColor(Color.green);
			colButtonPathSizeMap7.setText("Size >= 7");
		}
		return colButtonPathSizeMap7;
	}

	/**
	 * This method initializes colButtonPathSectorMap	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonPathSectorMapTextColor() {
		if (colButtonPathSectorMapTextColor == null) {
			colButtonPathSectorMapTextColor = new ColorizedButton();
			colButtonPathSectorMapTextColor.setPreferredSize(new Dimension(100, 25));
			colButtonPathSectorMapTextColor.setColor(Color.white);
			colButtonPathSectorMapTextColor.setText("Text color");
		}
		return colButtonPathSectorMapTextColor;
	}

	/**
	 * This method initializes colButtonPassabilityMapPassable	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonPassabilityMapPassable() {
		if (colButtonPassabilityMapPassable == null) {
			colButtonPassabilityMapPassable = new ColorizedButton();
			colButtonPassabilityMapPassable.setPreferredSize(new Dimension(100, 25));
			colButtonPassabilityMapPassable.setColor(Color.green);
			colButtonPassabilityMapPassable.setText("Passable");
		}
		return colButtonPassabilityMapPassable;
	}

	/**
	 * This method initializes colButtonPassabilityMapImpassable	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonPassabilityMapImpassable() {
		if (colButtonPassabilityMapImpassable == null) {
			colButtonPassabilityMapImpassable = new ColorizedButton();
			colButtonPassabilityMapImpassable.setPreferredSize(new Dimension(100, 25));
			colButtonPassabilityMapImpassable.setColor(Color.red);
			colButtonPassabilityMapImpassable.setText("Impassable");
		}
		return colButtonPassabilityMapImpassable;
	}

	/**
	 * This method initializes colButtonPassabilityMapBorderWater	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonPassabilityMapBorderWater() {
		if (colButtonPassabilityMapBorderWater == null) {
			colButtonPassabilityMapBorderWater = new ColorizedButton();
			colButtonPassabilityMapBorderWater.setPreferredSize(new Dimension(100, 25));
			colButtonPassabilityMapBorderWater.setColor(Color.cyan);
			colButtonPassabilityMapBorderWater.setText("Water Bord.");
		}
		return colButtonPassabilityMapBorderWater;
	}

	/**
	 * This method initializes colButtonPassabilityMapDeepWater	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonPassabilityMapDeepWater() {
		if (colButtonPassabilityMapDeepWater == null) {
			colButtonPassabilityMapDeepWater = new ColorizedButton();
			colButtonPassabilityMapDeepWater.setPreferredSize(new Dimension(100, 25));
			colButtonPassabilityMapDeepWater.setColor(new Color(0, 153, 153));
			colButtonPassabilityMapDeepWater.setText("Deep Water");
		}
		return colButtonPassabilityMapDeepWater;
	}

	/**
	 * This method initializes colButtonPathSizeMap5	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonPathSizeMap5() {
		if (colButtonPathSizeMap5 == null) {
			colButtonPathSizeMap5 = new ColorizedButton();
			colButtonPathSizeMap5.setPreferredSize(new Dimension(100, 25));
			colButtonPathSizeMap5.setColor(Color.yellow);
			colButtonPathSizeMap5.setText("Size 5 Max.");
		}
		return colButtonPathSizeMap5;
	}

	/**
	 * This method initializes colButtonPathSizeMap3	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonPathSizeMap3() {
		if (colButtonPathSizeMap3 == null) {
			colButtonPathSizeMap3 = new ColorizedButton();
			colButtonPathSizeMap3.setPreferredSize(new Dimension(100, 25));
			colButtonPathSizeMap3.setColor(Color.white);
			colButtonPathSizeMap3.setText("Size 3 Max.");
		}
		return colButtonPathSizeMap3;
	}

	/**
	 * This method initializes colButtonPathSizeMap1	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonPathSizeMap1() {
		if (colButtonPathSizeMap1 == null) {
			colButtonPathSizeMap1 = new ColorizedButton();
			colButtonPathSizeMap1.setPreferredSize(new Dimension(100, 25));
			colButtonPathSizeMap1.setColor(Color.white);
			colButtonPathSizeMap1.setText("Size 1 Max.");
		}
		return colButtonPathSizeMap1;
	}

	/**
	 * This method initializes colButtonPathSizeMap0	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonPathSizeMap0() {
		if (colButtonPathSizeMap0 == null) {
			colButtonPathSizeMap0 = new ColorizedButton();
			colButtonPathSizeMap0.setPreferredSize(new Dimension(100, 25));
			colButtonPathSizeMap0.setColor(Color.white);
			colButtonPathSizeMap0.setText("No Access");
		}
		return colButtonPathSizeMap0;
	}

	/**
	 * This method initializes colButtonImpassGenPass	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonImpassGenPass() {
		if (colButtonImpassGenPass == null) {
			colButtonImpassGenPass = new ColorizedButton();
			colButtonImpassGenPass.setPreferredSize(new Dimension(100, 25));
			colButtonImpassGenPass.setColor(Color.green);
			colButtonImpassGenPass.setText("Gen. Pass");
		}
		return colButtonImpassGenPass;
	}

	/**
	 * This method initializes colButtonImpassGenImpass	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonImpassGenImpass() {
		if (colButtonImpassGenImpass == null) {
			colButtonImpassGenImpass = new ColorizedButton();
			colButtonImpassGenImpass.setPreferredSize(new Dimension(100, 25));
			colButtonImpassGenImpass.setColor(Color.red);
			colButtonImpassGenImpass.setText("Gen. Impass");
		}
		return colButtonImpassGenImpass;
	}
	
	private ColorizedButton getColButtonPathSectorMapInaccessible() {
		if (colButtonPathSectorMapSector1Color == null) {
			colButtonPathSectorMapSector1Color = new ColorizedButton();
			colButtonPathSectorMapSector1Color.setText("Sector = 1");
			colButtonPathSectorMapSector1Color.setPreferredSize(new Dimension(100, 25));
			colButtonPathSectorMapSector1Color.setColor(Color.RED);
		}
		return colButtonPathSectorMapSector1Color;
	}
	private JCheckBox getJCheckBoxPathSectorMapShowText() {
		if (jCheckBoxPathSectorMapShowText == null) {
			jCheckBoxPathSectorMapShowText = new JCheckBox();
			jCheckBoxPathSectorMapShowText.setVerticalAlignment(SwingConstants.CENTER);
			jCheckBoxPathSectorMapShowText.setText("Show sector nbr.");
		}
		return jCheckBoxPathSectorMapShowText;
	}
	private ColorizedButton getColButtonPathSectorMapDefaultSectorColor() {
		if (colButtonPathSectorMapDefaultSectorColor == null) {
			colButtonPathSectorMapDefaultSectorColor = new ColorizedButton();
			colButtonPathSectorMapDefaultSectorColor.setText("Sector >1");
			colButtonPathSectorMapDefaultSectorColor.setPreferredSize(new Dimension(100, 25));
			colButtonPathSectorMapDefaultSectorColor.setColor(Color.LIGHT_GRAY);
		}
		return colButtonPathSectorMapDefaultSectorColor;
	}
	private ColorizedButton getColButtonPathSectorMapBordersColor() {
		if (colButtonPathSectorMapBordersColor == null) {
			colButtonPathSectorMapBordersColor = new ColorizedButton();
			colButtonPathSectorMapBordersColor.setText("Borders");
			colButtonPathSectorMapBordersColor.setPreferredSize(new Dimension(100, 25));
			colButtonPathSectorMapBordersColor.setColor(Color.GREEN);
		}
		return colButtonPathSectorMapBordersColor;
	}
}  //  @jve:decl-index=0:visual-constraint="10,10"
