package fr.dow.mapeditor.dialogs.setdisplay;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;
import fr.dow.mapeditor.tools.ColorizedButton;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.params.MarkersDisPars;

public class SetMarkerDisplayDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanel7 = null;
	private JPanel jPanel8 = null;
	private JButton jButton3 = null;
	private JPanel jPanel9 = null;
	private JButton jButton11 = null;
	private JButton jButton21 = null;
	private JPanel jPanel4 = null;
	private JCheckBox jCheckBoxShowMarkers = null;
	private JCheckBox jCheckBoxShowProxRadius = null;
	private ColorizedButton colButtonTextColor = null;
	private JCheckBox jCheckBoxShowTitle = null;

	/**
	 * @param owner
	 */
	public SetMarkerDisplayDialog(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(432, 289);
		this.setTitle("Markers display parameters");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel7(), BorderLayout.SOUTH);
			jContentPane.add(getJPanel4(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel7	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel7() {
		if (jPanel7 == null) {
			jPanel7 = new JPanel();
			jPanel7.setLayout(new BorderLayout());
			jPanel7.add(getJPanel8(), java.awt.BorderLayout.WEST);
			jPanel7.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanel7;
	}

	/**
	 * This method initializes jPanel8	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel8() {
		if (jPanel8 == null) {
			FlowLayout flowLayout6 = new FlowLayout();
			flowLayout6.setHgap(10);
			jPanel8 = new JPanel();
			jPanel8.setLayout(flowLayout6);
			jPanel8.add(getJButton3(), null);
		}
		return jPanel8;
	}

	/**
	 * This method initializes jButton3	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton3() {
		if (jButton3 == null) {
			jButton3 = new JButton();
			jButton3.setPreferredSize(new Dimension(100, 23));
			jButton3.setText("Default");
			jButton3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					restoreDefault();
				}
			});
		}
		return jButton3;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButton11(), null);
			jPanel9.add(getJButton21(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButton11	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton11() {
		if (jButton11 == null) {
			jButton11 = new JButton();
			jButton11.setPreferredSize(new Dimension(100, 23));
			jButton11.setText("Apply");
			jButton11.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					apply();
				}
			});
		}
		return jButton11;
	}

	/**
	 * This method initializes jButton21	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton21() {
		if (jButton21 == null) {
			jButton21 = new JButton();
			jButton21.setPreferredSize(new Dimension(100, 23));
			jButton21.setText("Close");
			jButton21.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					close();
				}
			});
		}
		return jButton21;
	}

	/**
	 * This method initializes jPanel4	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel4() {
		if (jPanel4 == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Ground texture", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51));
			titledBorder.setTitle("Markers");
			jPanel4 = new JPanel();
			jPanel4.setPreferredSize(new Dimension(0, 20));
			jPanel4.setLayout(new BoxLayout(getJPanel4(), BoxLayout.Y_AXIS));
			jPanel4.setBorder(titledBorder);
			jPanel4.add(getJCheckBoxShowMarkers(), null);
			jPanel4.add(getJCheckBoxShowProxRadius(), null);
			jPanel4.add(getJCheckBoxShowTitle(), null);
			jPanel4.add(getColButtonTextColor(), null);
		}
		return jPanel4;
	}

	/**
	 * This method initializes jCheckBoxShowMarkers	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxShowMarkers() {
		if (jCheckBoxShowMarkers == null) {
			jCheckBoxShowMarkers = new JCheckBox();
			jCheckBoxShowMarkers.setSelected(true);
			jCheckBoxShowMarkers.setVerticalAlignment(SwingConstants.CENTER);
			jCheckBoxShowMarkers.setText("Show Markers");
		}
		return jCheckBoxShowMarkers;
	}

	/**
	 * This method initializes jCheckBoxShowProxRadius	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxShowProxRadius() {
		if (jCheckBoxShowProxRadius == null) {
			jCheckBoxShowProxRadius = new JCheckBox();
			jCheckBoxShowProxRadius.setText("Show Proximity Radius");
			jCheckBoxShowProxRadius.setSelected(true);
			jCheckBoxShowProxRadius.setVerticalAlignment(SwingConstants.CENTER);
		}
		return jCheckBoxShowProxRadius;
	}

	/**
	 * This method initializes colButtonTextColor	
	 * 	
	 * @return fr.dow.mapeditor.tools.ColorizedButton	
	 */
	private ColorizedButton getColButtonTextColor() {
		if (colButtonTextColor == null) {
			colButtonTextColor = new ColorizedButton();
			colButtonTextColor.setPreferredSize(new Dimension(100, 25));
			colButtonTextColor.setColor(Color.white);
			colButtonTextColor.setText("Text color");
		}
		return colButtonTextColor;
	}

	/**
	 * This method initializes jCheckBoxShowTitle	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxShowTitle() {
		if (jCheckBoxShowTitle == null) {
			jCheckBoxShowTitle = new JCheckBox();
			jCheckBoxShowTitle.setSelected(true);
			jCheckBoxShowTitle.setVerticalAlignment(SwingConstants.CENTER);
			jCheckBoxShowTitle.setText("Show Marker name");
		}
		return jCheckBoxShowTitle;
	}

	private MarkersDisPars markerDisPars;
	
	public void close() {
		this.setVisible(false);
	}

	public void restoreDefault() {
		jCheckBoxShowMarkers.setSelected(markerDisPars.showMarkers);
		jCheckBoxShowProxRadius.setSelected(markerDisPars.showProxRadius);
		jCheckBoxShowTitle.setSelected(markerDisPars.showTitle);
		colButtonTextColor.setColor(markerDisPars.textColor);
	}

	public void apply() {
		markerDisPars.showMarkers=jCheckBoxShowMarkers.isSelected();
		markerDisPars.showProxRadius=jCheckBoxShowProxRadius.isSelected();
		markerDisPars.showTitle=jCheckBoxShowTitle.isSelected();
		markerDisPars.textColor=colButtonTextColor.getColor();
		markerDisPars.fireUpdateEvent(1);
	}
	
	public static void showDialog(Frame frame, MapViewer mapViewer) {
		SetMarkerDisplayDialog dialog = new SetMarkerDisplayDialog(frame);
		dialog.markerDisPars=mapViewer.getRenderer().mapDisplayParams.markerDisPars;
		dialog.setLocationRelativeTo(mapViewer);
		dialog.restoreDefault();
		dialog.setVisible(true);
		return;
	}
	
}  //  @jve:decl-index=0:visual-constraint="10,10"
