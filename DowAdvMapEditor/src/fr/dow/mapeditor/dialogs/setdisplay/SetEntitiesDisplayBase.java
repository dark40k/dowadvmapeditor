package fr.dow.mapeditor.dialogs.setdisplay;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JCheckBox;

public abstract class SetEntitiesDisplayBase extends JDialog {

	private static final long serialVersionUID = 1L;
	protected JPanel jContentPane = null;
	protected JPanel jPanelAction = null;
	protected JPanel jPanel9 = null;
	protected JButton jButtonApply = null;
	protected JPanel jPanelEntityPreview = null;
	/**
	 * @param owner
	 */
	public SetEntitiesDisplayBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(1028, 663);
		this.setModal(true);
		this.setTitle("Select Entity Blueprint - Mod=");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanelAction(), BorderLayout.SOUTH);
			jContentPane.add(getJSplitPane(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanelAction	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelAction() {
		if (jPanelAction == null) {
			jPanelAction = new JPanel();
			jPanelAction.setLayout(new BorderLayout());
			jPanelAction.add(getJPanel9(), java.awt.BorderLayout.EAST);
			jPanelAction.add(getJPanelLeft(), BorderLayout.WEST);
		}
		return jPanelAction;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonApply(), null);
			jPanel9.add(getJButtonClose(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonApply	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonApply() {
		if (jButtonApply == null) {
			jButtonApply = new JButton();
			jButtonApply.setPreferredSize(new Dimension(100, 23));
			jButtonApply.setText("Apply");
		}
		return jButtonApply;
	}

	/**
	 * This method initializes jPanelEntityPreview	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelEntityPreview() {
		if (jPanelEntityPreview == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Entity preview");
			jPanelEntityPreview = new JPanel();
			jPanelEntityPreview.setLayout(new BorderLayout());
			jPanelEntityPreview.setEnabled(true);
			jPanelEntityPreview.setBorder(titledBorder);
			jPanelEntityPreview.addComponentListener(new java.awt.event.ComponentAdapter() {
				public void componentResized(java.awt.event.ComponentEvent e) {
					jPanelEntityPreview.setMinimumSize(new Dimension(10,10));
				}
			});
		}
		return jPanelEntityPreview;
	}

	protected JPanel jPanelBPSelection = null;
	protected JButton jButtonClose = null;
	protected JScrollPane jScrollPaneParts = null;
	protected JSplitPane jSplitPane = null;
	protected JPanel jPanelLeft = null;
	protected JSplitPane jSplitPane1 = null;
	protected JPanel jPanelParts = null;
	protected JPanel jPanelEntities = null;
	protected JScrollPane jScrollPaneEntities = null;
	protected JCheckBox jCheckBoxFilterSingle = null;
	/**
	 * This method initializes jPanelBPSelection	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelBPSelection() {
		if (jPanelBPSelection == null) {
			jPanelBPSelection = new JPanel();
			jPanelBPSelection.setLayout(new BorderLayout());
			jPanelBPSelection.setPreferredSize(new Dimension(200, 453));
			jPanelBPSelection.setBorder(BorderFactory.createTitledBorder(null, "Blueprint Selection", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213)));
			jPanelBPSelection.add(getJSplitPane1(), BorderLayout.CENTER);
		}
		return jPanelBPSelection;
	}

	/**
	 * This method initializes jButtonClose	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonClose() {
		if (jButtonClose == null) {
			jButtonClose = new JButton();
			jButtonClose.setPreferredSize(new Dimension(100, 23));
			jButtonClose.setText("Close");
		}
		return jButtonClose;
	}

	/**
	 * This method initializes jScrollPaneParts	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPaneParts() {
		if (jScrollPaneParts == null) {
			jScrollPaneParts = new JScrollPane();
		}
		return jScrollPaneParts;
	}

	/**
	 * This method initializes jSplitPane	
	 * 	
	 * @return javax.swing.JSplitPane	
	 */
	private JSplitPane getJSplitPane() {
		if (jSplitPane == null) {
			jSplitPane = new JSplitPane();
			jSplitPane.setDividerLocation(400);
			jSplitPane.setContinuousLayout(true);
			jSplitPane.setLeftComponent(getJPanelBPSelection());
			jSplitPane.setRightComponent(getJPanelEntityPreview());
		}
		return jSplitPane;
	}

	/**
	 * This method initializes jPanelLeft	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelLeft() {
		if (jPanelLeft == null) {
			FlowLayout flowLayout71 = new FlowLayout();
			flowLayout71.setHgap(10);
			jPanelLeft = new JPanel();
			jPanelLeft.setLayout(flowLayout71);
		}
		return jPanelLeft;
	}

	/**
	 * This method initializes jSplitPane1	
	 * 		 * @return javax.swing.JSplitPane	
	 */
	private JSplitPane getJSplitPane1() {
		if (jSplitPane1 == null) {
			jSplitPane1 = new JSplitPane();
			jSplitPane1.setDividerLocation(200);
			jSplitPane1.setTopComponent(getJPanelParts());
			jSplitPane1.setBottomComponent(getJPanelEntities());
			jSplitPane1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		}
		return jSplitPane1;
	}

	/**
	 * This method initializes jPanelParts	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelParts() {
		if (jPanelParts == null) {
			TitledBorder titledBorder1 = BorderFactory.createTitledBorder(null, "Bones visisbilty", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder1.setTitle("Entity's Parts");
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.fill = GridBagConstraints.BOTH;
			gridBagConstraints.weighty = 1.0;
			gridBagConstraints.weightx = 1.0;
			jPanelParts = new JPanel();
			jPanelParts.setLayout(new GridBagLayout());
			jPanelParts.setBorder(titledBorder1);
			jPanelParts.add(getJScrollPaneParts(), gridBagConstraints);
		}
		return jPanelParts;
	}

	/**
	 * This method initializes jPanelEntities	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelEntities() {
		if (jPanelEntities == null) {
			TitledBorder titledBorder2 = BorderFactory.createTitledBorder(null, "Loaded entities list", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder2.setTitle("Entities");
			jPanelEntities = new JPanel();
			jPanelEntities.setLayout(new BorderLayout());
			jPanelEntities.setBorder(titledBorder2);
			jPanelEntities.add(getJScrollPaneEntities(), BorderLayout.CENTER);
			jPanelEntities.add(getJCheckBoxFilterSingle(), BorderLayout.NORTH);
		}
		return jPanelEntities;
	}

	/**
	 * This method initializes jScrollPaneEntities	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPaneEntities() {
		if (jScrollPaneEntities == null) {
			jScrollPaneEntities = new JScrollPane();
		}
		return jScrollPaneEntities;
	}

	/**
	 * This method initializes jCheckBoxFilterSingle	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxFilterSingle() {
		if (jCheckBoxFilterSingle == null) {
			jCheckBoxFilterSingle = new JCheckBox();
			jCheckBoxFilterSingle.setText("Filter single part entities");
		}
		return jCheckBoxFilterSingle;
	}
		
	//
	// End of custom part 
	//

}  //  @jve:decl-index=0:visual-constraint="10,10"
