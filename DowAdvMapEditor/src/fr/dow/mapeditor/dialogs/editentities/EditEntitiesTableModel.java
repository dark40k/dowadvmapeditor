package fr.dow.mapeditor.dialogs.editentities;

import java.util.HashSet;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.sgbmap.units.Entities;
import fr.dow.gamedata.sgbmap.units.Entity;
import fr.dow.gamedata.sgbmap.units.EntityBlueprints;
import fr.dow.gamedata.sgbmap.units.PlayerID;
import fr.dow.gamedata.sgbmap.units.Squads;
import fr.dow.mapviewer.display.MapViewer;

public class EditEntitiesTableModel extends AbstractTableModel {

	  static enum EditEntitiesTableColumnsEnum {
	    VALID("Valid") {
			@Override public Object getValue(EntityData entityData) { return (Boolean) entityData.isValid; }
			@Override public Class<?> getColumnClass() { return Boolean.class; }
			@Override public boolean isEditable() { return false; }
	    },
		ID("Id") {
			@Override public Object getValue(EntityData entityData) {	return entityData.id; }
			@Override public Class<?> getColumnClass() { return Integer.class; }
			@Override public boolean isEditable() { return false; }
	    }, 
		BLUEPRINT("Blueprint") {
			@Override public Object getValue(EntityData entityData) {	return entityData.blueprint; }
			@Override public Class<?> getColumnClass() { return String.class; }
			@Override public boolean isEditable() { return false; }
	    }, 
		PLAYER("Player") {
			@Override public Object getValue(EntityData entityData) {	return entityData.playerID; }
			@Override public Class<?> getColumnClass() { return Integer.class; }
			@Override public boolean isEditable() { return false; }
	    }, 
		X("X") {
			@Override public Object getValue(EntityData entityData) { return entityData.x; }
			@Override public Class<?> getColumnClass() { return Float.class; }
			@Override public void setValue(EntityData entityData, Object value) { entityData.setX((Float)value); }
	    }, 
		Y("Y") {
			@Override public Object getValue(EntityData entityData) {	return entityData.y; }
			@Override public Class<?> getColumnClass() { return Float.class; }
			@Override public void setValue(EntityData entityData, Object value)  { entityData.setY((Float)value); }
	    }, 
		Z("Z") {
			@Override public Object getValue(EntityData entityData) {	return entityData.z; }
			@Override public Class<?> getColumnClass() { return Float.class; }
			@Override public void setValue(EntityData entityData, Object value)  { entityData.setZ((Float)value); }
	    },
		HEAD_DEG("HeadDeg") {
			@Override public Object getValue(EntityData entityData) {	return entityData.headingDeg; }
			@Override public Class<?> getColumnClass() { return Double.class; }
			@Override public void setValue(EntityData entityData, Object value)  { entityData.setHeadingDeg((Double)value); }
	    },
		PITCH_DEG("PitchDeg") {
			@Override public Object getValue(EntityData entityData) {	return entityData.pitchDeg; }
			@Override public Class<?> getColumnClass() { return Double.class; }
			@Override public void setValue(EntityData entityData, Object value)  { entityData.setPitchDeg((Double)value); }
	    },
		ROLL_DEG("RollDeg") {
			@Override public Object getValue(EntityData entityData) {	return entityData.rollDeg; }
			@Override public Class<?> getColumnClass() { return Double.class; }
			@Override public void setValue(EntityData entityData, Object value)  { entityData.setRollDeg((Double)value); }
	    },
		SCALEX("ScaleX") {
			@Override public Object getValue(EntityData entityData) {	return entityData.sX; }
			@Override public Class<?> getColumnClass() { return Double.class; }
			@Override public void setValue(EntityData entityData, Object value)  { entityData.setsX((Double)value); }
	    },
		SCALEY("ScaleY") {
			@Override public Object getValue(EntityData entityData) {	return entityData.sY; }
			@Override public Class<?> getColumnClass() { return Double.class; }
			@Override public void setValue(EntityData entityData, Object value)  { entityData.setsY((Double)value); }
	    },
		SCALEZ("ScaleZ") {
			@Override public Object getValue(EntityData entityData) {	return entityData.sZ; }
			@Override public Class<?> getColumnClass() { return Double.class; }
			@Override public void setValue(EntityData entityData, Object value)  { entityData.setsZ((Double)value); }
	    }, 
		ADJUSTABLEHEIGHT("Adjustable Height") {
			@Override public Object getValue(EntityData entityData) { return (Boolean) entityData.adjustableHeight; }
			@Override public Class<?> getColumnClass() { return Boolean.class; }
			@Override public void setValue(EntityData entityData, Object value)  { entityData.setAdjustableHeight((Boolean)value); }
	    };

	    // Internal Data
		private String headerName;
	
		// Constructor
		private EditEntitiesTableColumnsEnum(String headerName) {
			this.headerName = headerName;
		}

	    // Data
	    public abstract Object getValue(EntityData entityData);
	    public abstract Class<?> getColumnClass();
		public boolean isEditable() { return true; }
		public void setValue(EntityData entityData, Object value) { throw new RuntimeException("Column is not editable:" + headerName); }
		
	    // Header
		public String getHeaderDisplayName() { return headerName; }
	    
	  };
	
	private MapViewer mapViewer;
	
	public class EntityData {
		Integer id;
		Boolean isValid;
		Float x,y,z;
		Double headingDeg,pitchDeg,rollDeg;
		Double sX,sY,sZ;
		String blueprint;
		PlayerID playerID;
		Boolean adjustableHeight;
				
		//
		// Setters
		//
		public void setX(Float x) {
			this.x = x;
			updatePosition();
			fireTableDataChanged();
		}
		public void setY(Float y) {
			this.y = y;
			updatePosition();
			fireTableDataChanged();
		}
		public void setZ(Float z) {
			this.z = z;
			updatePosition();
			fireTableDataChanged();
		}
		public void setHeadingDeg(Double heading) {
			this.headingDeg = heading;
			updateAngles();
			fireTableDataChanged();
		}
		public void setPitchDeg(Double pitch) {
			this.pitchDeg = pitch;
			updateAngles();
			fireTableDataChanged();
		}
		public void setRollDeg(Double roll) {
			this.rollDeg = roll;
			updateAngles();
			fireTableDataChanged();
		}
		public void setsX(Double sX) {
			this.sX = sX;
			updateScales();
			fireTableDataChanged();
		}
		public void setsY(Double sY) {
			this.sY = sY;
			updateScales();
			fireTableDataChanged();
		}
		public void setsZ(Double sZ) {
			this.sZ = sZ;
			updateScales();
			fireTableDataChanged();
		}
		public void setAdjustableHeight(Boolean adjustableHeight) {
			this.adjustableHeight = adjustableHeight;
			getEntity().setMeAdjustableHeightStatus(adjustableHeight);
			fireTableDataChanged();
		}
		
		public Entity getEntity() { return mapViewer.getMap().entities.get(id); }
		
		public void updatePosition() { getEntity().setPosition(x, y, z); mapViewer.getRenderer().mapDisplayParams.playerDisplayParams.fireUpdateEvent(1); }
		public void updateAngles() { getEntity().setAnglesRad( Math.toRadians(headingDeg), Math.toRadians(pitchDeg), Math.toRadians(rollDeg)); mapViewer.getRenderer().mapDisplayParams.playerDisplayParams.fireUpdateEvent(1); }
		public void updateScales() { getEntity().setScale(sX, sY, sZ); mapViewer.getRenderer().mapDisplayParams.playerDisplayParams.fireUpdateEvent(1); }
		
	}
	
	Vector<EntityData> mapDataLines=new Vector<EntityData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
	private EditEntitiesTableColumnsEnum getColumn(int columnIndex) {
		return EditEntitiesTableColumnsEnum.values()[columnIndex];
	}
	
	@Override
	public int getColumnCount() { return EditEntitiesTableColumnsEnum.values().length; }
	
    public Class<?> getColumnClass(int col) {return getColumn(col).getColumnClass(); }
    
    public String getColumnName(int col) { return getColumn(col).getHeaderDisplayName(); }
    
	@Override
	public int getRowCount() { return mapDataLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (mapDataLines==null) return null;
		if (rowIndex>=mapDataLines.size()) return null;
		return getColumn(columnIndex).getValue(mapDataLines.get(rowIndex));
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex>=mapDataLines.size()) return false;
		return getColumn(columnIndex).isEditable();
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		setValueAtColumn(aValue, rowIndex, getColumn(columnIndex));
	}
	
	public void setValueAtColumn(Object aValue, int rowIndex, EditEntitiesTableColumnsEnum column) {
		if (rowIndex>=mapDataLines.size()) return;
		column.setValue(mapDataLines.get(rowIndex), aValue);
	}
		
	public void updateEntityTable(boolean onlySelected) {
		
		Vector<EntityData> newDataLines=new Vector<EntityData>();
		
		EntityBlueprints entityBlueprints=mapViewer.getMap().entityBlueprints;
		Entities entities=mapViewer.getMap().entities;
		
		Squads squads=mapViewer.getMap().squads;
		
		HashSet<Integer> selectedEntities = mapViewer.getRenderer().currentSelection.entities;
		
		for (Integer entityID : entities.keySet())	{
			
			if (squads.getSquadID(entityID)!=null) continue;
			
			if (onlySelected)
				if (!selectedEntities.contains(entityID))
					continue;
			
			Entity entity=entities.get(entityID);
			
			EntityData entityData=new EntityData();
			entityData.id=entityID;
			entityData.blueprint=entityBlueprints.get(entity.getBlueprintIndex()).replace("\\", "/").replace("data:attrib/ebps/","");
			entityData.playerID=entity.getPlayerID();			
			entityData.x=entity.getX();
			entityData.y=entity.getY();
			entityData.z=entity.getZ();
			entityData.headingDeg=Math.toDegrees(entity.getHeadingRad());
			entityData.pitchDeg=Math.toDegrees(entity.getPitchRad());
			entityData.rollDeg=Math.toDegrees(entity.getRollRad());
			entityData.sX=entity.getSX();
			entityData.sY=entity.getSY();
			entityData.sZ=entity.getSZ();
			entityData.adjustableHeight=entity.getMeAdjustableHeightStatus();
			
			entityData.isValid=mapViewer.getMod().isValidFile(entityBlueprints.get(entity.getBlueprintIndex()).substring(5).replace("\\", "/"));
			
			newDataLines.add(entityData);
			
		}
		
		mapDataLines=newDataLines;
		fireTableDataChanged();
		
	}
	
	public void setMapViewer(MapViewer mapViewer, boolean onlySelected) {
		this.mapViewer=mapViewer;
		updateEntityTable(onlySelected);
	}	
			
}
