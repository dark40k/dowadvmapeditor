package fr.dow.mapeditor.dialogs.editentities;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.dow.gamedata.sgbmap.units.Entities;
import fr.dow.gamedata.sgbmap.units.Entity;
import fr.dow.gamedata.sgbmap.units.EntityBlueprints;
import fr.dow.gamedata.sgbmap.units.PlayerID;
import fr.dow.gamedata.sgbmap.units.Squads;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.editentities.EditEntitiesTableModel.EditEntitiesTableColumnsEnum;
import fr.dow.mapeditor.dialogs.selectentitylblueprint.SelectEntityBP;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeEvent;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeListener;

public class EditEntitiesDialog extends EditEntitiesDialogBase implements ActionListener, SelectionChangeListener {

	MapViewer mapViewer;

	EditEntitiesTableModel entitiesTable;

	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		EditEntitiesDialog dialog = new EditEntitiesDialog(mapEditor, mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		mapViewer.getRenderer().curFilter.lockAllExcept(FilterType.ENTITY);
		dialog.setVisible(true);
		return;
	}

	public void close() {
		mapViewer.getRenderer().curFilter.unlockAll();
		this.setVisible(false);
	}

	public EditEntitiesDialog(MapEditor mapEditor, MapViewer mapViewer) {
		super(mapEditor);

		this.mapViewer = mapViewer;

		entitiesTable = (EditEntitiesTableModel) jTableMapList.getModel();
		entitiesTable.setMapViewer(mapViewer, jCheckBoxMapSelectionFilter.isSelected());

		jTableMapList.setAutoCreateRowSorter(true);
		jTableMapList.getColumnModel().getColumn(0).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(1).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(2).setPreferredWidth(250);
		jTableMapList.getColumnModel().getColumn(3).setPreferredWidth(25);
		jTableMapList.getColumnModel().getColumn(4).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(5).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(6).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(7).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(8).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(9).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(10).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(11).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(12).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(13).setPreferredWidth(10);
		
		jCheckBoxMapSelectionFilter.addActionListener(this);

		jButtonClose.addActionListener(this);

		jButtonTableSelAll.addActionListener(this);
		jButtonTableSelClear.addActionListener(this);
		jButtonTableSelFromMap.addActionListener(this);
		jButtonTableSelToMap.addActionListener(this);

		jButtonEntityPlayer.addActionListener(this); // TODO EditEntities : add entity size
		jButtonEntityAngle.setEnabled(false); // TODO EditEntities : add entity angle
		jButtonEntityBlueprint.addActionListener(this);
		jButtonEntityDelete.addActionListener(this);

		jButtonImportTextFile.addActionListener(this);
		jButtonExportTextFile.addActionListener(this);

		jButtonAdjustableHeightOn.addActionListener(this);
		jButtonAdjustableHeightOff.addActionListener(this);
		
		mapViewer.getRenderer().currentSelection.addSelectionChangeListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == jCheckBoxMapSelectionFilter) {
			updateTableRows();
			return;
		}

		if (e.getSource() == jButtonClose) {
			close();
			return;
		}

		if (e.getSource() == jButtonTableSelAll) {
			tableSelectAll();
			return;
		}
		if (e.getSource() == jButtonTableSelClear) {
			tableSelClear();
			return;
		}
		if (e.getSource() == jButtonTableSelFromMap) {
			tableSelFromMap();
			return;
		}
		if (e.getSource() == jButtonTableSelToMap) {
			tableSelToMap();
			return;
		}

		if (e.getSource() == jButtonEntityPlayer) {
			entityPlayer();
			return;
		}

		if (e.getSource() == jButtonEntityBlueprint) {
			entityBlueprint();
			return;
		}
		
		if (e.getSource() == jButtonEntityDelete) {
			entityDelete();
			return;
		}

		if (e.getSource() == jButtonImportTextFile) {
			importTextFile();
			return;
		}

		if (e.getSource() == jButtonExportTextFile) {
			exportTextFile();
			return;
		}

		if (e.getSource() == jButtonAdjustableHeightOn) {
			setAdjustableHeightOn();
			return;
		}

		if (e.getSource() == jButtonAdjustableHeightOff) {
			setAdjustableHeightOff();
			return;
		}

	}

	@Override
	public void selectionChange(SelectionChangeEvent evt) {
		if (jCheckBoxMapSelectionFilter.isSelected()) updateTableRows();
	}

	private void updateTableRows() {
		
		HashSet<Integer> tableSelDecals = new HashSet<Integer>();

		for (int row : jTableMapList.getSelectedRows()) {
			int i = jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelDecals.add(entitiesTable.mapDataLines.get(i).id);
		}

		jTableMapList.clearSelection();

		entitiesTable.updateEntityTable(jCheckBoxMapSelectionFilter.isSelected());

		for (int i = 0; i < entitiesTable.mapDataLines.size(); i++)
			if (tableSelDecals.contains(entitiesTable.mapDataLines.get(i).id)) {
				int row = jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}
	
	private void entityPlayer() {
		
		Vector<String> options=new Vector<String>();
		for (PlayerID playerID : PlayerID.values()) options.add(playerID.toString());
		
		String s = (String)JOptionPane.showInputDialog(	mapViewer,
		                    "Select new player owner",
		                    "Player Owner",
		                    JOptionPane.QUESTION_MESSAGE,
		                    null, options.toArray(),
		                    "basic_marker");

		//detect cancel
		if ((s == null) || (s.length() == 0)) return;

		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			mapViewer.commandsGroups.doEntityChangeOwner(entitiesTable.mapDataLines.get(i).id,PlayerID.getPlayerID(s));
		}
		
		updateTableRows();
		
	}
	
	private void entityDelete() {
		for (int row : jTableMapList.getSelectedRows()) {
			int i = jTableMapList.getRowSorter().convertRowIndexToModel(row);
			mapViewer.commandsGroups.deleteEntity(entitiesTable.mapDataLines.get(i).id);
		}
		updateTableRows();
	}

	private void setAdjustableHeightOn() {
		for (int row : jTableMapList.getSelectedRows())
			entitiesTable.setValueAtColumn(true, jTableMapList.convertRowIndexToModel(row), EditEntitiesTableModel.EditEntitiesTableColumnsEnum.ADJUSTABLEHEIGHT);
		updateTableRows();
	}
	
	private void setAdjustableHeightOff() {
		for (int row : jTableMapList.getSelectedRows())
			entitiesTable.setValueAtColumn(false, jTableMapList.convertRowIndexToModel(row), EditEntitiesTableModel.EditEntitiesTableColumnsEnum.ADJUSTABLEHEIGHT);
		updateTableRows();
	}
	
	private void entityBlueprint() {

		if (jTableMapList.getSelectedRowCount() == 0) return;

		int index = jTableMapList.getRowSorter().convertRowIndexToModel(jTableMapList.getSelectedRows()[0]);
		int id = entitiesTable.mapDataLines.get(index).id;
		int bpIndex = mapViewer.getMap().entities.get(id).getBlueprintIndex();
		String blueprint = mapViewer.getMap().entityBlueprints.get(bpIndex);

		String newBlueprint = SelectEntityBP.showDialog(this, mapViewer.getMap(), mapViewer.getMod(), blueprint);

		int blueprintIndex = mapViewer.getMap().entityBlueprints.indexOf(newBlueprint);

		if (blueprintIndex < 0) {
			blueprintIndex = mapViewer.getMap().entityBlueprints.size();
			mapViewer.getMap().entityBlueprints.add(newBlueprint);
		}

		for (int row : jTableMapList.getSelectedRows()) {
			int i = jTableMapList.getRowSorter().convertRowIndexToModel(row);
			mapViewer.getMap().entities.get(entitiesTable.mapDataLines.get(i).id).setBlueprintIndex(blueprintIndex);
		}

		mapViewer.getRenderer().mapDisplayParams.playerDisplayParams.fireUpdateEvent(1);
		
		entitiesTable.updateEntityTable(jCheckBoxMapSelectionFilter.isSelected());
		
	}

	private void tableSelectAll() {
		jTableMapList.selectAll();
	}

	private void tableSelClear() {
		jTableMapList.clearSelection();
	}

	private void tableSelFromMap() {

		HashSet<Integer> selectedEntities = mapViewer.getRenderer().currentSelection.entities;
		jTableMapList.clearSelection();

		for (int i = 0; i < entitiesTable.mapDataLines.size(); i++)
			if (selectedEntities.contains(entitiesTable.mapDataLines.get(i).id)) {
				int row = jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}

	}

	private void tableSelToMap() {

		HashSet<Integer> selectedEntities = mapViewer.getRenderer().currentSelection.entities;
		selectedEntities.clear();

		for (int row : jTableMapList.getSelectedRows()) {
			int i = jTableMapList.getRowSorter().convertRowIndexToModel(row);
			selectedEntities.add(entitiesTable.mapDataLines.get(i).id);
		}

	}

	private void exportTextFile() {
		
        JFileChooser chooser = new JFileChooser();
        // chooser.setCurrentDirectory(currentDirectory);
        chooser.setFileFilter(new FileNameExtensionFilter("Text file (*.txt)", "txt"));
        
        int res = chooser.showSaveDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        File outFile = chooser.getSelectedFile();
        if (outFile == null) { System.err.println("No file selected"); return; }
        
        if (!outFile.getName().endsWith(".txt")) outFile=new File(outFile.getPath()+".txt");

        if (outFile.exists()) {
        	int rep=JOptionPane.showConfirmDialog(this, "Overwrite existing file?","File already exist",JOptionPane.YES_NO_OPTION); 
        	if (rep!=JOptionPane.OK_OPTION) return; 
        }
        
        try {
			writeTable(outFile);
			JOptionPane.showMessageDialog(this, "Export Success","Export Text file",JOptionPane.INFORMATION_MESSAGE);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Export Failed","Export Text file",JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	public void importTextFile() {
		
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("Text file (*.txt)", "txt"));
        
        int res = chooser.showOpenDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        File inFile = chooser.getSelectedFile();
        
        if (inFile == null) { 
        	System.err.println("No file selected"); 
        	return; 
        }
        
        if (!inFile.exists()) {
        	JOptionPane.showMessageDialog(this, "File does not exist./nImport Aborted.","Import Decals Text file",JOptionPane.ERROR_MESSAGE);
        	return;
        }
        
    	int rep=JOptionPane.showConfirmDialog(this, "Delete existing entities before import?","Import Decals Text file",JOptionPane.YES_NO_OPTION); 
    	if (rep==JOptionPane.OK_OPTION) mapViewer.commandsGroups.doEntitiesDeleteAll(false); 
        
        jTableMapList.clearSelection(); 
        
		readTable(inFile);
		
	}
	
	public void writeTable(File outFile) throws FileNotFoundException {
		
		PrintWriter out=new PrintWriter(outFile);
		
		out.print(EditEntitiesTableColumnsEnum.ID.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.PLAYER.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.X.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.Y.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.Z.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.HEAD_DEG.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.PITCH_DEG.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.ROLL_DEG.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.SCALEX.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.SCALEY.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.SCALEZ.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.ADJUSTABLEHEIGHT.getHeaderDisplayName()+"\t");
		out.print(EditEntitiesTableColumnsEnum.BLUEPRINT.getHeaderDisplayName()+"\t");
		out.println();
		
		EntityBlueprints entityBlueprints=mapViewer.getMap().entityBlueprints;
		Entities entities=mapViewer.getMap().entities;
		Squads squads=mapViewer.getMap().squads;
		HashSet<Integer> selectedEntities = mapViewer.getRenderer().currentSelection.entities;
		
		for (Integer entityID : entities.keySet())	{
			
			if (squads.getSquadID(entityID)!=null) continue;
			
			if (jCheckBoxMapSelectionFilter.isSelected())
				if (!selectedEntities.contains(entityID))
					continue;

			Entity entity=entities.get(entityID);
			
			out.print(entity.getId()+"\t");
			out.print(entity.getPlayerID()+"\t");
			out.print(entity.getX()+"\t");
			out.print(entity.getY()+"\t");
			out.print(entity.getZ()+"\t");
			out.print(Math.toDegrees(entity.getHeadingRad())+"\t");
			out.print(Math.toDegrees(entity.getPitchRad())+"\t");
			out.print(Math.toDegrees(entity.getRollRad())+"\t");
			out.print(entity.getSX()+"\t");
			out.print(entity.getSY()+"\t");
			out.print(entity.getSZ()+"\t");
			out.print(((entity.getMeAdjustableHeightStatus()) ? "1" : "0") + "\t");
			out.print(entityBlueprints.get(entity.getBlueprintIndex()).replace("\\", "/").replace("data:attrib/ebps/","")+"\t");
			out.println();
			
		}

		out.close();
		
	}

	private void readTable(File inFile) {

		int            iLine      = -1;
		String         strLine    = null;
		BufferedReader objBRdr    = null;
		FileReader     objFRdr    = null;

		try {
			
			objFRdr = new FileReader(inFile);
			
			if (objFRdr != null) {
				
				objBRdr = new BufferedReader(objFRdr);
				
				if (objBRdr != null) {
					
					while (objBRdr.ready()) {
						strLine  = null;
						strLine = objBRdr.readLine().trim();
						iLine++;

						if (iLine==0) continue; // skip 1st line
						
						String[] strColumns=strLine.split("\t");
						
						Integer id=new Integer(strColumns[0]);
						PlayerID playerID=PlayerID.getPlayerID(strColumns[1]);
						
						Float x=new Float(strColumns[2]);
						Float y=new Float(strColumns[3]);
						Float z=new Float(strColumns[4]);
						
						Double headingDeg=new Double(strColumns[5]);
						Double pitchDeg=new Double(strColumns[6]);
						Double rollDeg=new Double(strColumns[7]);

						Double sX=new Double(strColumns[8]);
						Double sY=new Double(strColumns[9]);
						Double sZ=new Double(strColumns[10]);
						
						String entityBP=strColumns[12];
						
						mapViewer.commandsGroups.doEntityUpdate(
								id, "data:attrib\\ebps\\"+entityBP.replace("/", "\\"), playerID,
								x, y, z,
								(float) Math.toRadians(headingDeg),(float) Math.toRadians(pitchDeg),(float) Math.toRadians(rollDeg),
								sX, sY, sZ);

						mapViewer.getMap().entities.get(id).setMeAdjustableHeightStatus(strColumns[11].equals("1"));
						
					}
				}
				objBRdr.close();
			}
		}
		catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(this, "File not found","Import Decals Text file failed",JOptionPane.ERROR_MESSAGE);
		}
		catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Format error,\nLine number="+iLine,"Import Decals Text file failed",JOptionPane.ERROR_MESSAGE);
		}
		catch (IOException e) {
			JOptionPane.showMessageDialog(this, "I/O error,\nLine number="+iLine,"Import Decals Text file failed",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
		updateTableRows();
				
	}

}
