package fr.dow.mapeditor.dialogs.selectdecalblueprint;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.media.opengl.awt.GLCanvas;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.jogamp.opengl.util.FPSAnimator;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.mapeditor.tools.ColorizedButton;

public class SelectDecalBP extends SelectDecalBPBase implements ListSelectionListener {

	SelectDecalRenderer decalRenderer;
	
	DowMod mod;
	SgbMap map;
	
	SelectDecalBPTableModel decalTable;
	
	private String defaultTexture=null;
	
	private String currentTexture=null;
	
	public void close() {
		currentTexture=defaultTexture;
		this.setVisible(false);
	}
	
	public void apply() {
		this.setVisible(false);
	}
	
	private SelectDecalBP(Frame owner,SgbMap map, DowMod mod) {
		super(owner);
		
		this.mod=mod;
		this.map=map;
		
		decalTable=(SelectDecalBPTableModel)jTableDecalList.getModel();
		
		decalTable.setData(map,mod);
				
		jTableDecalList.setAutoCreateRowSorter(true);
		jTableDecalList.getColumnModel().getColumn(0).setPreferredWidth(10);
		jTableDecalList.getColumnModel().getColumn(1).setPreferredWidth(200);
		
		jTableDecalList.getRowSorter().toggleSortOrder(1);
		
		jTableDecalList.getSelectionModel().addListSelectionListener(this);
		
		GLCanvas canvas = new GLCanvas();
		
		decalRenderer=new SelectDecalRenderer();
	    canvas.addGLEventListener(decalRenderer);
	    
		jPanelDecalPreview.add(canvas,BorderLayout.CENTER);
		
		//Init animator
		System.out.println("Starting animation");	
		FPSAnimator animator = new FPSAnimator( canvas, 25 );
		//animator.setRunAsFastAsPossible(false);
		animator.start(); 
		
		jButtonApply.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						apply();
					}
				}
			);
		
		jButtonCancel.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						close();
					}
				}
			);
		
		ColorizedButton colorButton=new ColorizedButton();
		colorButton.setPreferredSize(new Dimension(100,23));
		colorButton.setText("Back. Color");
		colorButton.setColor(new Color(1f,1f,1f,0f));
		jPanelLeft.add(colorButton);
		
		
		colorButton.addPropertyChangeListener( 
				new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent evt) {
						if (evt.getPropertyName().equals("Color")) {
							decalRenderer.setNewBackgroundColor((Color) evt.getNewValue());
						}
					}
				}
		);
		
	}

	
	private void setDefaultBP(String dowBPPath) {
		
		System.out.println("Start with default "+dowBPPath);
		
		int selRow=decalTable.findPathRowIndex(dowBPPath);
		
		if (jTableDecalList.getSelectedRow()>=0) jTableDecalList.clearSelection();
		
		if (selRow>=0) {
			selRow=jTableDecalList.getRowSorter().convertRowIndexToView(selRow);
			jTableDecalList.addRowSelectionInterval(selRow, selRow);
			defaultTexture=dowBPPath;
		}
		
	}
	
	private String getSelectedBP() {
		return currentTexture;
	}
	
	public static String showDialog(Component owner, SgbMap map, DowMod mod, String defaultBP) {
		
		SelectDecalBP dialog = new SelectDecalBP(null,map, mod);
		dialog.setLocationRelativeTo(owner);
		dialog.setDefaultBP(defaultBP);
		dialog.setVisible(true);
		return dialog.getSelectedBP();
		
	}
	
	private void updateTextureDisplay() {
		
		int selRow=jTableDecalList.getSelectedRow();
		
		String newTexture=null;
		
		if (selRow<0) {
			if (currentTexture!=null) {
	        	decalRenderer.clearTexture();
	        	currentTexture=null;				
			}
			return;
		}
		
		selRow=jTableDecalList.getRowSorter().convertRowIndexToModel(selRow);
		newTexture=decalTable.getPath(selRow);
		
		if (newTexture.equalsIgnoreCase(currentTexture)) return;
		
		currentTexture=newTexture;		
		decalRenderer.displayNewTexure(newTexture, mod);
        
	}

	
	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		updateTextureDisplay();
	}

	
}
