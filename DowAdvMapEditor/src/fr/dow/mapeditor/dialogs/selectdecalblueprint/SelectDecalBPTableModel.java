package fr.dow.mapeditor.dialogs.selectdecalblueprint;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.filesystem.DowFile;
import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.gamedata.sgbmap.terrain.DecalFileLinks;

public class SelectDecalBPTableModel  extends AbstractTableModel {
	
	private static final String[] columnNames = { "Loaded", "Blueprint" };

	private Vector<LineData> tableData=new Vector<LineData>();
	
	public Class<? extends Object> getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }
	
	@Override
	public int getColumnCount() { return columnNames.length; }

	@Override
	public int getRowCount() {
		return tableData.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (tableData==null) return null;
		if (tableData.get(rowIndex)==null) return null;
		
		switch (columnIndex) {
			case 0 : return tableData.get(rowIndex).isLoaded;
			case 1 : return tableData.get(rowIndex).decalName;
		}
		
		return null;
	}
	
	@Override
    public String getColumnName(int col) { return columnNames[col]; }

	public void setData(SgbMap map, DowMod mod) {
		tableData.clear();
		addData(mod.getFolder("art/decals"),map.terrain.decalFileLinks);
		fireTableDataChanged();
	}

	public String getPath(int rowIndex) {
		return tableData.get(rowIndex).decalName;
	}
	
	public int findPathRowIndex(String dowPath) {
		for (int i=0;i<tableData.size();i++) 
			if (dowPath.equals(tableData.get(i).decalName))
				return i;
		return -1;
	}
	
	private void addData(DowFolder dowFolder, DecalFileLinks decalFileLinks) {
		
		for (DowFolder subFolder : dowFolder.getSubFoldersList().values()) addData(subFolder,decalFileLinks); 
		
		for (DowFile subFile : dowFolder.getSubFilesList().values() ) {
			String decalPath=subFile.getDowPath();
			int dotPos=decalPath.lastIndexOf('.');
			if (dotPos<0) {
				System.out.println("Skipping decal filename (no extension) : "+decalPath);
				continue;
			}
			decalPath=decalPath.substring(0, dotPos);
			tableData.add(new LineData(decalFileLinks.containsValue(decalPath),decalPath));
		}
	}
	
	
	private class LineData {
		Boolean isLoaded;
		String decalName;
		public LineData(boolean isLoaded,String decalName) {
			this.isLoaded=isLoaded;
			this.decalName=decalName;
		}
	}
	
}
