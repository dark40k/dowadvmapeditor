package fr.dow.mapeditor.dialogs.rotateentities;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.dow.gamedata.sgbmap.units.Entities;
import fr.dow.gamedata.sgbmap.units.Entity;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeEvent;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeListener;

public class RotateEntitiesDialog extends RotateEntitiesDialogBase implements ActionListener, ChangeListener,
		SelectionChangeListener {

	private MapViewer mapViewer;

	private Entities entities0;

	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		RotateEntitiesDialog dialog = new RotateEntitiesDialog(mapEditor, mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		mapViewer.getRenderer().curFilter.lockAllExcept(FilterType.ENTITY);
		dialog.setVisible(true);
		return;
	}

	public void close() {
		mapViewer.getRenderer().curFilter.unlockAll();
		mapViewer.getRenderer().currentSelection.removeSelectionChangeListener(this);
		this.setVisible(false);
	}

	public RotateEntitiesDialog(MapEditor mapEditor, MapViewer mapViewer) {
		super(mapEditor);

		this.mapViewer = mapViewer;

		entities0 = (Entities) mapViewer.getMap().entities.clone();

		jSliderHeading.addChangeListener(this);
		jSliderPitch.addChangeListener(this);
		jSliderRoll.addChangeListener(this);

		jButtonApply.addActionListener(this);
		jButtonCancel.addActionListener(this);

		mapViewer.getRenderer().currentSelection.addSelectionChangeListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jButtonApply) {
			close();
			return;
		}
		if (e.getSource() == jButtonCancel) {
			reset();
			close();
			return;
		}
	}

	private void reset() {
		Entities entities=mapViewer.getMap().entities;
		for (Integer entityID : entities0.keySet())
			entities.get(entityID).setAnglesRad(
					entities0.get(entityID).getHeadingRad(), 
					entities0.get(entityID).getPitchRad(),
					entities0.get(entityID).getRollRad());
		mapViewer.getRenderer().mapDisplayParams.playerDisplayParams.fireUpdateEvent(1);
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		if (arg0.getSource() == jSliderHeading) {
			Entities entities = mapViewer.getMap().entities;
			double newHeading = Math.toRadians(jSliderHeading.getValue());
			for (Integer entityID : mapViewer.getRenderer().currentSelection.entities)
				entities.get(entityID).setAnglesRad(newHeading, entities.get(entityID).getPitchRad(), entities.get(entityID).getRollRad());
			mapViewer.getRenderer().mapDisplayParams.playerDisplayParams.fireUpdateEvent(1);
			return;
		}
		if (arg0.getSource() == jSliderPitch) {
			Entities entities = mapViewer.getMap().entities;
			double newPitch = Math.toRadians(jSliderPitch.getValue());
			for (Integer entityID : mapViewer.getRenderer().currentSelection.entities)
				entities.get(entityID).setAnglesRad(entities.get(entityID).getHeadingRad(), newPitch, entities.get(entityID).getRollRad());
			mapViewer.getRenderer().mapDisplayParams.playerDisplayParams.fireUpdateEvent(1);
			return;
		}
		if (arg0.getSource() == jSliderRoll) {
			Entities entities = mapViewer.getMap().entities;
			double newRoll = Math.toRadians(jSliderRoll.getValue());
			for (Integer entityID : mapViewer.getRenderer().currentSelection.entities)
				entities.get(entityID).setAnglesRad(entities.get(entityID).getHeadingRad(), entities.get(entityID).getPitchRad(), newRoll);
			mapViewer.getRenderer().mapDisplayParams.playerDisplayParams.fireUpdateEvent(1);
			return;
		}

	}

	@Override
	public void selectionChange(SelectionChangeEvent evt) {

		reset();

		if (mapViewer.getRenderer().currentSelection.entities.size() > 0) {
			int entityID = mapViewer.getRenderer().currentSelection.entities.iterator().next();
			Entity entity = mapViewer.getMap().entities.get(entityID);
			jSliderHeading.setValue((int) Math.round(Math.toRadians(entity.getHeadingRad())));
			jSliderPitch.setValue((int) Math.round(Math.toRadians(entity.getPitchRad())));
			jSliderRoll.setValue((int) Math.round(Math.toRadians(entity.getRollRad())));
		}

		Entities entities = mapViewer.getMap().entities;

		double newHeading = Math.toRadians(jSliderHeading.getValue());
		for (Integer entityID : mapViewer.getRenderer().currentSelection.entities)
			entities.get(entityID).setAnglesRad(newHeading, entities.get(entityID).getPitchRad(), entities.get(entityID).getRollRad());

		double newPitch = Math.toRadians(jSliderPitch.getValue());
		for (Integer entityID : mapViewer.getRenderer().currentSelection.entities)
			entities.get(entityID).setAnglesRad(entities.get(entityID).getHeadingRad(), newPitch, entities.get(entityID).getRollRad());

		double newRoll = Math.toRadians(jSliderRoll.getValue());
		for (Integer entityID : mapViewer.getRenderer().currentSelection.entities)
			entities.get(entityID).setAnglesRad(entities.get(entityID).getHeadingRad(), entities.get(entityID).getPitchRad(), newRoll);

		mapViewer.getRenderer().mapDisplayParams.playerDisplayParams.fireUpdateEvent(1);

	}

}
