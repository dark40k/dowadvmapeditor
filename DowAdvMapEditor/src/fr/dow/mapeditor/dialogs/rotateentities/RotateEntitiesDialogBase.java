package fr.dow.mapeditor.dialogs.rotateentities;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import java.awt.Font;

import javax.swing.JSlider;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

public abstract class RotateEntitiesDialogBase extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanelAction = null;
	private JPanel jPanel9 = null;
	protected JButton jButtonApply = null;
	protected JPanel jPanelMapList = null;
	protected JButton jButtonCancel = null;
	private JPanel jPanel = null;
	private JPanel jPanel1 = null;
	private JPanel jPanel2 = null;
	protected JSlider jSliderHeading = null;
	protected JSlider jSliderPitch = null;
	protected JSlider jSliderRoll = null;
	/**
	 * @param owner
	 */
	public RotateEntitiesDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(400, 357);
		this.setModal(false);
		this.setTitle("Rotate Entities");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanelAction(), BorderLayout.SOUTH);
			jContentPane.add(getJPanelMapList(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanelAction	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelAction() {
		if (jPanelAction == null) {
			jPanelAction = new JPanel();
			jPanelAction.setLayout(new BorderLayout());
			jPanelAction.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanelAction;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonApply(), null);
			jPanel9.add(getJButtonCancel(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonApply	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonApply() {
		if (jButtonApply == null) {
			jButtonApply = new JButton();
			jButtonApply.setPreferredSize(new Dimension(100, 23));
			jButtonApply.setText("Apply");
		}
		return jButtonApply;
	}

	/**
	 * This method initializes jPanelMapList	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMapList() {
		if (jPanelMapList == null) {
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.gridx = 0;
			gridBagConstraints2.fill = GridBagConstraints.BOTH;
			gridBagConstraints2.weightx = 1.0;
			gridBagConstraints2.weighty = 1.0;
			gridBagConstraints2.gridwidth = 1;
			gridBagConstraints2.gridy = 3;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 0;
			gridBagConstraints1.fill = GridBagConstraints.BOTH;
			gridBagConstraints1.weightx = 1.0;
			gridBagConstraints1.weighty = 1.0;
			gridBagConstraints1.gridwidth = 1;
			gridBagConstraints1.gridy = 1;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.gridwidth = 1;
			gridBagConstraints.fill = GridBagConstraints.BOTH;
			gridBagConstraints.weightx = 1.0;
			gridBagConstraints.weighty = 1.0;
			gridBagConstraints.gridy = 0;
			jPanelMapList = new JPanel();
			jPanelMapList.setLayout(new GridBagLayout());
			jPanelMapList.setEnabled(true);
			jPanelMapList.setBorder(BorderFactory.createTitledBorder(null, "Rotation parameters", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213)));
			jPanelMapList.add(getJPanel(), gridBagConstraints);
			jPanelMapList.add(getJPanel1(), gridBagConstraints1);
			jPanelMapList.add(getJPanel2(), gridBagConstraints2);
		}
		return jPanelMapList;
	}

	/**
	 * This method initializes jButtonCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonCancel() {
		if (jButtonCancel == null) {
			jButtonCancel = new JButton();
			jButtonCancel.setPreferredSize(new Dimension(100, 23));
			jButtonCancel.setText("Cancel");
		}
		return jButtonCancel;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(new BorderLayout());
			jPanel.setBorder(BorderFactory.createTitledBorder(null, "Heading", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213)));
			jPanel.add(getJSlider(), java.awt.BorderLayout.CENTER);
		}
		return jPanel;
	}

	/**
	 * This method initializes jPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Attitude", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Pitch");
			jPanel1 = new JPanel();
			jPanel1.setLayout(new BorderLayout());
			jPanel1.setBorder(titledBorder);
			jPanel1.add(getJSlider1(), BorderLayout.CENTER);
		}
		return jPanel1;
	}

	/**
	 * This method initializes jPanel2	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel2() {
		if (jPanel2 == null) {
			TitledBorder titledBorder1 = BorderFactory.createTitledBorder(null, "Rotation", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder1.setBorder(null);
			titledBorder1.setTitle("Roll");
			jPanel2 = new JPanel();
			jPanel2.setLayout(new BorderLayout());
			jPanel2.setBorder(titledBorder1);
			jPanel2.add(getJSlider2(), BorderLayout.CENTER);
		}
		return jPanel2;
	}

	/**
	 * This method initializes jSlider	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getJSlider() {
		if (jSliderHeading == null) {
			jSliderHeading = new JSlider();
			jSliderHeading.setOrientation(JSlider.HORIZONTAL);
			jSliderHeading.setMinimum(-180);
			jSliderHeading.setMajorTickSpacing(45);
			jSliderHeading.setValue(0);
			jSliderHeading.setPaintTicks(true);
			jSliderHeading.setPaintLabels(true);
			jSliderHeading.setMaximum(180);
		}
		return jSliderHeading;
	}

	/**
	 * This method initializes jSlider1	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getJSlider1() {
		if (jSliderPitch == null) {
			jSliderPitch = new JSlider();
			jSliderPitch.setMajorTickSpacing(45);
			jSliderPitch.setMinimum(-90);
			jSliderPitch.setOrientation(JSlider.HORIZONTAL);
			jSliderPitch.setPaintLabels(true);
			jSliderPitch.setPaintTicks(true);
			jSliderPitch.setValue(0);
			jSliderPitch.setMaximum(90);
		}
		return jSliderPitch;
	}

	/**
	 * This method initializes jSlider2	
	 * 	
	 * @return javax.swing.JSlider	
	 */
	private JSlider getJSlider2() {
		if (jSliderRoll == null) {
			jSliderRoll = new JSlider();
			jSliderRoll.setMajorTickSpacing(45);
			jSliderRoll.setMinimum(-180);
			jSliderRoll.setOrientation(JSlider.HORIZONTAL);
			jSliderRoll.setPaintLabels(true);
			jSliderRoll.setPaintTicks(true);
			jSliderRoll.setValue(0);
			jSliderRoll.setMaximum(180);
		}
		return jSliderRoll;
	}
		
	//
	// End of custom part 
	//

}  //  @jve:decl-index=0:visual-constraint="10,10"
