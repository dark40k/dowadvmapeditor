package fr.dow.mapeditor.dialogs.rotateentities;

import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventListener;
import java.util.EventObject;

import javax.swing.JSlider;
import javax.swing.JCheckBox;

public class ExtendedSlider extends JPanel implements ActionListener, MouseListener, ChangeListener {

	private static final long serialVersionUID = 1L;
	private JTextField jTextField = null;
	private JSlider jSlider = null;
	private JCheckBox jCheckBox = null;

	public ExtendedSlider(String title,int vmin,int vmax, int step) {
		super();
		this.setSize(75, 200);
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createTitledBorder(null, title, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213)));
		this.add(getJTextField(), BorderLayout.NORTH);
		this.add(getJSlider(vmin,vmax,step), BorderLayout.CENTER);
		this.add(getJCheckBox(), BorderLayout.SOUTH);
		
		jSlider.addMouseListener(this);
		jSlider.addChangeListener(this);
		
	}

	public int getPosition() {
		return jSlider.getValue();
	}
	
	private JTextField getJTextField() {
		if (jTextField == null) {
			jTextField = new JTextField();
		}
		return jTextField;
	}

	private JSlider getJSlider(int vmin,int vmax, int step) {
		if (jSlider == null) {
			jSlider = new JSlider();
			jSlider.setOrientation(JSlider.VERTICAL);
			jSlider.setPaintLabels(true);
			jSlider.setMaximum(vmin);
			jSlider.setMinimum(vmax);
			jSlider.setMajorTickSpacing(step);
			jSlider.setValue((vmax-vmin)/2);
			jSlider.setPaintTicks(true);
		}
		return jSlider;
	}

	private JCheckBox getJCheckBox() {
		if (jCheckBox == null) {
			jCheckBox = new JCheckBox();
			jCheckBox.setText("Snap");
		}
		return jCheckBox;
	}

	/*
	 * 
	 * 
	 * 
	 * 
	 */
	protected EventListenerList listenerList = new EventListenerList();

	class PositionChangeEvent extends EventObject {
		public PositionChangeEvent(Object source) {
			super(source);
		}
	}

	interface PositionChangeListener extends EventListener {
		public void positionChange(PositionChangeEvent evt);
	}

	public void addPositionChangeListener(PositionChangeListener listener) {
		listenerList.add(PositionChangeListener.class, listener);
	}
	
	public void removePositionChangeListener(PositionChangeListener listener) {
		listenerList.remove(PositionChangeListener.class, listener);
	}
	
	void firePositionChangeEvent() {
		PositionChangeEvent evt=new PositionChangeEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == PositionChangeListener.class) {
				((PositionChangeListener) listeners[i+1]).positionChange(evt);
			}
		}
	}
	
	/*
	 * Listeners
	 * 
	 */
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
	}

} 
