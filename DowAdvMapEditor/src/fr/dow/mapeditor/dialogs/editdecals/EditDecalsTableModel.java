package fr.dow.mapeditor.dialogs.editdecals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.sgbmap.terrain.Decal;
import fr.dow.gamedata.sgbmap.terrain.DecalFileLinks;
import fr.dow.gamedata.sgbmap.terrain.Decals;
import fr.dow.mapviewer.display.MapViewer;

public class EditDecalsTableModel extends AbstractTableModel {

	private static final String[] columnNames = { "Valid", "ID", "X","Z","Size","Angle", "Blueprint" };

	private MapViewer mapViewer;
	
	class DecalData {
		Integer id;
		Boolean isValid;
		Float x;
		Float z;
		Float size;
		Float angle;
		String blueprint;
	}
	
	Vector<DecalData> mapDataLines=new Vector<DecalData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
    public Class<?> getColumnClass(int c) {
		switch (c) {
		case 0 : return Boolean.class;
		case 1 : return Integer.class; 
		case 2 : return Float.class; 
		case 3 : return Float.class; 
		case 4 : return Float.class; 
		case 5 : return Float.class; 
		case 6 : return String.class; 
		}
		return null;
    }
    
    public String getColumnName(int col) { return columnNames[col]; }
    
	@Override
	public int getColumnCount() { return columnNames.length; }

	@Override
	public int getRowCount() { return mapDataLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (mapDataLines==null) return null;
		if (rowIndex>=mapDataLines.size()) return null;
		
		switch (columnIndex) {
		case 0 : return mapDataLines.get(rowIndex).isValid;
		case 1 : return mapDataLines.get(rowIndex).id; 
		case 2 : return mapDataLines.get(rowIndex).x; 
		case 3 : return mapDataLines.get(rowIndex).z; 
		case 4 : return mapDataLines.get(rowIndex).size; 
		case 5 : return mapDataLines.get(rowIndex).angle; 
		case 6 : return mapDataLines.get(rowIndex).blueprint; 
		}
		
		return null;
		
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex>=mapDataLines.size()) return false;
		switch (columnIndex) {
			case 2 : return true;
			case 3 : return true;
			case 4 : return true;
			case 5 : return true;
			default : return false;
		}
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (rowIndex>=mapDataLines.size()) return;
		DecalData decalData=mapDataLines.get(rowIndex);
		switch (columnIndex) {
			case 2 : decalData.x=(Float)aValue; break;
			case 3 : decalData.z=(Float)aValue; break;
			case 4 : decalData.size=(Float)aValue; break;
			case 5 : decalData.angle=(Float)aValue; break;
			default : return;
		}
		
		mapViewer.commandsEditTerrain.doChangeDecalPos(decalData.id,decalData.x,decalData.z,decalData.size,(float)Math.toRadians(decalData.angle));
		fireTableDataChanged();
		
	}
	
	public void updateDecalTable(boolean onlySelected) {
		
		Vector<DecalData> newDataLines=new Vector<DecalData>();
		
		Decals decals=mapViewer.getMap().terrain.decals;
		DecalFileLinks decalFileLinks=mapViewer.getMap().terrain.decalFileLinks;
		
		HashSet<Integer> selectedDecals = mapViewer.getRenderer().currentSelection.decals;
		
		for (Integer decalID : decals.keySet())	{
			
			if (onlySelected)
				if (!selectedDecals.contains(decalID))
					continue;
			
			Decal decal=decals.get(decalID);
			
			DecalData decalData=new DecalData();
			decalData.id=decalID;
			decalData.x=decal.get_x();
			decalData.z=decal.get_z();
			decalData.size=decal.get_size();
			decalData.angle=(float) Math.toDegrees(decal.get_angle());
			decalData.blueprint=decalFileLinks.get(decal.get_decalPtr());
			
			if (mapViewer.getMod().isValidFile(decalData.blueprint+".dds")) decalData.isValid=true;
			else if (mapViewer.getMod().isValidFile(decalData.blueprint+".tga")) decalData.isValid=true;
			else decalData.isValid=false;
			
			newDataLines.add(decalData);
			
		}
		
		mapDataLines=newDataLines;
		fireTableDataChanged();
		
	}
	
	public void setMapViewer(MapViewer mapViewer, boolean onlySelected) {
		this.mapViewer=mapViewer;
		updateDecalTable(onlySelected);
	}	
	
	public void writeTable(File outFile) throws FileNotFoundException {
		
		PrintWriter out=new PrintWriter(outFile);
		
		for (int i=1;i<columnNames.length;i++) {
			out.print(columnNames[i]+"\t");
		}
		out.println();
		
		for (int j=0;j<mapDataLines.size();j++) {
			for (int i=1;i<columnNames.length;i++) {
				out.print(getValueAt(j, i).toString()+"\t");
			}
			out.println();
		}
		
		out.close();
		
	}
	
	
}
