package fr.dow.mapeditor.dialogs.editdecals;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.editdecals.EditDecalsTableModel.DecalData;
import fr.dow.mapeditor.dialogs.selectdecalblueprint.SelectDecalBP;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeEvent;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeListener;

public class EditDecalsDialog extends EditDecalsDialogBase implements ActionListener, SelectionChangeListener {

	MapViewer mapViewer;
	
	EditDecalsTableModel decalTable;
	
	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		EditDecalsDialog dialog = new EditDecalsDialog(mapEditor, mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		mapViewer.getRenderer().curFilter.lockAllExcept(FilterType.DECAL);
		dialog.setVisible(true);
		return;
	}	
	
	public void close() {
		mapViewer.getRenderer().curFilter.unlockAll();
		this.setVisible(false);
	}
	
	public EditDecalsDialog(MapEditor mapEditor, MapViewer mapViewer) {
		super(mapEditor);
		
		this.mapViewer=mapViewer;
		
		decalTable=(EditDecalsTableModel)jTableMapList.getModel();
		decalTable.setMapViewer(mapViewer,jCheckBoxMapSelectionFilter.isSelected());
				
		jTableMapList.setAutoCreateRowSorter(true);
		jTableMapList.getColumnModel().getColumn(0).setPreferredWidth(10);
		jTableMapList.getColumnModel().getColumn(1).setPreferredWidth(20);
		jTableMapList.getColumnModel().getColumn(2).setPreferredWidth(20);
		jTableMapList.getColumnModel().getColumn(3).setPreferredWidth(20);
		jTableMapList.getColumnModel().getColumn(4).setPreferredWidth(20);
		jTableMapList.getColumnModel().getColumn(5).setPreferredWidth(20);
		jTableMapList.getColumnModel().getColumn(6).setPreferredWidth(250);
	
		jCheckBoxMapSelectionFilter.addActionListener(this);
		
		jButtonClose.addActionListener(this);
		
		jButtonTableSelAll.addActionListener(this);
		jButtonTableSelClear.addActionListener(this);
		jButtonTableSelFromMap.addActionListener(this);
		jButtonTableSelToMap.addActionListener(this);
		
		jButtonDecalSize.addActionListener(this);
		jButtonDecalAngle.addActionListener(this);
		jButtonDecalBlueprint.addActionListener(this);
		jButtonDecalDelete.addActionListener(this);
		
		jButtonImportTextFile.addActionListener(this); 
		jButtonExportTextFile.addActionListener(this); 
		
		mapViewer.getRenderer().currentSelection.addSelectionChangeListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource()==jCheckBoxMapSelectionFilter) { updateTable(); return; }
		
		if (e.getSource()==jButtonClose) { close(); return; }
		
		if (e.getSource()==jButtonTableSelAll) { tableSelectAll(); return; }
		if (e.getSource()==jButtonTableSelClear){ tableSelClear(); return; }
		if (e.getSource()==jButtonTableSelFromMap) { tableSelFromMap(); return; }
		if (e.getSource()==jButtonTableSelToMap) { tableSelToMap(); return; }
		
		if (e.getSource()==jButtonDecalSize) { decalSize(); return; }
		if (e.getSource()==jButtonDecalAngle) { decalAngle(); return; }
		if (e.getSource()==jButtonDecalBlueprint) { decalBlueprint(); return; }
		if (e.getSource()==jButtonDecalDelete) { decalDelete(); return; }
		
		if (e.getSource()==jButtonExportTextFile) { exportTextFile(); return; }
		if (e.getSource()==jButtonImportTextFile) { importTextFile(); return; }
		
	}

	private void updateTable() {
		
		HashSet<Integer> tableSelDecals = new HashSet<Integer>();

		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelDecals.add(decalTable.mapDataLines.get(i).id);
		}

		jTableMapList.clearSelection();
		
		decalTable.updateDecalTable(jCheckBoxMapSelectionFilter.isSelected());
		
		for (int i=0;i<decalTable.mapDataLines.size();i++) 
			if (tableSelDecals.contains(decalTable.mapDataLines.get(i).id)){
				int row=jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}
	
	@Override
	public void selectionChange(SelectionChangeEvent evt) {
		if (jCheckBoxMapSelectionFilter.isSelected()) {
			updateTable();
		}
	}
	
	private void decalDelete() {
		
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			mapViewer.commandsEditTerrain.doDecalDelete(decalTable.mapDataLines.get(i).id);
		}
		
		updateTable();
		
	}

	private void decalSize() {
		
		if (jTableMapList.getSelectedRowCount()==0) return;
		
		String s = (String)JOptionPane.showInputDialog(
                this,
                "Enter new decal(s) size :\n",
                "Customized Dialog",
                JOptionPane.QUESTION_MESSAGE
                );

		if (s==null) return;
		if (s.length()==0) return;
		
		Float newSize=null;
		try { newSize=Float.valueOf(s); } catch (NumberFormatException e) { return; }
		
		if ((newSize<=0)|(newSize>1)) return;
		
		for (int row : jTableMapList.getSelectedRows()) {
			DecalData decalData=decalTable.mapDataLines.get(
					jTableMapList.getRowSorter().convertRowIndexToModel(row)
				);
			mapViewer.commandsEditTerrain.doChangeDecalPos(
					decalData.id, decalData.x, decalData.z, newSize, decalData.angle 
				);
		}
		
		updateTable();		
		
	}
	
	private void decalAngle() {
		
		if (jTableMapList.getSelectedRowCount()==0) return;
		
		String s = (String)JOptionPane.showInputDialog(
                this,
                "Enter new decal(s) Angle :\n",
                "Customized Dialog",
                JOptionPane.QUESTION_MESSAGE
                );

		if (s==null) return;
		if (s.length()==0) return;
		
		Float newAngle=null;
		try { newAngle=Float.valueOf(s); } catch (NumberFormatException e) { return; }
		
		if ((newAngle<0)|(newAngle>360)) return;
		
		for (int row : jTableMapList.getSelectedRows()) {
			DecalData decalData=decalTable.mapDataLines.get(
					jTableMapList.getRowSorter().convertRowIndexToModel(row)
				);
			mapViewer.commandsEditTerrain.doChangeDecalPos(
					decalData.id, decalData.x, decalData.z, decalData.size, (float) Math.toRadians(newAngle)
				);
		}
		
		updateTable();	
		
	}
	
	private void decalBlueprint() {
		
		int[] selRows = jTableMapList.getSelectedRows();
		
		if (selRows.length==0) return;
		
		int selRow=jTableMapList.getRowSorter().convertRowIndexToModel(selRows[0]);
		
		String newDecalBP=SelectDecalBP.showDialog(this, mapViewer.getMap(), mapViewer.getMod(), decalTable.mapDataLines.get(selRow).blueprint);
		if (newDecalBP==null) return;
		
		
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			mapViewer.commandsEditTerrain.doChangeDecalBP(decalTable.mapDataLines.get(i).id,newDecalBP);
		}
		
		updateTable();
		
	}

	private void tableSelectAll() { 
		jTableMapList.selectAll(); 
	}
	
	private void tableSelClear() {
		jTableMapList.clearSelection(); 
	}

	private void tableSelFromMap() {
		
		HashSet<Integer> selectedDecals = mapViewer.getRenderer().currentSelection.decals;
		jTableMapList.clearSelection();
				
		for (int i=0;i<decalTable.mapDataLines.size();i++)
			if (selectedDecals.contains(decalTable.mapDataLines.get(i).id)) {
				int row=jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}

	private void tableSelToMap() {
		
		HashSet<Integer> selectedDecals = mapViewer.getRenderer().currentSelection.decals;
		selectedDecals.clear();
		
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			selectedDecals.add(decalTable.mapDataLines.get(i).id);
		}
		
		updateTable();
		
	}
	
	private void exportTextFile() {
		
        JFileChooser chooser = new JFileChooser();
        // chooser.setCurrentDirectory(currentDirectory);
        chooser.setFileFilter(new FileNameExtensionFilter("Text file (*.txt)", "txt"));
        
        int res = chooser.showSaveDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        File outFile = chooser.getSelectedFile();
        if (outFile == null) { System.err.println("No file selected"); return; }
        
        if (!outFile.getName().endsWith(".txt")) outFile=new File(outFile.getPath()+".txt");

        if (outFile.exists()) {
        	int rep=JOptionPane.showConfirmDialog(this, "Overwrite existing file?","File already exist",JOptionPane.YES_NO_OPTION); 
        	if (rep!=JOptionPane.OK_OPTION) return; 
        }
        
        try {
			decalTable.writeTable(outFile);
			JOptionPane.showMessageDialog(this, "Export Success","Export Text file",JOptionPane.INFORMATION_MESSAGE);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Export Failed","Export Text file",JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	public void importTextFile() {
		
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("Text file (*.txt)", "txt"));
        
        int res = chooser.showOpenDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        File inFile = chooser.getSelectedFile();
        
        if (inFile == null) { 
        	System.err.println("No file selected"); 
        	return; 
        }
        
        if (!inFile.exists()) {
        	JOptionPane.showMessageDialog(this, "File does not exist./nImport Aborted.","Import Decals Text file",JOptionPane.ERROR_MESSAGE);
        	return;
        }
        
    	int rep=JOptionPane.showConfirmDialog(this, "Delete existing decals before import?","Import Decals Text file",JOptionPane.YES_NO_OPTION); 
    	if (rep==JOptionPane.OK_OPTION) mapViewer.commandsEditTerrain.doDecalDeleteAll(); 
        
        jTableMapList.clearSelection(); 
        
		readTable(inFile);
		
	}

	private void readTable(File inFile) {

		int            iLine      = -1;
		String         strLine    = null;
		BufferedReader objBRdr    = null;
		FileReader     objFRdr    = null;

		try {
			
			objFRdr = new FileReader(inFile);
			
			if (objFRdr != null) {
				
				objBRdr = new BufferedReader(objFRdr);
				
				if (objBRdr != null) {
					
					while (objBRdr.ready()) {
						strLine  = null;
						strLine = objBRdr.readLine().trim();
						iLine++;

						if (iLine==0) continue; // skip 1st line
						
						String[] strColumns=strLine.split("\t");

						Integer id = new Integer(strColumns[0]);
						Float x = new Float(strColumns[1]);
						Float z=new Float(strColumns[2]);
						Float size=new Float(strColumns[3]);
						Float angle=new Float(strColumns[4]);
						String blueprint=strColumns[5];

						mapViewer.commandsEditTerrain.doDecalUpdate(id,blueprint,x,z,size,(float) Math.toRadians(angle));

					}
				}
				objBRdr.close();
			}
		}
		catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(this, "File not found","Import Decals Text file failed",JOptionPane.ERROR_MESSAGE);
		}
		catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Format error,\nLine number="+iLine,"Import Decals Text file failed",JOptionPane.ERROR_MESSAGE);
		}
		catch (IOException e) {
			JOptionPane.showMessageDialog(this, "I/O error,\nLine number="+iLine,"Import Decals Text file failed",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
		updateTable();
				
	}
	
}
