package fr.dow.mapeditor.dialogs;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import java.awt.Font;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class AboutDialogBase extends JDialog {


	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanel7 = null;
	private JPanel jPanel9 = null;
	protected JButton jButton21 = null;
	private JPanel jPanel = null;
	private JTextArea textArea;
	private JScrollPane scrollPane;
	/**
	 * @param owner
	 */
	public AboutDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(792, 573);
		this.setTitle("About AME");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel7(), BorderLayout.SOUTH);
			jContentPane.add(getJPanel(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel7	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel7() {
		if (jPanel7 == null) {
			jPanel7 = new JPanel();
			jPanel7.setLayout(new BorderLayout());
			jPanel7.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanel7;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButton21(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButton21	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton21() {
		if (jButton21 == null) {
			jButton21 = new JButton();
			jButton21.setPreferredSize(new Dimension(100, 23));
			jButton21.setText("Close");
		}
		return jButton21;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Licence");
			jPanel = new JPanel();
			jPanel.setLayout(new BoxLayout(getJPanel(), BoxLayout.Y_AXIS));
			jPanel.setBorder(titledBorder);
			jPanel.add(getScrollPane());
		}
		return jPanel;
	}
	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
			textArea.setFont(new Font("Monospaced", Font.PLAIN, 11));
			try {
				textArea.read(new InputStreamReader(getClass().getResourceAsStream("/fr/dow/mapeditor/About-Licence.txt")),null);
			} catch (IOException e) {
				e.printStackTrace();
			}
			textArea.setEditable(false);
		}
		return textArea;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTextArea());
		}
		return scrollPane;
	}
}  //  @jve:decl-index=0:visual-constraint="10,10"
