package fr.dow.mapeditor.dialogs;

import java.awt.Frame;
import java.awt.event.ActionListener;

import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.mapeditor.CopyData;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.renderer.MapViewerRenderer;
import fr.dow.mapviewer.display.selection.SelFilter;
import fr.dow.mapviewer.display.selection.SelArea;
import fr.dow.mapviewer.display.selection.Selection;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;

public class PasteDialog extends PasteDialogBase{

	private CopyData copyData;
	private MapViewer mapViewer;
	private MapViewerRenderer mapRenderer;
	
	private int offsetX;
	private int offsetZ;
	private int i1,j1,i2,j2;
	
	private boolean tileMode;
	private int tileSize;
	private int tileOffsetX;
	private int tileOffsetZ;
	
	private void initSelection() {
		
		mapRenderer.currentSelection.clear(new SelFilter(FilterType.ALL));
		mapRenderer.curFilter.set(FilterType.NONE);
		
		tileSize=mapViewer.getMap().terrain.ground.groundData.get_texTileSize();
		
		enableTilePaste(tileSize==copyData.sgbMap.terrain.ground.groundData.get_texTileSize());
		
		SelArea selGroundArea=copyData.selection.area;
		i1=selGroundArea.i1;
		j1=selGroundArea.j1;
		i2=selGroundArea.i2;
		j2=selGroundArea.j2;
		
		centerSelection();
		
		updateSelection();
		
	}
	
	private void enableTilePaste(boolean status) {
		if (status) {
			jCheckBoxPasteTiles.setEnabled(true);
		}
		else {
			jCheckBoxPasteTiles.setEnabled(false);
			tileMode=false;
			jCheckBoxPasteTiles.setSelected(false);
		}
			
	}
	
	private void enableCellStep(boolean status) {
		jButtonXNCell.setEnabled(status);
		jButtonXPCell.setEnabled(status);
		jButtonZNCell.setEnabled(status);
		jButtonZPCell.setEnabled(status);
	}
	
	private void updateSelection() {
		
		if (tileMode!=jCheckBoxPasteTiles.isSelected()) {
			
			tileMode=jCheckBoxPasteTiles.isSelected();		
			
			if (tileMode) centerSelection();
			
			enableCellStep(!tileMode);
		}
		
		mapRenderer.currentSelection.clear(new SelFilter(FilterType.ALL));
		
		
		if (tileMode) {
			
			int NX1=copyData.sgbMap.terrain.ground.texTiles.get_texTileNX();
			
			int NX2=mapViewer.getMap().terrain.ground.texTiles.get_texTileNX();
			int NZ2=mapViewer.getMap().terrain.ground.texTiles.get_texTileNZ();

			for (int i : copyData.selection.tiles) {
				int ti=i%NX1 + tileOffsetX;
				int tj=i/NX1 + tileOffsetZ;
				if (ti<0) continue;
				if (ti>(NX2-1)) continue;
				if (tj<0) continue;
				if (tj>(NZ2-1)) continue;
				mapRenderer.currentSelection.tiles.add( ti + NX2 * tj );
			}
			
		}
		
		mapRenderer.currentSelection.area.setArea(i1+offsetX,j1+offsetZ,i2+offsetX,j2+offsetZ);
	}
	
	private void centerSelection() {
		
		int ic=mapViewer.getMap().terrain.ground.groundData.get_centerXnbr();
		int jc=mapViewer.getMap().terrain.ground.groundData.get_centerZnbr();
		offsetX=(ic-Math.abs(i1-i2)/2)-Math.min(i1, i2);
		offsetZ=(jc-Math.abs(j1-j2)/2)-Math.min(j1, j2);
		offsetX=offsetX-offsetX%tileSize;
		offsetZ=offsetZ-offsetZ%tileSize;
		
		tileOffsetX=offsetX/tileSize;
		tileOffsetZ=offsetZ/tileSize;
		
	}
	
	private void doPasteData() {

		SelFilter selFilter=new SelFilter(FilterType.NONE);
		if (jCheckBoxPasteHeightMap.isSelected() | jCheckBoxPasteGroundTexture.isSelected()) selFilter.add(FilterType.GROUNDAREA);
		if (jCheckBoxPasteTiles.isSelected()) selFilter.add(FilterType.TILE);
		if (jCheckBoxPasteDecals.isSelected()) selFilter.add(FilterType.DECAL);
		if (jCheckBoxPasteEntitiesSquads.isSelected()) selFilter.add(FilterType.ENTITY);
		if (jCheckBoxPasteMarkers.isSelected()) selFilter.add(FilterType.MARKER);
		
		Selection pasteSelection=new Selection();
		pasteSelection.addAll(selFilter, copyData.selection);
		
		CopyData pasteData=new CopyData(pasteSelection,(SgbMap)copyData.sgbMap.clone());
	
		mapViewer.commandsEditMap.pasteSubMap(pasteData,jCheckBoxPasteHeightMap.isSelected(), jCheckBoxPasteGroundTexture.isSelected(), offsetX,offsetZ);
		
	}
	
	public void close() {
		this.setVisible(false);
	}
	
	public static void showDialog(Frame frame, MapViewer mapViewer, CopyData copyData) {
		PasteDialog dialog = new PasteDialog(frame);
		dialog.copyData=copyData;
		dialog.mapViewer=mapViewer;
		dialog.mapRenderer=mapViewer.getRenderer();
		dialog.initSelection();
		dialog.setLocationRelativeTo(mapViewer);
		dialog.setVisible(true);
		return;
	}
	
	public PasteDialog(Frame owner) {
		super(owner);
		
		ActionListener moveAction = new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {

				if (e.getSource()==jButtonXPTile) { offsetX+=tileSize; tileOffsetX+=1; }
				if (e.getSource()==jButtonXNTile) { offsetX-=tileSize; tileOffsetX-=1; }
				if (e.getSource()==jButtonZPTile) { offsetZ+=tileSize; tileOffsetZ+=1; }
				if (e.getSource()==jButtonZNTile) { offsetZ-=tileSize; tileOffsetZ-=1; }
				
				if (e.getSource()==jButtonXPCell) { offsetX+=1; }
				if (e.getSource()==jButtonXNCell) { offsetX-=1; }
				if (e.getSource()==jButtonZPCell) { offsetZ+=1; }
				if (e.getSource()==jButtonZNCell) { offsetZ-=1; }
				
				updateSelection();
				
			}};
		
		jButtonXPTile.addActionListener(moveAction);
		jButtonXNTile.addActionListener(moveAction);
		jButtonZPTile.addActionListener(moveAction);
		jButtonZNTile.addActionListener(moveAction);
		jButtonXPCell.addActionListener(moveAction);
		jButtonXNCell.addActionListener(moveAction);
		jButtonZPCell.addActionListener(moveAction);
		jButtonZNCell.addActionListener(moveAction);
			
		jButtonReset.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				centerSelection();
				updateSelection();
			}
		});
		
		jButtonPaste.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				doPasteData();
			}
		});
		
		jCheckBoxPasteTiles.addChangeListener(new javax.swing.event.ChangeListener() {
			public void stateChanged(javax.swing.event.ChangeEvent e) {
				updateSelection();
			}
		});
		
		jButtonClose.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				close();
			}
		});
		
	}

}
