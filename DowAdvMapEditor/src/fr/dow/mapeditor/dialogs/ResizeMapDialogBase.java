package fr.dow.mapeditor.dialogs;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import java.awt.Font;
import java.awt.Color;

import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class ResizeMapDialogBase extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanel5 = null;
	private JPanel jPanel7 = null;
	private JPanel jPanel8 = null;
	protected JButton jButtonDefault = null;
	private JPanel jPanel9 = null;
	protected JButton jButtonApply = null;
	protected JButton jButtonClose = null;
	private JPanel jPanel = null;
	protected JRadioButton jRadioButtonX128 = null;
	protected JRadioButton jRadioButtonX256 = null;
	protected JRadioButton jRadioButtonX512 = null;
	protected JRadioButton jRadioButtonX1024 = null;
	protected JRadioButton jRadioButtonX2048 = null;
	private JPanel jPanel2 = null;
	protected JRadioButton jRadioButtonZ128 = null;
	protected JRadioButton jRadioButtonZ256 = null;
	protected JRadioButton jRadioButtonZ512 = null;
	protected JRadioButton jRadioButtonZ1024 = null;
	protected JRadioButton jRadioButtonZ2048 = null;
	
	/**
	 * @param owner
	 */
	public ResizeMapDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(403, 223);
		this.setModal(true);
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel5(), BorderLayout.CENTER);
			jContentPane.add(getJPanel7(), BorderLayout.SOUTH);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel5	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel5() {
		if (jPanel5 == null) {
			GridLayout gridLayout = new GridLayout();
			gridLayout.setRows(1);
			gridLayout.setColumns(2);
			jPanel5 = new JPanel();
			jPanel5.setLayout(gridLayout);
			jPanel5.add(getJPanel(), null);
			jPanel5.add(getJPanel2(), null);
		}
		return jPanel5;
	}

	/**
	 * This method initializes jPanel7	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel7() {
		if (jPanel7 == null) {
			jPanel7 = new JPanel();
			jPanel7.setLayout(new BorderLayout());
			jPanel7.add(getJPanel8(), java.awt.BorderLayout.WEST);
			jPanel7.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanel7;
	}

	/**
	 * This method initializes jPanel8	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel8() {
		if (jPanel8 == null) {
			FlowLayout flowLayout6 = new FlowLayout();
			flowLayout6.setHgap(10);
			jPanel8 = new JPanel();
			jPanel8.setLayout(flowLayout6);
			jPanel8.add(getJButtonDefault(), null);
		}
		return jPanel8;
	}

	/**
	 * This method initializes jButtonDefault	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonDefault() {
		if (jButtonDefault == null) {
			jButtonDefault = new JButton();
			jButtonDefault.setPreferredSize(new Dimension(100, 23));
			jButtonDefault.setText("Default");
		}
		return jButtonDefault;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonApply(), null);
			jPanel9.add(getJButtonClose(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonApply	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonApply() {
		if (jButtonApply == null) {
			jButtonApply = new JButton();
			jButtonApply.setPreferredSize(new Dimension(100, 23));
			jButtonApply.setText("Apply");
		}
		return jButtonApply;
	}

	/**
	 * This method initializes jButtonClose	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonClose() {
		if (jButtonClose == null) {
			jButtonClose = new JButton();
			jButtonClose.setPreferredSize(new Dimension(100, 23));
			jButtonClose.setText("Close");
		}
		return jButtonClose;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(new BoxLayout(getJPanel(), BoxLayout.Y_AXIS));
			jPanel.setBorder(BorderFactory.createTitledBorder(null, "X axis", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
			jPanel.add(getJRadioButtonX128(), null);
			jPanel.add(getJRadioButtonX256(), null);
			jPanel.add(getJRadioButtonX512(), null);
			jPanel.add(getJRadioButtonX1024(), null);
			jPanel.add(getJRadioButtonX2048(), null);
			
			ButtonGroup buttonGroupX=new ButtonGroup();
			buttonGroupX.add(jRadioButtonX128);
			buttonGroupX.add(jRadioButtonX256);
			buttonGroupX.add(jRadioButtonX512);
			buttonGroupX.add(jRadioButtonX1024);
			buttonGroupX.add(jRadioButtonX2048);
			
		}
		return jPanel;
	}

	/**
	 * This method initializes jRadioButtonX128	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButtonX128() {
		if (jRadioButtonX128 == null) {
			jRadioButtonX128 = new JRadioButton();
			jRadioButtonX128.setText("128");
		}
		return jRadioButtonX128;
	}

	/**
	 * This method initializes jRadioButtonX256	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButtonX256() {
		if (jRadioButtonX256 == null) {
			jRadioButtonX256 = new JRadioButton();
			jRadioButtonX256.setText("256");
		}
		return jRadioButtonX256;
	}

	/**
	 * This method initializes jRadioButtonX512	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButtonX512() {
		if (jRadioButtonX512 == null) {
			jRadioButtonX512 = new JRadioButton();
			jRadioButtonX512.setText("512");
		}
		return jRadioButtonX512;
	}

	/**
	 * This method initializes jRadioButtonX1024	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButtonX1024() {
		if (jRadioButtonX1024 == null) {
			jRadioButtonX1024 = new JRadioButton();
			jRadioButtonX1024.setText("1024");
		}
		return jRadioButtonX1024;
	}

	/**
	 * This method initializes jRadioButtonX2048	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButtonX2048() {
		if (jRadioButtonX2048 == null) {
			jRadioButtonX2048 = new JRadioButton();
			jRadioButtonX2048.setText("2048");
		}
		return jRadioButtonX2048;
	}

	/**
	 * This method initializes jPanel2	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel2() {
		if (jPanel2 == null) {
			jPanel2 = new JPanel();
			jPanel2.setLayout(new BoxLayout(getJPanel2(), BoxLayout.Y_AXIS));
			jPanel2.setBorder(BorderFactory.createTitledBorder(null, "Z axis", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213)));
			jPanel2.add(getJRadioButtonZ128(), null);
			jPanel2.add(getJRadioButtonZ256(), null);
			jPanel2.add(getJRadioButtonZ512(), null);
			jPanel2.add(getJRadioButtonZ1024(), null);
			jPanel2.add(getJRadioButtonZ2048(), null);
			
			ButtonGroup buttonGroupZ=new ButtonGroup();
			buttonGroupZ.add(jRadioButtonZ128);
			buttonGroupZ.add(jRadioButtonZ256);
			buttonGroupZ.add(jRadioButtonZ512);
			buttonGroupZ.add(jRadioButtonZ1024);
			buttonGroupZ.add(jRadioButtonZ2048);
			
		}
		return jPanel2;
	}

	/**
	 * This method initializes jRadioButtonZ128	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButtonZ128() {
		if (jRadioButtonZ128 == null) {
			jRadioButtonZ128 = new JRadioButton();
			jRadioButtonZ128.setText("128");
		}
		return jRadioButtonZ128;
	}

	/**
	 * This method initializes jRadioButtonZ256	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButtonZ256() {
		if (jRadioButtonZ256 == null) {
			jRadioButtonZ256 = new JRadioButton();
			jRadioButtonZ256.setText("256");
		}
		return jRadioButtonZ256;
	}

	/**
	 * This method initializes jRadioButtonZ512	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButtonZ512() {
		if (jRadioButtonZ512 == null) {
			jRadioButtonZ512 = new JRadioButton();
			jRadioButtonZ512.setText("512");
		}
		return jRadioButtonZ512;
	}

	/**
	 * This method initializes jRadioButtonZ1024	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButtonZ1024() {
		if (jRadioButtonZ1024 == null) {
			jRadioButtonZ1024 = new JRadioButton();
			jRadioButtonZ1024.setText("1024");
		}
		return jRadioButtonZ1024;
	}

	/**
	 * This method initializes jRadioButtonZ2048	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButtonZ2048() {
		if (jRadioButtonZ2048 == null) {
			jRadioButtonZ2048 = new JRadioButton();
			jRadioButtonZ2048.setText("2048");
		}
		return jRadioButtonZ2048;
	}
	
	
	
}  //  @jve:decl-index=0:visual-constraint="10,10"
