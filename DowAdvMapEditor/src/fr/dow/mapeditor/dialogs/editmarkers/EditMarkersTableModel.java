package fr.dow.mapeditor.dialogs.editmarkers;

import java.util.HashSet;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.sgbmap.units.Marker;
import fr.dow.gamedata.sgbmap.units.Markers;
import fr.dow.mapviewer.display.MapViewer;

public class EditMarkersTableModel extends AbstractTableModel {

	private static final String[] columnNames = { "ID", "Name", "Type", "X", "Y", "Z", "Prox.", "CustomNumber", "CustomString" };

	private MapViewer mapViewer;
	
	class MarkerData {
		Integer id;
		String name;
		String type;
		Float x;
		Float y;
		Float z;
		Float proximity;
		Float customNumber;
		String customString;
	}
	
	Vector<MarkerData> markerDataLines=new Vector<MarkerData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
	public Class<?> getColumnClass(int c) {
		switch (c) {
		case 0 : return Integer.class;
		case 1 : return String.class; 
		case 2 : return String.class; 
		case 3 : return Float.class; 
		case 4 : return Float.class; 
		case 5 : return Float.class; 
		case 6 : return Float.class; 
		case 7 : return Float.class; 
		case 8 : return String.class; 
		}
		return null;
    }
    
    public String getColumnName(int col) { return columnNames[col]; }
    
	@Override
	public int getColumnCount() { return columnNames.length; }

	@Override
	public int getRowCount() { return markerDataLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (markerDataLines==null) return null;
		if (rowIndex>=markerDataLines.size()) return null;
		
		switch (columnIndex) {
		case 0 : return markerDataLines.get(rowIndex).id;
		case 1 : return markerDataLines.get(rowIndex).name; 
		case 2 : return markerDataLines.get(rowIndex).type; 
		case 3 : return markerDataLines.get(rowIndex).x; 
		case 4 : return markerDataLines.get(rowIndex).y; 
		case 5 : return markerDataLines.get(rowIndex).z; 
		case 6 : return markerDataLines.get(rowIndex).proximity; 
		case 7 : return markerDataLines.get(rowIndex).customNumber; 
		case 8 : return markerDataLines.get(rowIndex).customString; 
		}
		
		return null;
		
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex>=markerDataLines.size()) return false;
		switch (columnIndex) {
			case 1 : return true;
			case 3 : return true;
			case 4 : return true;
			case 5 : return true;
			case 6 : return true;
			case 7 : return (markerDataLines.get(rowIndex).customNumber!=null);
			case 8 : return (markerDataLines.get(rowIndex).customString!=null);
			default : return false;
		}
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (rowIndex>=markerDataLines.size()) return;
		MarkerData markerData=markerDataLines.get(rowIndex);
		switch (columnIndex) {
			case 1 : 
				markerData.name=(String)aValue; 
				mapViewer.commandsEditTerrain.doChangeMarkerName(markerData.id,markerData.name);
				break;
			case 3 : 
				markerData.x=(Float)aValue; 
				mapViewer.commandsEditTerrain.doChangeMarkerPos(markerData.id,markerData.x,markerData.y,markerData.z);
				break;
			case 4 : 
				markerData.y=(Float)aValue; 
				mapViewer.commandsEditTerrain.doChangeMarkerPos(markerData.id,markerData.x,markerData.y,markerData.z);
				break;
			case 5 : 
				markerData.z=(Float)aValue; 
				mapViewer.commandsEditTerrain.doChangeMarkerPos(markerData.id,markerData.x,markerData.y,markerData.z);
				break;
			case 6 : 
				markerData.proximity=(Float)aValue; 
				mapViewer.commandsEditTerrain.doChangeMarkerProximity(markerData.id,markerData.proximity);
				break;
			case 7 : 
				markerData.customNumber=(Float)aValue; 
				mapViewer.commandsEditTerrain.doChangeMarkerCustomNumber(markerData.id,markerData.customNumber);
				break;
			case 8 : 
				markerData.customString=(String)aValue; 
				mapViewer.commandsEditTerrain.doChangeMarkerCustomString(markerData.id,markerData.customString);
				break;
			default : 
				return;
		}
		
		
		fireTableDataChanged();
		
	}
	
	public void updateMarkerTable(boolean onlySelected) {
		
		Vector<MarkerData> newDataLines=new Vector<MarkerData>();
		
		Markers markers=mapViewer.getMap().markers;
		
		HashSet<Integer> selectedMarkers = mapViewer.getRenderer().currentSelection.markers;
		
		for (Integer markerID : markers.keySet())	{
			
			if (onlySelected)
				if (!selectedMarkers.contains(markerID))
					continue;
			
			Marker marker=markers.get(markerID);
			
			MarkerData markerData=new MarkerData();
			markerData.id=markerID;
			markerData.name=marker.get_name();
			markerData.type=marker.get_markertype().toString();
			markerData.x=marker.get_x();
			markerData.y=marker.get_y();
			markerData.z=marker.get_z();
			markerData.proximity=marker.get_proximity();
			markerData.customNumber=marker.get_customNumber();
			markerData.customString=marker.get_customString();
			
			newDataLines.add(markerData);
			
		}
		
		markerDataLines=newDataLines;
		fireTableDataChanged();
		
	}
	
	public void setMapViewer(MapViewer mapViewer, boolean onlySelected) {
		this.mapViewer=mapViewer;
		updateMarkerTable(onlySelected);
	}	
	
	
}
