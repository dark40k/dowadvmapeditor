package fr.dow.mapeditor.dialogs.editmarkers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.JOptionPane;

import fr.dow.gamedata.sgbmap.units.MarkerType;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeEvent;
import fr.dow.mapviewer.display.selection.Selection.SelectionChangeListener;

public class EditMarkersDialog extends EditMarkersDialogBase implements ActionListener, SelectionChangeListener {

	MapViewer mapViewer;
	
	EditMarkersTableModel markerTable;
	
	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		EditMarkersDialog dialog = new EditMarkersDialog(mapEditor, mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		mapViewer.getRenderer().curFilter.lockAllExcept(FilterType.MARKER);
		dialog.setVisible(true);
		return;
	}	
	
	public void close() {
		mapViewer.getRenderer().curFilter.unlockAll();
		this.setVisible(false);
	}
	
	public EditMarkersDialog(MapEditor mapEditor, MapViewer mapViewer) {
		super(mapEditor);
		
		this.mapViewer=mapViewer;
		
		markerTable=(EditMarkersTableModel)jTableMapList.getModel();
		markerTable.setMapViewer(mapViewer,jCheckBoxMapSelectionFilter.isSelected());
				
		jTableMapList.setAutoCreateRowSorter(true);
		jTableMapList.getColumnModel().getColumn(0).setPreferredWidth(20);
		jTableMapList.getColumnModel().getColumn(1).setPreferredWidth(100);
		jTableMapList.getColumnModel().getColumn(2).setPreferredWidth(50);
		jTableMapList.getColumnModel().getColumn(3).setPreferredWidth(25);
		jTableMapList.getColumnModel().getColumn(4).setPreferredWidth(25);
		jTableMapList.getColumnModel().getColumn(5).setPreferredWidth(25);
		jTableMapList.getColumnModel().getColumn(6).setPreferredWidth(25);
		jTableMapList.getColumnModel().getColumn(7).setPreferredWidth(25);
		jTableMapList.getColumnModel().getColumn(8).setPreferredWidth(100);
	
		jCheckBoxMapSelectionFilter.addActionListener(this);
		
		jButtonClose.addActionListener(this);
		
		jButtonTableSelAll.addActionListener(this);
		jButtonTableSelClear.addActionListener(this);
		jButtonTableSelFromMap.addActionListener(this);
		jButtonTableSelToMap.addActionListener(this);
		
		jButtonMarkerType.addActionListener(this);
		jButtonMarkerDelete.addActionListener(this);
		
		jButtonImportTextFile.setEnabled(false); // TODO EditDecals : add ImportTextFile
		jButtonExportTextFile.setEnabled(false); // TODO EditDecals : add ExportTextFile
		
		mapViewer.getRenderer().currentSelection.addSelectionChangeListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource()==jCheckBoxMapSelectionFilter) { updateTable(); return; }
		
		if (e.getSource()==jButtonClose) { close(); return; }
		
		if (e.getSource()==jButtonTableSelAll) { tableSelectAll(); return; }
		if (e.getSource()==jButtonTableSelClear){ tableSelClear(); return; }
		if (e.getSource()==jButtonTableSelFromMap) { tableSelFromMap(); return; }
		if (e.getSource()==jButtonTableSelToMap) { tableSelToMap(); return; }
		
		if (e.getSource()==jButtonMarkerType) { markerType(); return; } 
		if (e.getSource()==jButtonMarkerDelete) { markerDelete(); return; }
		
	}

	private void updateTable() {
		
		HashSet<Integer> tableSelMarkers = new HashSet<Integer>();

		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			tableSelMarkers.add(markerTable.markerDataLines.get(i).id);
		}

		jTableMapList.clearSelection();
		
		markerTable.updateMarkerTable(jCheckBoxMapSelectionFilter.isSelected());
		
		for (int i=0;i<markerTable.markerDataLines.size();i++) 
			if (tableSelMarkers.contains(markerTable.markerDataLines.get(i).id)){
				int row=jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}
	
	@Override
	public void selectionChange(SelectionChangeEvent evt) {
		// TODO Auto-generated method stub
		if (jCheckBoxMapSelectionFilter.isSelected()) {
			updateTable();
		}
	}
	
	private void markerDelete() {
		
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			mapViewer.commandsEditTerrain.doMarkerDelete(markerTable.markerDataLines.get(i).id);
		}
		
		updateTable();
		
	}

	private void markerType() {

		Vector<String> options=new Vector<String>();
		for (MarkerType markerType : MarkerType.values()) options.add(markerType.getTypeName());
		
		String s = (String)JOptionPane.showInputDialog(	mapViewer,
		                    "Select new type for marker(s)",
		                    "Marker Type",
		                    JOptionPane.QUESTION_MESSAGE,
		                    null, options.toArray(),
		                    "basic_marker");

		//detect cancel
		if ((s == null) || (s.length() == 0)) return;

		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			mapViewer.commandsEditTerrain.doMarkerChangeType(markerTable.markerDataLines.get(i).id,MarkerType.getFromTypeName(s));
		}
		
		updateTable();
		
	}
	
	private void tableSelectAll() { 
		jTableMapList.selectAll(); 
	}
	
	private void tableSelClear() {
		jTableMapList.clearSelection(); 
	}

	private void tableSelFromMap() {
		
		HashSet<Integer> selectedDecals = mapViewer.getRenderer().currentSelection.markers;
		jTableMapList.clearSelection();
				
		for (int i=0;i<markerTable.markerDataLines.size();i++)
			if (selectedDecals.contains(markerTable.markerDataLines.get(i).id)) {
				int row=jTableMapList.getRowSorter().convertRowIndexToView(i);
				jTableMapList.addRowSelectionInterval(row, row);
			}
		
	}

	private void tableSelToMap() {
		
		HashSet<Integer> selectedDecals = mapViewer.getRenderer().currentSelection.markers;
		selectedDecals.clear();
		
		for (int row : jTableMapList.getSelectedRows()) {
			int i=jTableMapList.getRowSorter().convertRowIndexToModel(row);
			selectedDecals.add(markerTable.markerDataLines.get(i).id);
		}
		
		updateTable();
		
	}

	
}
