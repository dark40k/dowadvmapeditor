package fr.dow.mapeditor.dialogs;

import java.awt.Frame;

import javax.swing.event.ChangeListener;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.selection.SelFilter;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;

public class CopyDialog extends CopyDialogBase {
	
	private MapViewer mapViewer;
	private MapEditor mapEditor;
	
	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		CopyDialog dialog = new CopyDialog(mapEditor);
		dialog.mapEditor=mapEditor;
		dialog.mapViewer=mapViewer;
		dialog.setLocationRelativeTo(mapViewer);
		dialog.setVisible(true);
		return;
	}	
	
	public void updateFilter() {
		
		SelFilter filter=new SelFilter(FilterType.NONE);
		
		if (jCheckBoxDisplayGroundArea.isSelected()) filter.add(FilterType.GROUNDAREA);
		if (jCheckBoxDisplayTiles.isSelected()) filter.add(FilterType.TILE);
		if (jCheckBoxDisplayDecals.isSelected()) filter.add(FilterType.DECAL);
		if (jCheckBoxDisplayEntities.isSelected()) filter.add(FilterType.ENTITY);
		if (jCheckBoxDisplayMarkers.isSelected()) filter.add(FilterType.MARKER);
		
		mapViewer.getRenderer().curFilter.set(filter);
		
	}
	
	public void copyArea() {
		mapEditor.copyArea(mapViewer);
	}
	
	public void close(){
		this.setVisible(false);
	}
	
	public CopyDialog(Frame owner) {
		
		super(owner);

		jButton11.addActionListener(
			new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					copyArea();
					close();
				}
			}
		);
		
		jButton21.addActionListener(
			new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) { close(); }
			}
		);
		
		
		ChangeListener updateFilerAction = 
			new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) { updateFilter(); }
			};
		
		jCheckBoxDisplayGroundArea.addChangeListener(updateFilerAction);
		jCheckBoxDisplayTiles.addChangeListener(updateFilerAction);
		jCheckBoxDisplayDecals.addChangeListener(updateFilerAction);
		jCheckBoxDisplayEntities.addChangeListener(updateFilerAction);
		jCheckBoxDisplayMarkers.addChangeListener(updateFilerAction);
		
	}

}
