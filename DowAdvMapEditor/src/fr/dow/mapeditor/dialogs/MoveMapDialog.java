package fr.dow.mapeditor.dialogs;

import java.awt.Frame;

import fr.dow.mapviewer.display.MapViewer;

public class MoveMapDialog extends MoveMapDialogBase {
	
	private MapViewer mapViewer;
	
	public void close() {
		this.setVisible(false);
	}
	
	public static void showDialog(Frame frame, MapViewer mapViewer) {
		MoveMapDialog dialog = new MoveMapDialog(frame);
		dialog.setMapViewer(mapViewer);
		dialog.setLocationRelativeTo(mapViewer);
		dialog.setVisible(true);
		return;
	}

	public void setMapViewer(MapViewer mapViewer) {
		this.mapViewer=mapViewer;
	}
	
	public MoveMapDialog(Frame owner) {
		super(owner);
		
		jButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				mapViewer.commandsEditMap.doMoveMap(true, 0, 1);
			}
		});
		
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				mapViewer.commandsEditMap.doMoveMap(false, 0, -1);
			}
		});
		jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				mapViewer.commandsEditMap.doMoveMap(true, -1, 0);
			}
		});
		jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				mapViewer.commandsEditMap.doMoveMap(false, 1, 0);
			}
		});
		jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				mapViewer.commandsEditMap.doMoveMap(true, 1, 0);
			}
		});
		jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				mapViewer.commandsEditMap.doMoveMap(true, 0, -1);
			}
		});
		jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				mapViewer.commandsEditMap.doMoveMap(false, 0, 1);
			}
		});
		jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				mapViewer.commandsEditMap.doMoveMap(false, -1, 0);
			}
		});
		jButton21.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				close();
			}
		});

		
		
	}


}
