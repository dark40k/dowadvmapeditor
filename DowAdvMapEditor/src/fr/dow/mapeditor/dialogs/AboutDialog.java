package fr.dow.mapeditor.dialogs;

import java.awt.Frame;

import fr.dow.mapeditor.MapEditor;

public class AboutDialog extends AboutDialogBase {

	public static void showDialog(MapEditor mapEditor) {
		AboutDialog dialog = new AboutDialog(mapEditor);
		dialog.setLocationRelativeTo(mapEditor);
		dialog.setVisible(true);
		return;
	}	
	
	public void close(){
		this.setVisible(false);
	}
	
	public AboutDialog(Frame owner) {
		super(owner);

		jButton21.addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						close();
					}
				}
			);
			
	}

	
}
