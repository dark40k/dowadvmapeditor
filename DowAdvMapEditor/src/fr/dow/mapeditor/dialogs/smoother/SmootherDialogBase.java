package fr.dow.mapeditor.dialogs.smoother;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;

public class SmootherDialogBase extends JDialog {


	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanel7 = null;
	private JPanel jPanel9 = null;
	protected JButton jButtonApply = null;
	protected JButton jButtonClose = null;
	private JPanel jPanelSmoothParams = null;
	private JPanel jPanelSmoothMethod = null;
	protected JComboBox<String> jComboBoxMethod = null;
	/**
	 * @param owner
	 */
	public SmootherDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(292, 220);
		this.setTitle("Smoothing tool");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel7(), BorderLayout.SOUTH);
			jContentPane.add(getJPanelSmoothParams(), BorderLayout.CENTER);
			jContentPane.add(getJPanelSmoothMethod(), BorderLayout.NORTH);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel7	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel7() {
		if (jPanel7 == null) {
			jPanel7 = new JPanel();
			jPanel7.setLayout(new BorderLayout());
			jPanel7.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanel7;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonApply(), null);
			jPanel9.add(getJButtonClose(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonApply	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonApply() {
		if (jButtonApply == null) {
			jButtonApply = new JButton();
			jButtonApply.setPreferredSize(new Dimension(100, 23));
			jButtonApply.setText("Apply");
		}
		return jButtonApply;
	}

	/**
	 * This method initializes jButtonClose	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonClose() {
		if (jButtonClose == null) {
			jButtonClose = new JButton();
			jButtonClose.setPreferredSize(new Dimension(100, 23));
			jButtonClose.setText("Close");
		}
		return jButtonClose;
	}

	/**
	 * This method initializes jPanelSmoothParams	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelSmoothParams() {
		if (jPanelSmoothParams == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Smoothing parameters");
			jPanelSmoothParams = new JPanel();
			jPanelSmoothParams.setLayout(new BoxLayout(getJPanelSmoothParams(), BoxLayout.Y_AXIS));
			jPanelSmoothParams.setBorder(titledBorder);
		}
		return jPanelSmoothParams;
	}

	/**
	 * This method initializes jPanelSmoothMethod	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelSmoothMethod() {
		if (jPanelSmoothMethod == null) {
			jPanelSmoothMethod = new JPanel();
			jPanelSmoothMethod.setLayout(new BoxLayout(getJPanelSmoothMethod(), BoxLayout.X_AXIS));
			jPanelSmoothMethod.setBorder(BorderFactory.createTitledBorder(null, "Method", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213)));
			jPanelSmoothMethod.add(getJComboBoxMethod(), null);
		}
		return jPanelSmoothMethod;
	}

	/**
	 * This method initializes jComboBoxMethod	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox<String> getJComboBoxMethod() {
		if (jComboBoxMethod == null) {
			jComboBoxMethod = new JComboBox<String>();
		}
		return jComboBoxMethod;
	}
	
}  //  @jve:decl-index=0:visual-constraint="10,10"
