package fr.dow.mapeditor.dialogs.smoother;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;

import fr.dow.gamedata.sgbmap.terrain.Ground;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.joglobject.JoglWireGround;
import fr.dow.mapviewer.display.selection.Selection;
import fr.dow.mapviewer.display.selection.SelArea.ChangeSelAreaEvent;
import fr.dow.mapviewer.display.selection.SelArea.ChangeSelAreaListener;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;

public class SmootherDialog extends SmootherDialogBase implements ActionListener, ChangeSelAreaListener {
	
	private SmootherTool smootherTool ;
	
	private MapViewer mapViewer;
	
	private JoglWireGround joglWireGround;

	private Selection mapSel;

	public static void showDialog(MapEditor mapEditor, MapViewer mapViewer) {
		SmootherDialog dialog = new SmootherDialog(mapEditor, mapViewer);
		dialog.setLocationRelativeTo(mapEditor);
		mapViewer.getRenderer().curFilter.lockAllExcept(FilterType.GROUNDAREA);
		dialog.setVisible(true);
		return;
	}
	
	public SmootherDialog(MapEditor mapEditor, MapViewer mapViewer) {
			
			super(mapEditor);
			
			this.mapViewer=mapViewer;
			
			Ground ground=mapViewer.getMap().terrain.ground;
			
			smootherTool=new SmootherTool(ground);
			
			joglWireGround=new JoglWireGround(
					smootherTool.getSmoothedHeightGrid(),
					ground.heightMap.get_heightGrid_Xaxis(),
					ground.heightMap.get_heightGrid_Zaxis(),
					ground.groundData.get_heightscale());
			
			mapViewer.getRenderer().addDisplayListener(joglWireGround);
			
			mapSel = mapViewer.getRenderer().currentSelection;
			mapSel.area.addChangeSelAreaListener(this);
			
			smootherTool.updateSelection(mapSel);
			joglWireGround.setDisplayWindow(mapSel.area.i1-1, mapSel.area.j1-1, mapSel.area.i2+1, mapSel.area.j2+1);
			
			jComboBoxMethod.setModel(new DefaultComboBoxModel<String>(SmootherTool.methodList));
			jComboBoxMethod.addActionListener(this);
			
			jButtonApply.addActionListener(this);
			jButtonClose.addActionListener(this);
			
	}

	private void apply() {
		smootherTool.applyHeight();
		mapViewer.getMap().terrain.updatePathFinding();
		mapViewer.getRenderer().mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
	}
	
	private void cancel() {
		mapViewer.getRenderer().removeDisplayListener(joglWireGround);
		mapSel.area.removeChangeSelAreaListener(this);
		this.setVisible(false);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource()==jComboBoxMethod) smootherTool.applyMethod(jComboBoxMethod.getSelectedIndex());
		
		if (e.getSource()==jButtonApply) apply();
		if (e.getSource()==jButtonClose) { cancel(); return; }
		
	}

	@Override
	public void changeSelArea(ChangeSelAreaEvent evt) {
		smootherTool.updateSelection(mapSel);
		smootherTool.applyMethod(jComboBoxMethod.getSelectedIndex());
		joglWireGround.setDisplayWindow(mapSel.area.i1-1, mapSel.area.j1-1, mapSel.area.i2+1, mapSel.area.j2+1);
	}
	
}
