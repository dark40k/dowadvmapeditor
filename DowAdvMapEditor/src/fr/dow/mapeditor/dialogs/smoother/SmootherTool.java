package fr.dow.mapeditor.dialogs.smoother;

import fr.dow.gamedata.sgbmap.terrain.Ground;
import fr.dow.gamedata.sgbmap.terrain.HeightMap;
import fr.dow.mapviewer.display.selection.Selection;

public class SmootherTool {
	
	public final static String[] methodList={
		"No Action",
		"Raw blocked area removal"
	};
	
	int i1,i2,j1,j2;
	
	private HeightMap heightMap;
	private float[][] newHeightGrid;
	private float[][] mapHeightGrid;
	private float heightScale;
	
	public SmootherTool(Ground ground) {
		
		heightScale=ground.groundData.get_heightscale();
		heightMap=ground.heightMap;
		
		mapHeightGrid=heightMap.get_heightGrid();
		
		newHeightGrid=new float[mapHeightGrid.length][];
		for (int i=0;i<mapHeightGrid.length;i++) newHeightGrid[i]=mapHeightGrid[i].clone();
		
	}
	
	public void updateSelection(Selection mapSel) {

		restoreHeight();
		
		i1=mapSel.area.i1;
		i2=mapSel.area.i2;
		j1=mapSel.area.j1;
		j2=mapSel.area.j2;
		
	}
	
	public void applyHeight() {
		for (int i=i1;i<=i2;i++)
			for (int j=j1;j<=j2;j++)
				mapHeightGrid[i][j]= Math.min(255,(int) (newHeightGrid[i][j]/heightScale)) * heightScale;
	}
	
	public void restoreHeight() {
		for (int i=i1;i<=i2;i++)
			for (int j=j1;j<=j2;j++)
				newHeightGrid[i][j]=mapHeightGrid[i][j];
	}
	
	public void applyMethod(int method) {
		
		switch (method) {
			case 0 : restoreHeight(); break;
			case 1 : applySmoothFilter1(); break;
		}
		
	}
	
	
	public void applySmoothFilter1() {
		restoreHeight();
		
		int imin,imax,jmin,jmax;
		int[][] h=new int[1+i2-i1][1+j2-j1];
		int[][] hmin=new int[1+i2-i1][1+j2-j1];
		int[][] hmax=new int[1+i2-i1][1+j2-j1];
		
		for (int i=i1;i<=i2;i++)
			for (int j=j1;j<=j2;j++) {
				h[i-i1][j-j1]=((byte) ( mapHeightGrid[i][j] / heightScale)) & 0xFF;
				hmin[i-i1][j-j1]=0;
				hmax[i-i1][j-j1]=255;
			}
		
		imin=0; imax=i2-i1;
		jmin=0; jmax=j2-j1;
		for (int i=imin;i<=imax;i++) { hmin[i][jmin]=h[i][jmin]; hmin[i][jmax]=h[i][jmax]; hmax[i][jmin]=h[i][jmin]; hmax[i][jmax]=h[i][jmax]; }
		for (int j=jmin;j<=jmax;j++) { hmin[imin][j]=h[imin][j]; hmin[imax][j]=h[imax][j]; hmax[imin][j]=h[imin][j]; hmax[imax][j]=h[imax][j]; }
		
		imin=1; imax=i2-i1-1;
		jmin=1; jmax=j2-j1-1;
		
		for (int i=imin;i<=imax;i++)
			for (int j=jmin;j<=jmax;j++) {
				hmin[i][j]=Math.max(hmin[i][j],Math.max(Math.max(h[i-1][j-1],h[i+1][j+1]),Math.max(h[i+1][j+1],h[i+1][j+1]))-6);
				hmin[i][j]=Math.max(hmin[i][j],Math.max(Math.max(h[i][j-1],h[i][j+1]),Math.max(h[i-1][j],h[i+1][j]))-4);
				hmax[i][j]=Math.min(hmax[i][j],Math.min(Math.min(h[i-1][j-1],h[i+1][j+1]),Math.min(h[i+1][j+1],h[i+1][j+1]))+6);
				hmax[i][j]=Math.min(hmax[i][j],Math.min(Math.min(h[i][j-1],h[i][j+1]),Math.min(h[i-1][j],h[i+1][j]))+4);
			}
		
		for (int i=imin;i<=imax;i++)
			for (int j=jmin;j<=jmax;j++) 
				if (hmin[i][j]<=hmax[i][j])
					h[i][j]=Math.max(Math.min(h[i][j], hmax[i][j]),hmin[i][j]);
				
		for (int i=i1;i<=i2;i++)
			for (int j=j1;j<=j2;j++) 
				newHeightGrid[i][j]=h[i-i1][j-j1]*heightScale;
		
	}

	public float[][] getSmoothedHeightGrid() {
		return newHeightGrid;
	}

}
