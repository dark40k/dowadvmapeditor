package fr.dow.mapeditor.dialogs.openmapinmod;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JButton;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import fr.dow.mapeditor.dialogs.openmapinmod.MapListTableModel;
import javax.swing.JCheckBox;

public abstract class OpenMapInModDialogBase extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel jPanelAction = null;
	private JPanel jPanel9 = null;
	protected JButton jButtonOpen = null;
	protected JButton jButtonCancel = null;
	protected JPanel jPanelMapList = null;
	/**
	 * @param owner
	 */
	public OpenMapInModDialogBase(Frame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(923, 630);
		this.setModal(true);
		this.setTitle("Open map");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanelAction(), BorderLayout.SOUTH);
			jContentPane.add(getJPanelMod(), BorderLayout.NORTH);
			jContentPane.add(getJPanelMapList(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanelAction	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelAction() {
		if (jPanelAction == null) {
			jPanelAction = new JPanel();
			jPanelAction.setLayout(new BorderLayout());
			jPanelAction.add(getJPanel9(), java.awt.BorderLayout.EAST);
		}
		return jPanelAction;
	}

	/**
	 * This method initializes jPanel9	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel9() {
		if (jPanel9 == null) {
			FlowLayout flowLayout7 = new FlowLayout();
			flowLayout7.setHgap(10);
			jPanel9 = new JPanel();
			jPanel9.setLayout(flowLayout7);
			jPanel9.add(getJButtonOpen(), null);
			jPanel9.add(getJButtonCancel(), null);
		}
		return jPanel9;
	}

	/**
	 * This method initializes jButtonOpen	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonOpen() {
		if (jButtonOpen == null) {
			jButtonOpen = new JButton();
			jButtonOpen.setPreferredSize(new Dimension(100, 23));
			jButtonOpen.setText("Open");
		}
		return jButtonOpen;
	}

	/**
	 * This method initializes jButtonCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonCancel() {
		if (jButtonCancel == null) {
			jButtonCancel = new JButton();
			jButtonCancel.setPreferredSize(new Dimension(100, 23));
			jButtonCancel.setText("Cancel");
		}
		return jButtonCancel;
	}

	/**
	 * This method initializes jPanelMapList	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMapList() {
		if (jPanelMapList == null) {
			TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder.setTitle("Map List");
			jPanelMapList = new JPanel();
			jPanelMapList.setLayout(new BorderLayout());
			jPanelMapList.setEnabled(true);
			jPanelMapList.setBorder(titledBorder);
			jPanelMapList.add(getJScrollPaneMapList(), BorderLayout.CENTER);
			jPanelMapList.add(getJCheckBoxRankFilter(), BorderLayout.NORTH);
		}
		return jPanelMapList;
	}

	private JPanel jPanelMod = null;
	protected JButton jButtonChangeMod = null;
	protected JLabel jLabelModName = null;
	protected JScrollPane jScrollPaneMapList = null;
	protected JTable jTableMapList = null;
	protected JCheckBox jCheckBoxRankFilter = null;
	
	/**
	 * This method initializes jPanelMod	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelMod() {
		if (jPanelMod == null) {
			jLabelModName = new JLabel();
			jLabelModName.setText("C:\\dsfsdfsdfsdfsdf\\sdfsdfsdf");
			jLabelModName.setHorizontalAlignment(SwingConstants.LEFT);
			TitledBorder titledBorder1 = BorderFactory.createTitledBorder(null, "Copied Data", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11), new Color(0, 70, 213));
			titledBorder1.setTitle("Mod");
			titledBorder1.setBorder(null);
			jPanelMod = new JPanel();
			jPanelMod.setLayout(new BorderLayout());
			jPanelMod.setBorder(titledBorder1);
			jPanelMod.add(getJButtonChangeMod(), BorderLayout.EAST);
			jPanelMod.add(jLabelModName, BorderLayout.CENTER);
		}
		return jPanelMod;
	}

	/**
	 * This method initializes jButtonChangeMod	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonChangeMod() {
		if (jButtonChangeMod == null) {
			jButtonChangeMod = new JButton();
			jButtonChangeMod.setPreferredSize(new Dimension(50, 23));
			jButtonChangeMod.setText("...");
		}
		return jButtonChangeMod;
	}

	/**
	 * This method initializes jScrollPaneMapList	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPaneMapList() {
		if (jScrollPaneMapList == null) {
			jScrollPaneMapList = new JScrollPane();
			jScrollPaneMapList.setName("jScrollPaneMapList");
			jScrollPaneMapList.setViewportView(getJTableMapList());
		}
		return jScrollPaneMapList;
	}

	/**
	 * This method initializes jTableMapList	
	 * 	
	 * @return javax.swing.JTable	
	 */
	private JTable getJTableMapList() {
		if (jTableMapList == null) {
			jTableMapList = new JTable();
			jTableMapList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			jTableMapList.setModel(new MapListTableModel());
			jTableMapList.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			jTableMapList.setShowGrid(true);
		}
		return jTableMapList;
	}

	/**
	 * This method initializes jCheckBoxRankFilter	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxRankFilter() {
		if (jCheckBoxRankFilter == null) {
			jCheckBoxRankFilter = new JCheckBox();
			jCheckBoxRankFilter.setName("jCheckBox");
			jCheckBoxRankFilter.setSelected(true);
			jCheckBoxRankFilter.setText("Hide lower ranks");
		}
		return jCheckBoxRankFilter;
	}
		
	//
	// End of custom part 
	//

}  //  @jve:decl-index=0:visual-constraint="10,10"
