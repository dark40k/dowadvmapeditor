package fr.dow.mapeditor.dialogs.openmapinmod;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.filesystem.DowFile;
import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.gamedata.filesystem.DowFolder;

public class MapListTableModel extends AbstractTableModel {

	private static final String[] columnNames = { "Type", "Map Name", "Rank", "Storing position" };

	private class MapData {
		String type;
		String name;
		Integer rank;
		String file;
		DowFileVersion dowFileVersion;
	}
	
	private Vector<MapData> mapDataLines=new Vector<MapData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
    public String getColumnName(int col) { return columnNames[col]; }
    
	@Override
	public int getColumnCount() { return 4; }

	@Override
	public int getRowCount() { return mapDataLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (mapDataLines==null) return null;
		if (rowIndex>mapDataLines.size()) return null;
		
		switch (columnIndex) {
		case 0 : return mapDataLines.get(rowIndex).type; 
		case 1 : return mapDataLines.get(rowIndex).name; 
		case 2 : return mapDataLines.get(rowIndex).rank; 
		case 3 : return mapDataLines.get(rowIndex).file; 
		}
		
		return null;
		
	}
	
	//
	// Methods for managing table datas
	//
	
	public DowFileVersion getDowFileVersion(int i) {
		MapData mapData=mapDataLines.get(i);
		if (mapData==null) return null;
		return mapData.dowFileVersion;
	}
	
	public void clear() {
		mapDataLines.clear();
		fireTableDataChanged();
	}
	
	public void addMapFolder(DowFolder mapFolder, String type, boolean onlyTop) {
		
		Vector<DowFile> mapDowFileList=mapFolder.getSubDowFileList(new ModuleFilter());
		
		for (DowFile dowFile : mapDowFileList) {
			
			int maxRank=dowFile.getVersionsCount();
			
			for (int i=maxRank-1; i>=0; i--) {
				MapData newMapData=new MapData();
				newMapData.type=type;
				newMapData.name=dowFile.getName();
				newMapData.rank=i;
				
				newMapData.dowFileVersion=dowFile.getDowVersion(i);
				
				if (newMapData.dowFileVersion.getFile()!=null) newMapData.file=newMapData.dowFileVersion.getFile().getAbsolutePath();
				if (newMapData.dowFileVersion.getSgaArchive()!=null) newMapData.file=newMapData.dowFileVersion.getSgaArchive().get_SgaFile().getAbsolutePath();
				
				mapDataLines.add(newMapData);
				
				if (onlyTop) break;
				
			}
			
		}
		
		Collections.sort(
				mapDataLines, 
				new Comparator<MapData>() {
					public int compare(MapData mapData1, MapData mapData2) {
						int comp=mapData1.name.compareTo(mapData2.name);
						if (comp!=0) return comp;
						return mapData1.rank.compareTo(mapData2.rank);
				  }
				});
		
		fireTableDataChanged();
	}

	//
	//
	//
	
	private static class ModuleFilter implements FilenameFilter {
		public boolean accept(File dir, String name) {
			int pos=name.lastIndexOf('.');
			if (pos<0) return false;
			return name.substring(pos).equals(".sgb");
		}
	}
	
	
}
