package fr.dow.mapeditor.dialogs.openmapinmod;

import java.awt.Frame;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.mapeditor.MapEditor;

public class OpenMapInModDialog extends OpenMapInModDialogBase {

	private static File currentDirectory;
	private static File lastModFile;
	
	private MapListTableModel mapListTable;
	
	private DowMod dowMod=null;
	
	private MapEditor mapEditor;
	
	public static void showDialog(MapEditor mapEditor, File defaultModPathFile) {
		OpenMapInModDialog dialog = new OpenMapInModDialog(mapEditor,defaultModPathFile);
		dialog.mapEditor=mapEditor;
		dialog.setLocationRelativeTo(mapEditor);
		dialog.setVisible(true);
		return;
	}	
	
	public OpenMapInModDialog(Frame owner, File defaultModFile) {
		
		super(owner);
		
		jButtonOpen.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) { doOpen(); }
		});
		
		jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) { closeDialog(); }
		});
		
		jButtonChangeMod.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) { doChangeMod(); }
		});
		
		jCheckBoxRankFilter.addChangeListener(new javax.swing.event.ChangeListener() {
			public void stateChanged(javax.swing.event.ChangeEvent e) { doRankChanged(); }
		});
		
		jTableMapList.setAutoCreateRowSorter(true);
		
		jTableMapList.getColumnModel().getColumn(0).setPreferredWidth(25);
		jTableMapList.getColumnModel().getColumn(1).setPreferredWidth(250);
		jTableMapList.getColumnModel().getColumn(2).setPreferredWidth(25);
		jTableMapList.getColumnModel().getColumn(3).setPreferredWidth(400);
		
		mapListTable=(MapListTableModel) jTableMapList.getModel();
		
		if (currentDirectory==null) currentDirectory=defaultModFile;
		
		updateMod(lastModFile);
		
	}

	public void doOpen() {
		
		int selectedRow=jTableMapList.getSelectedRow();
		
		if (selectedRow<0) return;
		
		DowFileVersion dowSgbFile=mapListTable.getDowFileVersion(selectedRow);
		
		DowFileVersion dowTgaFile=dowSgbFile.replaceExtension(".sgb", ".tga");
		
		if (dowTgaFile==null) {
			System.out.println("Error : associated tga file not found.");
			return;
		}
		
		mapEditor.openMap(dowSgbFile,dowTgaFile,dowMod);
		
		closeDialog();
	}
	
	public void closeDialog() {
		this.setVisible(false);
	}
	
	public void doChangeMod() {
    	
        JFileChooser chooser = new JFileChooser();
        
        if (currentDirectory!=null)
        	if (currentDirectory.isDirectory()) 
        		chooser.setCurrentDirectory(currentDirectory);
        
        chooser.setFileFilter(new FileNameExtensionFilter("Dawn of War Mod", "module"));
        
        int res = chooser.showOpenDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        File chosen = chooser.getSelectedFile();
        if (chosen == null) { System.err.println("No file selected"); return; }
        
        currentDirectory=chosen.getParentFile();
        
    	updateMod(chosen);
	}

	public void doRankChanged() {
		
	}
	
    private void updateMod(File modFile) {
    	
		dowMod=DowMod.buildFromModuleFile(modFile);
    			
    	if (dowMod==null) {
    		lockMapSelection();
    		return;
    	}
    	
    	mapListTable.clear();
    	addMaps("multi","scenarios/mp",false);
    	addMaps("single","scenarios/sp",false);
    	
    	unlockMapSelection();
    	
    	lastModFile=modFile;
    }

    private void addMaps(String type,String folderName,boolean onlyTop) {
    	DowFolder dowFolderMP=dowMod.getFolder(folderName);
    	if (dowFolderMP==null) return;
    	mapListTable.addMapFolder(dowFolderMP,type,onlyTop);
    }
    
    
    private void lockMapSelection() {
    	// TODO clear data in table
    	jLabelModName.setText("Click ... to select mod that contains maps  =>");
    	jButtonOpen.setEnabled(false);
    	jCheckBoxRankFilter.setEnabled(false);
    	jTableMapList.setEnabled(false);
    	
    }
    private void unlockMapSelection() {
    	jLabelModName.setText(dowMod.getModFile().getAbsolutePath());
    	jButtonOpen.setEnabled(true);
    	jCheckBoxRankFilter.setEnabled(true);
    	jTableMapList.setEnabled(true);
    }
    
}
