package fr.dow.mapeditor.tools;

import java.util.Comparator;

public class CompareGroupNames implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		
		if (o1.equals(o2)) return 0;
		
		String head1=o1.replaceAll("\\d*$", "");
		String head2=o2.replaceAll("\\d*$", "");
		
		if (!head1.equals(head2)) return o1.compareTo(o2);
		
		int tail1=Integer.valueOf(o1.substring(head1.length()));
		int tail2=Integer.valueOf(o2.substring(head2.length()));
		
		return (tail1>tail2 ? 1 : -1);
	}

}
