package fr.dow.mapeditor.tools;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.*;
import javax.swing.*;

public class ColorizedButton extends JButton implements ActionListener{

	private static ColorizeImageFilter imageFilter = new ColorizeImageFilter();

	private Color color;

	public ColorizedButton() {
		super();
		this.color = Color.white;
		this.addActionListener(this);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color=color;
	}

	public void paintComponent(Graphics g) {
		boolean opaque = isOpaque();
		if (opaque) {
			// Fill background
			g.setColor(getBackground());
			g.fillRect(0, 0, getWidth(), getHeight());
		}

		// Let the UI paint to offscreen image
		setOpaque(false);
		Image img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g2 = img.getGraphics();
		g2.setFont(g.getFont());
		super.paintComponent(g2);

		// Colorize!
		img = imageFilter.colorize(this, img, color);

		// Paint offscreen image to button
		g.drawImage(img, 0, 0, null);
		setOpaque(opaque);
	}

	private static class ColorizeImageFilter extends RGBImageFilter {
		double cr, cg, cb;
		int bgRGB, fgRGB;

		public ColorizeImageFilter() {
			canFilterIndexColorModel = true;
		}

		public Image colorize(JComponent comp, Image fromImage, Color c) {
			cr = c.getRed()   / 255.0;
			cg = c.getGreen() / 255.0;
			cb = c.getBlue()  / 255.0;
			bgRGB = comp.getBackground().getRGB();
			fgRGB = comp.getForeground().getRGB();
			ImageProducer producer = new FilteredImageSource(fromImage.getSource(), this);
			return new ImageIcon(comp.createImage(producer)).getImage();
		}

		public int filterRGB(int x, int y, int rgb) {
			int alpha = rgb & 0xff000000;

			if (rgb == bgRGB || rgb == fgRGB || alpha < 0x80000000) {
				return rgb;
			}

			// Assume all rgb values are shades of gray
			double grayLevel = (rgb & 0xff) / 255.0;
			double r, g, b;

			r = cr * grayLevel;
			g = cg * grayLevel;
			b = cb * grayLevel;

			return (alpha +
					(((int)(r * 255)) << 16) +
					(((int)(g * 255)) << 8) +
					(int)(b * 255));
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		Color oldColor=color;
		Color newColor = JColorChooser.showDialog(this, "Change color", color);
		if (newColor != null) {
			color=newColor;
			firePropertyChange("Color", oldColor, newColor);
		}
	}						
	
	
}
