package fr.dow.mapeditor.tools;

import java.awt.Component;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import fr.dow.gamedata.IniFile;

public class FindValidMod {

	
	public static File showSelectionDialog(Component component, String mapFilename, boolean allways) {
		
		File modFile=null;
		
    	File[] modFileList=findValidMod(mapFilename.substring(0,mapFilename.toLowerCase().lastIndexOf("/data/scenarios")));
    	
    	if (modFileList.length>1) {
    		
    		String[] moduleNames=new String[modFileList.length];
    		for (int i=0;i<modFileList.length;i++) moduleNames[i]=modFileList[i].getName();
    		
    		String s = (String)JOptionPane.showInputDialog(
    							component,
    		                    "Several modules are available to with this map:\n",
    		                    "Select module",
    		                    JOptionPane.QUESTION_MESSAGE,
    		                    null,
    		                    moduleNames,
    		                    moduleNames[0]);
    		
    		if ((s == null) || (s.length() == 0)) return null;

    		for (int i=0;i<modFileList.length;i++) 
    			if (moduleNames[i].equals(s)) {
    				modFile=modFileList[i];
    				break;
    			}
    		
    	}
    	else modFile=modFileList[0];
		
		return modFile;
	}
	
	
	public static File[] findValidMod(String modAbsolutePath) {
		
		File modFolder=new File(modAbsolutePath);
		String modFolderName=modFolder.getName();
		
		File[] modules=new File(modFolder.getParent()).listFiles( new ModuleFilter() );		
		ArrayList<File> validMods=new ArrayList<File>();
		for (File module : modules) {
			
			IniFile modINI=new IniFile(module.getAbsolutePath());

			// check if mod directory is the same as the searched one
			if  (! modFolderName.equalsIgnoreCase(modINI.getStringProperty("global", "ModFolder"))) continue;
			
			// check if mod is playable
			if (modINI.getIntegerProperty("global", "Playable")==0) continue;
			
			validMods.add(module);
		}
		
		return validMods.toArray(new File[validMods.size()]);
		
	}
	
	private static class ModuleFilter implements FilenameFilter {
		public boolean accept(File dir, String name) {
			int pos=name.lastIndexOf('.');
			if (pos<0) return false;
			return ".module".equals(name.substring(pos));
		}
	}
	
}
