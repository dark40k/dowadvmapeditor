package fr.dow.mapeditor;

import java.util.EventListener;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.event.EventListenerList;

public class RecentMapsList {

	public static final Integer maxMaps=10;
	
	public Vector<String> recentMapNames=new Vector<String>(maxMaps);
	
	public void addMap(String mapName) {
		String formattedMapName=mapName.replace('/','\'');
		recentMapNames.remove(formattedMapName);
		recentMapNames.add(0,formattedMapName);
		recentMapNames.setSize(maxMaps);
		
		fireRecentMapListUpdateEvent();
	}
	
	@SuppressWarnings("unchecked")
	public void setMapList(Vector<String> mapNames) {
		recentMapNames=(Vector<String>) mapNames.clone();
		recentMapNames.setSize(maxMaps);
		fireRecentMapListUpdateEvent();
	}
	
	//
	// Change event
	//
	
	private EventListenerList listenerList = new EventListenerList();

	public class UpdateRecentMapListEvent extends EventObject {
		public UpdateRecentMapListEvent(Object source) {
			super(source);
		}
	}

	public interface UpdateRecentMapListListener extends EventListener {
		public void recentMapListUpdated(UpdateRecentMapListEvent evt);
	}

	public void addUpdateListener(UpdateRecentMapListListener listener) {
		listenerList.add(UpdateRecentMapListListener.class, listener);
	}
	
	public void removeUpdateListener(UpdateRecentMapListListener listener) {
		listenerList.remove(UpdateRecentMapListListener.class, listener);
	}
	
	public void fireRecentMapListUpdateEvent() {
		UpdateRecentMapListEvent evt=new UpdateRecentMapListEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateRecentMapListListener.class) {
				((UpdateRecentMapListListener) listeners[i+1]).recentMapListUpdated(evt);
			}
		}
	}
	
}
