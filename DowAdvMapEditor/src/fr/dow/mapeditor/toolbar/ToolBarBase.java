package fr.dow.mapeditor.toolbar;

import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.awt.Dimension;

public class ToolBarBase extends JToolBar {

	private JPanel jPanel = null;
	protected JButton jButtonDecals = null;
	protected JButton jButtonTiles = null;
	protected JButton jButtonEntities = null;
	protected JButton jButtonMarkers = null;
	protected JButton jButtonGround = null;
	private JLabel jLabel = null;
	protected JButton jButtonViewPlayers = null;
	protected JButton jButtonViewWorld = null;
	protected JButton jButtonViewDecals = null;
	protected JButton jButtonViewWater = null;
	protected JComboBox<String> jComboBoxViewLayer = null;
	protected JButton jButtonViewMarkers = null;
	private JLabel jLabel1 = null;

	/**
	 * This method initializes 
	 * 
	 */
	public ToolBarBase() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
        this.add(getJPanel());
			
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jLabel1 = new JLabel();
			jLabel1.setText("View : ");
			jLabel = new JLabel();
			jLabel.setText("Selection filter : ");
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setAlignment(FlowLayout.LEFT);
			flowLayout.setVgap(1);
			flowLayout.setHgap(2);
			jPanel = new JPanel();
			jPanel.setLayout(flowLayout);
			jPanel.add(jLabel, null);
			jPanel.add(getJButtonTiles(), null);
			jPanel.add(getJButtonDecals(), null);
			jPanel.add(getJButtonEntities(), null);
			jPanel.add(getJButtonMarkers(), null);
			jPanel.add(getJButtonGround(), null);
			jPanel.add(jLabel1, null);
			jPanel.add(getJButtonViewPlayers(), null);
			jPanel.add(getJButtonViewWorld(), null);
			jPanel.add(getJButtonViewDecals(), null);
			jPanel.add(getJButtonViewWater(), null);
			jPanel.add(getJButtonViewMarkers(), null);
			jPanel.add(getJComboBoxViewLayer(), null);
		}
		return jPanel;
	}

	/**
	 * This method initializes jButtonDecals	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonDecals() {
		if (jButtonDecals == null) {
			jButtonDecals = new JButton();
			jButtonDecals.setText("Decals");
		}
		return jButtonDecals;
	}

	/**
	 * This method initializes jButtonTiles	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonTiles() {
		if (jButtonTiles == null) {
			jButtonTiles = new JButton();
			jButtonTiles.setText("Tiles");
			jButtonTiles.setEnabled(true);
			jButtonTiles.setSelected(false);
		}
		return jButtonTiles;
	}

	/**
	 * This method initializes jButtonEntities	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonEntities() {
		if (jButtonEntities == null) {
			jButtonEntities = new JButton();
			jButtonEntities.setText("Entities");
		}
		return jButtonEntities;
	}

	/**
	 * This method initializes jButtonMarkers	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonMarkers() {
		if (jButtonMarkers == null) {
			jButtonMarkers = new JButton();
			jButtonMarkers.setText("Markers");
		}
		return jButtonMarkers;
	}

	/**
	 * This method initializes jButtonGround	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonGround() {
		if (jButtonGround == null) {
			jButtonGround = new JButton();
			jButtonGround.setText("Ground");
		}
		return jButtonGround;
	}

	/**
	 * This method initializes jButtonViewPlayers	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonViewPlayers() {
		if (jButtonViewPlayers == null) {
			jButtonViewPlayers = new JButton();
			jButtonViewPlayers.setEnabled(true);
			jButtonViewPlayers.setText("Players");
			jButtonViewPlayers.setSelected(false);
		}
		return jButtonViewPlayers;
	}

	/**
	 * This method initializes jButtonViewWorld	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonViewWorld() {
		if (jButtonViewWorld == null) {
			jButtonViewWorld = new JButton();
			jButtonViewWorld.setEnabled(true);
			jButtonViewWorld.setText("World");
			jButtonViewWorld.setSelected(false);
		}
		return jButtonViewWorld;
	}

	/**
	 * This method initializes jButtonViewDecals	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonViewDecals() {
		if (jButtonViewDecals == null) {
			jButtonViewDecals = new JButton();
			jButtonViewDecals.setEnabled(true);
			jButtonViewDecals.setText("Decals");
			jButtonViewDecals.setSelected(false);
		}
		return jButtonViewDecals;
	}

	/**
	 * This method initializes jButtonViewWater	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonViewWater() {
		if (jButtonViewWater == null) {
			jButtonViewWater = new JButton();
			jButtonViewWater.setEnabled(true);
			jButtonViewWater.setText("Water");
			jButtonViewWater.setSelected(false);
		}
		return jButtonViewWater;
	}

	/**
	 * This method initializes jComboBoxViewLayer	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox<String> getJComboBoxViewLayer() {
		if (jComboBoxViewLayer == null) {
			jComboBoxViewLayer = new JComboBox<String>();
			jComboBoxViewLayer.setPreferredSize(new Dimension(80, 20));
			jComboBoxViewLayer.setPreferredSize(new Dimension(80, 20));
		}
		return jComboBoxViewLayer;
	}

	/**
	 * This method initializes jButtonViewMarkers	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonViewMarkers() {
		if (jButtonViewMarkers == null) {
			jButtonViewMarkers = new JButton();
			jButtonViewMarkers.setEnabled(true);
			jButtonViewMarkers.setText("Markers");
			jButtonViewMarkers.setSelected(false);
		}
		return jButtonViewMarkers;
	}

}
