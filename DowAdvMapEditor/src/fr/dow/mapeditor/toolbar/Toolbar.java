package fr.dow.mapeditor.toolbar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.dow.gamedata.sgbmap.units.PlayerID;
import fr.dow.mapviewer.display.MapViewer;
import fr.dow.mapviewer.display.params.MapDisPars;
import fr.dow.mapviewer.display.params.BaseDisPars.UpdateEvent;
import fr.dow.mapviewer.display.params.BaseDisPars.UpdateListener;

import fr.dow.mapviewer.display.selection.SelFilter;
import fr.dow.mapviewer.display.selection.SelFilter.ChangeFilterEvent;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;

public class Toolbar extends ToolBarBase implements ActionListener,SelFilter.ChangeFilterListener, UpdateListener {

	private MapViewer mapViewer;
	private SelFilter selFilter;
	private MapDisPars mapDisPars;
	
	public Toolbar() {
		super();
		
		jButtonTiles.addActionListener(this);
		jButtonDecals.addActionListener(this);
		jButtonEntities.addActionListener(this);
		jButtonMarkers.addActionListener(this);
		jButtonGround.addActionListener(this);
		
		jButtonViewPlayers.addActionListener(this);
		jButtonViewWorld.addActionListener(this);
		jButtonViewDecals.addActionListener(this);
		jButtonViewWater.addActionListener(this);
		jButtonViewMarkers.addActionListener(this);
		
		jComboBoxViewLayer.addItem("None");
		jComboBoxViewLayer.addItem("Cover");
		jComboBoxViewLayer.addItem("Impass");
		jComboBoxViewLayer.addItem("Footfall");
		jComboBoxViewLayer.addItem("Passability Map");
		jComboBoxViewLayer.addItem("Pathsize Map");
		jComboBoxViewLayer.addItem("Path Sector");
		jComboBoxViewLayer.addActionListener(this);
		
		updateDisplay();
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource()==jButtonTiles) {
			jButtonTiles.setSelected(!jButtonTiles.isSelected());
			if (selFilter!=null) selFilter.setFilter(FilterType.TILE, jButtonTiles.isSelected());
		}
		if (e.getSource()==jButtonDecals) {
			jButtonDecals.setSelected(!jButtonDecals.isSelected());
			if (selFilter!=null) selFilter.setFilter(FilterType.DECAL, jButtonDecals.isSelected());
		}
		if (e.getSource()==jButtonEntities) {
			jButtonEntities.setSelected(!jButtonEntities.isSelected());
			if (selFilter!=null) selFilter.setFilter(FilterType.ENTITY, jButtonEntities.isSelected());
		}
		if (e.getSource()==jButtonMarkers) {
			jButtonMarkers.setSelected(!jButtonMarkers.isSelected());
			if (selFilter!=null) selFilter.setFilter(FilterType.MARKER, jButtonMarkers.isSelected());
		}
		if (e.getSource()==jButtonGround) {
			jButtonGround.setSelected(!jButtonGround.isSelected());
			if (selFilter!=null) selFilter.setFilter(FilterType.GROUNDAREA, jButtonGround.isSelected());
		}
		
		if (e.getSource()==jButtonViewPlayers) {
			if (mapDisPars!=null) {
				setAllPlayersVisibility(!jButtonViewPlayers.isSelected());
				mapDisPars.playerDisplayParams.fireUpdateEvent(1);
			}
		}

		if (e.getSource()==jButtonViewWorld) {
			if (mapDisPars!=null) {
				mapDisPars.playerDisplayParams.players.get(PlayerID.World).isVisible=!jButtonViewWorld.isSelected();
				mapDisPars.playerDisplayParams.fireUpdateEvent(1);
			}
		}

		if (e.getSource()==jButtonViewDecals) {
			if (mapDisPars!=null) {
				mapDisPars.terrainDisplayParams.decals.isVisible=!jButtonViewDecals.isSelected();
				mapDisPars.terrainDisplayParams.fireUpdateEvent(1);
			}
		}

		if (e.getSource()==jButtonViewWater) {
			if (mapDisPars!=null) {
				mapDisPars.terrainDisplayParams.water.isVisible=!jButtonViewWater.isSelected();
				mapDisPars.terrainDisplayParams.fireUpdateEvent(1);
			}
		}

		if (e.getSource()==jComboBoxViewLayer) {
			if (mapDisPars!=null) {
				mapDisPars.terrainDisplayParams.coverOverlay.isVisible=(jComboBoxViewLayer.getSelectedIndex()==1);
				mapDisPars.terrainDisplayParams.impassOverlay.isVisible=(jComboBoxViewLayer.getSelectedIndex()==2);
				mapDisPars.terrainDisplayParams.footfallOverlay.isVisible=(jComboBoxViewLayer.getSelectedIndex()==3);
				mapDisPars.terrainDisplayParams.passabilityDisPars.isVisible=(jComboBoxViewLayer.getSelectedIndex()==4);
				mapDisPars.terrainDisplayParams.preciseDisPars.isVisible=(jComboBoxViewLayer.getSelectedIndex()==5);
				mapDisPars.terrainDisplayParams.pathDisPars.isVisible=(jComboBoxViewLayer.getSelectedIndex()==6);
				mapDisPars.terrainDisplayParams.fireUpdateEvent(1);
			}
		}

		if (e.getSource()==jButtonViewMarkers) {
			if (mapDisPars!=null) {
				mapDisPars.markerDisPars.showMarkers=!jButtonViewMarkers.isSelected();
				mapDisPars.markerDisPars.fireUpdateEvent(1);
			}
		}
		
	}
	
    public void setLinkedMapViewer(MapViewer newMapViewer) {
    	
    	if (mapViewer!=null) {
    		selFilter.removeChangeFilterListener(this);
    		mapDisPars.removeUpdateListener(this);
    	}

    	mapViewer=newMapViewer;
    	
    	selFilter=mapViewer.getRenderer().curFilter;
    	selFilter.addChangeFilterListener(this);

    	mapDisPars=mapViewer.getRenderer().mapDisplayParams;
		mapDisPars.addUpdateListener(this);
    	
    	updateDisplay();
    	
    }
    
	public void removeLinkedMapViewer(MapViewer oldMapViewer) {
		if (mapViewer!=oldMapViewer) return;
		mapViewer=null;
		selFilter.removeChangeFilterListener(this);
		selFilter=null;
		mapDisPars.removeUpdateListener(this);
		mapDisPars=null;
		updateDisplay();
	}

	@Override
	public void changeFilter(ChangeFilterEvent evt) {
		updateDisplay();
	}

	private void updateDisplay() {

		if (mapViewer==null) {
			jButtonTiles.setEnabled(false);
			jButtonTiles.setSelected(false);

			jButtonDecals.setEnabled(false);
			jButtonDecals.setSelected(false);
			
			jButtonEntities.setEnabled(false);
			jButtonEntities.setSelected(false);
			
			jButtonMarkers.setEnabled(false);
			jButtonMarkers.setSelected(false);
			
			jButtonGround.setEnabled(false);				
			jButtonGround.setSelected(false);		
			
			jButtonViewPlayers.setEnabled(false);
			jButtonViewPlayers.setSelected(false);
			
			jButtonViewWorld.setEnabled(false);
			jButtonViewWorld.setSelected(false);
			
			jButtonViewDecals.setEnabled(false);
			jButtonViewDecals.setSelected(false);
			
			jButtonViewWater.setEnabled(false);
			jButtonViewWater.setSelected(false);
			
			jComboBoxViewLayer.setEnabled(false);
			jComboBoxViewLayer.setSelectedIndex(0);
			
			jButtonViewMarkers.setEnabled(false);
			jButtonViewMarkers.setSelected(false);
			
			return;
		}
		
		jButtonTiles.setEnabled(!selFilter.isLocked(FilterType.TILE));
		jButtonTiles.setSelected(!selFilter.isFiltered(FilterType.TILE));
		
		jButtonDecals.setEnabled(!selFilter.isLocked(FilterType.DECAL));
		jButtonDecals.setSelected(!selFilter.isFiltered(FilterType.DECAL));
		
		jButtonEntities.setEnabled(!selFilter.isLocked(FilterType.ENTITY));
		jButtonEntities.setSelected(!selFilter.isFiltered(FilterType.ENTITY));

		jButtonMarkers.setEnabled(!selFilter.isLocked(FilterType.MARKER));
		jButtonMarkers.setSelected(!selFilter.isFiltered(FilterType.MARKER));
		
		jButtonGround.setEnabled(!selFilter.isLocked(FilterType.GROUNDAREA));
		jButtonGround.setSelected(!selFilter.isFiltered(FilterType.GROUNDAREA));

		jButtonViewPlayers.setEnabled(true);
		jButtonViewPlayers.setSelected(getAllPlayersVisibility());
		
		jButtonViewWorld.setEnabled(true);
		jButtonViewWorld.setSelected(mapDisPars.playerDisplayParams.players.get(PlayerID.World).isVisible);
				
		jButtonViewDecals.setEnabled(true);
		jButtonViewDecals.setSelected(mapDisPars.terrainDisplayParams.decals.isVisible);
		
		jButtonViewWater.setEnabled(true);
		jButtonViewWater.setSelected(mapDisPars.terrainDisplayParams.water.isVisible);
		
		jComboBoxViewLayer.setEnabled(true);
		jComboBoxViewLayer.setSelectedIndex(getOverlayIndex());
		
		jButtonViewMarkers.setEnabled(true);
		jButtonViewMarkers.setSelected(mapDisPars.markerDisPars.showMarkers);
		
	}

	@Override
	public void changeUpdateLevel(UpdateEvent evt) {
		updateDisplay();
	}


	private void setAllPlayersVisibility(boolean isVisible) {
		for (PlayerID playerID : PlayerID.playerValues()) mapDisPars.playerDisplayParams.players.get(playerID).isVisible=isVisible;
	}
	
	private boolean getAllPlayersVisibility() {
		for (PlayerID playerID : PlayerID.playerValues()) 
			if (mapDisPars.playerDisplayParams.players.get(playerID).isVisible)
				return true;
		return false;
	}

	private int getOverlayIndex() {
		if (mapDisPars.terrainDisplayParams.coverOverlay.isVisible) return 1;
		if (mapDisPars.terrainDisplayParams.impassOverlay.isVisible) return 2;
		if (mapDisPars.terrainDisplayParams.footfallOverlay.isVisible) return 3;
		if (mapDisPars.terrainDisplayParams.passabilityDisPars.isVisible) return 4;
		if (mapDisPars.terrainDisplayParams.preciseDisPars.isVisible) return 5;
		if (mapDisPars.terrainDisplayParams.pathDisPars.isVisible) return 6;
		return 0;
	}
	
}
