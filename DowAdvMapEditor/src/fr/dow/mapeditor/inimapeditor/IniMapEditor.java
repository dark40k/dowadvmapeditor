package fr.dow.mapeditor.inimapeditor;

import java.io.File;
import java.util.Vector;

import fr.dow.gamedata.IniFile;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.RecentMapsList;


public class IniMapEditor {

	private static final String iniFileName="./mapEditor.ini";

	private static final String sectionNameRecentMap="Recent Maps";
	private static final String propertyBaseNameRecentMap="recent.";
	
	IniFile iniFile;
	
	public IniMapEditor() {
		iniFile=new IniFile(iniFileName);
	}
	
	public void apply(MapEditor mapEditor) {

		DowMod.getDowModParams().importFromIniFile(iniFile);
		
		importRecentMapListFromIniFile(mapEditor.recentMapList);
		
	}
	
	public void save(MapEditor mapEditor) {
		
		DowMod.getDowModParams().exportToIniFile(iniFile);
		
		updateRecentMapListToIniFile(mapEditor.recentMapList);
		
		iniFile.save();
	}
	

	private void importRecentMapListFromIniFile(RecentMapsList recentMapsList) {
		
		Vector<String> mapNames=new Vector<String>(RecentMapsList.maxMaps);
		
		Integer mapNbr=RecentMapsList.maxMaps-1;
		do {
			String mapName=iniFile.getStringProperty(sectionNameRecentMap, propertyBaseNameRecentMap+mapNbr);
			if (mapName!=null) 
				if ((new File(mapName)).exists())
					mapNames.add(0,mapName);
			mapNbr--;
		} while (mapNbr>=0);
		
		recentMapsList.setMapList(mapNames);
	}

	private void updateRecentMapListToIniFile(RecentMapsList recentMapsList) {
		
		iniFile.addSection(sectionNameRecentMap, null);
		
		for (Integer mapNbr=0; mapNbr<recentMapsList.recentMapNames.size();mapNbr++) {
			iniFile.setStringProperty(
					sectionNameRecentMap, 
					propertyBaseNameRecentMap+mapNbr, 
					recentMapsList.recentMapNames.get(mapNbr), 
					null);
		}
		
	}
}
