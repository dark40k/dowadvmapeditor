package fr.dow.gamedata.rgdattribute;

import java.util.LinkedHashMap;

import fr.dow.gamedata.RelicChunkDATA;


public class AttributeTable implements Attribute {
	
	/*
	Header block
		DWORD checksum : checksum CRC 32 starting from end of block
	 	DWORD size : data size starting from end of block (size of top level Data block)

	Data block Header
		DWORD SubDataNbr : nbr of sub datas
		repeat SubDataNbr X 
			DWORD Code : data code, see dictionnary to know what it is
			DWORD DataType : 
				0x00-> float => DWORD 
				0x02-> boolean (coded on a single byte) => BYTE 
				0x03-> string (byte=0 for end) => BYTE[] 
				0x04-> string coded on 2 bytes per caracter, 2nd is zero, byte=0 for end => BYTE[]
				0x64->datablock => points to the start of a sub Data block header
			DWORD Offset : position of data starting from header end (i.e. 0 for first data)
		end
		BYTE[] datas, subdatas,...
	*/

	private String _data;
	
	private LinkedHashMap<Integer, Attribute> _subData=new LinkedHashMap<Integer, Attribute>();
	
	public AttributeTable(RelicChunkDATA chunk, int pos) {
		
		int subDataNbr=chunk.dataGetInt(pos); pos+=4;
		
		int dataPos=pos+subDataNbr*12;
		
		for (int i=0; i<subDataNbr;i++) {
			int subDataCode=chunk.dataGetInt(pos); pos+=4;
			int subDataType=chunk.dataGetInt(pos); pos+=4;
			int subDataOffset=chunk.dataGetInt(pos); pos+=4;
			
			if (subDataCode!=0x49D60FAE)
				switch (subDataType) {
				case 0x00 : _subData.put(subDataCode, new AttributeFloat(chunk,dataPos+subDataOffset)); break;  
				case 0x02 : _subData.put(subDataCode, new AttributeBoolean(chunk,dataPos+subDataOffset)); break;
				case 0x03 : _subData.put(subDataCode, new AttributeString(chunk,dataPos+subDataOffset,AttributeString.STANDARD)); break;
				case 0x04 : _subData.put(subDataCode, new AttributeString(chunk,dataPos+subDataOffset,AttributeString.DOUBLE)); break;
				case 0x64 : _subData.put(subDataCode, new AttributeTable(chunk,dataPos+subDataOffset)); break;
				default : throw new RuntimeException("subDataType is unknown " + subDataType);
				}
			else {
				AttributeString tmpAttr;
				switch (subDataType) {
				case 0x03 : tmpAttr=new AttributeString(chunk,dataPos+subDataOffset,AttributeString.STANDARD); break;
				case 0x04 : tmpAttr=new AttributeString(chunk,dataPos+subDataOffset,AttributeString.DOUBLE); break;
				default : throw new RuntimeException("subDataType is unknown for $REF " + subDataType);
				}
				_data=tmpAttr.data();
			}
			
		}
		
	}
	
	public Attribute subData(Integer key) {
		return _subData.get(key);
	}
	
	public Attribute subData(String attributeName) {
		JenkinsHash hasher=new JenkinsHash();
		int key=(int) hasher.hash(attributeName.getBytes());
		return _subData.get(key);
	}
	
	public String data() {
		return _data;
	}
	
}
