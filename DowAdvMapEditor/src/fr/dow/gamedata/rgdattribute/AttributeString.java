package fr.dow.gamedata.rgdattribute;

import fr.dow.gamedata.RelicChunkDATA;

public class AttributeString implements Attribute {
	
	private String _data;
	
	public static final int STANDARD=0;
	public static final int DOUBLE=1;
	
	public AttributeString(RelicChunkDATA chunk,int pos,int readMode) {
		
		int step=1;
		switch (readMode) {
			case STANDARD : step=1; break; 
			case DOUBLE : step=2; break;
			default : throw new RuntimeException("Read mode is unkonwn");
		}
		
		int pos1=pos;
		byte[] chunkdata=chunk.dataArray();
		
		while (chunkdata[pos1]!=0) pos1+=step;
		
		byte[] subArray={};
		
		switch (readMode) {
			case STANDARD : 
				subArray=new byte[pos1-pos]; 
				System.arraycopy(chunkdata, pos, subArray, 0, pos1-pos);
				break; 
			case DOUBLE : 
				subArray=new byte[(pos1-pos)/2]; 
				for (int i=pos;i<pos1;i+=2) subArray[(i-pos)/2]=chunkdata[i];
				break;
			default : throw new RuntimeException("Read mode is unkonwn");
		}
		
		_data=new String(subArray);
		
	}
	
	public String data() {
		return _data;
	}
	
}
