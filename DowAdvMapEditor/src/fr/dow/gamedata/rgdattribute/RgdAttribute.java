package fr.dow.gamedata.rgdattribute;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFile;

public class RgdAttribute {

/*
Header block
	DWORD checksum : checksum CRC 32 starting from end of block
 	DWORD size : data size starting from end of block (size of top level Data block)

Data block Header
	DWORD SubDataNbr : nbr of sub datas
	repeat SubDataNbr X 
		DWORD Code : data code, see dictionnary to know what it is
		DWORD DataType : 
			0x00-> float => DWORD 
			0x02-> binary (coded on a single byte) => BYTE 
			0x03-> string (byte=0 for end) => BYTE[] 
			0x04-> string coded on 2 bytes per caracter, 2nd is zero, byte=0 for end => BYTE[]
			0x64->datablock => points to the start of a sub Data block header
		DWORD Offset : position of data starting from header end (i.e. 0 for first data)
	end
	BYTE[] datas, subdatas,...
*/
	public static AttributeTable readRGD(InputStream streamIn) {
		
		//load all chunks
		RelicChunkFile whmChunkFile;
		try {
			whmChunkFile = new RelicChunkFile(streamIn);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Could not read input stream");
		}
	
		//
		// read first chunk
		//
		RelicChunkDATA AEGD=(RelicChunkDATA) whmChunkFile.get(0); // Chunk should be of AEGD type
		
		
		return new AttributeTable(AEGD,8);
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {
		
		System.out.println("Loading file data ");

		FileInputStream attrfile=new FileInputStream("E:/Francois/Dawn of war/Modding/Extraction_W40kData/attrib/ebps/environment/urban/dressing/dressing_oildrum_01.rgd");
		
		AttributeTable test=readRGD(attrfile); 

		AttributeTable entity_blueprint_ext=(AttributeTable) test.subData("entity_blueprint_ext");
		AttributeString attr_animator=(AttributeString) entity_blueprint_ext.subData("animator");
		AttributeFloat attr_scale_x=(AttributeFloat) entity_blueprint_ext.subData("scale_x");
		AttributeFloat attr_scale_y=(AttributeFloat) entity_blueprint_ext.subData("scale_y");
		AttributeFloat attr_scale_z=(AttributeFloat) entity_blueprint_ext.subData("scale_z");
		
		String animator=attr_animator.data();
		float scale_x=attr_scale_x.data();
		float scale_y=attr_scale_y.data();
		float scale_z=attr_scale_z.data();
		
		System.out.println("Loading done ");
		
	}
	
	
	
}
