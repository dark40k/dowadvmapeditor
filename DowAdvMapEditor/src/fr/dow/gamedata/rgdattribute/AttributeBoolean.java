package fr.dow.gamedata.rgdattribute;

import fr.dow.gamedata.RelicChunkDATA;

public class AttributeBoolean implements Attribute {
	
	private Boolean _data;
	
	public AttributeBoolean(RelicChunkDATA chunk,int pos) {
		_data=chunk.dataArray()[pos]!=0;
	}
	
	public Boolean data() {
		return _data;
	}
	
}
