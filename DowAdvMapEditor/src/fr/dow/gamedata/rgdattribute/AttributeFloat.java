package fr.dow.gamedata.rgdattribute;

import fr.dow.gamedata.RelicChunkDATA;

public class AttributeFloat implements Attribute {

	private Float _data;
	
	public AttributeFloat(RelicChunkDATA chunk,int pos) {
		_data=chunk.dataGetFloat(pos);
	}
	
	public Float data() {
		return _data;
	}
	
}
