package fr.dow.gamedata.sgbmap;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.dow.gamedata.RelicChunkDATA;

public class MapData implements Cloneable {

	private int _nbrPlayers;
	private int _mapsize;
	private String _modName;
	
	private String _mapName;
	private String _mapScenario;
	
	private Integer _unknown;
	
	public MapData(RelicChunkDATA WMHD) {
		
		WMHD.dataPosition(0);
		
		_nbrPlayers=WMHD.dataGetInt();
		
		_mapsize=WMHD.dataGetInt();
		
		_modName=WMHD.dataGetString();
		
		_mapName=WMHD.dataGetDblString();
		
		_mapScenario=WMHD.dataGetDblString();
		
		_unknown=WMHD.dataGetInt();
		
	}
	
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	public RelicChunkDATA exportWMHD() {
		
		ByteBuffer WMHDData=ByteBuffer.allocate(24+_modName.length()+_mapName.length()*2+_mapScenario.length()*2).order(ByteOrder.LITTLE_ENDIAN);
		WMHDData.putInt(_nbrPlayers);
		WMHDData.putInt(_mapsize);
		WMHDData.putInt(_modName.length());
		WMHDData.put(_modName.getBytes());
		
		WMHDData.putInt(_mapName.length());
		for (int i=0;i<_mapName.length();i++) {
			WMHDData.putChar(_mapName.charAt(i));
		}
		
		WMHDData.putInt(_mapScenario.length());
		for (int i=0;i<_mapScenario.length();i++) {
			WMHDData.putChar(_mapScenario.charAt(i));
		}
		
		WMHDData.putInt(_unknown);
		
		RelicChunkDATA WMHD=new RelicChunkDATA("WMHD",2001,"");
		WMHD.dataWrapArray(WMHDData.array());
		
		return WMHD;
	}
	
	public int get_nbrPlayers() {
		return _nbrPlayers;
	}

	public int get_mapsize() {
		return _mapsize;
	}

	public String get_modName() {
		return _modName;
	}

	public String get_mapName() {
		return _mapName;
	}

	public String get_mapScenario() {
		return _mapScenario;
	}

	public Integer get_unknown() {
		return _unknown;
	}

	public void resize(int newSize) {
		_mapsize=newSize;
	}
	
}
