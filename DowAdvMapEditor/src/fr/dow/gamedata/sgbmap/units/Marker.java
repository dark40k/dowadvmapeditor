package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Vector;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;

public class Marker implements Cloneable {

	private int _id;
	private String _name;
	private MarkerType _markertype;

	private float _proximity;
	
	//private float _customNumber; // defined in SMAT subchunk
	//private String _customString; // defined in SMAT subchunk
	
	private float _x;
	private float _y;
	private float _z;
	private int _nbrattr;
	
	private Vector<MarkerAttr> attrs;
	
	@SuppressWarnings("unused")
	public Marker(RelicChunkFOLD SMKF){
		
		RelicChunkDATA SMKD=SMKF.subChunkDATA("SMKD");
		
		SMKD.dataPosition(0);
		
		_id=SMKD.dataGetInt();	
		
		_name=SMKD.dataGetString();	

		_markertype=MarkerType.getFromTypeName(SMKD.dataGetString());	
		
		_x=SMKD.dataGetFloat();
		
		_y=SMKD.dataGetFloat();
		
		_z=SMKD.dataGetFloat();
		
		float red=SMKD.dataGetFloat();
		float green=SMKD.dataGetFloat();
		float blue=SMKD.dataGetFloat();
		
		_proximity=SMKD.dataGetFloat();
		
		//number of SMAT subchunks containing custom data for custom marker
		_nbrattr=SMKD.dataGetInt();
		
		attrs=new Vector<MarkerAttr>();
		Vector<RelicChunk> SMATList=SMKF.subChunkList("SMAT");
		for (int i=0; i<SMATList.size();i++) attrs.add(new MarkerAttr((RelicChunkDATA) SMATList.get(i)));
		
	}

	@SuppressWarnings("unchecked") //cloning vectors does not allow type checking
	public Object clone() {
		Marker o = null;
		try {
			o = (Marker) super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o.attrs=(Vector<MarkerAttr>)o.attrs.clone();
		for (int i=0;i<o.attrs.size();i++) o.attrs.set(i, (MarkerAttr)o.attrs.get(i).clone());

		return o;
	}
	
	public RelicChunkFOLD exportSMKF() {
		
		ByteBuffer SMKDData=ByteBuffer.allocate(44+_name.length()+_markertype.toString().length()).order(ByteOrder.LITTLE_ENDIAN);
		SMKDData.putInt(_id);
		SMKDData.putInt(_name.length());
		SMKDData.put(_name.getBytes());
		SMKDData.putInt(_markertype.toString().length());
		SMKDData.put(_markertype.toString().getBytes());
		SMKDData.putFloat(_x);
		SMKDData.putFloat(_y);
		SMKDData.putFloat(_z);
		SMKDData.putFloat(_markertype.getRed());
		SMKDData.putFloat(_markertype.getGreen());
		SMKDData.putFloat(_markertype.getBlue());
		SMKDData.putFloat(_proximity);
		SMKDData.putInt(_nbrattr);
		
		RelicChunkDATA SMKD=new RelicChunkDATA("SMKD",1,"");
		SMKD.dataWrapArray(SMKDData.array());

		RelicChunkFOLD SMKF=new RelicChunkFOLD("SMKF",1,"");
		SMKF.get_subChunks().add(SMKD);
		
		for (int i=0; i<attrs.size();i++) SMKF.get_subChunks().add(attrs.get(i).exportSMAT());
		
		return SMKF;
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int id) {
		_id=id;
	}
	
	public String get_name() {
		return _name;
	}
	
	public void set_name(String name) {
		_name=name;
	}

	public MarkerType get_markertype() {
		return _markertype;
	}

	public void set_type(MarkerType markerType) {
		if (_markertype.hasCustomNumber() && !markerType.hasCustomNumber()) attrs.remove(MarkerType.CUSTOMNUMBER);
		if (_markertype.hasCustomString() && !markerType.hasCustomString()) attrs.remove(MarkerType.CUSTOMSTRING);
		
		_markertype=markerType;

		if (_markertype.hasCustomNumber() && get_attr(MarkerType.CUSTOMNUMBER)==null) attrs.add(MarkerAttr.newVal(MarkerType.CUSTOMNUMBER,0.0f));
		if (_markertype.hasCustomString() && get_attr(MarkerType.CUSTOMSTRING)==null) attrs.add(MarkerAttr.newString(MarkerType.CUSTOMSTRING,""));

	}

	public float get_proximity() {
		return _proximity;
	}

	public float get_x() {
		return _x;
	}

	public float get_y() {
		return _y;
	}

	public float get_z() {
		return _z;
	}

	public int get_nbrcustomdata() {
		return _nbrattr;
	}

	public void move(float moveX, float moveZ) {
		_x+=moveX;
		_z+=moveZ;
		
	}

	public void rotateClockwise() {
		float tmp=_x;
		_x=_z;
		_z=-tmp;
	}

	public void set_position(Float x, Float y, Float z) {
		_x=x;
		_y=y;
		_z=z;
	}

	public void set_proximity(Float proximity) {
		_proximity=proximity;
	}
	
	private MarkerAttr get_attr(String name) {
		for (int i=0; i<attrs.size();i++) 
			if (name.equals(attrs.get(i).propName)) 
				return attrs.get(i);
		return null;
	}

	public Float get_customNumber() {
		if (!_markertype.hasCustomNumber()) return null;
		return get_attr(MarkerType.CUSTOMNUMBER).propVal;
	}

	public String get_customString() {
		if (!_markertype.hasCustomString()) return null;
		return get_attr(MarkerType.CUSTOMSTRING).propString;
	}
	
	public void set_customNumber(Float customNumber) {
		get_attr(MarkerType.CUSTOMNUMBER).propVal=customNumber;
	}

	public void set_customString( String customString) {
		get_attr(MarkerType.CUSTOMSTRING).propString=customString;
	}

	
}
