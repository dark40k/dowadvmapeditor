package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;

public class Entity implements Cloneable {

	private int blueprintIndex;
	private int id;
	private PlayerID playerID;

	private float x, y, z; // position
	private double sX = 1, sY = 1, sZ = 1; // scaling parameters
	private double headingRad, pitchRad, rollRad; // rotation parameters
	
	private float[] transf;

	private boolean meAdjustableHeightStatus=true; // true when adjustable height is activated
	
	private final static byte[] EBEDDATA = { 0, 0, 0, 0 };
	private final static byte[] SEBDDATA = { 0 };

	public Entity(int id, int blueprintIndex, PlayerID playerID) {
		this.id = id;
		this.blueprintIndex = blueprintIndex;
		this.playerID = playerID;
		updateTransf();
	}

	public Entity(RelicChunkFOLD ENTY) {

		RelicChunkDATA ENTI = ENTY.subChunkDATA("ENTI");
		// Chunk chunkEBED=chunkENTY.find_firstsubChunk("EBED");
		// Chunk chunkSEBD=chunkENTY.find_firstsubChunk("SEBD");

		ENTI.dataPosition(0);

		id = ENTI.dataGetInt();
		blueprintIndex = ENTI.dataGetInt();
		playerID=PlayerID.World;
		
		float m00 = ENTI.dataGetFloat();
		float m10 = ENTI.dataGetFloat();
		float m20 = ENTI.dataGetFloat();

		float m01 = ENTI.dataGetFloat();
		float m11 = ENTI.dataGetFloat();
		float m21 = ENTI.dataGetFloat();

		float m02 = ENTI.dataGetFloat();
		float m12 = ENTI.dataGetFloat();
		float m22 = ENTI.dataGetFloat();

		sX = Math.sqrt(m00 * m00 + m10 * m10 + m20 * m20);
		m00 /= sX;
		m10 /= sX;
		m20 /= sX;
		sY = Math.sqrt(m01 * m01 + m11 * m11 + m21 * m21);
		m01 /= sY;
		m11 /= sY;
		m21 /= sY;
		sZ = Math.sqrt(m02 * m02 + m12 * m12 + m22 * m22);
		m02 /= sZ;
		m12 /= sZ;
		m22 /= sZ;

		// source =
		// http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToEuler/index.htm
		// Assuming the angles are in radians.
		if (m11 > 0.998) { // singularity at north pole
			headingRad = Math.atan2(-m20, m00);
			pitchRad = 0;
			rollRad = 0;
		} else if (m11 < -0.998) { // singularity at south pole
			headingRad = Math.atan2(-m20, m00);
			pitchRad = 0;
			rollRad = 0;
		} else {
			headingRad = Math.atan2(-m01, m21);
			rollRad = Math.atan2(-m10, m12);
			pitchRad = Math.acos(m11);
		}

		x = ENTI.dataGetFloat();
		y = ENTI.dataGetFloat();
		z = ENTI.dataGetFloat();

		updateTransf();
		
	}

	public void updateTransf() {

		double ch = Math.cos(headingRad);
		double sh = Math.sin(headingRad);
		double ca = Math.cos(pitchRad);
		double sa = Math.sin(pitchRad);
		double cr = Math.cos(rollRad);
		double sr = Math.sin(rollRad);

		// ch*cr-sh*ca*sr -sh*sa ch*sr+sh*ca*cr
		// -sa*sr ca sa*cr
		// -sh*cr-ch*ca*sr -ch*sa -sh*sr+ch*ca*cr

		// source :
		// http://www.euclideanspace.com/maths/geometry/rotations/euler/index.htm
		float[] newTransf = { (float) (sX * (ch * cr - sh * ca * sr)), (float) (sX * (-sa * sr)),
				(float) (-sX * (-sh * cr - ch * ca * sr)), 0f, (float) (sY * (-sh * sa)), (float) (sY * (ca)),
				(float) (-sY * (-ch * sa)), 0f, (float) (sZ * (ch * sr + sh * ca * cr)), (float) (sZ * (sa * cr)),
				(float) (-sZ * (-sh * sr + ch * ca * cr)), 0f, x, y, -z, 1f };

		transf=newTransf;
	}

	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		}
		catch (CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}

	public RelicChunkFOLD exportENTY() {

		ByteBuffer ENTIData = ByteBuffer.allocate(56).order(ByteOrder.LITTLE_ENDIAN);
		ENTIData.putInt(id);
		ENTIData.putInt(blueprintIndex);

		float[] transf = getTransf();
		ENTIData.putFloat(transf[0]);
		ENTIData.putFloat(transf[1]);
		ENTIData.putFloat(-transf[2]);
		ENTIData.putFloat(transf[4]);
		ENTIData.putFloat(transf[5]);
		ENTIData.putFloat(-transf[6]);
		ENTIData.putFloat(transf[8]);
		ENTIData.putFloat(transf[9]);
		ENTIData.putFloat(-transf[10]);
		ENTIData.putFloat(transf[12]);
		ENTIData.putFloat(transf[13]);
		ENTIData.putFloat(-transf[14]);

		RelicChunkDATA ENTI = new RelicChunkDATA("ENTI", 2001, "");
		ENTI.dataWrapArray(ENTIData.array());

		RelicChunkDATA EBED = new RelicChunkDATA("EBED", 2000, "");
		EBED.dataWrapArray(EBEDDATA);

		RelicChunkDATA SEBD = new RelicChunkDATA("SEBD", 2000, "");
		SEBD.dataWrapArray(SEBDDATA);

		RelicChunkFOLD ENTY = new RelicChunkFOLD("ENTY", 2000, "");
		ENTY.get_subChunks().add(ENTI);
		ENTY.get_subChunks().add(EBED);
		ENTY.get_subChunks().add(SEBD);

		return ENTY;
	}

	public void move(float moveX, float moveZ) {
		x += moveX;
		z += moveZ;
	}

	public void rotateClockwise() {
		headingRad += Math.PI / 2;
		if (headingRad > Math.PI) headingRad -= Math.PI * 2;
	}

	//
	//
	// Getters
	//
	//

	public int getBlueprintIndex() { return blueprintIndex; }

	public int getId() { return id; }

	public PlayerID getPlayerID() { return playerID; }

	public float getX() { return x; }

	public float getY() { return y; }

	public float getZ() { return z; }

	public double getSX() { return sX; }

	public double getSY() { return sY; }

	public double getSZ() { return sZ; }

	public double getHeadingRad() { return headingRad; }

	public double getPitchRad() { return pitchRad; }

	public double getRollRad() { return rollRad; }
	
	public float[] getTransf() { return transf; }

	public boolean getMeAdjustableHeightStatus() { return meAdjustableHeightStatus; }
	
	//
	//
	// Setters
	//
	//

	public void setBlueprintIndex(int blueprintIndex) {
		this.blueprintIndex = blueprintIndex;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPlayerID(PlayerID playerID) {
		this.playerID = playerID;
	}

	public void setPosition(float x,float y,float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		updateTransf();
	}

	public void setScale(double sx, double sy, double sz) {
		sX = sx;
		sY = sy;
		sZ = sz;
		updateTransf();
	}

	public void setAnglesRad(double headingRad, double pitchRad, double rollRad) {
		this.headingRad = headingRad;
		this.pitchRad = pitchRad;
		this.rollRad = rollRad;
		updateTransf();
	}

	public void setMeAdjustableHeightStatus(boolean newValue) { 
		meAdjustableHeightStatus = newValue; 
	}
	
}
