package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Vector;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.mapeditor.tools.CompareGroupNames;

public class Sgroups  extends LinkedHashMap<Integer,Sgroup> {
	
	public Sgroups(RelicChunkFOLD SGMP) {
		
		super();
		
		// Iterates over SSGP subchunks 
		Iterator<RelicChunk> it=SGMP.subChunkList("SSGP").iterator();
		
		while (it.hasNext()) {
			Sgroup newSgroup = new Sgroup((RelicChunkFOLD) it.next());
			put(newSgroup.get_id(),newSgroup);
		}		
		
	}

	public RelicChunkFOLD exportSGMG() {
		RelicChunkDATA SMSZ=new RelicChunkDATA("SMSZ",1,"mapsize"+RelicChunkFOLD.zeroChar);
		ByteBuffer SMSZData=ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
		SMSZData.putInt(size());
		SMSZ.dataWrapArray(SMSZData.array());
		
		RelicChunkFOLD SGMP=new RelicChunkFOLD("SGMP",1,"Squad GroupMap"+RelicChunkFOLD.zeroChar);
		SGMP.get_subChunks().add(SMSZ);
		for (Sgroup sgroup : values()) SGMP.get_subChunks().add(sgroup.exportSSGP());
		
		RelicChunkFOLD SGMG=new RelicChunkFOLD("SGMG",1,"Squad Group Manager"+RelicChunkFOLD.zeroChar);
		SGMG.get_subChunks().add(SGMP);
		
		return SGMG;
	}
	
	public Object clone() {
		Sgroups o=(Sgroups)super.clone();
		for (Integer key : keySet()) o.put(key, (Sgroup)o.get(key).clone() );
		return o;
	}

	public void renumberSortedByName() {

		HashMap<String,Sgroup> nameTable=new HashMap<String,Sgroup>();
		
		for (Sgroup sgroup : this.values()) nameTable.put(sgroup.get_name(),sgroup);
		
		Vector<String> names=new Vector<String>(nameTable.keySet());
		Collections.sort(names);
		
		this.clear();
		
		int index=1;
		for (String name : names) {
			Sgroup sgroup=nameTable.get(name);
			sgroup.set_id(index++);
			this.put(sgroup.get_id(), sgroup);
		}
		
	}

	public void move(HashSet<Integer> movePack, int startID) {
		
		HashMap<String,Sgroup> nameTable=new HashMap<String,Sgroup>();
		
		for (Integer  sgroupID : movePack) {
			Sgroup sgroup=this.get(sgroupID);
			nameTable.put(sgroup.get_name(),sgroup);
			this.remove(sgroupID);
		}
		
		Vector<String> names=new Vector<String>(nameTable.keySet());
		Collections.sort(names,new CompareGroupNames());
		
		for (int i=0;i<names.size();i++) {
			int newSgroupID=startID+i;
			push(newSgroupID);
			Sgroup sgroup=nameTable.get(names.get(i));
			sgroup.set_id(newSgroupID);
			this.put(newSgroupID, sgroup);
		}
		
	}
	
	public void push(int sgroupID) {
		if (this.containsKey(sgroupID)) {
			push(sgroupID+1);
			Sgroup sgroup=this.get(sgroupID);
			this.remove(sgroupID);
			sgroup.set_id(sgroupID+1);
			this.put(sgroupID+1, sgroup);
		}
	}

	public Sgroup newSgroup(String name) {
		int sgroupID=0;
		for (int tmpID : this.keySet()) if (tmpID>sgroupID) sgroupID=tmpID;
		sgroupID++;
		
		Sgroup sgroup=new Sgroup(sgroupID,name);
		this.put(sgroupID,sgroup);
		return sgroup;
	}

	public Sgroup getByName(String groupName) {
		for (Sgroup sgroup : this.values()) if (sgroup.get_name().equals(groupName)) return sgroup;
		return null;
	}

}
