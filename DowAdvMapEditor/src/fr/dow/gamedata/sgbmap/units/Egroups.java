package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Vector;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.mapeditor.tools.CompareGroupNames;

public class Egroups extends LinkedHashMap<Integer,Egroup> {

	public Egroups(RelicChunkFOLD EGMP) {
		
		super();
		
		// Iterates over ESGP subchunks 
		Iterator<RelicChunk> it=EGMP.subChunkList("ESGP").iterator();
		
		while (it.hasNext()) {
			Egroup newEgroup = new Egroup((RelicChunkFOLD) it.next());
			put(newEgroup.get_id(),newEgroup);
		}		
		
	}

	public RelicChunkFOLD exportGMGR() {
		
		RelicChunkDATA EMSZ=new RelicChunkDATA("EMSZ",1,"mapsize"+RelicChunkFOLD.zeroChar);
		ByteBuffer EMSZData=ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
		EMSZData.putInt(size());
		EMSZ.dataWrapArray(EMSZData.array());
		
		RelicChunkFOLD EGMP=new RelicChunkFOLD("EGMP",1,"Entity GroupMap"+RelicChunkFOLD.zeroChar);
		EGMP.get_subChunks().add(EMSZ);
		
		for (Egroup egroup : values()) EGMP.get_subChunks().add(egroup.exportESGP());
		
		RelicChunkFOLD GMGR=new RelicChunkFOLD("GMGR",1,"Group Manager"+RelicChunkFOLD.zeroChar);
		GMGR.get_subChunks().add(EGMP);
		
		return GMGR;
		
	}
	
	public Object clone() {
		Egroups o=(Egroups)super.clone();
		for (Integer key : keySet()) o.put(key, (Egroup)o.get(key).clone() );
		return o;
	}

	public void renumberSortedByName() {
		
		HashMap<String,Egroup> nameTable=new HashMap<String,Egroup>();
		
		for (Egroup  egroup : this.values()) nameTable.put(egroup.get_name(),egroup);
		
		Vector<String> names=new Vector<String>(nameTable.keySet());
		Collections.sort(names,new CompareGroupNames());
		
		this.clear();
		
		int index=1;
		for (String name : names) {
			Egroup egroup=nameTable.get(name);
			egroup.set_id(index++);
			this.put(egroup.get_id(), egroup);
		}
		
	}

	public void move(HashSet<Integer> tableSelEgroupIDs, int startID) {
		
		HashMap<String,Egroup> nameTable=new HashMap<String,Egroup>();
		
		for (Integer  egroupID : tableSelEgroupIDs) {
			Egroup egroup=this.get(egroupID);
			nameTable.put(egroup.get_name(),egroup);
			this.remove(egroupID);
		}
		
		Vector<String> names=new Vector<String>(nameTable.keySet());
		Collections.sort(names,new CompareGroupNames());
		
		for (int i=0;i<names.size();i++) {
			int newEgroupID=startID+i;
			push(newEgroupID);
			Egroup egroup=nameTable.get(names.get(i));
			egroup.set_id(newEgroupID);
			this.put(newEgroupID, egroup);
		}
		
	}
	
	public void push(int egroupID) {
		if (this.containsKey(egroupID)) {
			push(egroupID+1);
			Egroup egroup=this.get(egroupID);
			this.remove(egroupID);
			egroup.set_id(egroupID+1);
			this.put(egroupID+1, egroup);
		}
	}

	public Egroup newEgroup(String name) {
		int sgroupID=0;
		for (int tmpID : this.keySet()) if (tmpID>sgroupID) sgroupID=tmpID;
		sgroupID++;
		
		Egroup egroup=new Egroup(sgroupID,name);
		this.put(sgroupID,egroup);
		return egroup;
	}

	public Egroup getByName(String groupName) {
		for (Egroup egroup : this.values()) if (egroup.get_name().equals(groupName)) return egroup;
		return null;
	}
	
}
