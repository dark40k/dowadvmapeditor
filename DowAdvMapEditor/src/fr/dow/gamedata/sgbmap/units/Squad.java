package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedHashSet;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;

public class Squad implements Cloneable {

	public int id;
	public int blueprintIndex;
	public PlayerID playerID;
	
	public LinkedHashSet<Integer> entitiesID;
	
	public Squad(RelicChunkFOLD chunkSQDD){
		
		RelicChunkDATA chunkSQDI=chunkSQDD.subChunkDATA("SQDI");
		RelicChunkDATA chunkSQDE=chunkSQDD.subChunkDATA("SQDE");
		
		id=chunkSQDI.dataGetInt(0);	
		blueprintIndex=chunkSQDI.dataGetInt(4);	
		
		entitiesID=new LinkedHashSet<Integer>();
		
		chunkSQDE.dataPosition(0);
		
		int squadSize=chunkSQDE.dataGetInt();
		for(int i = 0; i < squadSize; i++) 
			entitiesID.add(chunkSQDE.dataGetInt());

	}
	
	public Squad(int id, int blueprintIndex, PlayerID playerID) {
		this.id=id;
		this.blueprintIndex=blueprintIndex;
		this.playerID=playerID;
		entitiesID=new LinkedHashSet<Integer>();
	}
	
	public Integer[] getEntitiesID() {
		return entitiesID.toArray(new Integer[0]);
	}
	
	
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	public RelicChunkFOLD exportSQDD() {
		
		ByteBuffer SQDIData=ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
		SQDIData.putInt(id);
		SQDIData.putInt(blueprintIndex);
		
		RelicChunkDATA SQDI=new RelicChunkDATA("SQDI",2000,"");
		SQDI.dataWrapArray(SQDIData.array());
		
		ByteBuffer SQDEData=ByteBuffer.allocate(4+4*entitiesID.size()).order(ByteOrder.LITTLE_ENDIAN);
		SQDEData.putInt(entitiesID.size());
		for (Integer entityID : entitiesID) SQDEData.putInt(entityID);
		
		RelicChunkDATA SQDE=new RelicChunkDATA("SQDE",2000,"");
		SQDE.dataWrapArray(SQDEData.array());
		
		RelicChunkFOLD SQDD=new RelicChunkFOLD("SQDD",2000,"");
		SQDD.get_subChunks().add(SQDI);
		SQDD.get_subChunks().add(SQDE);
		
		return SQDD;
	}

}
