package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Vector;

import fr.dow.gamedata.RelicChunkDATA;

public class EntityBlueprints extends Vector<String> {

	public EntityBlueprints(RelicChunkDATA EBPT) {
		
		// reset reader
		EBPT.dataPosition(0);
		
		// read number of blueprints to load
		int nbp=EBPT.dataGetInt();
		
		// load data
		for (int i=0;i<nbp;i++) this.add(EBPT.dataGetString());
		
	}

	public RelicChunkDATA exportEBPT() {
		
		int size=4;
		for (String blueprint : this) size+=4+blueprint.length();
		
		ByteBuffer EBPTData=ByteBuffer.allocate(size).order(ByteOrder.LITTLE_ENDIAN);
		EBPTData.putInt(this.size());
		for (String blueprint : this) {
			EBPTData.putInt(blueprint.length());
			EBPTData.put(blueprint.getBytes());
		}
		
		RelicChunkDATA EBPT=new RelicChunkDATA("EBPT",2000,"");
		EBPT.dataWrapArray(EBPTData.array());
		
		return EBPT;
	}

	public Integer addNewBlueprint(String blueprint) {
		int i=this.indexOf(blueprint);
		if (i>=0) return i;
		this.add(blueprint);
		return this.size()-1;
	}
	
}
