package fr.dow.gamedata.sgbmap.units;

import java.util.HashMap;
import java.util.LinkedHashMap;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.mapeditor.CopyData;

public class Squads implements Cloneable {

	public LinkedHashMap<Integer,Squad> squadList;
	
	public LinkedHashMap<Integer,Integer> revSquadList;
	
	public Squads(RelicChunkFOLD SQDL){
		
		// reset storage
		squadList=new LinkedHashMap<Integer,Squad>();
		revSquadList=new LinkedHashMap<Integer,Integer>();
		
		// Iterates over subchunks 
		for (RelicChunk subChunk : SQDL.get_subChunks()) add(new Squad((RelicChunkFOLD)subChunk));
		
	}

	public void add(Squad squad) {
		squadList.put(squad.id,squad);
		for (Integer entityID : squad.entitiesID) {
			revSquadList.put(entityID,squad.id);
		}
	}
	
	public void addEntityToSquad(int squadID, int entityID) {
		squadList.get(squadID).entitiesID.add(entityID);
		revSquadList.put(entityID, squadID);
	}
	
	public Integer getSquadID(int entityID) {
		return revSquadList.get(entityID);
	}
	
	public Squad getSquad(int squadID) {
		return squadList.get(squadID);
	}
	
	
	public RelicChunkFOLD exportSQDL() {
		
		RelicChunkFOLD SQDL=new RelicChunkFOLD("SQDL",2000,"");
		for (Squad squad : squadList.values()) SQDL.get_subChunks().add(squad.exportSQDD());
		
		return SQDL;	
	}
	
	@SuppressWarnings("unchecked") //cloning of LinkedHashMap cannot be checked
	public Object clone() {
		
		Squads newSquads = null;
		try {
			newSquads = (Squads) super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		newSquads.squadList=(LinkedHashMap<Integer,Squad>)squadList.clone();
		for (Integer key : newSquads.squadList.keySet()) newSquads.squadList.put(key, (Squad)newSquads.squadList.get(key).clone() );
		
		return newSquads;
	}

	public void pasteSubMap(CopyData pasteData, SquadBlueprints squadBlueprints, HashMap<Integer, Integer> pasteEntityIDLinkTable) {
		
		//store blueprint index
		HashMap<Integer,Integer> newBlueprintIndexLink=new HashMap<Integer,Integer>();
		
		// store new squadID for old squadID
		HashMap<Integer, Integer> pasteSquadIDLinkTable=new HashMap<Integer, Integer>();
		
		// find newSquadID reference
		int newSquadIDCounter=50000;
		for (Integer squadID : squadList.keySet()) if (newSquadIDCounter<=squadID) newSquadIDCounter=squadID+1;
		
		// scan all pasted entities
		for (Integer pasteEntityID : pasteData.selection.entities ) {
			
			//get squad reference
			Integer pasteSquadID=pasteData.sgbMap.squads.getSquadID(pasteEntityID);
			
			//skip if squad reference is null : entity is not associated to squad
			if (pasteSquadID==null) continue;
			
			//get new squad reference if it already exist
			Integer newSquadID=pasteSquadIDLinkTable.get(pasteSquadID);
			
			//if squad reference does not exist then create new squad and add squad 
			if (newSquadID==null) {
				newSquadID=newSquadIDCounter++;
				Squad pasteSquad=pasteData.sgbMap.squads.squadList.get(pasteSquadID);
				
				int pasteBlueprintIndex=pasteSquad.blueprintIndex;
				
				Integer newBlueprintIndex=newBlueprintIndexLink.get(pasteBlueprintIndex);
				
				//if blueprint is unknown then check if it can be found in existing table
				if (newBlueprintIndex==null) {
					
					// extract blueprint name
					String pasteBlueprintName=pasteData.sgbMap.squadBlueprints.get(pasteBlueprintIndex);
					
					// scan new table to check if it can be found
					for (int blueprintIndex=0;blueprintIndex<squadBlueprints.size();blueprintIndex++) 
						if (pasteBlueprintName.equals(squadBlueprints.get(blueprintIndex))) {
							newBlueprintIndex=blueprintIndex;
							
							break;
						}
					
					// scan failed => need to create new blueprint
					if (newBlueprintIndex==null) {
						newBlueprintIndex=squadBlueprints.size();
						squadBlueprints.add(newBlueprintIndex,pasteBlueprintName);
					}
					
					// store associated blueprint index for future use
					newBlueprintIndexLink.put(pasteBlueprintIndex,newBlueprintIndex);
				}
				
				// add new squad to squadlist
				add(new Squad(newSquadID,newBlueprintIndex,pasteSquad.playerID));
				
				// store squadID reference for future use
				pasteSquadIDLinkTable.put(pasteSquadID, newSquadID);

			}
			
			//add entity to squad
			addEntityToSquad(newSquadID, pasteEntityIDLinkTable.get(pasteEntityID));
			
			
		}
		
	}

}
