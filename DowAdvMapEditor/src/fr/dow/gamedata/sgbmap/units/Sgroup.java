package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashSet;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;

public class Sgroup implements Cloneable {

	private int _id;
	private String _name;
	
	private HashSet<Integer> _squadsID;
	
	public Sgroup(int id, String name) {
		this._id=id;
		this._name=name;
		this._squadsID=new HashSet<Integer>();
	}
	
	public Sgroup(RelicChunkFOLD SSGP){
		
		RelicChunkDATA SGPI=SSGP.subChunkDATA("SGPI");
		RelicChunkDATA SGRP=SSGP.subChunkDATA("SGRP");
		
		SGPI.dataPosition(0);
		_id=SGPI.dataGetInt();	
		_name=SGPI.dataGetString();

		SGRP.dataPosition(0);
		int nSquads=SGRP.dataGetInt();
		_squadsID=new HashSet<Integer>(nSquads);
		for (int i = 0; i < nSquads; i++) _squadsID.add(SGRP.dataGetInt());

	}
	
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	public Integer get_id() {
		return _id;
	}
	
	public void set_id(int newID) {
		_id=newID;
	}

	public String get_name() {
		return _name;
	}
	
	public void set_name(String newName) {
		_name=newName;
	}

	public void clear() {
		_squadsID.clear();
	}
	
	public void add(int squadID) {
		_squadsID.add(squadID);
		
	}
		
	public void add(HashSet<Integer> squadIDs) {
		_squadsID.addAll(squadIDs);
	}
	

	public void remove(HashSet<Integer> squadIDs) {
		for (int squadID : squadIDs) 
				_squadsID.remove(squadID);
		
	}

	@SuppressWarnings("unchecked")
	public void intersect(HashSet<Integer> squadIDs) {
		for (int squadID : (HashSet<Integer>) _squadsID.clone()) 
			if (!squadIDs.contains(squadID))
				_squadsID.remove(squadID);
		
	}

	
	public boolean contains(Integer squadID) {
		for (int id : _squadsID) if (id==squadID) return true;
		return false;
	}
	
	public boolean contains(HashSet<Integer> squadIDs) {
		for (int squadID : squadIDs) 
			if (contains(squadID))
				return true;
		return false;
	}
	
	public boolean containsAll(HashSet<Integer> squadIDs) {
		for (int squadID : squadIDs) 
			if (!contains(squadID)) 
				return false;
		return true;
	}
	
	public Integer get_numberofsquads(){
		return _squadsID.size();
	}

	@SuppressWarnings("unchecked")
	public HashSet<Integer> get_squadIDs() {
		return (HashSet<Integer>) _squadsID.clone();
	}
	
	public RelicChunkFOLD exportSSGP() {
		
		ByteBuffer SGPIData=ByteBuffer.allocate(8+_name.length()).order(ByteOrder.LITTLE_ENDIAN);
		SGPIData.putInt(_id);
		SGPIData.putInt(_name.length());
		SGPIData.put(_name.getBytes());
		
		RelicChunkDATA SGPI=new RelicChunkDATA("SGPI",1,"sgroupinfo"+RelicChunkFOLD.zeroChar);
		SGPI.dataWrapArray(SGPIData.array());
		
		ByteBuffer SGRPData=ByteBuffer.allocate(4+_squadsID.size()*4).order(ByteOrder.LITTLE_ENDIAN);
		SGRPData.putInt(_squadsID.size());
		for (Integer squadID : _squadsID) SGRPData.putInt(squadID);
		
		RelicChunkDATA SGRP=new RelicChunkDATA("SGRP",1,"Squad Group"+RelicChunkFOLD.zeroChar);
		SGRP.dataWrapArray(SGRPData.array());
		
		RelicChunkDATA ROSC=new RelicChunkDATA("ROSC",1,"");
		ROSC.dataWrapArray(new byte[4]);
		
		RelicChunkFOLD SSGP=new RelicChunkFOLD("SSGP",3,"Squad Group"+RelicChunkFOLD.zeroChar);
		SSGP.get_subChunks().add(SGPI);
		SSGP.get_subChunks().add(SGRP);
		SSGP.get_subChunks().add(ROSC);
		
		return SSGP;
		
	}

	
}
