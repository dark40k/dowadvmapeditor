package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.dow.gamedata.RelicChunkDATA;

public class MarkerAttr implements Cloneable {

	public String propName;
	public boolean propType;
	public float propVal;
	public String propString;
	
	public MarkerAttr(RelicChunkDATA SMAT){
		
		propName=SMAT.dataGetString();
	
		propType=( SMAT.dataGetInt()!=0 );
		propVal=SMAT.dataGetFloat();

		propString=SMAT.dataGetString();
		
	}
	
	private MarkerAttr(String propName, boolean propType, float propVal, String propString ){
		this.propName=propName;
		this.propType=false;
		this.propVal=propVal;
		this.propString=propString;
	}
	
	public static MarkerAttr newVal(String propName, float propVal ) {
		return new MarkerAttr(propName,  false,  propVal, null);
	}
	
	public static MarkerAttr newString(String propName, String propString ) {
		return new MarkerAttr(propName,  true,  0.0f, propString);
	}
	
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	public RelicChunkDATA exportSMAT() {
		ByteBuffer SMATData=ByteBuffer.allocate(16+propName.length()+propString.length()).order(ByteOrder.LITTLE_ENDIAN);
		SMATData.putInt(propName.length());
		SMATData.put(propName.getBytes());
		SMATData.putInt(propType ? 1 : 0);
		SMATData.putFloat(propVal);
		SMATData.putInt(propString.length());
		SMATData.put(propString.getBytes());
		
		RelicChunkDATA SMAT=new RelicChunkDATA("SMAT",1,"attr");
		SMAT.dataWrapArray(SMATData.array());

		return SMAT;
	}
	
}
