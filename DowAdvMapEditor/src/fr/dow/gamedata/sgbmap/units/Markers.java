package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;
import java.util.LinkedHashMap;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.mapeditor.CopyData;

public class Markers extends LinkedHashMap<Integer,Marker> {

	public Markers(RelicChunkFOLD SMKR) {
		
		super();
		
		// Iterates over SSGP subchunks 
		Iterator<RelicChunk> it=SMKR.subChunkList("SMKF").iterator();
		
		while (it.hasNext()) {
			Marker newMarker = new Marker((RelicChunkFOLD) it.next());
			put(newMarker.get_id(),newMarker);
		}		
		
	}

	public RelicChunkFOLD exportSMKR() {
		RelicChunkDATA SMKI=new RelicChunkDATA("SMKI",1,"Scar markers info"+RelicChunkFOLD.zeroChar);
		ByteBuffer SMKIData=ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
		SMKIData.putInt(size());
		SMKI.dataWrapArray(SMKIData.array());
		
		RelicChunkFOLD SMKR=new RelicChunkFOLD("SMKR",1,"Scar markers"+RelicChunkFOLD.zeroChar);
		SMKR.get_subChunks().add(SMKI);
		for (Marker marker : values()) SMKR.get_subChunks().add(marker.exportSMKF());
		
		return SMKR;
	}

	public void markerDelete(int markerID) {
		remove(markerID);
	}
	
	public void move(float moveX, float moveZ) {
		for (Marker marker : values()) marker.move(moveX,moveZ);
	}
	
	public void rotateClockwise() {
		for (Marker marker : values()) marker.rotateClockwise();
		
	}
	public Object clone() {
		Markers o=(Markers)super.clone();
		for (Integer key : keySet()) o.put(key, (Marker)o.get(key).clone() );
		return o;
	}

	public void pasteSubMap(CopyData pasteData, float moveX, float moveZ) {

		//extract marker counter
		int newMarkerIDCounter=1;
		for (Integer markerID : this.keySet()) if (newMarkerIDCounter<=markerID) newMarkerIDCounter=markerID+1;
		
		//paste markers
		for (Integer pasteMarkerID : pasteData.selection.markers) {
			
			Marker newMarker=(Marker)pasteData.sgbMap.markers.get(pasteMarkerID).clone();
			newMarker.set_id(newMarkerIDCounter++);
			newMarker.move(moveX, moveZ);
			
			//check that marker name is unique
			while (getMarkerByName(newMarker.get_name())!=null)
				newMarker.set_name(nextName(newMarker.get_name()));
			
			this.put(newMarker.get_id(),newMarker);
			
		}
		
	}
	
	public Marker getMarkerByName(String name) {
		for (Marker marker : this.values()) 
			if (name.equals(marker.get_name())) 
				return marker;
		return null;
	}
	
	private String nextName(String name) {
		try {
			Integer pos=name.lastIndexOf("_");
			return name.substring(0, pos+1)+(Integer.parseInt(name.substring(pos+1))+1);
		}
		catch( Exception e ) {return name+"_1"; } 
	}
	
}
