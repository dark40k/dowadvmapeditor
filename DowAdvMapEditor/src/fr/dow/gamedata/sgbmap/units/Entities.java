package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.mapeditor.CopyData;

public class Entities extends LinkedHashMap<Integer,Entity> {
	
	public Entities(RelicChunkFOLD ENTL) {

		super();
		
		// Iterates over subchunks 
		Iterator<RelicChunk> it=ENTL.get_subChunks().iterator();
		
		while (it.hasNext()) {
			Entity newEntity = new Entity((RelicChunkFOLD) it.next());
			put(newEntity.getId(),newEntity);
		}
		
	}

	public void importEINF(RelicChunkDATA EINF) {
		
		int nEntities = EINF.get_datasize() / 8;
		
		for (int i=0; i < nEntities; i++) {
			
			Integer id=EINF.dataGetInt();
			boolean meAdjustableHeightStatus=(EINF.dataGetInt()!=0);
			
			if (containsKey(id))
				get(id).setMeAdjustableHeightStatus(meAdjustableHeightStatus);
			
		}
		
	}
	
	public RelicChunkFOLD exportENTL() {
		
		RelicChunkFOLD ENTL=new RelicChunkFOLD("ENTL",2000,"");
		for (Entity entity : values()) ENTL.get_subChunks().add(entity.exportENTY());
		
		return ENTL;
	}

	public RelicChunkDATA exportEINF() {
		
		ByteBuffer EINFData = ByteBuffer.allocate(size()*8).order(ByteOrder.LITTLE_ENDIAN);
		
		for (Entity entity : values()) {
			EINFData.putInt(entity.getId());
			EINFData.putInt((entity.getMeAdjustableHeightStatus())?1:0);
		}
		
		RelicChunkDATA EINF = new RelicChunkDATA("EINF",2000,"Entity Information"+RelicChunkFOLD.zeroChar);
		EINF.dataWrapArray(EINFData.array());
		
		return EINF;
	}
	
	public void move(float moveX, float moveZ) {
		for (Entity entity : values()) entity.move(moveX,moveZ);		
	}
	
	public void rotateClockwise() {
		for (Entity entity : values()) entity.rotateClockwise();	
	}
	
	public Object clone() {
		Entities o=(Entities)super.clone();
		for (Integer key : keySet()) o.put(key, (Entity)o.get(key).clone() );
		return o;
	}

	//returns the newID for entities that where pasted based on their old IDs
	public HashMap<Integer, Integer> pasteSubMap(CopyData pasteData, float moveX, float moveZ, EntityBlueprints entityBlueprints) {
		
		HashMap<Integer, Integer> pasteIDLinkTable=new HashMap<Integer, Integer>();
		
		EntityBlueprints pasteEntityBlueprints=pasteData.sgbMap.entityBlueprints;
		
		// calculate next ID
		int newEntityIDCounter=1000;
		for (Integer ID : this.keySet()) if (newEntityIDCounter<=ID) newEntityIDCounter=ID+1;
		
		// stores new BP index based on old BP index
		HashMap<Integer,Integer> newMapBPIDIndex=new HashMap<Integer,Integer>();
		
		for (Integer pasteEntityID : pasteData.selection.entities) {
			
			// copy paste entity
			Entity newEntity=(Entity)pasteData.sgbMap.entities.get(pasteEntityID).clone();
			
			// assign new ID to entity and store it
			pasteIDLinkTable.put(newEntity.getId(), newEntityIDCounter);
			newEntity.setId(newEntityIDCounter++);
			this.put(newEntity.getId(),newEntity);
			
			//move new entity
			newEntity.move(moveX, moveZ);
			
			//calculate new EntityBlueprint ref
			int newEntityBlueprintIndex=-1;
			int pasteBlueprintIndex=newEntity.getBlueprintIndex();
			
			if (newMapBPIDIndex.containsKey(pasteBlueprintIndex)) {
				newEntityBlueprintIndex=newMapBPIDIndex.get(pasteBlueprintIndex);
			} else {
				
				// find destination paste pointer with same blueprint or create a new one
				String blueprintName=pasteEntityBlueprints.get(pasteBlueprintIndex);
				
				// search existing links
				for (int blueprintIndex=0;blueprintIndex<entityBlueprints.size();blueprintIndex++) 
					if (blueprintName.equals(entityBlueprints.get(blueprintIndex))) {
						newEntityBlueprintIndex=blueprintIndex;
						break;
					}
				
				// if no link found then create a new link
				if (newEntityBlueprintIndex<0) {
					newEntityBlueprintIndex=entityBlueprints.size();
					entityBlueprints.add(blueprintName);
				}
				
				// store link for future use
				newMapBPIDIndex.put(pasteBlueprintIndex,newEntityBlueprintIndex);
				
			}
			newEntity.setBlueprintIndex(newEntityBlueprintIndex);
			
		}
		
		return pasteIDLinkTable;
	}

	public void update(Integer id, Integer blueprintPtr, PlayerID playerID, Float x, Float y, Float z, float headingRad, float pitchRad,
			float rollRad, Double sX, Double sY, Double sZ) {
		
		if (!this.containsKey(id))
			this.put(id,new Entity(id,blueprintPtr,playerID));
		
		this.get(id).setBlueprintIndex(blueprintPtr);
		this.get(id).setPlayerID(playerID);
		this.get(id).setPosition(x, y, z);
		this.get(id).setAnglesRad(headingRad, pitchRad, rollRad);
		this.get(id).setScale(sX, sY, sZ);
		
	}

	
}
