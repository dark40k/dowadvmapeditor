package fr.dow.gamedata.sgbmap.units;

public enum MarkerType {
	
	BASIC("basic_marker", 255f, 255f, 0f, false, false), 
	AI("ai_chokepoint_marker", 0f, 0f, 255f, false, false), 
	CUSTOM("custom_marker", 0f, 255f, 255f, true, true);
	
	public final static String CUSTOMNUMBER="CustomNumber";
	public final static String CUSTOMSTRING="CustomString";
	
	private String type;
	private Float red,green,blue;
	private boolean doesHaveCustomString, doesHaveCustomNumber;
	
	private MarkerType(String type, Float red, Float green, Float blue,boolean hasCustomNumber, boolean hasCustomString) { 
		this.type=type; 
		this.red=red;
		this.green=green;
		this.blue=blue;
		this.doesHaveCustomString=hasCustomString;
		this.doesHaveCustomNumber=hasCustomNumber;
		}
	
	public String getTypeName() { return type; }
	
	public static MarkerType getFromTypeName(String typeName) {
		for (MarkerType markerType : MarkerType.values()) 
			if (markerType.type.equals(typeName)) 
				return markerType;
		return null;
	}

	public float getRed() { return red; }
	public float getGreen() { return green; }
	public float getBlue() { return blue; }

	public boolean hasCustomString() {	return doesHaveCustomString; }
	public boolean hasCustomNumber() {	return doesHaveCustomNumber; }
	
	@Override 
	public String toString() {
		return type;
	}
	
}
