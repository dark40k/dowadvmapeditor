package fr.dow.gamedata.sgbmap.units;

public enum PlayerID {

	World(0,"World"),
	Player1(1000, "Player1"),
	Player2(1001, "Player2"),
	Player3(1002, "Player3"),
	Player4(1003, "Player4"),
	Player5(1004, "Player5"),
	Player6(1005, "Player6"),
	Player7(1006, "Player7"),
	Player8(1007, "Player8");
	
	private Integer playerVal;
	private String playerName;
	
	private PlayerID(int playerVal, String playerName) {
		this.playerName=playerName;
		this.playerVal=playerVal;
	}
	
	public Integer getVal() {
		return playerVal;
	}
	
	@Override 
	public String toString() {
		return playerName;
	}

	public static PlayerID getPlayerID(int playerVal) {
		for (PlayerID playerID : PlayerID.values())
			if (playerVal==playerID.getVal())
				return playerID;
		return null;
	}
	
	public static PlayerID getPlayerID(String playerName) {
		for (PlayerID playerID : PlayerID.values())
			if (playerID.toString().equalsIgnoreCase(playerName))
				return playerID;
		return null;
	}
	
	public static PlayerID[] playerValues() {

		PlayerID[] list=new PlayerID[8];
		
		for (int i=0;i<8;i++) list[i]=getPlayerID(i+1000);
				
		return list;
	}
	
}
