package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Vector;

import fr.dow.gamedata.RelicChunkDATA;

public class SquadBlueprints extends Vector<String> {
	
	public SquadBlueprints(RelicChunkDATA SBPT) {
		
		// reset reader
		SBPT.dataPosition(0);
		
		// read number of blueprints to load
		int nbp=SBPT.dataGetInt();
		
		for (int i=0;i<nbp;i++) this.add(SBPT.dataGetString());
		
	}

	public RelicChunkDATA exportSBPT() {
		
		int size=4;
		for (String blueprint : this) size+=4+blueprint.length();
		
		ByteBuffer SBPTData=ByteBuffer.allocate(size).order(ByteOrder.LITTLE_ENDIAN);
		SBPTData.putInt(this.size());
		for (String blueprint : this) {
			SBPTData.putInt(blueprint.length());
			SBPTData.put(blueprint.getBytes());
		}
		
		RelicChunkDATA SBPT=new RelicChunkDATA("SBPT",2000,"");
		SBPT.dataWrapArray(SBPTData.array());
		
		return SBPT;
		
	}
	
}
