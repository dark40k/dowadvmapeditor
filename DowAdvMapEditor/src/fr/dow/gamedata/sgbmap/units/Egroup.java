package fr.dow.gamedata.sgbmap.units;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashSet;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;

public class Egroup implements Cloneable {

	private int _id;
	private String _name;
	
	private HashSet<Integer> _entitiesID;
	
	public Egroup(RelicChunkFOLD ESGP){
		
		RelicChunkDATA EGPI=ESGP.subChunkDATA("EGPI");
		RelicChunkDATA EGRP=ESGP.subChunkDATA("EGRP");
		
		EGPI.dataPosition(0);
		_id=EGPI.dataGetInt();	
		_name=EGPI.dataGetString();

		EGRP.dataPosition(0);
		int nbr=EGRP.dataGetInt();
		_entitiesID=new HashSet<Integer>(nbr);
		for(int i = 0; i < nbr; i++) _entitiesID.add(EGRP.dataGetInt());

	}
	
	public Egroup(int id, String name) {
		this._id=id;
		this._name=name;
		this._entitiesID=new HashSet<Integer>();
	}

	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	public Integer get_id() {
		return _id;
	}
	
	public void set_id(int id) {
		_id=id;
		
	}

	public String get_name() {
		return _name;
	}
	
	public void set_name(String name) {
		_name=name;
	}

	public Integer getNumberOfEntities(){
		return _entitiesID.size();
	}
	
	@SuppressWarnings("unchecked")
	public HashSet<Integer> get_EntityIDs() {
		return (HashSet<Integer>) _entitiesID.clone();
	}

	public void clear() {
		_entitiesID.clear();
	}

	public void add(HashSet<Integer> selectedEntityIDs) {
		_entitiesID.addAll(selectedEntityIDs);
	}
	
	@SuppressWarnings("unchecked")
	public void intersect(HashSet<Integer> selectedEntitiesIDs) {
		for (Integer entityID : (HashSet<Integer>) _entitiesID.clone())
			if (!selectedEntitiesIDs.contains(entityID))
				_entitiesID.remove(entityID);
	}
	
	public void remove(HashSet<Integer> selectedSquadIDs) {
		for (Integer entityID : selectedSquadIDs)
			_entitiesID.remove(entityID);
	}

	public RelicChunkFOLD exportESGP() {
		
		ByteBuffer EGPIData=ByteBuffer.allocate(8+_name.length()).order(ByteOrder.LITTLE_ENDIAN);
		EGPIData.putInt(_id);
		EGPIData.putInt(_name.length());
		EGPIData.put(_name.getBytes());
		
		RelicChunkDATA EGPI=new RelicChunkDATA("EGPI",1,"egroupinfo"+RelicChunkFOLD.zeroChar);
		EGPI.dataWrapArray(EGPIData.array());
		
		ByteBuffer EGRPData=ByteBuffer.allocate(4+_entitiesID.size()*4).order(ByteOrder.LITTLE_ENDIAN);
		EGRPData.putInt(_entitiesID.size());
		for (int entityID : _entitiesID) EGRPData.putInt(entityID);
		
		RelicChunkDATA EGRP=new RelicChunkDATA("EGRP",1,"Entity Group"+RelicChunkFOLD.zeroChar);
		EGRP.dataWrapArray(EGRPData.array());
		
		RelicChunkFOLD ESGP=new RelicChunkFOLD("ESGP",1,"Entity Group"+RelicChunkFOLD.zeroChar);
		ESGP.get_subChunks().add(EGPI);
		ESGP.get_subChunks().add(EGRP);
		
		return ESGP;
	}

	public boolean containsAny(HashSet<Integer> entityIDs) {
		for (Integer entityID : entityIDs)
			if (_entitiesID.contains(entityID)) 
				return true;
		return false;
	}



}
