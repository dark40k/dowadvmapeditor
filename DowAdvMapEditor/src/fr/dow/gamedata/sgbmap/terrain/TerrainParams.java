package fr.dow.gamedata.sgbmap.terrain;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;

public class TerrainParams implements Cloneable {
	
	private String _WaveName;
	private Integer _waveRed;
	private Integer _waveGreen;
	private Integer _waveBlue;
	private Integer _waveAlpha;
	private Float _waveRepeat;

	private RelicChunkDATA EFFC;
	
	public TerrainParams(RelicChunkDATA EFFC) {
		
		this.EFFC=EFFC;
		
		EFFC.dataPosition(0x30);
		_WaveName=EFFC.dataGetString();
		EFFC.dataSkipBytes(4);
		_waveBlue=EFFC.dataGetByte();
		_waveGreen=EFFC.dataGetByte();
		_waveRed=EFFC.dataGetByte();
		_waveAlpha=EFFC.dataGetByte();
		_waveRepeat=EFFC.dataGetFloat(); // repeat is texture coord step per each vertex
		
	}
	
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	public RelicChunk exportEFFC() {
		return EFFC;
	}
	
	public String get_WaveName() {
		return _WaveName;
	}

	public Integer get_waveRed() {
		return _waveRed;
	}

	public Integer get_waveGreen() {
		return _waveGreen;
	}

	public Integer get_waveBlue() {
		return _waveBlue;
	}

	public Integer get_waveAlpha() {
		return _waveAlpha;
	}

	public Float get_waveRepeat() {
		return _waveRepeat;
	}

	
}
