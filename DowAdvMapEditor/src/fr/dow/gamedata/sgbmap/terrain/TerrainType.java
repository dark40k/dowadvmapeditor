package fr.dow.gamedata.sgbmap.terrain;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.dow.gamedata.RelicChunkDATA;

public class TerrainType implements Cloneable {

	public static final int COVERNONE=0;
	public static final int COVERLIGHT=1;
	public static final int COVERHEAVY=2;
	public static final int COVERNEGATIVE=3;
	public static final int COVERBLOCKING=4;
	public static final int COVERSTEALTH=5;
	
	public static final int FOOTFALLUNKNOWN = 0;
	public static final int FOOTFALLDIRTSAND = 1;
	public static final int FOOTFALLROCK = 2;
	public static final int FOOTFALLGRASS = 3;
	public static final int FOOTFALLWATER = 4;
	
	private RelicChunkDATA _TTYP;
	
	private int _footfallOffset;
	private int _footfallSizeX;
	private int _footfallSizeZ;
	private int _coverOffset;
	private int _coverSizeX; // Warning : it is assumed to be equal with _footfallSizeX
	private int _coverSizeZ; // Warning : it is assumed to be equal with _footfallSizeZ
	
	public TerrainType(RelicChunkDATA TTYP) {
		
		_TTYP=TTYP;
		
		TTYP.dataPosition(4);
		_footfallSizeX=TTYP.dataGetInt();
		_footfallSizeZ=TTYP.dataGetInt();
		_footfallOffset=12;
		
		TTYP.dataPosition(_footfallOffset+_footfallSizeX*_footfallSizeZ);
		_coverSizeX=TTYP.dataGetInt();
		_coverSizeZ=TTYP.dataGetInt();
		_coverOffset=_footfallOffset+_footfallSizeX*_footfallSizeZ+8;
		
	}

	public Object clone() {
		TerrainType o = null;
		try {
			o = (TerrainType) super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o._TTYP=(RelicChunkDATA)o._TTYP.clone();
		
		return o;
	}
	
	
	public int get_footfallOffset() {
		return _footfallOffset;
	}

	public int get_footfallSizeX() {
		return _footfallSizeX;
	}

	public int get_footfallSizeZ() {
		return _footfallSizeZ;
	}

	public ByteArrayInputStream get_coverData() {
		return new ByteArrayInputStream(_TTYP.dataArray(),_coverOffset,_coverSizeX*_coverSizeZ);
	}
	
	public int get_coverOffset() {
		return _coverOffset;
	}

	public int get_coverSizeX() {
		return _coverSizeX;
	}

	public int get_coverSizeZ() {
		return _coverSizeZ;
	}

	public ByteArrayInputStream get_footfallData() {
		return new ByteArrayInputStream(_TTYP.dataArray(),_footfallOffset,_footfallSizeX*_footfallSizeZ);
	}

	public RelicChunkDATA exportTTYP() {
		return _TTYP;
	}

	public void resize(int newCellX, int newCellZ) {
		byte[] TTYPData=_TTYP.dataArray();
		
		int newSizeX=newCellX/2+1;
		int newSizeZ=newCellZ/2+1;
		
		int offsetXfootfall=(newSizeX-_footfallSizeX)/2;
		int offsetZfootfall=(newSizeZ-_footfallSizeZ)/2;
		
		int offsetXcover=(newSizeX-_coverSizeX)/2;
		int offsetZcover=(newSizeZ-_coverSizeZ)/2;
		
		ByteBuffer newTTYPData=ByteBuffer.allocate(20+2*newSizeX*newSizeZ).order(ByteOrder.LITTLE_ENDIAN);
		newTTYPData.putInt(2);
		
		newTTYPData.putInt(newSizeX);
		newTTYPData.putInt(newSizeZ);
		for (int j=0;j<newSizeZ;j++) 
			for (int i=0;i<newSizeX;i++)
			{
				int i1=Math.min(Math.max(i-offsetXfootfall,0),_footfallSizeX-1);
				int j1=Math.min(Math.max(j-offsetZfootfall,0),_footfallSizeZ-1);
				newTTYPData.put(TTYPData[_footfallOffset+j1*_footfallSizeX+i1]);;
			}
			
		newTTYPData.putInt(newSizeX);
		newTTYPData.putInt(newSizeZ);
		for (int j=0;j<newSizeZ;j++) 
			for (int i=0;i<newSizeX;i++)
			{
				int i1=Math.min(Math.max(i-offsetXcover,0),_coverSizeX-1);
				int j1=Math.min(Math.max(j-offsetZcover,0),_coverSizeZ-1);
				newTTYPData.put(TTYPData[_coverOffset+j1*_coverSizeX+i1]);;
			}
		
		_TTYP.dataWrapArray(newTTYPData.array());
		_footfallSizeX=newSizeX;
		_footfallSizeZ=newSizeZ;
		_coverSizeX=newSizeX;
		_coverSizeZ=newSizeZ;
		_coverOffset=_footfallOffset+_footfallSizeX*_footfallSizeZ+8;
	}

	public void move(int moveX, int moveZ) {
		
		byte[] TTYPData=_TTYP.dataArray();
		
		ByteBuffer newTTYPData=ByteBuffer.allocate(20+_footfallSizeX*_footfallSizeZ+_coverSizeX*_coverSizeZ).order(ByteOrder.LITTLE_ENDIAN);
		newTTYPData.putInt(2);
		
		newTTYPData.putInt(_footfallSizeX);
		newTTYPData.putInt(_footfallSizeZ);
		for (int j=0;j<_footfallSizeZ;j++) 
			for (int i=0;i<_footfallSizeX;i++)
			{
				int i1=Math.min(Math.max(i-moveX,0),_footfallSizeX-1);
				int j1=Math.min(Math.max(j-moveZ,0),_footfallSizeZ-1);
				newTTYPData.put(TTYPData[_footfallOffset+j1*_footfallSizeX+i1]);;
			}
			
		newTTYPData.putInt(_coverSizeX);
		newTTYPData.putInt(_coverSizeZ);
		for (int j=0;j<_coverSizeZ;j++) 
			for (int i=0;i<_coverSizeX;i++)
			{
				int i1=Math.min(Math.max(i-moveX,0),_coverSizeX-1);
				int j1=Math.min(Math.max(j-moveZ,0),_coverSizeZ-1);
				newTTYPData.put(TTYPData[_coverOffset+j1*_coverSizeX+i1]);;
			}
		
		_TTYP.dataWrapArray(newTTYPData.array());
		
	}
	
	public void rotateClockwise() {
		
		byte[] TTYPData=_TTYP.dataArray();
		
		int newSizeX=_footfallSizeZ;
		int newSizeZ=_footfallSizeX;
		
		ByteBuffer newTTYPData=ByteBuffer.allocate(20+2*newSizeX*newSizeZ).order(ByteOrder.LITTLE_ENDIAN);
		newTTYPData.putInt(2);
		
		newTTYPData.putInt(newSizeX);
		newTTYPData.putInt(newSizeZ);
		for (int j=0;j<newSizeZ;j++) 
			for (int i=0;i<newSizeX;i++)
				newTTYPData.put(TTYPData[_footfallOffset+i*_footfallSizeX+newSizeZ-1-j]);
			
		newTTYPData.putInt(newSizeX);
		newTTYPData.putInt(newSizeZ);
		for (int j=0;j<newSizeZ;j++) 
			for (int i=0;i<newSizeX;i++)
				newTTYPData.put(TTYPData[_coverOffset+i*_coverSizeX+newSizeZ-1-j]);
		
		_TTYP.dataWrapArray(newTTYPData.array());
		_footfallSizeX=newSizeX;
		_footfallSizeZ=newSizeZ;
		_coverSizeX=newSizeX;
		_coverSizeZ=newSizeZ;
		_coverOffset=_footfallOffset+_footfallSizeX*_footfallSizeZ+8;
	}

	
/*
 *  Paste not working (size is halved, offset is probably wrong)
 * 
 * 
	public void pasteSubMap(CopyData pasteData, int offsetX, int offsetZ) {

		byte[] pasteTTYPData=pasteData.sgbMap.terrain.ground.terrainType._TTYP.dataArray();
		
		byte[] newTTYPData=(byte[]) _TTYP.dataArray().clone();
		
		SelGroundArea selGroundArea=pasteData.selection.groundAreas.selGroundAreas.firstElement();
		
		// reduce copy area inside source map
		int imin=forceBorder(Math.min(selGroundArea.i1,selGroundArea.i2),0,_footfallSizeX-1);
		int imax=forceBorder(Math.max(selGroundArea.i1,selGroundArea.i2),0,_footfallSizeZ-1);
		int jmin=forceBorder(Math.min(selGroundArea.j1,selGroundArea.j2),0,_footfallSizeX-1);
		int jmax=forceBorder(Math.max(selGroundArea.j1,selGroundArea.j2),0,_footfallSizeZ-1);

		// reduce copy area inside destination map
		if ((imin+offsetX)<0) imin=-offsetX;
		if ((imax+offsetX)>(_footfallSizeX-1)) imax=_footfallSizeX-1-offsetX;
		if ((jmin+offsetZ)<0) jmin=-offsetZ;
		if ((jmax+offsetZ)>(_footfallSizeZ-1)) jmax=_footfallSizeZ-1-offsetZ;
		
		for (int i=imin; i<=imax;i++)
			for (int j=jmin; j<=jmax;j++) {
				newTTYPData[_footfallOffset+i+offsetX+_footfallSizeX*(j+offsetZ)]=pasteTTYPData[_footfallOffset+i+_footfallSizeX*j];
				newTTYPData[_coverOffset+i+offsetX+_footfallSizeX*(j+offsetZ)]=pasteTTYPData[_coverOffset+i+_footfallSizeX*j];
			}
		
		_TTYP.dataWrapArray(newTTYPData);
	}
	
	private static int forceBorder(int i, int imin, int imax) {
		if (i<imin) return imin;
		if (i>imax) return imax;
		return i;
	}
*/

}
