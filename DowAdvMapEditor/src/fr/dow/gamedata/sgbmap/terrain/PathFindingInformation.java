package fr.dow.gamedata.sgbmap.terrain;

import fr.dow.gamedata.RelicChunkFOLD;

public class PathFindingInformation implements Cloneable {

	public PassabilityMap passabilityMap;
	public PathSectorMap pathSectorMap;
	public PathSizeMap pathSizeMap;
	
	public PathFindingInformation(RelicChunkFOLD PFDR){
		
		passabilityMap=new PassabilityMap(PFDR.subChunkDATA("IMPA"));
		
		pathSizeMap=new PathSizeMap(PFDR.subChunkDATA("PRMP"));
		
		pathSectorMap=new PathSectorMap(PFDR.subChunkDATA("PASM"));
		
	}

	public RelicChunkFOLD exportPFDR() {
		RelicChunkFOLD PFDR=new RelicChunkFOLD("PFDR",2001,"Pathfinding Information"+RelicChunkFOLD.zeroChar);
		PFDR.get_subChunks().add(passabilityMap.exportIMPA());
		PFDR.get_subChunks().add(pathSizeMap.exportPRMP());
		PFDR.get_subChunks().add(pathSectorMap.exportPASM());
		return PFDR;
	}
	
	public Object clone() {
		PathFindingInformation o = null;
		try {
			o = (PathFindingInformation)super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o.passabilityMap=(PassabilityMap)o.passabilityMap.clone();
		o.pathSectorMap=(PathSectorMap)o.pathSectorMap.clone();
		o.pathSizeMap=(PathSizeMap)o.pathSizeMap.clone();
		
		return o;
	}
	
	public void update(Ground ground) {
		passabilityMap.update(ground);
		pathSizeMap.update(passabilityMap);
	}
	
}

/*
	public RelicChunkFOLD exportPFDR() {
		
		RelicChunkFile mapChunkFile=null;
		try {
			mapChunkFile = new RelicChunkFile("C:/Program Files/THQ/Dawn of War - Dark Crusade/DXP2/Data/scenarios/mp/0_DXP2_512x512x2.sgb");
		} catch (IOException e) {
			e.printStackTrace();
		}
		RelicChunkFOLD SCEN=(RelicChunkFOLD) mapChunkFile.get(0); // Chunk should be of type SCEN
		RelicChunkFOLD refPFDR=SCEN.subChunkFOLD("WSTC").subChunkFOLD("MSTC").subChunkFOLD("PFDR");
		
		RelicChunkFOLD PFDR=new RelicChunkFOLD("PFDR",2001,"Pathfinding Information"+RelicChunkFOLD.zeroChar);
		PFDR.get_subChunks().add(refPFDR.subChunkDATA("IMPA"));
		PFDR.get_subChunks().add(refPFDR.subChunkDATA("PRMP"));
		PFDR.get_subChunks().add(refPFDR.subChunkDATA("PASM"));
		return PFDR;
	}
*/