package fr.dow.gamedata.sgbmap.terrain;

import java.nio.ByteBuffer;

import fr.dow.gamedata.RelicChunkDATA;

public class Decal implements Cloneable {
	
	private int _id;
	private int _decalPtr;
	private float _x;
	private float _z;
	private float _size;
	private float _angle;
	
	public Decal(int id, int decalPtr,float x, float z, float size, float angle) {
		_id=id;
		_decalPtr=decalPtr;
		_x=x;
		_z=z;
		_size=size;
		_angle=angle;
	}
	
	public Decal(RelicChunkDATA chunkdata) {
		_id=chunkdata.dataGetInt();
		_decalPtr=chunkdata.dataGetInt();
		_x=chunkdata.dataGetFloat();
		_z=chunkdata.dataGetFloat();
		_size=chunkdata.dataGetFloat();
		_angle=chunkdata.dataGetFloat();
	}

	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	public void putDecal(ByteBuffer data) {
		data.putInt(_id);
		data.putInt(_decalPtr);
		data.putFloat(_x);
		data.putFloat(_z);
		data.putFloat(_size);
		data.putFloat(_angle);
	}

	public void move(float moveX, float moveZ) {
		_x+=moveX;
		_z+=moveZ;
	}
	
	public void rotateClockwise() {
		float tmp=_x;
		_x=_z;
		_z=-tmp;
		_angle+=0.5*Math.PI;
	}
	
	public void set(int decalPtr,float x, float z, float size, float angle) {
		_decalPtr=decalPtr;
		_x=x;
		_z=z;
		_size=size;
		_angle=angle;
	}
	
	
	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public int get_decalPtr() {
		return _decalPtr;
	}

	public void set_decalPtr(int ptr) {
		_decalPtr = ptr;
	}

	public float get_x() {
		return _x;
	}

	public void set_x(float _x) {
		this._x = _x;
	}

	public float get_z() {
		return _z;
	}

	public void set_z(float _z) {
		this._z = _z;
	}

	public float get_size() {
		return _size;
	}

	public void set_size(float _size) {
		this._size = _size;
	}

	public float get_angle() {
		return _angle;
	}

	public void set_angle(float _angle) {
		this._angle = _angle;
	}



}
