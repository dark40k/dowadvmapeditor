package fr.dow.gamedata.sgbmap.terrain;

import fr.dow.gamedata.RelicChunkDATA;

public class PathSectorMap implements Cloneable {

	private RelicChunkDATA PASM;
	public Integer imax;
	public Integer jmax;
	
	public PathSectorMap(RelicChunkDATA PASM) {
		this.PASM=PASM;
		
		PASM.dataPosition(0);
		
		imax=PASM.dataGetInt();
		jmax=PASM.dataGetInt();
		
	}

	public Object clone() {
		PathSectorMap o = null;
		try {
			o = (PathSectorMap)super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o.PASM=(RelicChunkDATA)o.PASM.clone();
		
		return o;
	}
	
	public int getData(int i,int j) {
		return (((int) PASM.dataArray()[12+i*2+j*imax*2])& 0xFF ) + ((((int) PASM.dataArray()[13+i*2+j*imax*2])& 0xFF ) << 8);
	}

	public RelicChunkDATA exportPASM() {
		return PASM;
	}

}
