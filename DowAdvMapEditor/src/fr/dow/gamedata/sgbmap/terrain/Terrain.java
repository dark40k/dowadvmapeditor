package fr.dow.gamedata.sgbmap.terrain;

import java.io.IOException;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.mapeditor.CopyData;

public class Terrain implements Cloneable{

	//
	// WARNING: all distances except specifically stated are NOT in m. 
	//
	
	public Ground ground;
	
	public DecalFileLinks decalFileLinks; // list filenames for decal pointers
	public Decals decals; // list filenames for decal pointers
	
	public TerrainParams terrainParams;

	public PathFindingInformation pathFindingInformation;
	
	public RelicChunkDATA HRZN;
	public RelicChunkDATA WCLR;
	public RelicChunkDATA OCLR;
	
	public Object clone() {
		
		Terrain o = null;
		try {
			o= (Terrain)super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o.ground=(Ground)o.ground.clone();
		
		o.decalFileLinks=(DecalFileLinks)o.decalFileLinks.clone();
		o.decals=(Decals)o.decals.clone();
		
		o.terrainParams=(TerrainParams)o.terrainParams.clone();
		o.pathFindingInformation=(PathFindingInformation)o.pathFindingInformation.clone();
		
		o.HRZN=(RelicChunkDATA)o.HRZN.clone(); 
		o.WCLR=(RelicChunkDATA)o.WCLR.clone(); 
		o.OCLR=(RelicChunkDATA)o.OCLR.clone(); 
		
		return o;
	}

	public Terrain(RelicChunkFOLD WSTC, DowFileVersion dowTgaFile) throws IOException{
		
		RelicChunkFOLD TERR=WSTC.subChunkFOLD("TERR");
		
		//
		// Ground (Heigtmap, TexTiles, terrain type, impass, ...)
		//
		ground=new Ground(TERR.subChunkFOLD("TFAC"), dowTgaFile);
		
		//
		// Decals
		//
		RelicChunkFOLD DECL=TERR.subChunkFOLD("DECL");
		decalFileLinks=new DecalFileLinks(DECL.subChunkDATA("SMAP"));
		decals=new Decals(DECL.subChunkDATA("ENTY"));
		
		terrainParams=new TerrainParams(TERR.subChunkDATA("EFFC"));
		
		HRZN=TERR.subChunkDATA("HRZN");
		WCLR=TERR.subChunkDATA("WCLR");
		OCLR=TERR.subChunkDATA("OCLR");
		
		//
		// Pathfinding information
		//
		pathFindingInformation=new PathFindingInformation(WSTC.subChunkFOLD("MSTC").subChunkFOLD("PFDR"));
		
	}

	public RelicChunkFOLD exportWSTC() {
		
		RelicChunkFOLD DECL=new RelicChunkFOLD("DECL",2000,"");
		DECL.get_subChunks().add(decalFileLinks.exportSMAP());
		DECL.get_subChunks().add(decals.exportENTY());
		
		RelicChunkFOLD TERR=new RelicChunkFOLD("TERR",2000,"");
		TERR.get_subChunks().add(ground.exportTFAC());
		TERR.get_subChunks().add(terrainParams.exportEFFC());
		TERR.get_subChunks().add(HRZN);
		TERR.get_subChunks().add(DECL);
		TERR.get_subChunks().add(WCLR);
		TERR.get_subChunks().add(OCLR);
		
		RelicChunkFOLD MSTC=new RelicChunkFOLD("MSTC",2000,"SigmaWorld Static Data"+RelicChunk.zeroChar);
		MSTC.get_subChunks().add(pathFindingInformation.exportPFDR());
		
		RelicChunkFOLD WSTC=new RelicChunkFOLD("WSTC",2001,"");
		WSTC.get_subChunks().add(TERR);
		WSTC.get_subChunks().add(MSTC);
		
		return WSTC;

	}

	public void resize(int newCellX, int newCellZ) {
		ground.resize(newCellX,newCellZ);
		updatePathFinding();
	}

	public void move(boolean stepTile, int moveX, int moveZ) {
		
		ground.move(stepTile,moveX,moveZ);
		
		if (stepTile) {
			int tileSize=ground.groundData.get_texTileSize();
			moveX*=tileSize;
			moveZ*=tileSize;
		}
		
		float cellSize=ground.groundData.get_cellsize();
		decals.move(moveX*cellSize,moveZ*cellSize);
		updatePathFinding();
	}

	public void pasteSubMap(CopyData pasteData, boolean heightMap, boolean textureMap, int offsetX, int offsetZ) {
		ground.pasteSubMap(pasteData,heightMap,textureMap,offsetX,offsetZ);
		
		GroundData gndDataSrc=pasteData.sgbMap.terrain.ground.groundData;
		GroundData gndDataDst=ground.groundData;
		float moveX=(float)(offsetX - gndDataDst.get_centerXnbr() + gndDataSrc.get_centerXnbr()) * gndDataDst.get_cellsize();
		float moveZ=(float)(offsetZ - gndDataDst.get_centerZnbr() + gndDataSrc.get_centerZnbr()) * gndDataDst.get_cellsize();
		
		decals.pasteSubMap(pasteData,moveX,moveZ,decalFileLinks);
		
		updatePathFinding();
	}

	public void rotateClockwise() {
		ground.rotateClockwise();
		decals.rotateClockwise();
		updatePathFinding();
	}

	public void changeDecalBP(int decalID, String newDecalBP) {
		int newDecalPtr=decalFileLinks.findDecalLink(newDecalBP);
		if (newDecalPtr<0) newDecalPtr=decalFileLinks.addDecalBP(newDecalBP);
		if (decals.containsKey(decalID)) decals.get(decalID).set_decalPtr(newDecalPtr);
	}

	public void changeDecalPos(int decalID, float x, float z, float size, float angle) {
		Decal decal=decals.get(decalID);
		decal.set_x(x);
		decal.set_z(z);
		decal.set_size(size);
		decal.set_angle(angle);
	}

	public void decalDelete(int decalID) {
		decals.remove(decalID); 
	}

	public void decalDeleteAll() {
		decals.clear();
	}

	public void updatePathFinding() {
		pathFindingInformation.update(ground);
	}

	public Integer decalCreate(String decalBP, float x, float z, float size, float angle) {
		int newDecalPtr=decalFileLinks.findDecalLink(decalBP);
		if (newDecalPtr<0) newDecalPtr=decalFileLinks.addDecalBP(decalBP);
		return decals.create(newDecalPtr,x,z,size,angle);
	}

	public void decalUpdate(int decalID, String decalBP, float x, float z, float size, float angle) {
		int newDecalPtr=decalFileLinks.findDecalLink(decalBP);
		if (newDecalPtr<0) newDecalPtr=decalFileLinks.addDecalBP(decalBP);
		decals.update(decalID,newDecalPtr,x,z,size,angle);
	}

	
}
