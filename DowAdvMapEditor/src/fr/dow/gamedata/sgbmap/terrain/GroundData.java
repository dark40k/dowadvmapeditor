package fr.dow.gamedata.sgbmap.terrain;


import fr.dow.gamedata.RelicChunkDATA;

public class GroundData implements Cloneable {

	private RelicChunkDATA _VDAT;
	
	//
	// local data (copy for faster access)
	//
	private int _centerXnbr; // position offset of center (u)
	private int _centerZnbr; // position offset of center (u)
	private float _cellsize; // size of a cell in m (sampling rate)
	private float _heightscale; // height factor : height in m = _heightscale * stored value
	private int _cellnbrX; // nbr of cells (vertex nbr is +1)
	private int _cellnbrZ; // nbr of cells (vertex nbr is +1)
	
	private int _texTileSize; // tile size expressed in dots (not m)
	private int _texTileRepeat; // nbr of texture repeat in a tile
	
	
	public GroundData(RelicChunkDATA VDAT) {
		_VDAT=VDAT;
		updateLocalData();
	}
	
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	private void updateLocalData() {
		
		_centerXnbr=_VDAT.dataGetInt(0);
		_centerZnbr=_VDAT.dataGetInt(4);
		
		_cellsize=_VDAT.dataGetFloat(8);
//		int v2=_VDAT.dataGetInt(12); // 1
		_heightscale=_VDAT.dataGetFloat(16);
//		int v4=_VDAT.dataGetInt(20); // 30
		_cellnbrX=_VDAT.dataGetInt(24);
		_cellnbrZ=_VDAT.dataGetInt(28);
//		int v7=_VDAT.dataGetInt(32); // 8
//		int v8=_VDAT.dataGetInt(36); // 2
		_texTileSize=_VDAT.dataGetInt(40); // 16 => tile size?
		_texTileRepeat=_VDAT.dataGetInt(44);
//		float v11=_VDAT.get_dataFloat(48); // 0 => 1.0
		// reste 2 octets inconnus
		
	}

	public void resize(int newCellX, int newCellZ) {
		
		_VDAT.dataPutInt(0, newCellX/2+1); //_centerXnbr
		_VDAT.dataPutInt(4, newCellZ/2+1); //_centerZnbr
		_VDAT.dataPutInt(24, newCellX); //_cellnbrX
		_VDAT.dataPutInt(28, newCellZ); //_cellnbrZ
		
		updateLocalData();
	}
	
	public void rotateClockwise() {
		int tmp=_VDAT.dataGetInt(0);
		_VDAT.dataPutInt(0, _VDAT.dataGetInt(4));
		_VDAT.dataPutInt(4, tmp);
		
		tmp=_VDAT.dataGetInt(24);
		_VDAT.dataPutInt(24, _VDAT.dataGetInt(28));
		_VDAT.dataPutInt(28, tmp);
		
		updateLocalData();
	}
	
	public RelicChunkDATA exportVDAT() {
		return _VDAT;
	}
	
	public float posToIndexX(float posX){
		return posX/get_cellsize()+get_centerXnbr()-1;
	}
	
	public float posToIndexZ(float posZ){
		return posZ/get_cellsize()+get_centerZnbr()-1;
	}
	
	public float get_XMin() {
		return (1-get_centerXnbr())*get_cellsize();
	}
	
	public float get_XMax() {
		return (1+get_cellnbrX()-get_centerXnbr())*get_cellsize();
	}
	
	public float get_ZMin() {
		return (1-get_centerZnbr())*get_cellsize();
	}
	
	public float get_ZMax() {
		return (1+get_cellnbrZ()-get_centerZnbr())*get_cellsize();
	}

	public int get_centerXnbr() {
		return _centerXnbr;
	}

	public int get_centerZnbr() {
		return _centerZnbr;
	}

	public float get_cellsize() {
		return _cellsize;
	}

	public float get_heightscale() {
		return _heightscale;
	}

	public int get_cellnbrX() {
		return _cellnbrX;
	}

	public int get_cellnbrZ() {
		return _cellnbrZ;
	}

	public int get_texTileSize() {
		return _texTileSize;
	}

	public int get_texTileRepeat() {
		return _texTileRepeat;
	}


	
}
