package fr.dow.gamedata.sgbmap.terrain;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.channels.WritableByteChannel;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.mapeditor.CopyData;
import fr.dow.mapviewer.display.selection.SelArea;

public class GroundTexture implements Cloneable {

	private int width;
	private int height;
	private ByteBuffer texture;
	
	public GroundTexture(DowFileVersion dowTgaFile) throws IOException {
		
		byte[] textureRawData=dowTgaFile.getData();
		
		ByteBuffer textureData=ByteBuffer.allocate(18).order(ByteOrder.LITTLE_ENDIAN);
		System.arraycopy(textureRawData, 0, textureData.array(), 0, 18);
		
		textureData.position(12);
		width=textureData.getShort();
		height=textureData.getShort();
		
		texture=ByteBuffer.allocate(width*height*4); //4 bytes per pixel
		System.arraycopy(textureRawData, 18, texture.array(), 0, width*height*4);
		
	}

	public Object clone() {
		GroundTexture o = null;
		try {
			o = (GroundTexture) super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o.texture=ByteBuffer.wrap(texture.array().clone()).order(ByteOrder.LITTLE_ENDIAN);
		o.texture.position(texture.position());
		
		return o;
	}
	
	public void save(String filename) throws IOException {
		
		FileOutputStream file = new FileOutputStream(filename);
		WritableByteChannel channel = file.getChannel();
		
		ByteBuffer textureData=ByteBuffer.allocate(18).order(ByteOrder.LITTLE_ENDIAN);
		
		textureData.put((byte)0); // idlength;
		textureData.put((byte)0); // colourmaptype;
		textureData.put((byte)2); // datatypecode;
		textureData.putShort((short) 0); // colourmaporigin;
		textureData.putShort((short) 0); // colourmaplength;
		textureData.put((byte)0); // colourmapdepth;
		textureData.putShort((short) 0); // x_origin;
		textureData.putShort((short) 0); // y_origin;
		textureData.putShort((short) width); // width;
		textureData.putShort((short) height); // height;
		textureData.put((byte)32); // bitsperpixel;
		textureData.put((byte)0); // imagedescriptor;
		
		textureData.rewind();
		channel.write(textureData);
		
		texture.rewind();
		channel.write(texture);
		
		file.close();
	}
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public ByteBuffer getTexture() {
		texture.rewind();
		return texture;
	}

	public void setTexture(ByteBuffer texture) {
		this.texture = texture;
	}

	public void resize(int newWidth, int newHeight) {
		
		int offsetWidth=(newWidth-width)/2;
		int offsetHeight=(newHeight-height)/2;
		
		int[][] textureData=new int[height][width];
		texture.rewind();
		IntBuffer intTexture = texture.asIntBuffer();
		for (int j=0;j<height;j++) 
			for (int i=0;i<width;i++)
				textureData[j][i]=intTexture.get();
		
		ByteBuffer newTexture=ByteBuffer.allocate(newWidth*newHeight*4);
		
		for (int j=0;j<newHeight;j++)
			for (int i=0;i<newWidth;i++)
			 {
				int i1=i-offsetWidth;
				int j1=j-offsetHeight;
				if ((i1<0) || (i1>=width) || (j1<0) || (j1>=height)) newTexture.putInt(0xFFFFFFFF);
				else newTexture.putInt(textureData[j1][i1]);
			}

		
		texture=newTexture;
		width=newWidth;
		height=newHeight;
		
	}
	
	public void rotateClockwise() {
		
		int newHeight=width;
		int newWidth=height;
		
		int[][] textureData=new int[height][width];
		texture.rewind();
		IntBuffer intTexture = texture.asIntBuffer();
		for (int j=0;j<height;j++) 
			for (int i=0;i<width;i++)
				textureData[j][i]=intTexture.get();
		
		ByteBuffer newTexture=ByteBuffer.allocate(newWidth*newHeight*4);
		
		for (int j=0;j<newHeight;j++)
			for (int i=0;i<newWidth;i++)
				newTexture.putInt(textureData[i][newHeight-1-j]);
		
		texture=newTexture;
		width=newWidth;
		height=newHeight;
		
	}

	public void move(int moveX, int moveZ) {
		
		int[][] textureData=new int[height][width];
		texture.rewind();
		IntBuffer intTexture = texture.asIntBuffer();
		for (int j=0;j<height;j++) 
			for (int i=0;i<width;i++)
				textureData[j][i]=intTexture.get();
		
		ByteBuffer newTexture=ByteBuffer.allocate(height*width*4);
		
		for (int j=0;j<height;j++)
			for (int i=0;i<width;i++)
			 {
				int i1=i-moveX;
				int j1=j-moveZ;
				if ((i1<0) || (i1>=width) || (j1<0) || (j1>=height)) newTexture.putInt(0xFFFFFFFF);
				else newTexture.putInt(textureData[j1][i1]);
			}

		
		texture=newTexture;
		
	}
	

	public void pasteSubMap(CopyData pasteData, int offsetX, int offsetZ) {
		
		int texOffX=offsetX*2;
		int texOffZ=offsetZ*2;
		
		GroundTexture pasteTexture=pasteData.sgbMap.terrain.ground.groundTexture;
		SelArea selGroundArea=pasteData.selection.area;
		
		// reduce copy area inside source map
		int imin=forceBorder(Math.min(selGroundArea.i1,selGroundArea.i2)*2,0,pasteTexture.width-1);
		int imax=forceBorder(Math.max(selGroundArea.i1,selGroundArea.i2)*2,0,pasteTexture.width-1);
		int jmin=forceBorder(Math.min(selGroundArea.j1,selGroundArea.j2)*2,0,pasteTexture.height-1);
		int jmax=forceBorder(Math.max(selGroundArea.j1,selGroundArea.j2)*2,0,pasteTexture.height-1);

		// reduce copy area inside destination map
		if ((imin+texOffX)<0) imin=-texOffX;
		if ((imax+texOffX)>(width-1)) imax=width-1-texOffX;
		if ((jmin+texOffZ)<0) imin=-texOffZ;
		if ((jmax+texOffZ)>(height-1)) jmax=height-1-texOffZ;
		
		// copy original picture
		ByteBuffer newTexture=ByteBuffer.wrap(texture.array().clone());
		newTexture.rewind();
		
		// paste picture
		pasteTexture.texture.rewind();

		for (int j=jmin; j<jmax;j++) {
			
			// align source buffer
			pasteTexture.texture.position(4*(imin+pasteTexture.height*j));
			
			// align destination buffer
			newTexture.position(4*(imin+texOffX+height*(j+texOffZ)));
			
			// transfer line
			for (int i=imin; i<imax;i++) {
				for (int z=0;z<4;z++) {
					byte col=pasteTexture.texture.get();
					newTexture.put(col);
				}
			}
		}
		
		texture=newTexture;
		
	}
	
	private static int forceBorder(int i, int imin, int imax) {
		if (i<imin) return imin;
		if (i>imax) return imax;
		return i;
	}

		
}
