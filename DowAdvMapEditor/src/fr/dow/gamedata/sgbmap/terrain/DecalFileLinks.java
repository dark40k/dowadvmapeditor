package fr.dow.gamedata.sgbmap.terrain;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedHashMap;

import fr.dow.gamedata.RelicChunkDATA;

public class DecalFileLinks extends LinkedHashMap<Integer,String> {

	public DecalFileLinks(RelicChunkDATA SMAP) {
		
		super();
		
		SMAP.dataPosition(0);
		
		int ndecalpaths=SMAP.dataGetInt();
		
		for (int i=0;i<ndecalpaths;i++) {
			String textpath=SMAP.dataGetString();
			int textkey=SMAP.dataGetInt();
			put(textkey, textpath);
		}
		
	}

	public RelicChunkDATA exportSMAP() {
		int decalsNameSize=0;
		for (String decalName : values()) decalsNameSize+=8+decalName.length();
		
		ByteBuffer SMAPData=ByteBuffer.allocate(4+decalsNameSize).order(ByteOrder.LITTLE_ENDIAN);
		SMAPData.putInt(size());
		for (Integer decalKey : keySet()) {
			String decalName=get(decalKey);
			SMAPData.putInt(decalName.length());
			SMAPData.put(decalName.getBytes());
			SMAPData.putInt(decalKey);
		}
		
		RelicChunkDATA SMAP=new RelicChunkDATA("SMAP",2000,"");
		SMAP.dataWrapArray(SMAPData.array());
		
		return SMAP;
	}
	
	public int findDecalLink(String decalBP) {
		for (int decalPtr : keySet())
			if (decalBP.equals(get(decalPtr)))
				return decalPtr;
		return -1;
	}

	public int addDecalBP(String decalBP) {
		int newPtr=-1;
		for (int decalPtr : keySet()) newPtr=Math.max(newPtr, decalPtr);
		newPtr++;
		put(newPtr,decalBP);
		return newPtr;
	}
	
}
