package fr.dow.gamedata.sgbmap.terrain;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedHashMap;

import fr.dow.gamedata.RelicChunkDATA;

public class TexTileFileLinks extends LinkedHashMap<Integer,String>{

	public TexTileFileLinks(RelicChunkDATA DSHD) {
		
		super();
		
		DSHD.dataPosition(0);
		
		int ntextpaths=DSHD.dataGetInt();

		for (int i=0;i<ntextpaths;i++) {
			String textpath=DSHD.dataGetString();
			int textkey=DSHD.dataGetInt();
			put(textkey, textpath);
		}
		
	}

	public RelicChunkDATA exportDSHD() {
		int texsNameSize=0;
		for (String texName : values()) texsNameSize+=8+texName.length();
		
		ByteBuffer DSHDData=ByteBuffer.allocate(4+texsNameSize).order(ByteOrder.LITTLE_ENDIAN);
		DSHDData.putInt(size());
		for (Integer texKey : keySet()) {
			String texName=get(texKey);
			DSHDData.putInt(texName.length());
			DSHDData.put(texName.getBytes());
			DSHDData.putInt(texKey);
		}
		
		RelicChunkDATA DSHD=new RelicChunkDATA("DSHD",2000,"");
		DSHD.dataWrapArray(DSHDData.array());
		
		return DSHD;
	}

	public int findBlueprint(String tileBP) {
		for (int tilePtr : keySet())
			if (tileBP.equals(get(tilePtr)))
				return tilePtr;
		return -1;
	}

	public int addBlueprint(String newTileBP) {
		int newPtr=-1;
		for (int tilePtr : keySet()) newPtr=Math.max(newPtr, tilePtr);
		newPtr++;
		put(newPtr,newTileBP);
		return newPtr;
	}
	
}
