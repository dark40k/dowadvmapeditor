package fr.dow.gamedata.sgbmap.terrain;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.LinkedHashMap;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.mapeditor.CopyData;

public class Decals extends LinkedHashMap<Integer, Decal>{

	private Integer nextdecalID=1;
	
	public Decals(RelicChunkDATA ENTY) {
		
		super();

		ENTY.dataPosition(0);
		
		int ndecals=ENTY.dataGetInt();
		
		for (int i=0;i<ndecals;i++) {
			Decal newdecal=new Decal(ENTY);
			put(newdecal.get_id(), newdecal);
			if (newdecal.get_id()>=nextdecalID) nextdecalID=newdecal.get_id()+1;
		}

		
	}

	public RelicChunkDATA exportENTY() {
		
		ByteBuffer ENTYData=ByteBuffer.allocate(4+size()*24).order(ByteOrder.LITTLE_ENDIAN);
		ENTYData.putInt(size());
		for (Decal decal : values()) decal.putDecal(ENTYData);
		
		RelicChunkDATA ENTY=new RelicChunkDATA("ENTY",2001,"");
		ENTY.dataWrapArray(ENTYData.array());
		
		return ENTY;
	}

	public boolean decalExists(int decalID) {
		return this.containsKey(decalID);
	}
	
	public void move(float moveX, float moveZ) {
		for (Decal decal : values()) decal.move(moveX,moveZ);
	}
	
	public void rotateClockwise() {
		for (Decal decal : values()) decal.rotateClockwise();
	}
	
	public Object clone() {
		Decals o=(Decals)super.clone();
		for (Integer key : keySet()) o.put(key, (Decal)o.get(key).clone() );
		return o;
	}
	
	public int create(int decalPtr, float x, float z, float size, float angle) {
		Decal newDecal=new Decal(nextdecalID++,decalPtr,x,z,size,angle);
		this.put(newDecal.get_id(), newDecal);
		return newDecal.get_id();
	}
	
	public void update(int decalID, int decalPtr, float x, float z, float size, float angle) {
		if (decalExists(decalID)) {
			Decal newDecal=this.get(decalID);
			newDecal.set(decalPtr,x,z,size,angle);
		} else {
			Decal newDecal=new Decal(decalID,decalPtr,x,z,size,angle);
			this.put(newDecal.get_id(), newDecal);
			if (decalID>=nextdecalID) nextdecalID=decalID+1;
		}
	}

	public void pasteSubMap(CopyData pasteData, float moveX, float moveZ, DecalFileLinks decalFileLinks) {

		DecalFileLinks pasteDecalFileLinks=pasteData.sgbMap.terrain.decalFileLinks;
				
		// provides new Ptr in destination for old Ptr in pastedata
		HashMap<Integer,Integer> newMapDecalPtrs=new HashMap<Integer,Integer>();
		
		// calculate max decal Ptr for destination map
		int maxPtr=0;
		for (Integer decalPtr : decalFileLinks.keySet()) if (decalPtr>maxPtr) maxPtr=decalPtr;
		
		for (Integer pasteDecalID : pasteData.selection.decals) {
			
			// copy decal
			Decal newDecal=(Decal)pasteData.sgbMap.terrain.decals.get(pasteDecalID).clone();

			// assign new ID
			newDecal.set_id(nextdecalID++);
			
			// move Decal
			newDecal.move(moveX, moveZ);
			
			//
			// calculate new Ptr
			//
			int pasteDecalPtr=newDecal.get_decalPtr();
			int newDecalPtr=-1;
			
			if (newMapDecalPtrs.containsKey(pasteDecalPtr)) // check if destination paste pointer is already known
				newDecalPtr=newMapDecalPtrs.get(pasteDecalPtr);
			else {

				// find destination paste pointer with same texture or create a new one
				String pasteDecalPtrName=pasteDecalFileLinks.get(pasteDecalPtr);
				
				// search existing links
				for (Integer decalPtr : decalFileLinks.keySet()) 
					if (pasteDecalPtrName.equals(decalFileLinks.get(decalPtr))) {
						newDecalPtr=decalPtr;
						break;
					}
				
				// if no link found then create a new link
				if (newDecalPtr<0) {
					maxPtr+=1;
					decalFileLinks.put(maxPtr, pasteDecalPtrName);
					newDecalPtr=maxPtr;
				}
				
				// store link for future use
				newMapDecalPtrs.put(pasteDecalPtr,newDecalPtr);
			}
			newDecal.set_decalPtr(newDecalPtr);
			
			//
			// add new decal to destination map
			//
			this.put(newDecal.get_id(),newDecal);
			
		}
		
	}

	
}
