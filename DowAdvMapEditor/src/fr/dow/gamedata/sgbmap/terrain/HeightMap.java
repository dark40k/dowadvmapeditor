package fr.dow.gamedata.sgbmap.terrain;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.mapeditor.CopyData;
import fr.dow.mapviewer.display.selection.SelArea;

public class HeightMap implements Cloneable {

	private int _hmMaxI;
	private int _hmMaxJ;
	
	private float[][] heightGrid;
	private float[] xGrid;
	private float[] zGrid;
	
	public HeightMap(RelicChunkDATA HMAP, GroundData groundData) {
		
		_hmMaxI=HMAP.dataGetInt(0);	
		_hmMaxJ=HMAP.dataGetInt(4);	
		
		byte[] hmap=HMAP.dataArray();
		
		heightGrid=new float[_hmMaxI][_hmMaxJ];
		for (int i=0; i<_hmMaxI; i++ )
			for (int j=0; j<_hmMaxJ; j++ )				
				heightGrid[i][j]= ( 0x00FF & ((int) hmap[8+j*_hmMaxI+i] ) ) * groundData.get_heightscale(); 
		
		updateXgrid(groundData);
		updateZgrid(groundData);
		
		
	}
	
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	public RelicChunkDATA exportHMAP(GroundData groundData) {
		
		ByteBuffer HMAPData=ByteBuffer.allocate(8+_hmMaxI*_hmMaxJ).order(ByteOrder.LITTLE_ENDIAN);
		HMAPData.putInt(_hmMaxI);
		HMAPData.putInt(_hmMaxJ);
		for (int j=0; j<_hmMaxJ; j++ )				
			for (int i=0; i<_hmMaxI; i++ )
				HMAPData.put((byte) (heightGrid[i][j]/groundData.get_heightscale())); 
		
		RelicChunkDATA HMAP=new RelicChunkDATA("HMAP",2000,"");
		HMAP.dataWrapArray(HMAPData.array());
		
		return HMAP;
	}	
	
	private void updateXgrid(GroundData groundData){
		xGrid=new float[groundData.get_cellnbrX()+1];
		for (int i=0; i<=groundData.get_cellnbrX(); i++ ) xGrid[i]= (i-groundData.get_centerXnbr()+1)*groundData.get_cellsize();
	}
	
	private void updateZgrid(GroundData groundData){
		zGrid=new float[groundData.get_cellnbrZ()+1];
		for (int i=0; i<=groundData.get_cellnbrZ(); i++ ) zGrid[i]= (i-groundData.get_centerZnbr()+1)*groundData.get_cellsize();
	}
	
	public float get_height(int i, int j) {
		return heightGrid[i][j]; 
	}
	
	public float[][] get_heightGrid() {
		return heightGrid;
	}
	
	public float[] get_heightGrid_Xaxis() {
		return xGrid;
	}
	
	public float[] get_heightGrid_Zaxis() {
		return zGrid;
	}

	public int get_hmMaxI() {
		return _hmMaxI;
	}

	public int get_hmMaxJ() {
		return _hmMaxJ;
	}

	public void resize(int newCellX, int newCellZ, GroundData groundData) {
		
		int newHmMaxI=newCellX+1;
		int newHmMaxJ=newCellZ+1;
		
		int offsetI=(newHmMaxI-_hmMaxI)/2;
		int offsetJ=(newHmMaxJ-_hmMaxJ)/2;
		
		float[][] newGrid=new float[newHmMaxI][newHmMaxJ];
		
		for (int i=0;i<newHmMaxI;i++)
			for (int j=0;j<newHmMaxJ;j++) {
				int i1=Math.min(Math.max(i-offsetI,0),_hmMaxI-1);
				int j1=Math.min(Math.max(j-offsetJ,0),_hmMaxJ-1);
				newGrid[i][j]=heightGrid[i1][j1];
			}
			
		heightGrid=newGrid;
		_hmMaxI=newHmMaxI;
		_hmMaxJ=newHmMaxJ;
		updateXgrid(groundData);
		updateZgrid(groundData);
		
	}

	public void move(int moveX, int moveZ) {
		
		float[][] newGrid=new float[_hmMaxI][_hmMaxJ];
		
		for (int i=0;i<_hmMaxI;i++)
			for (int j=0;j<_hmMaxJ;j++) {
				int i1=Math.min(Math.max(i-moveX,0),_hmMaxI-1);
				int j1=Math.min(Math.max(j-moveZ,0),_hmMaxJ-1);
				newGrid[i][j]=heightGrid[i1][j1];
			}
			
		heightGrid=newGrid;
		
	}
	
	public void rotateClockwise(GroundData groundData) {
		
		int newHmMaxI=_hmMaxJ;
		int newHmMaxJ=_hmMaxI;
		
		float[][] newGrid=new float[newHmMaxI][newHmMaxJ];
		
		for (int i=0;i<newHmMaxI;i++)
			for (int j=0;j<newHmMaxJ;j++) {
				newGrid[i][j]=heightGrid[newHmMaxJ-1-j][i];
			}
		
		heightGrid=newGrid;
		_hmMaxI=newHmMaxI;
		_hmMaxJ=newHmMaxJ;

		updateXgrid(groundData);
		updateZgrid(groundData);
	}


	public void pasteSubMap(CopyData pasteData, int offsetX, int offsetZ) {

		HeightMap pasteHeightMap=pasteData.sgbMap.terrain.ground.heightMap;
		
		SelArea selGroundArea=pasteData.selection.area;
		
		// reduce copy area inside source map
		int imin=forceBorder(Math.min(selGroundArea.i1,selGroundArea.i2),0,pasteHeightMap.xGrid.length-1);
		int imax=forceBorder(Math.max(selGroundArea.i1,selGroundArea.i2),0,pasteHeightMap.xGrid.length-1);
		int jmin=forceBorder(Math.min(selGroundArea.j1,selGroundArea.j2),0,pasteHeightMap.zGrid.length-1);
		int jmax=forceBorder(Math.max(selGroundArea.j1,selGroundArea.j2),0,pasteHeightMap.zGrid.length-1);

		// reduce copy area inside destination map
		if ((imin+offsetX)<0) imin=-offsetX;
		if ((imax+offsetX)>(xGrid.length-1)) imax=xGrid.length-1-offsetX;
		if ((jmin+offsetZ)<0) jmin=-offsetZ;
		if ((jmax+offsetZ)>(zGrid.length-1)) jmax=zGrid.length-1-offsetZ;
		
		float[][] newGrid=(float[][])heightGrid.clone();
		
		for (int i=imin; i<=imax;i++)
			for (int j=jmin; j<=jmax;j++)				
				newGrid[i+offsetX][j+offsetZ]=pasteHeightMap.heightGrid[i][j];
	
		heightGrid=newGrid;
	}
	
	private static int forceBorder(int i, int imin, int imax) {
		if (i<imin) return imin;
		if (i>imax) return imax;
		return i;
	}

	
}
