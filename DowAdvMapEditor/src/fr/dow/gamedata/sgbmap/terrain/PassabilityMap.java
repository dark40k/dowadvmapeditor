package fr.dow.gamedata.sgbmap.terrain;

import java.io.ByteArrayInputStream;

import fr.dow.gamedata.RelicChunkDATA;

public class PassabilityMap implements Cloneable {

	public static final byte DEEPWATER = 1;
	public static final byte PASSABLE = 2;
	public static final byte BORDERWATER = 3;
	public static final byte IMPASSABLE = 7;
	
	private RelicChunkDATA IMPA;
	public Integer imax;
	public Integer jmax;
	
	public PassabilityMap(RelicChunkDATA IMPA) {
		this.IMPA=IMPA;
		
		IMPA.dataPosition(0);
		
		imax=IMPA.dataGetInt();
		jmax=IMPA.dataGetInt();
		
	}

	public Object clone() {
		PassabilityMap o = null;
		try {
			o = (PassabilityMap)super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o.IMPA=(RelicChunkDATA)o.IMPA.clone();
		
		return o;
	}

	public ByteArrayInputStream get_Data() {
		return new ByteArrayInputStream(IMPA.dataArray(),12,imax*jmax);
	}
	
	public byte getPassability(int i,int j) {
		return IMPA.dataArray()[12+i+j*imax];
	}

	public RelicChunkDATA exportIMPA() {
		return IMPA;
	}
	
	public void update(Ground ground) {
		
		imax=ground.groundData.get_cellnbrX()/2;
		jmax=ground.groundData.get_cellnbrZ()/2;
		
		float scale = ground.groundData.get_heightscale();
		float[][] heightMap = ground.heightMap.get_heightGrid();
		
		int hi0=(ground.heightMap.get_hmMaxI()-1)/2 - imax/2;
		int hj0=(ground.heightMap.get_hmMaxJ()-1)/2 - jmax/2;
		
		Impass impass=ground.impass;
		
		byte[] newPass=new byte[12+imax*jmax];
		for (int i=0;i<12;i++) newPass[i]=IMPA.dataArray()[i];
		
		for(int i=0;i<imax;i++)
			for (int j=0;j<jmax;j++) {
				
				int locImpass=impass.get_passability(i, j);
				if (locImpass==Impass.IMPASSABLE) { newPass[12+i+j*imax]=IMPASSABLE; continue; }
				
				int hi=i+hi0;
				int hj=j+hj0;
				
				int h00=(int) (heightMap[hi][hj] / scale) & 0xFF;
				int h10=(int) (heightMap[hi+1][hj] / scale) & 0xFF;
				int h01=(int) (heightMap[hi][hj+1] / scale) & 0xFF;
				int h11=(int) (heightMap[hi+1][hj+1] / scale) & 0xFF;
				int hmin=Math.min(Math.min(h00, h11), Math.min(h10,h01));
				int hmax=Math.max(Math.max(h00, h11), Math.max(h10,h01));
				
				if (hmax<=8) { newPass[12+i+j*imax]=DEEPWATER; continue; }
				
				if (locImpass==Impass.PASSABLE) { newPass[12+i+j*imax]= (hmin<12) ? BORDERWATER : PASSABLE ; continue; }
				
				if (hmin<12) { newPass[12+i+j*imax]=BORDERWATER; continue; }
				
				if (calcSlopeAllowance(heightMap, hi, hj, scale)) newPass[12+i+j*imax]=PASSABLE;
				else newPass[12+i+j*imax]=IMPASSABLE;
			
			}
		
		IMPA.dataWrapArray(newPass);
		
	}
	
	public static boolean calcSlopeAllowance(float[][] heightMap, int i, int j, float scale) {
		
		int i1=i+1;
		int j1=j+1;
		
		int h00= (int) ( heightMap[i][j] / scale) & 0xFF;
		
		int h10= (int) ( heightMap[i1][j] / scale) & 0xFF;
		
		int h01= (int) ( heightMap[i][j1] / scale) & 0xFF;
		
		int h11= (int) ( heightMap[i1][j1] / scale) & 0xFF;
		
		if (Math.abs(h00-h11)>6) return false;
		if (Math.abs(h10-h01)>6) return false;
		
		if (Math.abs(h00-h10)>4) return false;
		if (Math.abs(h00-h01)>4) return false;
		if (Math.abs(h11-h10)>4) return false;
		if (Math.abs(h11-h01)>4) return false;
		
		return true;
	}
	
}
