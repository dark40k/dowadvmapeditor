package fr.dow.gamedata.sgbmap.terrain;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.dow.gamedata.RelicChunkDATA;

public class Impass implements Cloneable {
	
	public static final byte GENERATED = 0;
	public static final byte PASSABLE = 2;
	public static final byte IMPASSABLE = 1;
	
	public static final byte GEN_PASSABLE=3; // invalid for saving, only for internal use
	public static final byte GEN_IMPASSABLE=4; // invalid for saving, only for internal use
	
	
	private int _offset;
	private int _sizeX;
	private int _sizeZ;
	
	private RelicChunkDATA _IMAP;
	
	public Impass(RelicChunkDATA IMAP) {
		
		_IMAP=IMAP;
		
		IMAP.dataPosition(0);
		_sizeX=IMAP.dataGetInt();
		_sizeZ=IMAP.dataGetInt();
		_offset=8;
		
	}
	
	public Object clone() {
		Impass o = null;
		try {
			o = (Impass) super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o._IMAP=(RelicChunkDATA)o._IMAP.clone();
		
		return o;
	}
	
	public int get_offset() {
		return _offset;
	}

	public int get_sizeX() {
		return _sizeX;
	}

	public int get_sizeZ() {
		return _sizeZ;
	}

	public byte get_passability(int i, int j) {
		return _IMAP.dataArray()[_offset+i+j*_sizeX];
	}
	
	public ByteArrayInputStream get_Data() {
		return new ByteArrayInputStream(_IMAP.dataArray(),_offset,_sizeX*_sizeZ);
	}

	public RelicChunkDATA exportIMAP() {
		return _IMAP;
	}

	public void resize(int newCellX, int newCellZ) {
		
		byte[] IMAPData=_IMAP.dataArray();
		
		int newSizeX=newCellX/2;
		int newSizeZ=newCellZ/2;
		
		int offsetX=(newSizeX-_sizeX)/2;
		int offsetZ=(newSizeZ-_sizeZ)/2;
		
		ByteBuffer newIMAPData=ByteBuffer.allocate(8+newSizeX*newSizeZ).order(ByteOrder.LITTLE_ENDIAN);
		
		newIMAPData.putInt(newSizeX);
		newIMAPData.putInt(newSizeZ);
		for (int j=0;j<newSizeZ;j++) 
			for (int i=0;i<newSizeX;i++)
			{
				int i1=Math.min(Math.max(i-offsetX,0),_sizeX-1);
				int j1=Math.min(Math.max(j-offsetZ,0),_sizeZ-1);
				newIMAPData.put(IMAPData[8+j1*_sizeX+i1]);;
			}
		
		_IMAP.dataWrapArray(newIMAPData.array());
		_sizeX=newSizeX;
		_sizeZ=newSizeZ;
		
	}

	public void move(int moveX, int moveZ) {
		
		byte[] IMAPData=_IMAP.dataArray();
		
		ByteBuffer newIMAPData=ByteBuffer.allocate(8+_sizeX*_sizeZ).order(ByteOrder.LITTLE_ENDIAN);
		
		newIMAPData.putInt(_sizeX);
		newIMAPData.putInt(_sizeZ);
		for (int j=0;j<_sizeZ;j++) 
			for (int i=0;i<_sizeX;i++)
			{
				int i1=Math.min(Math.max(i-moveX,0),_sizeX-1);
				int j1=Math.min(Math.max(j-moveZ,0),_sizeZ-1);
				newIMAPData.put(IMAPData[8+j1*_sizeX+i1]);;
			}
		
		_IMAP.dataWrapArray(newIMAPData.array());
		
	}

	public void rotateClockwise() {
		
		byte[] IMAPData=_IMAP.dataArray();
		
		int newSizeX=_sizeZ;
		int newSizeZ=_sizeX;
		
		ByteBuffer newIMAPData=ByteBuffer.allocate(8+newSizeX*newSizeZ).order(ByteOrder.LITTLE_ENDIAN);
		
		newIMAPData.putInt(newSizeX);
		newIMAPData.putInt(newSizeZ);
		for (int j=0;j<newSizeZ;j++) 
			for (int i=0;i<newSizeX;i++)
				newIMAPData.put(IMAPData[8+i*_sizeX+newSizeZ-1-j]);;
		
		_IMAP.dataWrapArray(newIMAPData.array());
		_sizeX=newSizeX;
		_sizeZ=newSizeZ;
		
	}
	
	public ByteArrayInputStream get_GeneratedData(Ground ground) {
		
		ByteArrayInputStream impass=new ByteArrayInputStream(_IMAP.dataArray(),_offset,_sizeX*_sizeZ);
		ByteBuffer computed=ByteBuffer.allocate(_sizeX*_sizeZ);
		
		float[][] heightMap=ground.heightMap.get_heightGrid();
		float scale=ground.groundData.get_heightscale();
		
		int i0=_sizeX/2;
		int i1=i0+_sizeX;
		int j0=_sizeZ/2;
		int j1=j0+_sizeZ;
		
		for (int j=j0; j<j1; j++)
		for (int i=i0; i<i1; i++)
			 {
				int cur=impass.read();
				if (cur!=GENERATED) computed.put((byte)cur);
				else computed.put((calcPassAllowance(heightMap,i,j, scale) ? GEN_PASSABLE : GEN_IMPASSABLE));
			}
		
		return new ByteArrayInputStream(computed.array());
	}

	
	public static boolean[][] calcPassAllowance(float[][] heightMap, float scale) {
		
		int imax=heightMap.length-1;
		int jmax=heightMap[0].length-1;
		
		boolean[][] passTable=new  boolean[imax][jmax];
		
		for (int i=0;i<imax;i++)
			for (int j=0; j<jmax; j++) 
				passTable[i][j]=calcPassAllowance(heightMap,i,j,scale);
		
		return passTable;
		
	}
	
	public static boolean calcPassAllowance(float[][] heightMap, int i, int j, float scale) {
		
		int i1=i+1;
		int j1=j+1;
		
		int h00= (byte) ( heightMap[i][j] / scale);
		if (h00<12) return false;
		
		int h10= (byte) ( heightMap[i1][j] / scale);
		if (h10<12) return false;
		
		int h01= (byte) ( heightMap[i][j1] / scale);
		if (h01<12) return false;
		
		int h11= (byte) ( heightMap[i1][j1] / scale);
		if (h11<12) return false;
		
		if (Math.abs(h00-h11)>6) return false;
		if (Math.abs(h10-h01)>6) return false;
		
		if (Math.abs(h00-h10)>4) return false;
		if (Math.abs(h00-h01)>4) return false;
		if (Math.abs(h11-h10)>4) return false;
		if (Math.abs(h11-h01)>4) return false;
		
		return true;
	}

}
