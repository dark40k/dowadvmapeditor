package fr.dow.gamedata.sgbmap.terrain;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.dow.gamedata.RelicChunkDATA;

public class PathSizeMap implements Cloneable{
	
	public static final int PATH0 = 0;
	public static final int PATH1 = 1;
	public static final int PATH3 = 3;
	public static final int PATH5 = 5;
	public static final int PATH7 = 7;

	private RelicChunkDATA PRMP;
	public Integer imax;
	public Integer jmax;
	public Integer v1;
	public Integer v2;
	public Integer v3;
	
	private static final int[][] filter = {
			{7,7,7,7,7,7,7,7,7},
			{7,5,5,5,5,5,5,5,7},
			{7,5,3,3,3,3,3,5,7},
			{7,5,3,1,1,1,3,5,7},
			{7,5,3,1,0,1,3,5,7},
			{7,5,3,1,1,1,3,5,7},
			{7,5,3,3,3,3,3,5,7},
			{7,5,5,5,5,5,5,5,7},
			{7,7,7,7,7,7,7,7,7} };
	
	public PathSizeMap(RelicChunkDATA PRMP) {
		this.PRMP=PRMP;
		
		PRMP.dataPosition(0);
		
		imax=PRMP.dataGetInt();
		jmax=PRMP.dataGetInt();
		v1=PRMP.dataGetInt();
		v2=PRMP.dataGetInt();
		v3=PRMP.dataGetInt();
		
	}

	public Object clone() {
		PathSizeMap o = null;
		try {
			o = (PathSizeMap)super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o.PRMP=(RelicChunkDATA)o.PRMP.clone();
		
		return o;
	}
	
	public ByteArrayInputStream get_Data() {
		return new ByteArrayInputStream(PRMP.dataArray(),20,imax*jmax);
	}
	
	public byte getData(int i,int j) {
		return PRMP.dataArray()[20+i+j*imax];
	}

	public RelicChunkDATA exportPRMP() {
		return PRMP;
	}

	public void update(PassabilityMap passMap) {
		
		imax=passMap.imax;
		jmax=passMap.jmax;
		
		int newData[][]=new int[imax][jmax];
		
		for (int j=0; j<jmax; j++) {
			int mj=Math.min(j*2+1, jmax*2-j*2-1);
			mj=Math.min(7,mj);
			for (int i=0; i<imax; i++) {
				int mi=Math.min(i*2+1,imax*2-i*2-1);	
				newData[i][j]=Math.min(mi,mj);
				}
			}
		
		for (int j=0; j<jmax; j++)
			for (int i=0; i<imax; i++)
				if (passMap.getPassability(i, j)!=PassabilityMap.PASSABLE) {
					
					int i1min=Math.max(0, i-4);
					int i1max=Math.min(imax-1, i+4);
					int j1min=Math.max(0, j-4);
					int j1max=Math.min(jmax-1, j+4);
					
					for (int i1=i1min;i1<=i1max;i1++)
						for (int j1=j1min;j1<j1max;j1++)
							newData[i1][j1]=Math.min(newData[i1][j1],filter[i1-i+4][j1-j+4]);
					
				}
				
		
		ByteBuffer newDataBuffer=ByteBuffer.allocate(20+imax*jmax).order(ByteOrder.LITTLE_ENDIAN);
		newDataBuffer.putInt(imax);
		newDataBuffer.putInt(jmax);
		newDataBuffer.putInt(v1);
		newDataBuffer.putInt(v2);
		newDataBuffer.putInt(v2);
		for (int j=0; j<jmax; j++) {
			for (int i=0; i<imax; i++) {
				newDataBuffer.put((byte)newData[i][j]);
				}
			}
		
		PRMP.dataWrapArray(newDataBuffer.array());
		
	}
	
}
