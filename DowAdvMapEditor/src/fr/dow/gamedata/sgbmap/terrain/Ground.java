package fr.dow.gamedata.sgbmap.terrain;

import java.io.IOException;

import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.mapeditor.CopyData;

public class Ground implements Cloneable{

	public GroundData groundData;
	public HeightMap heightMap;
	public TerrainType terrainType;
	public Impass impass;
	
	public GroundTexture groundTexture;
	
	public TexTileFileLinks texTileFileLinks;
	public TexTiles texTiles;
	
	public Object clone() {
		
		Ground o = null;
		try {
			o= (Ground)super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o.groundData=(GroundData)o.groundData.clone();
		o.heightMap=(HeightMap)o.heightMap.clone();
		o.terrainType=(TerrainType)o.terrainType.clone();
		o.impass=(Impass)o.impass.clone();
		
		o.groundTexture=(GroundTexture)o.groundTexture.clone();
		o.texTileFileLinks=(TexTileFileLinks)o.texTileFileLinks.clone();
		
		o.texTiles=(TexTiles)o.texTiles.clone();
		
		return o;
	}
	
	public Ground(RelicChunkFOLD TFAC, DowFileVersion dowTgaFile) throws IOException {
		
		groundData=new GroundData(TFAC.subChunkDATA("VDAT"));
		
		groundTexture=new GroundTexture(dowTgaFile);
		
		RelicChunkFOLD CHAN=TFAC.subChunkFOLD("CHAN");
		
		heightMap=new HeightMap(CHAN.subChunkDATA("HMAP"), groundData);
		
		terrainType=new TerrainType(CHAN.subChunkDATA("TTYP"));
		
		impass=new Impass(CHAN.subChunkDATA("IMAP"));
		
		texTileFileLinks=new TexTileFileLinks(TFAC.subChunkDATA("DSHD"));
		
		texTiles=new TexTiles(TFAC.subChunkDATA("DETT"), groundData);
		
	}

	public RelicChunkFOLD exportTFAC() {
		
		RelicChunkFOLD CHAN=new RelicChunkFOLD("CHAN",2001,"");
		CHAN.get_subChunks().add(heightMap.exportHMAP(groundData));
		CHAN.get_subChunks().add(terrainType.exportTTYP());
		CHAN.get_subChunks().add(impass.exportIMAP());
		
		RelicChunkFOLD TFAC=new RelicChunkFOLD("TFAC",2000,"");
		TFAC.get_subChunks().add(groundData.exportVDAT());
		TFAC.get_subChunks().add(CHAN);
		TFAC.get_subChunks().add(texTiles.exportDETT());
		TFAC.get_subChunks().add(texTileFileLinks.exportDSHD());
		
		return TFAC;
	}

	public void resize(int newCellX, int newCellZ) {
		groundData.resize(newCellX,newCellZ);
		groundTexture.resize(newCellX*2,newCellZ*2);
		heightMap.resize(newCellX,newCellZ,groundData);
		terrainType.resize(newCellX,newCellZ);
		impass.resize(newCellX,newCellZ);
		texTiles.resize(newCellX/groundData.get_texTileSize(),newCellZ/groundData.get_texTileSize());
	}

	public void move(boolean stepTile, int moveX, int moveZ) {

		if (stepTile) {
			texTiles.move(moveX, moveZ);
			
			int tileSize=groundData.get_texTileSize();
			moveX*=tileSize;
			moveZ*=tileSize;
			
		}
		
		heightMap.move(moveX, moveZ);
		terrainType.move(moveX, moveZ);
		impass.move(moveX, moveZ);
		groundTexture.move(moveX*2, moveZ*2);
	}
	
	public void rotateClockwise() {
		groundData.rotateClockwise();
		groundTexture.rotateClockwise();
		heightMap.rotateClockwise(groundData);
		terrainType.rotateClockwise();
		impass.rotateClockwise();
		texTiles.rotateClockwise();
	}

	public float getY(float X, float Z) {
		
		float[][] heightGrid=heightMap.get_heightGrid();
		float cellSize=groundData.get_cellsize();
		float maxX=heightMap.get_hmMaxI()-1;
		float maxZ=heightMap.get_hmMaxJ()-1;
		
		float x=X/cellSize+groundData.get_centerXnbr()-1;
		float z=Z/cellSize+groundData.get_centerZnbr()-1;
		
		if (x<0f) x=0f;
		if (x>maxX) x=maxX;
		if (z<0f) z=0f;
		if (z>maxZ) z=maxZ;
		
		int x0=new Float(x).intValue();
		int z0=new Float(z).intValue();

		float dx=x-x0;
		float dz=z-z0;
		
		if (dz>dx) {
			if (dx==0) return heightGrid[x0][z0]+(heightGrid[x0][z0+1]-heightGrid[x0][z0])*dz;
			return heightGrid[x0][z0]+(heightGrid[x0+1][z0+1]-heightGrid[x0][z0+1])*dx+(heightGrid[x0][z0+1]-heightGrid[x0][z0])*dz;
		}
		else {
			if (dz==0) {
				if (dx==0) return heightGrid[x0][z0];
				else return heightGrid[x0][z0]+(heightGrid[x0+1][z0]-heightGrid[x0][z0])*dx;
			}
			return heightGrid[x0][z0]+(heightGrid[x0+1][z0]-heightGrid[x0][z0])*dx+(heightGrid[x0+1][z0+1]-heightGrid[x0+1][z0])*dz;
		}
			
	}

	public void pasteSubMap(CopyData pasteData, boolean pasteHeightMap, boolean pasteTextureMap, int offsetX, int offsetZ) {
		if (pasteHeightMap) heightMap.pasteSubMap(pasteData,offsetX,offsetZ);
		// TODO terrainType.pasteSubMap(pasteData,offsetX,offsetZ);
		// TODO impass.pasteSubMap(pasteData,offsetX,offsetZ);
		if (pasteTextureMap) groundTexture.pasteSubMap(pasteData,offsetX,offsetZ);
		
		texTiles.pasteSubMap(pasteData,offsetX,offsetZ,texTileFileLinks);
	}

	public void changeTileBP(Integer tileID, String newTileBP) {
		int newTilePtr=texTileFileLinks.findBlueprint(newTileBP);
		if (newTilePtr<0) newTilePtr=texTileFileLinks.addBlueprint(newTileBP);
		texTiles.setTilePtr(tileID,newTilePtr);		
	}

}
