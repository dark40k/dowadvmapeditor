package fr.dow.gamedata.sgbmap.terrain;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.mapeditor.CopyData;

public class TexTiles implements Cloneable {

	private RelicChunkDATA _DETT;
	private int _texTileNX;
	private int _texTileNZ;
	
	
	public TexTiles(RelicChunkDATA DETT, GroundData groundData) {
		_DETT=DETT;
		
		_texTileNX =groundData.get_cellnbrX()/groundData.get_texTileSize(); // nombre de tiles selon X
		_texTileNZ =groundData.get_cellnbrZ()/groundData.get_texTileSize(); // nombre de tiles selon Z

	}
	
	public Object clone() {
		TexTiles o = null;
		try {
			o = (TexTiles) super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		o._DETT=(RelicChunkDATA)o._DETT.clone();
		
		return o;
	}
	
	
	
	public int get_texTilePtr(int i, int j) {
		return _DETT.dataGetInt(4+4*i+4*j*_texTileNX);
	}

	public int get_texTilePtr(int tileID) {
		return _DETT.dataGetInt(4+4*tileID);
	}	
	
	public int count_texTilePtr(int Ptr){
		
		int count=0;
		
		for (int i=0; i<_texTileNX; i++) 
			for (int j=0; j<_texTileNZ; j++)
				if (get_texTilePtr(i,j)==Ptr) 
					count++;
					
		return count;
		
	}

	public int get_texTileNX() {
		return _texTileNX;
	}

	public int get_texTileNZ() {
		return _texTileNZ;
	}

	public static int getId(int i,int j, int NX,int NY) {
		return i+j*NX;
	}
	
	public int getId(int i,int j) {
		return i+j*_texTileNX;
	}
	
	public static int getXfromId(int id, int NX,int NY) {
		return id%NX;
	}
	
	public static int getZfromId(int id, int NX,int NY) {
		return id/NX;
	}

	public RelicChunkDATA exportDETT() {
		return _DETT;
	}
	
	public void resize(int newTileX, int newTileZ) {
		
		int offsetX=(newTileX-_texTileNX)/2;
		int offsetZ=(newTileZ-_texTileNZ)/2;
		
		ByteBuffer newDETTData=ByteBuffer.allocate(4+newTileX*newTileZ*4).order(ByteOrder.LITTLE_ENDIAN);
		
		newDETTData.putInt(newTileX*newTileZ);
		
		for (int j=0;j<newTileZ;j++)
			for (int i=0;i<newTileX;i++)
			{
				int i1=Math.min(Math.max(i-offsetX,0),_texTileNX-1);
				int j1=Math.min(Math.max(j-offsetZ,0),_texTileNZ-1);
				newDETTData.putInt(get_texTilePtr(i1,j1));;
			}
		
		_DETT.dataWrapArray(newDETTData.array());
		_texTileNX=newTileX;
		_texTileNZ=newTileZ;
		
	}

	public void move(int moveX, int moveZ) {
		
		ByteBuffer newDETTData=ByteBuffer.allocate(4+_texTileNX*_texTileNZ*4).order(ByteOrder.LITTLE_ENDIAN);
		
		newDETTData.putInt(_texTileNX*_texTileNZ);
		
		for (int j=0;j<_texTileNZ;j++)
			for (int i=0;i<_texTileNX;i++)
			{
				int i1=Math.min(Math.max(i-moveX,0),_texTileNX-1);
				int j1=Math.min(Math.max(j-moveZ,0),_texTileNZ-1);
				newDETTData.putInt(get_texTilePtr(i1,j1));;
			}
		
		_DETT.dataWrapArray(newDETTData.array());
		
	}
	
	public void rotateClockwise() {
		
		int newTileNX=_texTileNZ;
		int newTileNZ=_texTileNX;
			
		ByteBuffer newDETTData=ByteBuffer.allocate(4+newTileNX*newTileNZ*4).order(ByteOrder.LITTLE_ENDIAN);
		
		newDETTData.putInt(newTileNX*newTileNZ);
		
		for (int j=0;j<newTileNZ;j++)
			for (int i=0;i<newTileNX;i++)
				newDETTData.putInt(get_texTilePtr(newTileNZ-1-j,i));
		
		_DETT.dataWrapArray(newDETTData.array());
		_texTileNX=newTileNX;
		_texTileNZ=newTileNZ;
		
		
	}
	

	public void pasteSubMap(CopyData pasteData, int offsetX, int offsetZ, TexTileFileLinks texTileFileLinks) {
		
		int texOffsetX=offsetX/pasteData.sgbMap.terrain.ground.groundData.get_texTileSize();
		int texOffsetZ=offsetZ/pasteData.sgbMap.terrain.ground.groundData.get_texTileSize();
		int pasteNX=pasteData.sgbMap.terrain.ground.texTiles._texTileNX;
		
		RelicChunkDATA newDETT=(RelicChunkDATA)_DETT.clone();
		
		TexTiles pasteTextTiles=pasteData.sgbMap.terrain.ground.texTiles;
		TexTileFileLinks pasteTexTileFileLinks=pasteData.sgbMap.terrain.ground.texTileFileLinks;
		
		// provides new pointer in destination for old pointer in pastedata
		HashMap<Integer,Integer> newMapTexPtr=new HashMap<Integer,Integer>();
		
		// counter for new textures to be added
		int maxPtr=0;
		for (Integer texPtr : texTileFileLinks.keySet()) if (texPtr>maxPtr) maxPtr=texPtr;
		
		for (Integer textId : pasteData.selection.tiles) {
			
			int pasteX=textId%pasteNX;
			int pasteZ=textId/pasteNX;
			
			// check if paste tile is not outside map due to offset
			if ((pasteX+texOffsetX)<0) continue;
			if ((pasteX+texOffsetX)>=_texTileNX) continue;
			if ((pasteZ+texOffsetZ)<0) continue;
			if ((pasteZ+texOffsetZ)>=_texTileNZ) continue;
			
			// get paste tile pointer
			int pasteTexPtr=pasteTextTiles.get_texTilePtr(pasteX, pasteZ);
			
			int newPtr=-1;
			// check if destination paste pointer is already known
			if (newMapTexPtr.containsKey(pasteTexPtr)) 
				newPtr=newMapTexPtr.get(pasteTexPtr);
			else {

				// find destination paste pointer with same texture or create a new one
				String pasteTexName=pasteTexTileFileLinks.get(pasteTexPtr);
				
				if (texTileFileLinks.containsValue(pasteTexName)) {
					for (Integer texPtr : texTileFileLinks.keySet()) 
						if (pasteTexName.equals(texTileFileLinks.get(texPtr))) {
							newPtr=texPtr;
							break;
						}
				}
				else {
					maxPtr+=1;
					texTileFileLinks.put(maxPtr, pasteTexName);
					newPtr=maxPtr;
				}
				
				// store link for future use
				newMapTexPtr.put(pasteTexPtr,newPtr);
			}
			
			//update destination
			newDETT.dataPutInt(4+4*(pasteX+texOffsetX)+4*(pasteZ+texOffsetZ)*_texTileNX,newPtr);
			
		}
		
		_DETT=newDETT;
		
	}

	public void setTilePtr(Integer tileID, int newTilePtr) {
		_DETT.dataPutInt(4+4*tileID, newTilePtr);
	}
	
}
