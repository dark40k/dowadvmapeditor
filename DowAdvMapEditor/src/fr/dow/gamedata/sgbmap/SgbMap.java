package fr.dow.gamedata.sgbmap;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.gamedata.RelicChunkFile;
import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.gamedata.sgbmap.terrain.GroundData;
import fr.dow.gamedata.sgbmap.terrain.Terrain;
import fr.dow.gamedata.sgbmap.units.Egroups;
import fr.dow.gamedata.sgbmap.units.Entities;
import fr.dow.gamedata.sgbmap.units.Entity;
import fr.dow.gamedata.sgbmap.units.EntityBlueprints;
import fr.dow.gamedata.sgbmap.units.Markers;
import fr.dow.gamedata.sgbmap.units.PlayerID;
import fr.dow.gamedata.sgbmap.units.Sgroups;
import fr.dow.gamedata.sgbmap.units.Squad;
import fr.dow.gamedata.sgbmap.units.SquadBlueprints;
import fr.dow.gamedata.sgbmap.units.Squads;
import fr.dow.mapeditor.CopyData;

public class SgbMap implements Cloneable {

	public File mapFile;
	
	public MapData mapData;
	
	public EntityBlueprints entityBlueprints;
	public SquadBlueprints squadBlueprints;
	
	public Entities entities = null;
	public Squads squads = null;
	public Egroups egroups  = null;
	public Sgroups sgroups  = null;
	public Markers markers  = null;
	
	public Terrain terrain;

	
	public RelicChunkFOLD ZNMG;
	public RelicChunkFOLD NAM;
	
	// public RelicChunkFOLD MECF;
	
	private static final byte[] zeroCharByte={0};
	private static final String zeroChar=new String(zeroCharByte);
		
	public SgbMap(DowFileVersion dowSgbMap, DowFileVersion dowTgaMap ) throws IOException{
		
		//
		// Store map name if available
		//
		this.mapFile=dowSgbMap.getFile();
		
		//
		// load file
		//
		RelicChunkFile mapChunkFile=new RelicChunkFile(dowSgbMap);
		
		//
		// get first level
		//
		RelicChunkFOLD SCEN=(RelicChunkFOLD) mapChunkFile.get(0); // Chunk should be of type SCEN
		
		//
		// WMHD : load map data
		//
		
		mapData=new MapData(SCEN.subChunkDATA("WMHD"));
		
		//
		// WSTC : load Terrain data
		//
		terrain=new Terrain(SCEN.subChunkFOLD("WSTC"),dowTgaMap);
		
		//
		// SMEG : load entities and squads data
		//
		RelicChunkFOLD SMEG=SCEN.subChunkFOLD("SMEG");
		
		entityBlueprints=new EntityBlueprints(SMEG.subChunkDATA("EBPT"));
		entities=new Entities(SMEG.subChunkFOLD("ENTL"));
		squadBlueprints=new SquadBlueprints(SMEG.subChunkDATA("SBPT"));
		squads=new Squads(SMEG.subChunkFOLD("SQDL"));
		
		load_players(SMEG.subChunkFOLD("PLLS"));
		
		RelicChunkFOLD SCAR=SMEG.subChunkFOLD("SCAR");
		RelicChunkFOLD GMGR=SCAR.subChunkFOLD("GMGR");
		RelicChunkFOLD SGMG=SCAR.subChunkFOLD("SGMG");
		
		egroups=new Egroups(GMGR.subChunkFOLD("EGMP"));
		sgroups=new Sgroups(SGMG.subChunkFOLD("SGMP"));
		markers=new Markers(SCAR.subChunkFOLD("SMKR"));
		
		ZNMG=SCAR.subChunkFOLD("ZNMG");
		NAM=SCAR.subChunkFOLD( zeroChar + "NAM");
		
		//
		// MECF : Mission editor configuration
		//
	
		RelicChunkFOLD MECF=SCEN.subChunkFOLD("MECF");
		entities.importEINF(MECF.subChunkDATA("EINF"));
	}

	public void save(File fileOut) throws IOException {
		
		RelicChunkFOLD SCAR=new RelicChunkFOLD("SCAR",2,"");
		SCAR.get_subChunks().add(egroups.exportGMGR());
		SCAR.get_subChunks().add(sgroups.exportSGMG());
		SCAR.get_subChunks().add(markers.exportSMKR());
		SCAR.get_subChunks().add(ZNMG); // 
		SCAR.get_subChunks().add(NAM); // animations
		
		RelicChunkFOLD SMEG=new RelicChunkFOLD("SMEG",2002,"");
		SMEG.get_subChunks().add(entityBlueprints.exportEBPT());
		SMEG.get_subChunks().add(entities.exportENTL());
		SMEG.get_subChunks().add(squadBlueprints.exportSBPT());
		SMEG.get_subChunks().add(squads.exportSQDL());
		SMEG.get_subChunks().add(exportPLLS());
		SMEG.get_subChunks().add(SCAR);
		
		RelicChunkFOLD MECF = new RelicChunkFOLD("MECF",2000,"Mission Editor Configuration"+RelicChunkFOLD.zeroChar);
		MECF.get_subChunks().add(entities.exportEINF());
		
		RelicChunkFOLD SCEN=new RelicChunkFOLD("SCEN",2004,"");
		SCEN.get_subChunks().add(mapData.exportWMHD());
		SCEN.get_subChunks().add(terrain.exportWSTC());
		SCEN.get_subChunks().add(SMEG);
		SCEN.get_subChunks().add(MECF);
		
		RelicChunkFile mapChunkFile=new RelicChunkFile();
		mapChunkFile.add(SCEN);
		mapChunkFile.save(fileOut);
		
		String fileOutName=fileOut.getAbsolutePath(); // find way to avoid string to rename File extension
		terrain.ground.groundTexture.save(fileOutName.substring(0,fileOutName.lastIndexOf('.'))+".tga");
		
	}
	
	private RelicChunkFOLD exportPLLS() {
		
		RelicChunkFOLD PLLS=new RelicChunkFOLD("PLLS",2000,"");
		
		for (PlayerID playerID : PlayerID.playerValues()) {
			
			ByteBuffer INFOData=ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
			INFOData.putInt(playerID.getVal());
			INFOData.putInt(0);
			
			Vector<Integer> PlayerEntities = new Vector<Integer>();
			for (Entity entity : entities.values()) if (entity.getPlayerID()==playerID) PlayerEntities.add(entity.getId()); 
			ByteBuffer EGRPData=ByteBuffer.allocate(4+PlayerEntities.size()*4).order(ByteOrder.LITTLE_ENDIAN);
			EGRPData.putInt(PlayerEntities.size());
			for (Object id : PlayerEntities.toArray()) EGRPData.putInt( (Integer) id);
			
			Vector<Integer> PlayerSquads = new Vector<Integer>();
			for (Squad squad : squads.squadList.values()) if (squad.playerID==playerID) PlayerSquads.add(squad.id); 
			ByteBuffer SGRPData=ByteBuffer.allocate(4+PlayerSquads.size()*4).order(ByteOrder.LITTLE_ENDIAN);
			SGRPData.putInt(PlayerSquads.size());
			for (Object id : PlayerSquads.toArray()) SGRPData.putInt( (Integer) id);
			
			RelicChunkDATA INFO=new RelicChunkDATA("INFO",2000,"");
			INFO.dataWrapArray(INFOData.array());
			
			RelicChunkDATA EGRP=new RelicChunkDATA("EGRP",2000,"Entity Group"+RelicChunkFOLD.zeroChar);
			EGRP.dataWrapArray(EGRPData.array());
			
			RelicChunkDATA SGRP=new RelicChunkDATA("SGRP",2000,"Squad Group"+RelicChunkFOLD.zeroChar);
			SGRP.dataWrapArray(SGRPData.array());
			
			RelicChunkFOLD PLYR=new RelicChunkFOLD("PLYR",2001,"");
			PLYR.get_subChunks().add(INFO);
			PLYR.get_subChunks().add(EGRP);
			PLYR.get_subChunks().add(SGRP);
			
			PLLS.get_subChunks().add(PLYR);
		}
		
		return PLLS;
	}

	private void load_players(RelicChunkFOLD PLLS) {
		
		// Iterates over PLYR subchunks 
		Iterator<RelicChunk> it=PLLS.subChunkList("PLYR").iterator();
		
		while (it.hasNext()) {
			
			RelicChunkFOLD chunkPLYR=(RelicChunkFOLD) it.next();
			
			PlayerID playerID=PlayerID.getPlayerID(chunkPLYR.subChunkDATA("INFO").dataGetInt(0));
			
			RelicChunkDATA chunkEGRP=chunkPLYR.subChunkDATA("EGRP");
			int nbr=chunkEGRP.dataGetInt(0);
			for (int i=0;i<nbr;i++) entities.get(chunkEGRP.dataGetInt(i*4+4)).setPlayerID(playerID);
			
			RelicChunkDATA chunkSGRP=chunkPLYR.subChunkDATA("SGRP");
			nbr=chunkSGRP.dataGetInt(0);
			for (int i=0;i<nbr;i++) squads.squadList.get(chunkSGRP.dataGetInt(i*4+4)).playerID=playerID;
			
		}
		
	}

	public void resizeMap(int newCellX, int newCellZ) {		
		mapData.resize(Math.max(newCellX, newCellZ));
		terrain.resize(newCellX,newCellZ);
		// TODO Resize: remove entities, markers and squads outside map
		
	}

	public void moveMap(boolean stepTile, int moveX, int moveZ) {
		terrain.move(stepTile,moveX,moveZ);
		
		if (stepTile) {
			int tileSize=terrain.ground.groundData.get_texTileSize();
			moveX*=tileSize;
			moveZ*=tileSize;
		}
		
		float cellSize=terrain.ground.groundData.get_cellsize();
		entities.move(moveX*cellSize,moveZ*cellSize);
		markers.move(moveX*cellSize,moveZ*cellSize);
	}
	
	public void rotateClockwise() {
		terrain.rotateClockwise();
		entities.rotateClockwise();
		markers.rotateClockwise();
	}
	
	public Object clone() {
		
		SgbMap newMap = null;
		try {
			newMap= (SgbMap)super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		newMap.mapData=(MapData)mapData.clone();
		
		newMap.entityBlueprints=(EntityBlueprints)entityBlueprints.clone();
		newMap.squadBlueprints=(SquadBlueprints)squadBlueprints.clone();
		
		newMap.entities=(Entities)entities.clone();
		newMap.squads=(Squads)squads.clone();
		newMap.egroups=(Egroups)egroups.clone();
		newMap.sgroups=(Sgroups)sgroups.clone();
		newMap.markers=(Markers)markers.clone();
		
		newMap.terrain=(Terrain)terrain.clone();
		
		newMap.ZNMG=(RelicChunkFOLD)ZNMG.clone();
		newMap.NAM=(RelicChunkFOLD)NAM.clone();
		
		//newMap.MECF=(RelicChunkFOLD)MECF.clone();
		
		return newMap;
	}

	public void pasteSubMap(CopyData pasteData, boolean heightMap, boolean textureMap, int offsetX, int offsetZ) {
		
		terrain.pasteSubMap(pasteData,heightMap,textureMap,offsetX,offsetZ);
		
		GroundData gndDataSrc=pasteData.sgbMap.terrain.ground.groundData;
		GroundData gndDataDst=terrain.ground.groundData;
		float moveX=(float)(offsetX - gndDataDst.get_centerXnbr() + gndDataSrc.get_centerXnbr()) * gndDataDst.get_cellsize();
		float moveZ=(float)(offsetZ - gndDataDst.get_centerZnbr() + gndDataSrc.get_centerZnbr()) * gndDataDst.get_cellsize();
		
		HashMap<Integer, Integer> entityIDLinkTable=entities.pasteSubMap(pasteData,moveX,moveZ,entityBlueprints);
		
		squads.pasteSubMap(pasteData,squadBlueprints,entityIDLinkTable);
		
		markers.pasteSubMap(pasteData,moveX,moveZ);
		
	}
	
	public static File setFileExt(File file, String newExtension) {
		String filename=file.getName();
		int pos=filename.lastIndexOf('.');
		if (pos<0) return new File(file.getAbsolutePath()+"."+newExtension);
		File newFile=new File(file.getParent()+File.separatorChar+filename.substring(0,filename.lastIndexOf('.'))+"."+newExtension);
		return newFile;
	}
	
	public void deleteEntities(boolean includeSquads) {
		
		if (includeSquads) {
			entities.clear();
			squads.squadList.clear();
			squads.revSquadList.clear();
		} else {
			Object[] listEntityID = entities.keySet().toArray();
			for (Object entityID : listEntityID) // WARNING : may be wrong to remove 
				if (! squads.revSquadList.containsKey(entityID))
					entities.remove(entityID);
		}
		
	}

	public void entityUpdate(Integer id, String entityBP, PlayerID playerID, Float x, Float y, Float z, float headingRad, float pitchRad,
			float rollRad, Double sX, Double sY, Double sZ) {
		entities.update(id,entityBlueprints.addNewBlueprint(entityBP), playerID, x, y, z, headingRad, pitchRad, rollRad, sX, sY, sZ);
	}
	
}
