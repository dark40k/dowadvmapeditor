package fr.dow.gamedata.texture;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.gl2.GLUgl2;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.gamedata.RelicChunkFile;
import fr.dow.gamedata.filesystem.DowMod;


public class RshTexture {

	
	public static int loadID(GL2 gl, String texFilename, DowMod modData) {
		
		byte[] rshFile=null;
		try {
			rshFile = modData.getFileData(texFilename);
		} catch (IOException e) {
			System.out.println("Warning : error for " + texFilename);
			e.printStackTrace();
		}
		
		if (rshFile==null) return 0;
		
		return loadID(gl,new ByteArrayInputStream(rshFile));
	}
	
	public static int loadID(GL2 gl, InputStream rshStream) {
		
		try {
			RelicChunkFOLD SHRF=(RelicChunkFOLD) new RelicChunkFile(rshStream).get(0);
			RelicChunkFOLD TXTR=SHRF.subChunkFOLD("TXTR");
			return loadID(gl,TXTR);
		} catch (IOException e) {
			System.out.println("Error : could not extract texture data from InputStream");
			e.printStackTrace();
		}
		
		return 0;
		
	}

	public static int loadID(GL2 gl, RelicChunkFOLD TXTR) {
		
		//
		// get new texture ID and bind it
		//
		int[] tableTextureID = new int[1]; 
		gl.glGenTextures(1, tableTextureID, 0);
		int textureID=tableTextureID[0];
		
		gl.glBindTexture(GL2.GL_TEXTURE_2D, textureID);
		
		//
		// extract data
		//
		RelicChunkFOLD IMAG=TXTR.subChunkFOLD("IMAG");
		RelicChunkDATA ATTR=IMAG.subChunkDATA("ATTR");
		RelicChunkDATA DATA=IMAG.subChunkDATA("DATA");
			
		int type=ATTR.dataGetInt(0);
		int width=ATTR.dataGetInt(4);
		int height=ATTR.dataGetInt(8);
		int mipMapCount=ATTR.dataGetInt(12);
		
		int blockSize=0;
		int internal_format=0;
		switch (type) {
			case 11 :
				blockSize = 16;
				internal_format = GL2.GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
				break;
			case 8 :
				blockSize = 8;
				internal_format = GL2.GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
				break;
			case 0 :
				GLUgl2 glu=new GLUgl2();
				glu.gluBuild2DMipmaps( 
						GL2.GL_TEXTURE_2D,
						GL2.GL_RGBA, 
						width, height, 
						GL2.GL_RGBA, 
						GL2.GL_UNSIGNED_BYTE, 
						ByteBuffer.wrap(DATA.dataArray())
						);
				return textureID;
			default : 
				System.err.println("Cancelling texture - Unknown texture type " + type);
				return textureID;
		}
		
		int offset = 0;
		int size = 0;
		int width1=width;
		int height1=height;
		for( int mip=0; (mip<mipMapCount) & ( (width1>0) | (height1>0) ); mip++ )
		{
		
			if (width1 == 0) width1 = 1;
			if (height1 == 0) height1 = 1;
			
			size = ((width1+3)/4)*((height1+3)/4) * blockSize;
			
			gl.glCompressedTexImage2D( GL2.GL_TEXTURE_2D, mip, internal_format, width1, height1, 0, size, ByteBuffer.wrap(DATA.dataArray(), offset, size));
		
			offset += size;
			width1 >>= 1;
			height1 >>= 1;
		}
		
		//
		// Add transparency for black color if it is DXT1 (OpenGL does not support DXT1 implicit transparency)
		//
		if (type==8) {
			
			int width2=width;
			int height2=height;
			
			//
			// force texture loading (see glCompressedTexImage2D documentation)
			//
		    @SuppressWarnings("unused")
			int tmpInternalFormat = TextureTools.getCurrentTextureFormat(gl, 0);
		    @SuppressWarnings("unused")
			int tmpWidth  = TextureTools.getCurrentTextureWidth(gl, 0);
		    
			for( int mip=0; (mip<mipMapCount) & ( (width2>0) | (height2>0) ); mip++ ) {
		    
				if (width2 == 0) width2 = 1;
				if (height2 == 0) height2 = 1;
				
			    int n=width2*height2*4;
			    
			    ByteBuffer res = ByteBuffer.allocate(n);
			    gl.glGetTexImage(GL2.GL_TEXTURE_2D, mip, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, res);
				
			    byte[] img=new byte[n];
			    
			   	res.get(img);
			    
			    for (int i=0;i<n;i+=4)
			    	if (img[i]==0) 
			        	if (img[i+1]==0) 
			            	if (img[i+2]==0)
			            			img[i+3]=0;
			    
			    gl.glTexImage2D(GL2.GL_TEXTURE_2D, mip, GL2.GL_RGBA, width2, height2, 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, ByteBuffer.wrap(img));
			    
			    width2>>=1;
			    height2>>=1;
		    }
		}
		
		return textureID;
			
	}
	
}
