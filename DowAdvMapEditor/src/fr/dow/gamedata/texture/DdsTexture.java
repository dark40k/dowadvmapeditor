package fr.dow.gamedata.texture;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.gl2.GLUgl2;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import fr.dow.gamedata.filesystem.DowMod;

public class DdsTexture {
	
	public static Texture load(String texFilename, DowMod modData) {
		
		try {			
			
			byte[] rshFile=modData.getFileData(texFilename);
			
			if (rshFile==null) return null;
			
			Texture newTex=TextureIO.newTexture(new ByteArrayInputStream(rshFile), true,TextureIO.DDS);

			// check that there are mipmaps => if mipmap=0 then regenerate mipmaps
			//
			// Note: Was previously "newTex.setTexParameteri( GL2.GL_GENERATE_MIPMAP, GL2.GL_TRUE);"
			// but this does not work with NVIDIA driver (worked with ATI).
			//
			if (rshFile[28]==0) if (rshFile[29]==0) if (rshFile[30]==0) if (rshFile[31]==0) {
				
				GLUgl2 glu=new GLUgl2();
				GL2 gl = GLUgl2.getCurrentGL().getGL2();
				
				int width=newTex.getWidth();
				int height=newTex.getHeight();
				
			    int n=width*height*4;
			    ByteBuffer res = ByteBuffer.allocate(n);
			    gl.glGetTexImage(GL2.GL_TEXTURE_2D, 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, res);
				glu.gluBuild2DMipmaps( GL2.GL_TEXTURE_2D, GL2.GL_RGBA, width, height, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, res);

			}
			
			return newTex;
			
		} catch (IOException e) {
			System.out.println("Warning : error for " + texFilename);
			e.printStackTrace();
		}
		
		return null;
	}

	public static int loadID(String texFilename, DowMod modData) {
		Texture newTex=load(texFilename, modData);
		if (newTex!=null) return newTex.getTextureObject();
		return 0;
	}
	
}
