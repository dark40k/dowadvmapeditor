package fr.dow.gamedata.whmmodel;

import fr.dow.gamedata.RelicChunkDATA;

public class Part {

//	DWORD	MaterialNameSize
//	BYTE	MaterialName[MaterialNameSize]
//	DWORD	PartVertexCount
//	WORD	VertexIndex[PartVertexCount]
//	DWORD	Unknown[2]
	
	public String textureName;
	public int[] vertexIndexList; 
	public int unknown01;
	public int unknown02;
	
	public Part(RelicChunkDATA chunk) {
		
		textureName=chunk.dataGetString();
		
		int vertexCount=chunk.dataGetInt();
		vertexIndexList=new int[vertexCount];
		
		for (int i=0; i<vertexCount; i++) vertexIndexList[i]=chunk.dataGetShort();
		
		unknown01=chunk.dataGetInt();
		unknown02=chunk.dataGetInt();
		
	}
	
}
