package fr.dow.gamedata.whmmodel.anim;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Vector;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;

@SuppressWarnings("unused")
public class Anim {

/*	#after ANIM/DATA Chunk headers read the following

	1-longint - number of frames in animation 91
	skip 4 bytes (unknown by Santos)
	
	1-longint - number of bones
	---------For number of bones do this loop
		1-longint - Bone Name Length
		Bytes of Bone Name Length for Bone_name
	   
		1-longint - number of position keys
		---------For number of keys do this loop
			1-float Frame Number
			1-float x_pos
			1 float y_pos
			1-float z_pos  for the number of position keys
			1-longint for the number of rotation keys
	
		1-longint - number of rotation keys
		---------For number of keys do this loop
			1-float Frame Number
			1-float x_rot
			1 float y_rot
			1-float z_rot
			1-float w-rot  for the number of position keys

		1-byte stale property 01 is not stale. 0 is stale

	# so if there is no animation, then bone data will be 0000 0000 1, 9 bytes.

	#-----------------------then the mesh info in the animation


	1-longint - number of meshes
	---------For number of meshes do this loop
		1-longint - mesh Name Length
			Bytes of Name Length for mesh_name
			
		skip 12 bytes (unknown)
		
		1-longint Number of visibility keys
		
		4 bytes unknown
		
		1-float - force invisible property

		---------For number of visibility keys do this loop
			1-float - number of frames
			1-float  - visbility

			8 bytes unknown
			
				

	#for number of visibility keys do this-----------I think I'll skip this for now, good to know its there though

	#       1-float - number of frames
	#       1-float  - visbility
	#now do texture keys
	#  skip 8 bytes - unknown
	#  1-longint - texture animation type
	#  1-longint number of texture keys
	#-------for number of texture keys do
	#           1-float, frame number
	#           1 float, texture offset
	#   if Data/Data is version 2 then need to read camera data
	*/
	
	public Vector<BoneData> boneDatas=new Vector<BoneData>();
	public Vector<MeshData> meshDatas=new Vector<MeshData>();

	public Anim(RelicChunkFOLD ANIM) {
	
//	# 1-longint - number of frames in animation 91
//	# skip 4 bytes (unknown by Santos)
//	# 1-longint - number of bones
//	# Then start loop to read the following bone data
//	#    1-longint - Bone Name Length
//	#    Bytes of Bone Name Length for Bone_name
//	# 1-longint - number of position keys
		
		RelicChunkDATA DATA=ANIM.subChunkDATA("DATA");
		DATA.dataPosition(0);
		
		int framesNbr=DATA.dataGetInt();
		
		float unknown01=DATA.dataGetFloat();
		
		int bonesNbr=DATA.dataGetInt();
		for (int i=0;i<bonesNbr;i++) boneDatas.add(new BoneData(DATA));

		int meshNbr=DATA.dataGetInt();
		for (int i=0;i<meshNbr;i++) meshDatas.add(new MeshData(DATA));

	}
	
	public class MeshData {
		
		String name;
		Vector<VisKey> visibilityKeys=new Vector<VisKey>();
		public MeshData(RelicChunkDATA DATA) {
			name=DATA.dataGetString();
			
			int unknown01=DATA.dataGetInt();
			int unknown02=DATA.dataGetInt();
			int unknown03=DATA.dataGetInt();
			
			int visKeyNbr=DATA.dataGetInt();
			for (int i=0;i<visKeyNbr;i++) visibilityKeys.add(new VisKey(DATA));
		}
	}
	
	public class VisKey {
		int unknown;
		float visibility;
		public VisKey(RelicChunkDATA DATA) {
			unknown=DATA.dataGetInt();
			visibility=DATA.dataGetFloat();
		}
	}

	public HashSet<String> getInvisibleMeshes() {
		
		HashSet<String> newInvisibleMeshes=new HashSet<String>();
		
		for (MeshData meshData : meshDatas ) 
			if (meshData.visibilityKeys.get(0).visibility==0f)
				newInvisibleMeshes.add(meshData.name);
		
		return newInvisibleMeshes;
		
	}
	
	
}
