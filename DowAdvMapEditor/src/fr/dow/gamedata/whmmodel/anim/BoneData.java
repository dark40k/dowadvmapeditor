package fr.dow.gamedata.whmmodel.anim;

import fr.dow.gamedata.RelicChunkDATA;

public class BoneData {

	private String _name;
	private int _posKeysNbr;
	private int _rotKeysNbr;
	
	private byte[] data;
	private int positionKeysOffset;
	private int rotationKeysOffset;
	
	//Vector<PositionKey> positionKeys=new Vector<PositionKey>();
	//Vector<RotationKey> rotationKeys=new Vector<RotationKey>();
	boolean stale;
	
	public BoneData(RelicChunkDATA DATA) {
		_name=DATA.dataGetString();
		
		data=DATA.dataArray();
		
		_posKeysNbr=DATA.dataGetInt();
		positionKeysOffset=DATA.dataPosition();
		DATA.dataSkipBytes(_posKeysNbr*16);
		//for (int i=0;i<positionKeysNbr;i++) positionKeys.add(new PositionKey(DATA));
		
		_rotKeysNbr=DATA.dataGetInt();
		rotationKeysOffset=DATA.dataPosition();
		DATA.dataSkipBytes(_rotKeysNbr*20);
		//for (int i=0;i<rotationKeysNbr;i++) rotationKeys.add(new RotationKey(DATA));
		
		stale=(DATA.dataGetByte() == 0);
		
	}
	
	public class PositionKey {
		
		public float frame;
		public float xPos;
		public float yPos;
		public float zPos;
		
		public PositionKey(int index) {
			if (index>=_posKeysNbr) throw new IndexOutOfBoundsException();
			int startIndex=positionKeysOffset+index*16;
			frame=get_dataFloat(startIndex);startIndex+=4;
			xPos=get_dataFloat(startIndex);startIndex+=4;
			yPos=get_dataFloat(startIndex);startIndex+=4;
			zPos=get_dataFloat(startIndex);startIndex+=4;
		}
		
	}
	
	public class RotationKey {
		
		public float frame;
		public float xRot;
		public float yRot;
		public float zRot;
		public float wRot;
		
		public RotationKey(int index) {
			if (index>=_rotKeysNbr) throw new IndexOutOfBoundsException();
			int startIndex=rotationKeysOffset+index*20;
			frame=get_dataFloat(startIndex);startIndex+=4;
			xRot=get_dataFloat(startIndex);startIndex+=4;
			yRot=get_dataFloat(startIndex);startIndex+=4;
			zRot=get_dataFloat(startIndex);startIndex+=4;
			wRot=get_dataFloat(startIndex);startIndex+=4;
		}
		
	}
	
	private Float get_dataFloat(Integer Pos){		
		return Float.intBitsToFloat( 
	            (0xff & data[Pos+0]) << 0  |
	            (0xff & data[Pos+1]) << 8  |
	            (0xff & data[Pos+2]) << 16 |
	            (0xff & data[Pos+3]) << 24
	            );		
	}

	public String get_name() {
		return _name;
	}

	public int get_posKeysNbr() {
		return _posKeysNbr;
	}

	public int get_rotKeysNbr() {
		return _rotKeysNbr;
	}
	
	
}
