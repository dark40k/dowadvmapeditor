package fr.dow.gamedata.whmmodel;

public class Skel {

//	DWORD	BoneNameSize
//	BYTE	BoneName[BoneNameSize]
//	DWORD	Level
//	Vector3 Position
//	Vector4 Rotation
	
	public String boneName="";
	public Integer level=0;
	public float[] position={0,0,0};
	public float[] rotation={0,0,0,0};
	
}
