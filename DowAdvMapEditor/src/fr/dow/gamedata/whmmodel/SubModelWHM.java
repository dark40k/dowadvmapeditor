package fr.dow.gamedata.whmmodel;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;

public class SubModelWHM {

	public String partName;
	
	public float[][] vertexPos;
	public float[][] vertexNormal;
	public float[][] texCoord;
	public Part[] parts;
	public Bvol bvol;
	
	
	public SubModelWHM(RelicChunkFOLD chunkMSLC) {
		
		partName=chunkMSLC.get_name();
		
		RelicChunkDATA DATA1=chunkMSLC.subChunkDATA("DATA");
		
//		BYTE	Unknown[13]
		DATA1.dataPosition(13); 
		
//		DWORD	BoneCount
//		repeat BoneCount X
//			DWORD	BoneNameSize
//			BYTE	BoneName[BoneNameSize]
//			DWORD	Unknown // This part has something to do with vertex weigths, I just haven't coded it yet
//		end repeat
		int boneCount=DATA1.dataGetInt(); 
		String boneName[]=new String[boneCount];
		int boneID[]=new int[boneCount];
		for (int i=0;i<boneCount;i++) {
			boneName[i]=DATA1.dataGetString(); 
			boneID[i]=DATA1.dataGetInt(); 
		}
		
//		DWORD	VertexCount
//		DWORD	Unknown		
//		Vector3 Vertices[VertexCount]
		int vertexCount=DATA1.dataGetInt(); 
		DATA1.dataSkipBytes(4);  
		vertexPos=new float[vertexCount][3];
		for (int i=0;i<vertexCount;i++) 
			for (int j=0;j<3;j++)
				vertexPos[i][j]=DATA1.dataGetFloat(); 
		
		
//		repeat	VertexCount X  // Only if BoneCount != 0
//			Vector3	VertexWeights[3] -> only 3 not 4, 4th is 1-sum of others
//			BYTE	BoneID[4]
//		end repeat
		float[][] vertexBoneWeight={{}};
		byte[][] vertexBoneID={{}};
		if (boneCount!=0) {
			vertexBoneWeight=new float[vertexCount][3];
			vertexBoneID=new byte[vertexCount][4];
			for (int i=0;i<vertexCount;i++) {
				for (int j=0;j<3;j++) vertexBoneWeight[i][j]=DATA1.dataGetFloat(); 
				vertexBoneID[i]=DATA1.dataSubArray(4); 
			}
		}
		
//		Vector3	Normal[VertexCount]
		vertexNormal=new float[vertexCount][3];
		for (int i=0;i<vertexCount;i++) 
			for (int j=0;j<3;j++)	
				vertexNormal[i][j]=DATA1.dataGetFloat();
		
		
//		Vector2 TexCoord[VertexCount]
		texCoord=new float[vertexCount][2];
		for (int i=0;i<vertexCount;i++) 
			for (int j=0;j<2;j++)	
				texCoord[i][j]=DATA1.dataGetFloat(); 
		
//		DWORD	Unknown
		DATA1.dataSkipBytes(4);
		
//		DWORD	MaterialParts
//		repeat MaterialParts X
//			DWORD	MaterialNameSize
//			BYTE	MaterialName[MaterialNameSize]
//			DWORD	PartVertexCount
//			WORD	VertexIndex[PartVertexCount]
//			DWORD	Unknown[2]
//		end repeat
		int  partNbr=DATA1.dataGetInt(); 
		parts=new Part[partNbr];
		for (int i=0;i<partNbr;i++) parts[i]=new Part(DATA1);
					
		RelicChunkDATA BVOL1=chunkMSLC.subChunkDATA("BVOL");
		bvol=new Bvol(BVOL1);
		
	}
	
	
}
