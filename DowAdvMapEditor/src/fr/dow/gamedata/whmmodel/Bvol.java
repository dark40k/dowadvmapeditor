package fr.dow.gamedata.whmmodel;

import fr.dow.gamedata.RelicChunkDATA;

public class Bvol {
//	BYTE	Unknown // Maybe number of bboxes??
//	Vector3	BBoxCenter
//	float	Length // Distance from bbox faces to the center
//	Matrix3	ScaleMatrix
	
	public int unknown=0;
	public float centerX;
	public float centerY;
	public float centerZ;
	public float scaleX;
	public float scaleY;
	public float scaleZ;
	public float[] rotation={0,0,0, 0,0,0, 0,0,0}; // 3x3 matrix, order is m11, m12, m13, m21, ... TBC
	
	public Bvol(RelicChunkDATA BVOL) {
		BVOL.dataPosition(0);
		unknown=BVOL.dataGetByte();
		centerX=BVOL.dataGetFloat();
		centerY=BVOL.dataGetFloat();
		centerZ=BVOL.dataGetFloat();
		scaleX=BVOL.dataGetFloat();
		scaleY=BVOL.dataGetFloat();
		scaleZ=BVOL.dataGetFloat();
		rotation[0]=BVOL.dataGetFloat();
		rotation[1]=BVOL.dataGetFloat();
		rotation[2]=BVOL.dataGetFloat();
		rotation[3]=BVOL.dataGetFloat();
		rotation[4]=BVOL.dataGetFloat();
		rotation[5]=BVOL.dataGetFloat();
		rotation[6]=BVOL.dataGetFloat();
		rotation[7]=BVOL.dataGetFloat();
		rotation[8]=BVOL.dataGetFloat();
	}
	
}
