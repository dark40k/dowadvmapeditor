package fr.dow.gamedata.whmmodel;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Vector;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.gamedata.RelicChunkFile;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.whemodel.LinkedAnim;
import fr.dow.gamedata.whemodel.WheModel;
import fr.dow.gamedata.whmmodel.anim.Anim;

public class WhmModel {


	public LinkedHashMap<String, Skel> _skeleton=new LinkedHashMap<String, Skel>();
	
	public Bvol _bvol;
	public LinkedHashMap<String, SubModelWHM> _subModels=new LinkedHashMap<String, SubModelWHM>();
	public LinkedHashMap<String, Anim> _anims=new LinkedHashMap<String, Anim>();
	
	public Vector<RelicChunk> linkedShaderChunks;
	public Vector<RelicChunk> textureIncludedChunks;
	public Vector<RelicChunk> includedShaderChunks;
	
	public WhmModel(String filename,DowMod modData) throws IOException{
		
		//load all chunks
		String whmFilename=filename.toLowerCase().replace('\\','/');
		byte[] whmData=modData.getFileData(whmFilename);
		RelicChunkFile whmChunkFile=new RelicChunkFile(new ByteArrayInputStream(whmData));

		/*
		 * Chunk not used, contains only title information
		 * 
		//
		// read FBIF chunk - Not used
		//
		RelicChunkDATA FBIF=(RelicChunkDATA) whmChunkFile.get(0); // Chunk should be of FBIF type
		String	CreatorName=FBIF.dataGetString(); 
		Integer	Version=FBIF.dataGetInt(); 
		String	SomeName=FBIF.dataGetString(); 
		String	DateName=FBIF.dataGetString(); 
		*/
		
		//
		// read second chunk
		//
		RelicChunkFOLD RSGM=(RelicChunkFOLD) whmChunkFile.get(1); // Chunk should be FOLDER of RSGM type
		
		// texture lists 
		linkedShaderChunks=RSGM.subChunkList("SSHR");
		textureIncludedChunks=RSGM.subChunkList("TXTR");
		includedShaderChunks=RSGM.subChunkList("SHDR");
		
		//
		// Skeleton data
		//
		RelicChunkDATA SKEL=RSGM.subChunkDATA("SKEL");
		if (SKEL!=null) {
			
			_skeleton=new LinkedHashMap<String, Skel>();
			
			SKEL.dataPosition(0);
			int skelnbr=SKEL.dataGetInt(); 
			
			for (int i=0;i<skelnbr;i++) {
				Skel tmpskel=new Skel();
				tmpskel.boneName=SKEL.dataGetString();
				tmpskel.level=SKEL.dataGetInt(); 
				for (int j=0;j<3;j++) tmpskel.position[j]=SKEL.dataGetFloat(); 
				for (int j=0;j<4;j++) tmpskel.rotation[j]=SKEL.dataGetFloat(); 
				_skeleton.put(tmpskel.boneName.trim().toLowerCase(), tmpskel);
			}
			
		}
		
		RelicChunkFOLD MSGR=RSGM.subChunkFOLD("MSGR");

		// load submodels that are included in current whm file
		Vector<RelicChunk> MSLCs=MSGR.subChunkList("MSLC");
		for (int i=0; i<MSLCs.size(); i++) {
			SubModelWHM subModel=new SubModelWHM((RelicChunkFOLD) MSLCs.get(i));
			_subModels.put(subModel.partName.trim().toLowerCase(), subModel);
		}

		// check if more submodels are needed and get them using path in DATA table
		RelicChunkDATA DATA=MSGR.subChunkDATA("DATA");
		int subModelLinksNbr=DATA.dataGetInt();
		WhmModel tmpWhmModel=null;
		String tmpWhmModelPath="";
		for (int i=0;i<subModelLinksNbr;i++) {
			
			// read DATA block for line
			String subModelName=DATA.dataGetString().toLowerCase();
			String subModelPath=DATA.dataGetString();
			DATA.dataSkipBytes(4); // int subModelValue=DATA.dataGetInt(); // not used. 
			
			// if submodel does not exist and path is available
			if (!_subModels.containsKey(subModelName) & subModelPath.length()>0) {
				if (!tmpWhmModelPath.equalsIgnoreCase(subModelPath)) {
					tmpWhmModelPath=subModelPath;
					tmpWhmModel=new WhmModel(tmpWhmModelPath+".whm",modData);
					addUnknownTextures(tmpWhmModel);
				}
				_subModels.put(subModelName, tmpWhmModel._subModels.get(subModelName));
			}
		}
		
		RelicChunkDATA BVOL=MSGR.subChunkDATA("BVOL");
		_bvol=new Bvol(BVOL);
		
		Vector<RelicChunk> ANIMs=RSGM.subChunkList("ANIM");
		for (RelicChunk ANIM : ANIMs ) {
			_anims.put(ANIM.get_name().trim().toLowerCase(), new Anim((RelicChunkFOLD) ANIM));
		}
		
	}
	
	public static Anim findAnim(String animName, String animSource, DowMod modData) {
		
		String wheFilename=(animSource+".whe").toLowerCase().replace('\\','/');
		try {
			byte[] wheData=modData.getFileData(wheFilename);
			WheModel wheModel=new WheModel(new ByteArrayInputStream(wheData));
			LinkedAnim linkedAnim=wheModel.linkedAnims.get(animName.toLowerCase());
			if (linkedAnim!=null) return findAnim(linkedAnim.animName, linkedAnim.sourceModel, modData);
		}
		catch (IOException e) {
			System.err.println("WhmModel.findAnim=, IOException for " + wheFilename);
		}
		
		try {
			WhmModel whmModel= new WhmModel(animSource+".whm",modData);
			return whmModel._anims.get(animName.toLowerCase());
		}
		catch (IOException e) {
			System.err.println("WhmModel.findAnim=, IOException for " + animSource + ".whm" );
		}
		
		return null;
		
	}

	private void addUnknownTextures(WhmModel whmSource) {
		
		HashSet<String> listNames;
		
		listNames=new HashSet<String>();
		for (RelicChunk SSHR : linkedShaderChunks) listNames.add(SSHR.get_name().trim().toLowerCase());
		for (RelicChunk SSHR : whmSource.linkedShaderChunks) 
			if (!listNames.contains(SSHR.get_name().trim().toLowerCase()))
				linkedShaderChunks.add(SSHR);

		listNames=new HashSet<String>();
		for (RelicChunk TXTR : textureIncludedChunks) listNames.add(TXTR.get_name().trim().toLowerCase());
		for (RelicChunk TXTR : whmSource.textureIncludedChunks) 
			if (!listNames.contains(TXTR.get_name().trim().toLowerCase()))
				textureIncludedChunks.add(TXTR);

		listNames=new HashSet<String>();
		for (RelicChunk SHDR : includedShaderChunks) listNames.add(SHDR.get_name().trim().toLowerCase());
		for (RelicChunk SHDR : whmSource.includedShaderChunks) 
			if (!listNames.contains(SHDR.get_name().trim().toLowerCase()))
				includedShaderChunks.add(SHDR);
		
	}
	
/*
 * For dev purpose - not used anymore
 * 
	public static void main(String[] args) throws IOException {
		
		System.out.println("Loading file data ");

		FileInputStream streamIn=new FileInputStream("E:/Francois/Dawn of war/Modding/Extraction_DXP2_whm-high/art/ebps/races/imperial_guard/troops/imperial_captain.whm");
		
		WhmModel test=new WhmModel(streamIn);
		
		System.out.println("Loading done ");
		
	}
*/		
	
}
