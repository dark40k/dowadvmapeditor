package fr.dow.gamedata.whemodel;

import fr.dow.gamedata.RelicChunkDATA;

public class SelUI {

	public final static int SELCIRCLE=0;
	public final static int SELRECTANGLE=1;
	
	public int selType;
	
	public float selScaleX;
	public float selScaleZ;
	public float selCenterX;
	public float selCenterZ;
	
	public float centerX;
	public float centerY;
	public float centerZ;
	public float scaleX;
	public float scaleY;
	public float scaleZ;
	public float[] rotation={0,0,0, 0,0,0, 0,0,0}; // 3x3 matrix, order is m11, m12, m13, m21, ... TBC
	
	public SelUI(RelicChunkDATA BVOL) {
		BVOL.dataPosition(0);
		selType=BVOL.dataGetInt();
		
		selScaleX=BVOL.dataGetFloat();
		selScaleZ=BVOL.dataGetFloat();
		selCenterX=BVOL.dataGetFloat();
		selCenterZ=BVOL.dataGetFloat();
		
		centerX=BVOL.dataGetFloat();
		centerY=BVOL.dataGetFloat();
		centerZ=BVOL.dataGetFloat();
		scaleX=BVOL.dataGetFloat();
		scaleY=BVOL.dataGetFloat();
		scaleZ=BVOL.dataGetFloat();
		rotation[0]=BVOL.dataGetFloat();
		rotation[1]=BVOL.dataGetFloat();
		rotation[2]=BVOL.dataGetFloat();
		rotation[3]=BVOL.dataGetFloat();
		rotation[4]=BVOL.dataGetFloat();
		rotation[5]=BVOL.dataGetFloat();
		rotation[6]=BVOL.dataGetFloat();
		rotation[7]=BVOL.dataGetFloat();
		rotation[8]=BVOL.dataGetFloat();
	}
	

}
