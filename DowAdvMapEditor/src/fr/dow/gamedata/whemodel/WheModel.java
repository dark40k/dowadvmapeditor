package fr.dow.gamedata.whemodel;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.NoSuchElementException;
import java.util.Vector;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.gamedata.RelicChunkFile;

public class WheModel {

	public LinkedHashMap<String,Action> actions=new LinkedHashMap<String,Action>();
	public LinkedHashMap<String,Motion> motions=new LinkedHashMap<String,Motion>();
	public LinkedHashMap<String,LinkedAnim> linkedAnims=new LinkedHashMap<String,LinkedAnim>();
	public SelUI selUI;
	
	public WheModel(InputStream streamIn) throws IOException{
		
		//load all chunks
		RelicChunkFile whmChunkFile=new RelicChunkFile(streamIn);
				
		RelicChunkFOLD REBP=(RelicChunkFOLD) whmChunkFile.get(1); // Chunk should be of REBP type
	
		//
		// read ACTS chunk => Actions list
		//
		RelicChunkDATA ACTS=REBP.subChunkDATA("ACTS"); // Chunk should be of FBIF type
		if (ACTS!=null) {
			int actionNbr=ACTS.dataGetInt();
			for (int i=0;i<actionNbr;i++) {
				Action newAction=new Action(ACTS);
				actions.put(newAction.name, newAction );
			}
		}
		
		//
		// read MTRE folder => MTON list : motions list
		//
		Vector<RelicChunk> MTONs=REBP.subChunkFOLD("MTRE").get_subChunks(); // Chunk should be of MTON type
		for (RelicChunk MTON : MTONs) {
			motions.put(MTON.get_name().trim(), new Motion( (RelicChunkDATA) MTON));
		}
		
		//
		// read ANIM chunks => linked animation (import from other models)
		// 
		Vector<RelicChunk> ANIMs=REBP.subChunkList("ANIM"); // Chunk should be of MTON type
		for (RelicChunk ANIM : ANIMs) {
			linkedAnims.put(ANIM.get_name().trim().toLowerCase(), new LinkedAnim( (RelicChunkFOLD) ANIM));
		}
		
		//
		// read SEUI chunk
		//
		selUI=new SelUI(REBP.subChunkDATA("SEUI"));
	}
	
	public String getIdleAnimation() {
		
		try {
			Action idleAction=actions.get("default");
			
			while (idleAction.subDataType!=Action.SUBDATA_TYPE_MOTION) 	{ 
				idleAction=actions.get(idleAction.subDataActions.lastElement().name);
			}
			
			Motion idleMotion=motions.get(idleAction.subDataMotions.lastElement().name);
			
			while (idleMotion.subDataAnimations.isEmpty()) idleMotion=motions.get(idleMotion.subDataMotions.lastElement().name);
			
			return idleMotion.subDataAnimations.lastElement().name;
		}
		catch (NullPointerException e) {
			return null;
		}
		// deals with definition mistakes
		catch (NoSuchElementException e) {
			return null;
		}
		
	}
	
	
	public static void main(String[] args) throws IOException {
		
		System.out.println("Loading file data ");

		String filename="E:/Francois/Dawn of war/Modding/Extraction_W40k_whm-high/art/ebps/races/space_marines/troops/land_raider.whe";
//		String filename="E:/Francois/Dawn of war/Modding/Extraction_W40k_whm-high/art/ebps/races/space_marines/structures/barracks.whe";
		
		FileInputStream streamIn=new FileInputStream(filename.replace('\\', '/'));
		
		WheModel test=new WheModel(streamIn);
		
		System.out.println("idle anim="+test.getIdleAnimation());
		
		System.out.println("Loading done ");
		
	}
	
	
}
