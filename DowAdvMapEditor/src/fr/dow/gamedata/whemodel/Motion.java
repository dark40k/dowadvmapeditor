package fr.dow.gamedata.whemodel;

import java.util.Vector;

import fr.dow.gamedata.RelicChunkDATA;

public class Motion {

	public Vector<SubMotion> subDataMotions=new Vector<SubMotion>();
	public Vector<SubAnimation> subDataAnimations=new Vector<SubAnimation>();
	
	public Motion(RelicChunkDATA MTON) {
		
		int subAnimationNbr=MTON.dataGetInt();
		for(int i=0;i<subAnimationNbr;i++) subDataAnimations.add(new SubAnimation(MTON));
		
		int subDataMotionNbr=MTON.dataGetInt();
		for(int i=0;i<subDataMotionNbr;i++) subDataMotions.add(new SubMotion(MTON));
		
		// following data is not read
		
	}

	
	public class SubAnimation {
		
		public String name;
		
		public SubAnimation(RelicChunkDATA ACTS) {
			name=ACTS.dataGetString();
		}
	}
	
	public class SubMotion {
		
		public String name;
		public float weight;
		
		public SubMotion(RelicChunkDATA MTON) {
			name=MTON.dataGetString();
			weight=MTON.dataGetFloat();
		}
		
	}
	
}
