package fr.dow.gamedata.whemodel;

import java.util.Vector;

import fr.dow.gamedata.RelicChunkDATA;

public class Action {

	public static final int SUBDATA_TYPE_MOTION=0;
	public static final int SUBDATA_TYPE_ACTION=1;
	
	public static final int TYPE_NONE=3;
	public static final int TYPE_IF=0;
	public static final int TYPE_ELSEIF=1;
	public static final int TYPE_ELSE=2;  
	
	public String name;
	public boolean baseAction=false;
	public int subDataType=SUBDATA_TYPE_MOTION;
	public Vector<SubData> subDataActions=new Vector<SubData>();
	public Vector<SubData> subDataMotions=new Vector<SubData>();
	
	public Action(RelicChunkDATA ACTS) {
		
		name=ACTS.dataGetString();
		
		int subDataMotionNbr=ACTS.dataGetInt();
		for(int i=0;i<subDataMotionNbr;i++) subDataMotions.add(new SubData(ACTS));
		
		if (subDataMotionNbr==0) subDataType=SUBDATA_TYPE_ACTION;
		
		int subDataActionNbr=ACTS.dataGetInt();
		for(int i=0;i<subDataActionNbr;i++) subDataActions.add(new SubData(ACTS));
		
	}

	
	public class SubData {
		
		public String name;
		public int type;
		public String condition;
		
		public SubData(RelicChunkDATA ACTS) {
			name=ACTS.dataGetString();
			type=ACTS.dataGetInt();
			if ( (type==TYPE_IF) | (type==TYPE_ELSEIF)) condition=ACTS.dataGetString();
		}
	}
	
}
