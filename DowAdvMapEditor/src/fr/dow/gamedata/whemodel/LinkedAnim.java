package fr.dow.gamedata.whemodel;

import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;

public class LinkedAnim {

	public String sourceModel;
	public String animName;
	
	public LinkedAnim(RelicChunkFOLD ANIM) {
		
		RelicChunkDATA XREF=ANIM.subChunkDATA("XREF");
		
		sourceModel=XREF.dataGetString();
		animName=XREF.dataGetString();
		
	}
	
}
