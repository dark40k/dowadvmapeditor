package fr.dow.mapviewer.display;

import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.setdisplay.SetEntitiesDisplayDialog;
import fr.dow.mapeditor.dialogs.setdisplay.SetGroundDisplayDialog;
import fr.dow.mapeditor.dialogs.setdisplay.SetMarkerDisplayDialog;
import fr.dow.mapeditor.dialogs.setdisplay.SetPlayersDisplayDialog;
import fr.dow.mapeditor.dialogs.setdisplay.SetSelectionDisplay;

public class CommandsView {
	
	MapViewer mapViewer;

	public CommandsView(MapViewer mapViewer) {
		this.mapViewer=mapViewer;
	}
	
	public void editVisibility(MapEditor mapEditor) { 
		SetEntitiesDisplayDialog.showDialog(mapEditor,mapViewer);    
	}

	public void editPlayersDisplay(MapEditor mapEditor) {
		SetPlayersDisplayDialog.showDialog(mapEditor, mapViewer);
	}
	
	public void editTerrainDisplay(MapEditor mapEditor) {
		SetGroundDisplayDialog.showDialog(mapEditor, mapViewer);
	}
	
	public void editMarkersDisplay(MapEditor mapEditor) {
		SetMarkerDisplayDialog.showDialog(mapEditor, mapViewer);
	}
	
	public void editSelectionDisplay(MapEditor mapEditor) {
		SetSelectionDisplay.showDialog(mapEditor, mapViewer);
	}
	
	public void setViewTop() {
		mapViewer.mapRenderer.mouse3DMoveHandler.setView(
				mapViewer.mapRenderer.mouse3DMoveHandler.getView().copyWithNewViewAngle(-90.0f, 0.0f)
				);
	}

	public void setViewStd() {
		mapViewer.mapRenderer.mouse3DMoveHandler.setView(
				mapViewer.mapRenderer.mouse3DMoveHandler.getView().copyWithNewViewAngle(-45.0f, -45.0f)
				);
	}

}
