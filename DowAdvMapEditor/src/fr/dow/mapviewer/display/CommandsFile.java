package fr.dow.mapviewer.display;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.dow.gamedata.sgbmap.terrain.GroundData;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.exportnsis.ExportNsisDialog;
import fr.dow.mapviewer.display.params.MapDisPars;

public class CommandsFile {

	MapViewer mapViewer;
	
	public CommandsFile(MapViewer mapViewer) {
		this.mapViewer=mapViewer;
	}

	public void createMinimap(MapEditor mapEditor) {
		
		GroundData groundData=mapViewer.map.terrain.ground.groundData;
		
		// check if display parameters are to be left as they are or updated
    	if (useStandardHiding()) {
    		
    		MapDisPars mapDisPars=mapViewer.mapRenderer.mapDisplayParams;
    		
    		for (int i=1000;i<1008;i++) mapDisPars.playerDisplayParams.players.get(i).isVisible=false;
    		mapDisPars.playerDisplayParams.players.get(0).isVisible=true;
    		mapDisPars.playerDisplayParams.players.get(0).isTextured=true;
    		mapDisPars.playerDisplayParams.fireUpdateEvent(1);
    		
    		mapDisPars.markerDisPars.showMarkers=false;
    		mapDisPars.markerDisPars.fireUpdateEvent(1);
    		
    		mapDisPars.terrainDisplayParams.ground.isVisible=true;
    		mapDisPars.terrainDisplayParams.ground.wireframe=false;
    		mapDisPars.terrainDisplayParams.decals.isVisible=true;
    		mapDisPars.terrainDisplayParams.decals.wireframe=false;
    		mapDisPars.terrainDisplayParams.water.isVisible=true;
    		mapDisPars.terrainDisplayParams.water.wireframe=false;
			mapDisPars.terrainDisplayParams.coverOverlay.isVisible=false;
			mapDisPars.terrainDisplayParams.impassOverlay.isVisible=false;
			mapDisPars.terrainDisplayParams.footfallOverlay.isVisible=false;
			mapDisPars.terrainDisplayParams.passabilityDisPars.isVisible=false;
			mapDisPars.terrainDisplayParams.preciseDisPars.isVisible=false;
			mapDisPars.terrainDisplayParams.pathDisPars.isVisible=false;
			mapDisPars.terrainDisplayParams.fireUpdateEvent(1);
			
    	}
		
    	//clear selection
    	mapViewer.mapRenderer.currentSelection.clearAll();
    	
		// Set view angle (centered, half of map visible on both X and Z axis)
		float coef=(float)(0.25/Math.tan(22.5*Math.PI/180));
		mapViewer.mapRenderer.mouse3DMoveHandler.setView( 0, coef*(groundData.get_ZMax()-groundData.get_ZMin()), 0, 270, 0);

		// compute minimap so that largest size is 256
		int miniMapWidth=512*groundData.get_cellnbrX()/Math.max(groundData.get_cellnbrX(), groundData.get_cellnbrZ());
		int miniMapHeight=512*groundData.get_cellnbrZ()/Math.max(groundData.get_cellnbrX(), groundData.get_cellnbrZ());
		
		// Resize display
		Dimension windim=mapViewer.getSize();
		Dimension contentdim=mapViewer.getContentPane().getSize();
		mapViewer.setSize(windim.width+miniMapWidth-contentdim.width, windim.height+miniMapHeight-contentdim.height);
		
		// Exit if filename is not available (map cannot be saved)
		// principle is to maintain the access to view orientation and resizing
		if (mapViewer.map.mapFile==null) {
			return;
		}
			
		// recover filename
		String mapFileName=mapViewer.map.mapFile.getAbsolutePath();
		
		// Ask for output file and manage ansqer (ok,cancel,...)
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File(mapFileName).getParentFile());
        chooser.setDialogTitle("Save Minimap");
        chooser.setDialogType(JFileChooser.SAVE_DIALOG);
        chooser.setFileFilter(new FileNameExtensionFilter("Dawn of War image", "tga"));
        chooser.setSelectedFile(new File(mapFileName.substring(0, mapFileName.lastIndexOf('.'))+"_mm.tga"));
        
        int res = chooser.showOpenDialog(null);
        
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        File chosen = chooser.getSelectedFile();
        
        if (chosen == null) { System.err.println("createMinimap : No file selected"); return; }
        if (chosen.exists()) {
        	int rep=JOptionPane.showConfirmDialog(mapViewer, "Overwrite existing file?","File already exist",JOptionPane.YES_NO_OPTION); 
        	if (rep!=JOptionPane.OK_OPTION) return; 
        }

        System.out.println("Creating minimap ="+chosen);

        // Set renderer to make a screenshot on next display and force display
        mapViewer.mapRenderer.RequireScreenShot(chosen, miniMapWidth, miniMapHeight);
        mapViewer.glCanvas.display();
		
	}
	private boolean useStandardHiding() {
		int repHiding=JOptionPane.showConfirmDialog(mapViewer, "Map is Read Only, \n Save map under new filename to generate minimap. ","Error",JOptionPane.ERROR_MESSAGE);		
		return (repHiding==JOptionPane.OK_OPTION);
	}

	public void save(MapEditor mapEditor) {
		
		if (mapViewer.map.mapFile==null) saveAs(mapEditor);
		
    	try {
    		mapViewer.map.save(mapViewer.map.mapFile);
		} catch (IOException e) {
			System.err.println("Failed to save map");
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Could not save map","Warning",JOptionPane.ERROR_MESSAGE);		
		}
	}

	public void saveAs(MapEditor mapEditor) {
		
    	// select map file
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(MapEditor.currentDirectory);
        chooser.setFileFilter(new FileNameExtensionFilter("Dawn of War Map", "sgb"));
        
        int res = chooser.showOpenDialog(null);
        if (res != JFileChooser.APPROVE_OPTION) return;
        
        File mapFile = chooser.getSelectedFile();
        if (mapFile == null) { System.err.println("No file selected"); return; }
        	
        MapEditor.currentDirectory=mapFile.getParentFile();
		
    	String mapFileName=(mapFile.getAbsolutePath()).replace('\\', '/');
    	if (!mapFileName.endsWith(".sgb")) mapFileName=mapFileName+".sgb";
    	try {
    		mapViewer.map.save(new File(mapFileName)); // TODO find way to avoid String to rename File extension
		} catch (IOException e) {
			System.err.println("Failed to save map");
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Could not save map","Warning",JOptionPane.ERROR_MESSAGE);		
		}
		
		mapEditor.recentMapList.addMap(mapFileName);
		mapViewer.updateTitle();
		
	}

	public void genInstaller(MapEditor mapEditor) {
		ExportNsisDialog.showDialog(mapEditor, mapViewer);
	}
	
	
}
