package fr.dow.mapviewer.display;

import fr.dow.mapviewer.display.selection.SelFilter.FilterType;

public class CommandsSelection {

	MapViewer mapViewer;

	public CommandsSelection(MapViewer mapViewer) {
		this.mapViewer=mapViewer;
	}
	
	public void clearSelection() {
		mapViewer.getRenderer().currentSelection.clearAll();
	}

	public void setFilterAll() {
		mapViewer.getRenderer().curFilter.set(FilterType.ALL);
	}

	public void setFilterNone() {
		mapViewer.getRenderer().curFilter.set(FilterType.NONE);
	}
	
}
