package fr.dow.mapviewer.display;

import javax.swing.JOptionPane;

import fr.dow.mapeditor.CopyData;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.MoveMapDialog;
import fr.dow.mapeditor.dialogs.ResizeMapDialog;

public class CommandsEditMap {

	MapViewer mapViewer;

	public CommandsEditMap(MapViewer mapViewer) {
		this.mapViewer=mapViewer;
	}
	
	public void resizeMap(MapEditor mapEditor) {
		ResizeMapDialog.showDialog(mapEditor, mapViewer);
	}

	public void doResizeMap(int newCellX, int newCellZ) {
		mapViewer.map.resizeMap(newCellX, newCellZ);
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
	}
	
	public void moveMap(MapEditor mapEditor) {
		MoveMapDialog.showDialog(mapEditor, mapViewer);
	}
	
	public void doMoveMap(boolean stepTile,int moveX,int moveZ) {
		mapViewer.map.moveMap(stepTile,moveX,moveZ);
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
		mapViewer.mapRenderer.mapDisplayParams.playerDisplayParams.fireUpdateEvent(1);
		mapViewer.mapRenderer.mapDisplayParams.markerDisPars.fireUpdateEvent(1);
	}
	
	public void rotateMap(MapEditor mapEditor) {
	    String message = "Rotate map 1/4 turn clockwise?";
	    String title = "Rotate map";
	    int reply = JOptionPane.showConfirmDialog(mapEditor, message, title, JOptionPane.YES_NO_OPTION);
	    if (reply == JOptionPane.YES_OPTION) doRotateMap();
	}
	
	public void doRotateMap() {
		mapViewer.map.rotateClockwise();
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
		mapViewer.mapRenderer.mapDisplayParams.playerDisplayParams.fireUpdateEvent(1);
		mapViewer.mapRenderer.mapDisplayParams.markerDisPars.fireUpdateEvent(1);
	}

	public void pasteSubMap(CopyData pasteData, boolean heightMap, boolean textureMap, int offsetX, int offsetZ) {
		mapViewer.map.pasteSubMap(pasteData,heightMap,textureMap,offsetX,offsetZ);
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
		mapViewer.mapRenderer.mapDisplayParams.playerDisplayParams.fireUpdateEvent(1);
		mapViewer.mapRenderer.mapDisplayParams.markerDisPars.fireUpdateEvent(1);
	}
	
	
}
