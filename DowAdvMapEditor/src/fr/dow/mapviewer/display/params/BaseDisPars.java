package fr.dow.mapviewer.display.params;

import java.util.EventListener;
import java.util.EventObject;

import javax.swing.event.EventListenerList;


public class BaseDisPars {


	//
	// Change event
	//
	
	private EventListenerList listenerList = new EventListenerList();

	public class UpdateEvent extends EventObject {
		int level;
		public UpdateEvent(Object source, int updateLevel) {
			super(source);
			level=updateLevel;
		}
		public int getLevel() { return level; }
	}

	public interface UpdateListener extends EventListener {
		public void changeUpdateLevel(UpdateEvent evt);
	}

	public void addUpdateListener(UpdateListener listener) {
		listenerList.add(UpdateListener.class, listener);
	}
	
	public void removeUpdateListener(UpdateListener listener) {
		listenerList.remove(UpdateListener.class, listener);
	}
	
	public void fireUpdateEvent(int updateLevel) {
		UpdateEvent evt=new UpdateEvent(this,updateLevel);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener) listeners[i+1]).changeUpdateLevel(evt);
			}
		}
	}

}
