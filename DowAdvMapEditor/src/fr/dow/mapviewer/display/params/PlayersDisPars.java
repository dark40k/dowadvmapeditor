package fr.dow.mapviewer.display.params;

import java.util.LinkedHashMap;

import fr.dow.gamedata.sgbmap.units.PlayerID;

public class PlayersDisPars extends BaseDisPars {

	public LinkedHashMap<PlayerID,PlayerData> players ;
	
	public PlayersDisPars() {
		
		players=new LinkedHashMap<PlayerID,PlayerData>();
		
		players.put(PlayerID.Player1, new PlayerData(PlayerID.Player1,true,true,1,0,0) );
		players.put(PlayerID.Player2, new PlayerData(PlayerID.Player2,true,true,0,1,0) );
		players.put(PlayerID.Player3, new PlayerData(PlayerID.Player3,true,true,1,1,0) );
		players.put(PlayerID.Player4, new PlayerData(PlayerID.Player4,true,true,0,0,1) );
		players.put(PlayerID.Player5, new PlayerData(PlayerID.Player5,true,true,1,0,1) );
		players.put(PlayerID.Player6, new PlayerData(PlayerID.Player6,true,true,0,1,1) );
		players.put(PlayerID.Player7, new PlayerData(PlayerID.Player7,true,true,1,0.5f,0) );
		players.put(PlayerID.Player8, new PlayerData(PlayerID.Player8,true,true,0.5f,0,1) );
		players.put(PlayerID.World, new PlayerData(PlayerID.World,true,true,1,1,1) );
		
	}
	
	public class PlayerData {
		public String name;
		public boolean isVisible;
		public boolean isTextured;
		public float red;
		public float green;
		public float blue;
		
		public PlayerData(PlayerID playerID,boolean isVisible,boolean isTextured,float red,float green,float blue) {
			this.name=playerID.toString();
			this.isVisible=isVisible;
			this.isTextured=isTextured;
			this.red=red;
			this.green=green;
			this.blue=blue;
		}
	}
	
}
