package fr.dow.mapviewer.display.params.terrain;

import fr.dow.mapviewer.display.params.BaseDisPars;

public class TerrainDisPars extends BaseDisPars implements BaseDisPars.UpdateListener {
	
	public GroundDisPars ground=new GroundDisPars();
	public DecalsDisPars decals=new DecalsDisPars();
	public WaterDisPars water=new WaterDisPars();
	public CoverOverlayDisPars coverOverlay=new CoverOverlayDisPars();
	public ImpassOverlayDisPars impassOverlay=new ImpassOverlayDisPars();
	public FootfallOverlayDisPars footfallOverlay=new FootfallOverlayDisPars();
	
	public PassabilityOverlayDisPars passabilityDisPars=new PassabilityOverlayDisPars();
	public PathSizeOverlayDisPars preciseDisPars=new PathSizeOverlayDisPars();
	public PathOverlayDisPars pathDisPars=new PathOverlayDisPars();

	public TerrainDisPars() {
		super();
		ground.addUpdateListener(this);
		decals.addUpdateListener(this);
		water.addUpdateListener(this);
		coverOverlay.addUpdateListener(this);
		impassOverlay.addUpdateListener(this);
		footfallOverlay.addUpdateListener(this);
		passabilityDisPars.addUpdateListener(this);
		preciseDisPars.addUpdateListener(this);
		pathDisPars.addUpdateListener(this);
	}

	@Override
	public void changeUpdateLevel(UpdateEvent evt) {
		fireUpdateEvent(evt.getLevel());
	}

	
}
