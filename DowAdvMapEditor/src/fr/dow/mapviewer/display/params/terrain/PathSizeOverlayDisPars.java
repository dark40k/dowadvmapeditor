package fr.dow.mapviewer.display.params.terrain;

import java.awt.Color;

import fr.dow.mapviewer.display.params.BaseDisPars;

public class PathSizeOverlayDisPars extends BaseDisPars {
	public boolean isVisible=false;
	public Color path0Color=new Color(50,50,50);
	public Color path1Color=Color.red;
	public Color path3Color=Color.yellow;
	public Color path5Color=new Color(0,150,0);
	public Color path7Color=Color.green;

}
