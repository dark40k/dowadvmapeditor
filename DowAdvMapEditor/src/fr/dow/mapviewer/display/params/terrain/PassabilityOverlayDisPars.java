package fr.dow.mapviewer.display.params.terrain;

import java.awt.Color;

import fr.dow.mapviewer.display.params.BaseDisPars;

public class PassabilityOverlayDisPars extends BaseDisPars {
	public boolean isVisible=false;
	
	public Color deepWaterColor=new Color(0, 153, 153);
	public Color passableColor=Color.green;
	public Color borderWaterColor=Color.cyan;
	public Color impassableColor=Color.red;

}
