package fr.dow.mapviewer.display.params.terrain;

import fr.dow.mapviewer.display.params.BaseDisPars;

public class DecalsDisPars extends BaseDisPars {
	public boolean isVisible=true;
	public boolean wireframe=false;
}
