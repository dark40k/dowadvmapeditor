package fr.dow.mapviewer.display.params.terrain;

import java.awt.Color;

import fr.dow.mapviewer.display.params.BaseDisPars;

public class PathOverlayDisPars extends BaseDisPars {
	public boolean isVisible=false;
	public Color defaultSectorColor=Color.LIGHT_GRAY;
	public Color sector1Color=Color.red;
	public Color borderColor=Color.green;
	public boolean showText=false;
	public Color textColor=Color.white;
}
