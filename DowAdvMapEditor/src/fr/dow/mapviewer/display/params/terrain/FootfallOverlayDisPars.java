package fr.dow.mapviewer.display.params.terrain;

import java.awt.Color;

import fr.dow.mapviewer.display.params.BaseDisPars;

public class FootfallOverlayDisPars extends BaseDisPars {
	public boolean isVisible=false;
	public Color unknownColor=Color.lightGray;
	public Color dirtsandColor=Color.yellow;
	public Color rockColor=Color.orange;
	public Color grassColor=Color.green;
	public Color waterColor=Color.cyan;
}
