package fr.dow.mapviewer.display.params.terrain;

import java.awt.Color;

import fr.dow.mapviewer.display.params.BaseDisPars;

public class CoverOverlayDisPars extends BaseDisPars {
	public boolean isVisible=false;
	public Color noneColor=Color.lightGray;
	public Color lightColor=Color.orange;
	public Color heavyColor=Color.red;
	public Color negativeColor=Color.green;
	public Color blockingColor=Color.pink;
	public Color stealthColor=Color.cyan;
}
