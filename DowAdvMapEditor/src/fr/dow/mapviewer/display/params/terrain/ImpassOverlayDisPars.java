package fr.dow.mapviewer.display.params.terrain;

import java.awt.Color;

import fr.dow.mapviewer.display.params.BaseDisPars;

public class ImpassOverlayDisPars extends BaseDisPars {
	public boolean isVisible=false;
	public Color passableColor=Color.green;
	public Color impassableColor=Color.red;
	public Color genImpassableColor=new Color(255,200,200);
	public Color genPassableColor=new Color(200,255,200);
}
