package fr.dow.mapviewer.display.params;

import java.awt.Color;

public class MapSelDisPars {

	public Color mouseArea;
	
	public SelDisPars selected;
	public SelDisPars over;
	
	public MapSelDisPars() {
		
		this.mouseArea=Color.green;
		
		this.selected=new SelDisPars();
		this.selected.decals=Color.white;
		this.selected.entities=Color.green;
		this.selected.markers=Color.red;
		this.selected.tiles=Color.magenta;
		this.selected.groundArea=Color.yellow;		
		
		this.over=new SelDisPars();
		this.over.decals=Color.orange;
		this.over.entities=Color.orange;
		this.over.markers=Color.orange;
		this.over.tiles=Color.orange;
		this.over.groundArea=Color.orange;		

	}
	
}
