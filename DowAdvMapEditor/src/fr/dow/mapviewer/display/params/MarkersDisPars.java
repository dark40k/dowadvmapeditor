package fr.dow.mapviewer.display.params;

import java.awt.Color;

public class MarkersDisPars extends BaseDisPars {

	public boolean showMarkers=true;
	public boolean showProxRadius=true;
	public boolean showTitle=true;
	public Color textColor=Color.white;
	
}
