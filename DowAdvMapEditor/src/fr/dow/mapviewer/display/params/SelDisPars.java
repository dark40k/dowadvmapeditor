package fr.dow.mapviewer.display.params;

import java.awt.Color;

public class SelDisPars {
	
	public Color tiles;
	public Color decals;
	public Color entities;
	public Color markers;
	public Color groundArea;

}
