package fr.dow.mapviewer.display.params;

import fr.dow.mapviewer.display.params.terrain.TerrainDisPars;

public class MapDisPars extends BaseDisPars implements BaseDisPars.UpdateListener {

	public PlayersDisPars playerDisplayParams = new PlayersDisPars();
	
	public EntitiesDisPars entitiesDisplayParams = new EntitiesDisPars();

	public TerrainDisPars terrainDisplayParams = new TerrainDisPars();
	
	public MarkersDisPars markerDisPars = new MarkersDisPars();
	
	public MapSelDisPars selDisPars = new MapSelDisPars();

	public MapDisPars() {
		super();
		markerDisPars.addUpdateListener(this);
		terrainDisplayParams.addUpdateListener(this);
		playerDisplayParams.addUpdateListener(this);
		entitiesDisplayParams.addUpdateListener(this);
	}

	@Override
	public void changeUpdateLevel(UpdateEvent evt) {
		fireUpdateEvent(evt.getLevel());
	}
	
}
