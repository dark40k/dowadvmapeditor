package fr.dow.mapviewer.display.joglobject;

import java.nio.IntBuffer;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.gl2.GLUgl2;

import com.jogamp.common.nio.Buffers;

import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.mapviewer.display.params.SelDisPars;
import fr.dow.mapviewer.display.renderer.ScreenPosition;
import fr.dow.mapviewer.display.renderer.WorldPosition;
import fr.dow.mapviewer.display.selection.SelFilter;
import fr.dow.mapviewer.display.selection.Selection;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;

public class JoglSelection {
	
	private static GLUgl2 glu = new GLUgl2();
	
	private final static int MAX_SELECTION_HITS_COUNT=5000;
	
	public static Selection getMousePointOver(GL2 gl, JoglWorld joglWorld, SgbMap map, SelFilter selectionFilter, ScreenPosition mouse1, ScreenPosition mouse0){
		
		Selection newSelection=new Selection();
		
		int[] viewport=new int[4];
        gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);
        
		int hitsCount; // The Number Of Objects That We Selected
		IntBuffer hitsBuffer=Buffers.newDirectIntBuffer(MAX_SELECTION_HITS_COUNT*5);
        gl.glSelectBuffer(MAX_SELECTION_HITS_COUNT*5, hitsBuffer); 
        
        // Puts OpenGL In Selection Mode. Nothing Will Be Drawn.  Object ID's and Extents Are Stored In The Buffer.
        gl.glRenderMode(GL2.GL_SELECT);
        gl.glInitNames(); // Initializes The Name Stack
        
        // Selects The Projection Matrix
        gl.glMatrixMode(GL2.GL_PROJECTION); 
        gl.glPushMatrix(); // Push The Projection Matrix
        gl.glLoadIdentity(); // Resets The Matrix
        
        double x,y,dx,dy;
        if (mouse0!=null) {
        	dx=Math.max(1.0,Math.abs((double) (mouse1.x-mouse0.x)));
        	dy=Math.max(1.0,Math.abs((double) (mouse1.y-mouse0.y)));
        	x=Math.min((double) mouse1.x,(double) mouse0.x)+dx/2.0;
        	y=Math.min((double) (viewport[3] - mouse1.y),(double) (viewport[3] - mouse0.y))+dy/2.0;
        } else {
        	x=(double) mouse1.x;
        	y=(double) (viewport[3] - mouse1.y);
        	dx=1.0;
        	dy=1.0;
        }
 
        glu.gluPickMatrix(x, y, dx, dy, viewport, 0);
        glu.gluPerspective(45.0f, (float) (viewport[2] - viewport[0]) / (float) (viewport[3] - viewport[1]), 0.1f, 10000.0f);
 
        // Select The Modelview Matrix
        gl.glMatrixMode(GL2.GL_MODELVIEW); 
        
        gl.glDisable(GL2.GL_CULL_FACE);
        
		// draw pickable objects
        if (selectionFilter.isNotFiltered(FilterType.TILE)) {
        	gl.glPushName(FilterType.TILE.getInt());
        	joglWorld.joglTerrain.joglTiles.drawPickable(gl);
        	gl.glPopName();
        }
        if (selectionFilter.isNotFiltered(FilterType.DECAL)) {
        	gl.glPushName(FilterType.DECAL.getInt());
        	joglWorld.joglTerrain.joglDecals.drawPickable(gl);
        	gl.glPopName();
        }
        if (selectionFilter.isNotFiltered(FilterType.ENTITY)) {
        	gl.glPushName(FilterType.ENTITY.getInt());
        	joglWorld.joglMapEntities.drawPickable(gl,map.entities,map.terrain.ground);
        	gl.glPopName();
        }
        if (selectionFilter.isNotFiltered(FilterType.MARKER)) {
        	gl.glPushName(FilterType.MARKER.getInt());
        	joglWorld.joglMarkers.drawPickable(gl,map.terrain.ground);
        	gl.glPopName();
        }
        
        gl.glEnable(GL2.GL_CULL_FACE);
        
        gl.glMatrixMode(GL2.GL_PROJECTION); // Select The Projection Matrix
        gl.glPopMatrix(); // Pop The Projection Matrix
        gl.glMatrixMode(GL2.GL_MODELVIEW); // Select The Modelview Matrix
        
        hitsCount = gl.glRenderMode(GL2.GL_RENDER); // Switch To Render Mode, Find Out How Many

        for (int i=0;i<hitsCount;i++) {
        	
        	// int stacksize=selectBuffer.get(i*5+0); // always equal to 2
        	// int zmin=selectBuffer.get(i*5+1); //not used
        	// int zmax=selectBuffer.get(i*5+2); //not used
        	int type=hitsBuffer.get(i*5+3);
        	int id=hitsBuffer.get(i*5+4);
        	
        	if (type==FilterType.TILE.getInt()) newSelection.tiles.add(id);
        	if (type==FilterType.DECAL.getInt()) newSelection.decals.add(id);
        	if (type==FilterType.ENTITY.getInt()) newSelection.entities.add(id);
        	if (type==FilterType.MARKER.getInt()) newSelection.markers.add(id);
        	
        	//System.out.println(type+" "+id);
        	
        }

        if ((selectionFilter.isNotFiltered(FilterType.GROUNDAREA)) && (mouse0!=null) && (mouse1!=null)) {
        	
        	WorldPosition worldPos0=WorldPosition.getNewPosition(gl,mouse0);
        	WorldPosition worldPos1=WorldPosition.getNewPosition(gl,mouse1);
        	WorldPosition worldPos2=WorldPosition.getNewPosition(gl,new ScreenPosition(mouse0.x,mouse1.y));
        	WorldPosition worldPos3=WorldPosition.getNewPosition(gl,new ScreenPosition(mouse1.x,mouse0.y));

        	if ( (worldPos0!=null) && (worldPos1!=null) && (worldPos2!=null) && (worldPos3!=null)) {
        	
	        	float xmin=Math.min(Math.min(worldPos0.x, worldPos1.x),Math.min(worldPos2.x, worldPos3.x));
	        	float xmax=Math.max(Math.max(worldPos0.x, worldPos1.x),Math.max(worldPos2.x, worldPos3.x));
	        	float zmin=Math.min(Math.min(worldPos0.z, worldPos1.z),Math.min(worldPos2.z, worldPos3.z));
	        	float zmax=Math.max(Math.max(worldPos0.z, worldPos1.z),Math.max(worldPos2.z, worldPos3.z));
	        	
	        	int i0= (int) Math.floor( xmin / map.terrain.ground.groundData.get_cellsize() ) + map.terrain.ground.groundData.get_centerXnbr() - 1;
	        	int j0= (int) Math.floor( zmin / map.terrain.ground.groundData.get_cellsize() ) + map.terrain.ground.groundData.get_centerZnbr() - 1;
	        	i0=Math.min(Math.max(i0, 0),map.terrain.ground.groundData.get_cellnbrX());
	        	j0=Math.min(Math.max(j0, 0),map.terrain.ground.groundData.get_cellnbrZ());
	
	        	int i1= (int) Math.ceil( xmax / map.terrain.ground.groundData.get_cellsize() ) + map.terrain.ground.groundData.get_centerXnbr() - 1;
	        	int j1= (int) Math.ceil( zmax / map.terrain.ground.groundData.get_cellsize() ) + map.terrain.ground.groundData.get_centerZnbr() - 1;
	        	i1=Math.min(Math.max(i1, 0),map.terrain.ground.groundData.get_cellnbrX());
	        	j1=Math.min(Math.max(j1, 0),map.terrain.ground.groundData.get_cellnbrZ());
	
	        	newSelection.area.setArea(i0,j0,i1,j1);
        	}
        }
        
        return newSelection;
	}
	
	public static void drawSelected(GL2 gl, Selection selection, SelFilter selFilter, SelDisPars selDisPars, JoglWorld joglWorld, SgbMap map, float offset) {
		
		if (!selFilter.isLocked(FilterType.TILE)) {
			gl.glColor4f(selDisPars.tiles.getRed()/255f, selDisPars.tiles.getGreen()/255f, selDisPars.tiles.getBlue()/255f,1.0f);
			for (Object id : selection.tiles.toArray()) 
				joglWorld.joglTerrain.joglTiles.drawPicked(gl,(Integer)id, offset);
		}
		
		if (!selFilter.isLocked(FilterType.DECAL)) {
			gl.glColor4f(selDisPars.decals.getRed()/255f, selDisPars.decals.getGreen()/255f, selDisPars.decals.getBlue()/255f,1.0f);
			for (Object id : selection.decals.toArray()) 
				joglWorld.joglTerrain.joglDecals.drawPicked(gl,(Integer) id, offset);
		}
		
		if (!selFilter.isLocked(FilterType.ENTITY)) {
			gl.glColor4f(selDisPars.entities.getRed()/255f, selDisPars.entities.getGreen()/255f, selDisPars.entities.getBlue()/255f,1.0f);
			for (Object id : selection.entities.toArray()) 
				joglWorld.joglMapEntities.drawPicked(gl,(Integer) id,map.entities,map.terrain.ground, offset);
		}
		
		if (!selFilter.isLocked(FilterType.MARKER)) {
			gl.glColor4f(selDisPars.markers.getRed()/255f, selDisPars.markers.getGreen()/255f, selDisPars.markers.getBlue()/255f,1.0f);
			for (Object id : selection.markers.toArray()) 
				joglWorld.joglMarkers.drawPicked(gl,(Integer)id, map.terrain.ground, offset);
		}
			
		if ((!selFilter.isLocked(FilterType.GROUNDAREA))& (selection.area!=null)) {
			gl.glColor4f(selDisPars.groundArea.getRed()/255f, selDisPars.groundArea.getGreen()/255f, selDisPars.groundArea.getBlue()/255f,1.0f);
			selection.area.drawPicked(gl, map, offset);
		}
		
	}

}
