package fr.dow.mapviewer.display.joglobject;

import javax.media.opengl.GL2;
import javax.media.opengl.GLException;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.terrain.Terrain;
import fr.dow.mapviewer.display.joglobject.jogloverlay.JoglOverlay;
import fr.dow.mapviewer.display.params.MarkersDisPars;
import fr.dow.mapviewer.display.params.terrain.TerrainDisPars;

public class JoglTerrain implements MarkersDisPars.UpdateListener  {

	private TerrainDisPars terrainDisplayParams;
	
	public JoglTiles joglTiles;
	
	public JoglGround joglGround;
	public JoglDecals joglDecals;
	public JoglWater joglWater;
	public JoglOverlay joglOverlay;
	
	private int _genList1; // pointer sur la display list JOGL
	private int _genList2; // pointer sur la display list JOGL

	//
	// Update level management
	//
	
	private int updateLevel=0;
	@Override
	public void changeUpdateLevel(MarkersDisPars.UpdateEvent evt) {
		if (evt.getLevel()>updateLevel) updateLevel=evt.getLevel();
	}
	
	//
	//
	//
	
	public JoglTerrain(GL2 gl, Terrain terrain, DowMod modData, TerrainDisPars terrainDisplayParams)  throws GLException {
		
		this.terrainDisplayParams=terrainDisplayParams;
		
		joglTiles = new JoglTiles(gl,terrain);
		joglGround = new JoglGround(gl, joglTiles, terrain, modData);
		joglDecals = new JoglDecals(gl,terrain, modData);
		joglWater = new JoglWater(gl,terrain, modData);
		joglOverlay = new JoglOverlay(gl,joglTiles,terrain,terrainDisplayParams);
		
		_genList1 = gl.glGenLists(1);
		_genList2 = gl.glGenLists(1);
    	
		updateGenLists(gl);
		
		terrainDisplayParams.addUpdateListener(this);
	}
	
	public void display1(GL2 gl, Terrain terrain, DowMod modData) {
		
		
		if (updateLevel>0) {
			
			if (updateLevel>1) {
	        	resetTerrain(gl, terrain,modData);
			}
			
    		joglOverlay.update(gl, terrain, terrainDisplayParams);
    		updateGenLists(gl);
    		
    		updateLevel=0;
		}
		
		
		gl.glCallList(_genList1);
		joglOverlay.display(gl);
	}
	
	public void display2(GL2 gl){
		gl.glCallList(_genList2);
	}

	private void resetTerrain(GL2 gl, Terrain terrain, DowMod modData) {
		
		joglTiles.resetTerrain(gl);
		joglGround.resetTerrain(gl,joglTiles,terrain,modData);
		joglWater.resetTerrain(gl,terrain);
		joglOverlay.resetTerrain(gl,joglTiles,terrain);
		joglDecals.resetTerrain(gl,terrain,modData);
		
		updateGenLists(gl);
	}
	
	private void updateGenLists(GL2 gl) {
		
		gl.glNewList( _genList1, GL2.GL_COMPILE );
		
		if (terrainDisplayParams.ground.isVisible) {
			if (terrainDisplayParams.ground.wireframe) gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_LINE);
			joglGround.display(gl);
			if (terrainDisplayParams.ground.wireframe) gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);
		}
		
		if (terrainDisplayParams.decals.isVisible) {
			if (terrainDisplayParams.decals.wireframe) gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_LINE);
			joglDecals.display(gl);
			if (terrainDisplayParams.decals.wireframe) gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);
		}
		
		gl.glEndList();
		
		gl.glNewList( _genList2, GL2.GL_COMPILE );
		
		if (terrainDisplayParams.water.isVisible) {
			if (terrainDisplayParams.water.wireframe) gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_LINE);
			joglWater.display(gl);
			if (terrainDisplayParams.water.wireframe) gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);
		}
		
		gl.glEndList();
		
	}

}
