package fr.dow.mapviewer.display.joglobject.jogloverlay;

import java.awt.Color;
import java.io.ByteArrayInputStream;

import fr.dow.gamedata.sgbmap.terrain.TerrainType;
import fr.dow.mapviewer.display.params.terrain.FootfallOverlayDisPars;

public class JoglOverlayDataFootfall extends JoglOverlayData {

	public JoglOverlayDataFootfall(TerrainType terrainType, FootfallOverlayDisPars displayParameters){
		
		Color[] colorTable=new Color[6];
		colorTable[TerrainType.FOOTFALLUNKNOWN]=displayParameters.unknownColor;
		colorTable[TerrainType.FOOTFALLDIRTSAND]=displayParameters.dirtsandColor;
		colorTable[TerrainType.FOOTFALLROCK]=displayParameters.rockColor;
		colorTable[TerrainType.FOOTFALLGRASS]=displayParameters.grassColor;
		colorTable[TerrainType.FOOTFALLWATER]=displayParameters.waterColor;

		int terrainDataWidth=terrainType.get_footfallSizeX();
		int terrainDataHeight=terrainType.get_footfallSizeZ();

		ByteArrayInputStream terrainData=terrainType.get_footfallData();
		
	    scale=((float) terrainDataWidth) / ( (float) terrainDataWidth + 2 )*((float) terrainDataWidth) / ( (float) terrainDataWidth + 1 );
		
	    createOverlayPicture(terrainData, colorTable, new Color(0,0,255,128), terrainDataWidth, terrainDataHeight);
	}

}
