package fr.dow.mapviewer.display.joglobject.jogloverlay;

import java.awt.Color;
import java.io.ByteArrayInputStream;

import fr.dow.gamedata.sgbmap.terrain.PassabilityMap;
import fr.dow.gamedata.sgbmap.terrain.Terrain;
import fr.dow.mapviewer.display.params.terrain.PassabilityOverlayDisPars;

public class JoglOverlayDataPassability extends JoglOverlayData {

	public JoglOverlayDataPassability(Terrain terrain, PassabilityOverlayDisPars passabilityDisPars){
		
		Color[] colorTable = new Color[8];
		colorTable[PassabilityMap.DEEPWATER]=passabilityDisPars.deepWaterColor;
		colorTable[PassabilityMap.PASSABLE]=passabilityDisPars.passableColor;
		colorTable[PassabilityMap.BORDERWATER]=passabilityDisPars.borderWaterColor;
		colorTable[PassabilityMap.IMPASSABLE]=passabilityDisPars.impassableColor;

		PassabilityMap passMap=terrain.pathFindingInformation.passabilityMap;
		
		int terrainDataWidth = passMap.imax;
		int terrainDataHeight = passMap.jmax;

		ByteArrayInputStream terrainData=passMap.get_Data();
		
	    scale=((float) terrainDataWidth) / ( (float) terrainDataWidth + 2 );
		
	    createOverlayPicture(terrainData, colorTable, new Color(0,0,255,128), terrainDataWidth, terrainDataHeight);
	}
	
}
