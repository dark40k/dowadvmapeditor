package fr.dow.mapviewer.display.joglobject.jogloverlay;

import java.awt.Color;
import java.io.ByteArrayInputStream;

import fr.dow.gamedata.sgbmap.terrain.Ground;
import fr.dow.gamedata.sgbmap.terrain.Impass;
import fr.dow.mapviewer.display.params.terrain.ImpassOverlayDisPars;

public class JoglOverlayDataImpass extends JoglOverlayData {

	public JoglOverlayDataImpass(Ground ground, ImpassOverlayDisPars displayParameters){
		
		Color[] colorTable = new Color[6];
		colorTable[Impass.PASSABLE]=displayParameters.passableColor;
		colorTable[Impass.IMPASSABLE]=displayParameters.impassableColor;
		colorTable[Impass.GEN_IMPASSABLE]=displayParameters.genImpassableColor;
		colorTable[Impass.GEN_PASSABLE]=displayParameters.genPassableColor;

		int terrainDataWidth = ground.impass.get_sizeX();
		int terrainDataHeight = ground.impass.get_sizeZ();

		ByteArrayInputStream terrainData=ground.impass.get_GeneratedData(ground);
		
	    scale=((float) terrainDataWidth) / ( (float) terrainDataWidth + 2 );
		
	    createOverlayPicture(terrainData, colorTable, new Color(0,0,255,128), terrainDataWidth, terrainDataHeight);
	}
}
