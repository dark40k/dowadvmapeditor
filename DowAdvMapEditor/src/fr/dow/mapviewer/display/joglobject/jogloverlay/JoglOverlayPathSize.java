package fr.dow.mapviewer.display.joglobject.jogloverlay;

import java.awt.Color;
import java.io.ByteArrayInputStream;

import fr.dow.gamedata.sgbmap.terrain.PathSizeMap;
import fr.dow.gamedata.sgbmap.terrain.Terrain;
import fr.dow.mapviewer.display.params.terrain.PathSizeOverlayDisPars;

public class JoglOverlayPathSize extends JoglOverlayData {
	
	public JoglOverlayPathSize(Terrain terrain, PathSizeOverlayDisPars pathSizeOverlayDisPars){
		
		Color[] colorTable = new Color[8];
		colorTable[PathSizeMap.PATH0]=pathSizeOverlayDisPars.path0Color;
		colorTable[PathSizeMap.PATH1]=pathSizeOverlayDisPars.path1Color;
		colorTable[PathSizeMap.PATH3]=pathSizeOverlayDisPars.path3Color;
		colorTable[PathSizeMap.PATH5]=pathSizeOverlayDisPars.path5Color;
		colorTable[PathSizeMap.PATH7]=pathSizeOverlayDisPars.path7Color;

		PathSizeMap passMap=terrain.pathFindingInformation.pathSizeMap;
		
		int terrainDataWidth = passMap.imax;
		int terrainDataHeight = passMap.jmax;

		ByteArrayInputStream terrainData=passMap.get_Data();
		
	    scale=((float) terrainDataWidth) / ( (float) terrainDataWidth + 2 );
		
	    createOverlayPicture(terrainData, colorTable, new Color(0,0,255,128), terrainDataWidth, terrainDataHeight);
	}
	
}
