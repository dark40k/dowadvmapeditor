package fr.dow.mapviewer.display.joglobject.jogloverlay;

import javax.media.opengl.GL2;
import javax.media.opengl.GLException;

import fr.dow.gamedata.sgbmap.terrain.Terrain;
import fr.dow.mapviewer.display.joglobject.JoglTiles;
import fr.dow.mapviewer.display.params.terrain.TerrainDisPars;

public class JoglOverlay {

	public int genList; // pointer sur la display list JOGL

	public int drawTiles; // pointer to display list that draws tiles
	
	public int texPtr; // pointer on OpenGL texture

	JoglOverlayData joglOverlayData=null;
	
	public boolean isVisible;
	
	public JoglOverlay(GL2 gl, JoglTiles joglTiles, Terrain terrain, TerrainDisPars terrainDisplayParams)  throws GLException {

		genList = gl.glGenLists(1);
		
		drawTiles = gl.glGenLists(1);
		
		int[] textureId = new int[1];
		gl.glGenTextures( 1, textureId, 0 );
		texPtr=textureId[0];

		resetTerrain(gl,joglTiles,terrain);
		
		update(gl,terrain,terrainDisplayParams);

	}

	public void display(GL2 gl){
		if (joglOverlayData!=null) joglOverlayData.display(gl);
	}


	public void update(GL2 gl, Terrain terrain,TerrainDisPars terrainDisplayParams) {

		
		//
		// get overlay data
		//
		
		if (terrainDisplayParams.coverOverlay.isVisible) 
			joglOverlayData=new JoglOverlayDataCover(terrain.ground.terrainType, terrainDisplayParams.coverOverlay);
		else if (terrainDisplayParams.footfallOverlay.isVisible) 
			joglOverlayData=new JoglOverlayDataFootfall(terrain.ground.terrainType, terrainDisplayParams.footfallOverlay);
		else if (terrainDisplayParams.impassOverlay.isVisible) 
			joglOverlayData=new JoglOverlayDataImpass(terrain.ground,terrainDisplayParams.impassOverlay);
		else if (terrainDisplayParams.passabilityDisPars.isVisible)
			joglOverlayData=new JoglOverlayDataPassability(terrain,terrainDisplayParams.passabilityDisPars);
		else if (terrainDisplayParams.preciseDisPars.isVisible)
			joglOverlayData=new JoglOverlayPathSize(terrain,terrainDisplayParams.preciseDisPars);
		else if (terrainDisplayParams.pathDisPars.isVisible)
			joglOverlayData=new JoglOverlayDataPathSector(terrain,terrainDisplayParams.pathDisPars);
		else {
			joglOverlayData=null;
			return;
		}
		
		joglOverlayData.updateOverlayGenList(gl, this);
		
	}

	public void resetTerrain(GL2 gl, JoglTiles joglTiles, Terrain terrain) {
		gl.glNewList( drawTiles, GL2.GL_COMPILE );
		for (int tileX=0; tileX<terrain.ground.texTiles.get_texTileNX(); tileX++)
			for (int tileZ=0; tileZ<terrain.ground.texTiles.get_texTileNZ(); tileZ++) {
				joglTiles.display(gl,tileX,tileZ);
			}
		gl.glEndList();				
	}

}
