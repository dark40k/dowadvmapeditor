package fr.dow.mapviewer.display.joglobject.jogloverlay;

import java.awt.Color;
import java.io.ByteArrayInputStream;

import javax.media.opengl.GL2;

import com.jogamp.opengl.util.gl2.GLUT;

import fr.dow.gamedata.sgbmap.terrain.GroundData;
import fr.dow.gamedata.sgbmap.terrain.HeightMap;
import fr.dow.gamedata.sgbmap.terrain.PathSectorMap;
import fr.dow.gamedata.sgbmap.terrain.Terrain;
import fr.dow.mapviewer.display.params.terrain.PathOverlayDisPars;

public class JoglOverlayDataPathSector extends JoglOverlayData {

	private float[] xCoord;
	private float[][] yCoord;
	private float[] zCoord;
	
	private float[] xBorder;
	private float[][] yBorder;
	private float[] zBorder;
	
	private int[][] zoneValue;
	
	private String[][] zoneText;
	
	private float textColorR;
	private float textColorG;
	private float textColorB;
	
	private float borderColorR;
	private float borderColorG;
	private float borderColorB;
	
	private GLUT glut = new GLUT();
	
	public JoglOverlayDataPathSector(Terrain terrain, PathOverlayDisPars pathOverlayDisPars) {
		
		textColorR=pathOverlayDisPars.textColor.getRed() / 255f;
		textColorG=pathOverlayDisPars.textColor.getGreen() / 255f;
		textColorB=pathOverlayDisPars.textColor.getBlue() / 255f;
		
		borderColorR=pathOverlayDisPars.borderColor.getRed() / 255f;
		borderColorG=pathOverlayDisPars.borderColor.getGreen() / 255f;
		borderColorB=pathOverlayDisPars.borderColor.getBlue() / 255f;
		
		PathSectorMap precMap=terrain.pathFindingInformation.pathSectorMap;
		
		GroundData groundData=terrain.ground.groundData;
		
		// calculate X coord
		xCoord=new float[precMap.imax];
		float xOffset=(1f-(float)precMap.imax)/2f*groundData.get_cellsize();
		for (int i=0; i<precMap.imax; i++ ) xCoord[i]= i*groundData.get_cellsize() + xOffset;
		
		// calculate Z coord
		zCoord=new float[precMap.jmax];
		float yOffset=(1f-(float)precMap.jmax)/2f*groundData.get_cellsize();
		for (int j=0; j<precMap.jmax; j++ ) zCoord[j]= -j*groundData.get_cellsize() - yOffset;
		
		// calculate Y coord + Data
		HeightMap hmap=terrain.ground.heightMap;
		
		yCoord=new float[precMap.imax][precMap.jmax];
		zoneValue=new int[precMap.imax][precMap.jmax];
		
		int i0=(hmap.get_hmMaxI()-precMap.imax)/2;
		int j0=(hmap.get_hmMaxI()-precMap.jmax)/2;

		for (int i=0; i<precMap.imax; i++ )
			for (int j=0; j<precMap.jmax; j++ )
				zoneValue[i][j]=precMap.getData(i, j);
		
		//
		// Calculate Sectors Text
		// 
		
		if (pathOverlayDisPars.showText) {
			zoneText=new String[precMap.imax][precMap.jmax];
			for (int i=0; i<precMap.imax; i++ )
				for (int j=0; j<precMap.jmax; j++ ) {
					yCoord[i][j]=hmap.get_height(i0+i, j0+j)+1f;
					zoneText[i][j]=Integer.toHexString(zoneValue[i][j]);
				}
		}
		
		//
		// calculate Sectors Borders
		//

		xBorder=new float[precMap.imax+1];
		yBorder=new float[precMap.imax+1][precMap.jmax+1];
		zBorder=new float[precMap.jmax+1];

		for (int i=0; i<=precMap.imax; i++ ) xBorder[i]=hmap.get_heightGrid_Xaxis()[i0+i];

		for (int j=0; j<=precMap.jmax; j++ ) zBorder[j]=hmap.get_heightGrid_Xaxis()[j0+j];
		
		for (int i=0; i<=precMap.imax; i++ )
			for (int j=0; j<=precMap.jmax; j++ )
				yBorder[i][j]=hmap.get_height(i0+i, j0+j)+0.2f;

		//
		// calculate Sectors Overlay
		//
		
		Color[] colorTable = new Color[2];
		colorTable[0]=pathOverlayDisPars.defaultSectorColor;
		colorTable[1]=pathOverlayDisPars.sector1Color;
		
		byte[] blockedArea=new byte[precMap.imax*precMap.jmax];
		
		for (int i=0; i<precMap.imax; i++ )
			for (int j=0; j<precMap.jmax; j++ ) 
				if (zoneValue[i][j]==1) 
					blockedArea[i+j*precMap.imax]=  1;
		
		ByteArrayInputStream terrainData=new ByteArrayInputStream(blockedArea);;
		
	    scale=((float) precMap.imax) / ( (float) precMap.imax + 2f );
		
	    createOverlayPicture(terrainData, colorTable, new Color(0,0,255,128), precMap.imax, precMap.jmax);
		
	}
	
	/**
	public void updateOverlayGenList(GL2 gl, JoglOverlay joglOverlay) {
	}
	*/
	
	public void display(GL2 gl) {
 		
		super.display(gl);
		
        gl.glDisable(GL2.GL_LIGHTING);
        
		gl.glPixelStorei(GL2.GL_UNPACK_ALIGNMENT, 4);
		
	    gl.glColor4f(textColorR, textColorG, textColorB, 1f);
	    
	    // Draw text
	    if (zoneText!=null) {
			for (int i=0;i<xCoord.length;i++)
				for (int j=0;j<zCoord.length;j++) {
					gl.glRasterPos3f(xCoord[i],yCoord[i][j],zCoord[j]);
					glut.glutBitmapString(GLUT.BITMAP_8_BY_13, zoneText[i][j] );
				}
	    }
	    
		// Draw lines

	    gl.glColor4f(borderColorR, borderColorG, borderColorB, 1f);
		
		gl.glBegin (GL2.GL_LINES);
		
		int imax=xCoord.length-1;
		int jmax=zCoord.length-1;
		for (int i=0;i<imax;i++)
			for (int j=0;j<=jmax;j++) {
				int i1=i+1;
				int j1=j+1;
				if (zoneValue[i][j] != zoneValue[i1][j]) {
					gl.glVertex3f( xBorder[i1], yBorder[i1][j], -zBorder[j]);
					gl.glVertex3f( xBorder[i1], yBorder[i1][j1], -zBorder[j1]);
				}
			}

		for (int i=0;i<=imax;i++)
			for (int j=0;j<jmax;j++) {
				int i1=i+1;
				int j1=j+1;
				if (zoneValue[i][j] != zoneValue[i][j1]) {
					gl.glVertex3f( xBorder[i], yBorder[i][j1], -zBorder[j1]);
					gl.glVertex3f( xBorder[i1], yBorder[i1][j1], -zBorder[j1]);
				}
			}
		
		gl.glEnd();
		
        gl.glEnable(GL2.GL_LIGHTING);
	}

}
