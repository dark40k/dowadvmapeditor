package fr.dow.mapviewer.display.joglobject.jogloverlay;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;

import javax.media.opengl.GL2;

import com.jogamp.common.nio.Buffers;

public class JoglOverlayData {

	public int genList;
	
	public float scale=0.5f;
	public ByteBuffer overlayPicture=null;
	public int overlayPictureWidth=0;
	public int overlayPictureHeight=0;
	
	public void createOverlayPicture(ByteArrayInputStream terrainData, Color[] colorTable, Color borderColor, int dataWidth, int dataHeight) {

		overlayPictureWidth=dataWidth+2;
		overlayPictureHeight=dataHeight+2;
		
		overlayPicture = Buffers.newDirectByteBuffer(overlayPictureWidth*overlayPictureHeight*4);

		terrainData.reset();
		
		int rowMax=dataHeight+1;
		int colMax=dataWidth+1;

		int borderPackedPixel=borderColor.getRGB();
		byte bpp1=(byte) ((borderPackedPixel >> 16) & 0xFF);
		byte bpp2=(byte) ((borderPackedPixel >> 8) & 0xFF);
		byte bpp3=(byte) ((borderPackedPixel >> 0) & 0xFF);
		byte bpp4=(byte) ((borderPackedPixel >> 24) & 0xFF);

		for (int col = 0; col < dataWidth+2; col++) { overlayPicture.put(bpp1); overlayPicture.put(bpp2); overlayPicture.put(bpp3); overlayPicture.put(bpp4); }

		for (int row = 1; row < rowMax; row++) {

			overlayPicture.put(bpp1); overlayPicture.put(bpp2); overlayPicture.put(bpp3); overlayPicture.put(bpp4);

			for (int col = 1; col < colMax; col++) {
				int posData=terrainData.read();
				int packedPixel=colorTable[posData].getRGB();
				overlayPicture.put((byte) ((packedPixel >> 16) & 0xFF));
				overlayPicture.put((byte) ((packedPixel >> 8) & 0xFF));
				overlayPicture.put((byte) ((packedPixel >> 0) & 0xFF));
				overlayPicture.put((byte) ( ( ((col & 1)==0) ^ ((row & 1)==0 ) ) ? 150 : 200 ) ) ;
			}

			overlayPicture.put(bpp1); overlayPicture.put(bpp2); overlayPicture.put(bpp3); overlayPicture.put(bpp4);	    
		}

		for (int col = 0; col < dataWidth+2; col++) { overlayPicture.put(bpp1); overlayPicture.put(bpp2); overlayPicture.put(bpp3); overlayPicture.put(bpp4); }

		overlayPicture.rewind();

	}
	
	public void display(GL2 gl){
		gl.glCallList(genList);
	}
	
	public void updateOverlayGenList(GL2 gl, JoglOverlay joglOverlay) {
		
		genList=joglOverlay.genList;
		
		//
		// udate overlay texture
		//
		
		gl.glBindTexture(GL2.GL_TEXTURE_2D, joglOverlay.texPtr);
	    gl.glPixelStorei(GL2.GL_UNPACK_ALIGNMENT, 1);
	    
		gl.glTexImage2D(GL2.GL_TEXTURE_2D, 0, GL2.GL_RGBA, overlayPictureWidth, overlayPictureHeight, 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, overlayPicture);
		
	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP);
	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP);
	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST);
	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_NEAREST);

	    
	    //
	    // update overlay display
	    //
	    
		gl.glNewList( joglOverlay.genList, GL2.GL_COMPILE );

		gl.glEnable(GL2.GL_BLEND);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
        
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, joglOverlay.texPtr);
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);
		
		gl.glMatrixMode(GL2.GL_TEXTURE);
		gl.glPushMatrix();
		
		float[] transf={
				2*scale, 0f, 0f, 0f,
				0f, 2*scale, 0f, 0f,
				0f, 0f, 1f, 0f,
				0.5f-scale, 0.5f-scale, 0f, 1f };

		gl.glLoadMatrixf(transf, 0);
		
		gl.glCallList(joglOverlay.drawTiles); // render ground

		gl.glPopMatrix();
		
		gl.glDisable(GL2.GL_TEXTURE_2D);
		gl.glDisable(GL2.GL_BLEND);
		gl.glMatrixMode(GL2.GL_MODELVIEW);

		gl.glEndList();			
	}
	
}
