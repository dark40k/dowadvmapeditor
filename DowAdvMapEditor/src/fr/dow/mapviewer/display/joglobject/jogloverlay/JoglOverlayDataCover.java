package fr.dow.mapviewer.display.joglobject.jogloverlay;

import java.awt.Color;
import java.io.ByteArrayInputStream;

import fr.dow.gamedata.sgbmap.terrain.TerrainType;
import fr.dow.mapviewer.display.params.terrain.CoverOverlayDisPars;

public class JoglOverlayDataCover extends JoglOverlayData{

	public JoglOverlayDataCover(TerrainType terrainType, CoverOverlayDisPars displayParameters){
		
		Color[] colorTable=new Color[6];
		colorTable[TerrainType.COVERNONE]=displayParameters.noneColor;
		colorTable[TerrainType.COVERLIGHT]=displayParameters.lightColor;
		colorTable[TerrainType.COVERHEAVY]=displayParameters.heavyColor;
		colorTable[TerrainType.COVERNEGATIVE]=displayParameters.negativeColor;
		colorTable[TerrainType.COVERBLOCKING]=displayParameters.blockingColor;
		colorTable[TerrainType.COVERSTEALTH]=displayParameters.stealthColor;

		int terrainDataWidth=terrainType.get_coverSizeX();
		int terrainDataHeight=terrainType.get_coverSizeZ();

		ByteArrayInputStream terrainData=terrainType.get_coverData();
		
	    scale=((float) terrainDataWidth) / ( (float) terrainDataWidth + 2 )*((float) terrainDataWidth) / ( (float) terrainDataWidth + 1 );

	    createOverlayPicture(terrainData, colorTable, new Color(0,0,255,128), terrainDataWidth, terrainDataHeight);
		
	}
	
}
