package fr.dow.mapviewer.display.joglobject.joglentity;

import javax.media.opengl.GL2;

import fr.dow.gamedata.sgbmap.terrain.Ground;
import fr.dow.gamedata.whemodel.SelUI;
import fr.dow.gamedata.whmmodel.Bvol;

public class JoglSelUI {

	public final static int SELCIRCLE=0;
	public final static int SELRECTANGLE=1;
	
	private static final int circleSegNbr = 12;
	private static final double circleStepAng = -2*Math.PI/circleSegNbr;
	private static final int selBorderSegNbr = 10;
	
	public int selType;
	
	public float scaleX;
	public float scaleZ;
	public float centerX;
	public float centerZ;
	
	public JoglSelUI(SelUI selUI) {
		selType=selUI.selType;
		scaleX=selUI.selScaleX;
		scaleZ=selUI.selScaleX;
		centerX=selUI.selCenterX;
		centerZ=selUI.selCenterZ;
	}

	public JoglSelUI(Bvol bvol) {
		selType=SELRECTANGLE;
		scaleX=bvol.scaleX;
		scaleZ=bvol.scaleZ;
		centerX=bvol.centerX;
		centerZ=bvol.centerZ;
	}

	public void displaySelected(GL2 gl, float[] transf, Ground ground,float offset) {

		if (selType==SELCIRCLE) {
			
			gl.glBegin (GL2.GL_LINE_LOOP);

			for (int i=0;i<circleSegNbr;i++) {
				float x0=(float) (centerX+Math.max(scaleX, scaleZ)*Math.cos(i*circleStepAng));
				float z0=(float) (centerZ+Math.max(scaleX, scaleZ)*Math.sin(i*circleStepAng));
				createVertexWorldCoord(gl,x0,z0,transf,ground,offset);
			}

			gl.glEnd();

		}
		else {
			
			
			gl.glBegin (GL2.GL_LINE_STRIP);
			
			drawLineStrip(gl, centerX+scaleX, centerZ+scaleZ, centerX-scaleX, centerZ+scaleZ, selBorderSegNbr,transf,ground,offset);
			drawLineStrip(gl, centerX-scaleX, centerZ+scaleZ, centerX-scaleX, centerZ-scaleZ, selBorderSegNbr,transf,ground,offset);
			drawLineStrip(gl, centerX-scaleX, centerZ-scaleZ, centerX+scaleX, centerZ-scaleZ, selBorderSegNbr,transf,ground,offset);
			drawLineStrip(gl, centerX+scaleX, centerZ-scaleZ, centerX+scaleX, centerZ+scaleZ, selBorderSegNbr,transf,ground,offset);

			gl.glEnd();
			
		}
			
	}
	
	private void drawLineStrip(GL2 gl, float x1,float z1,float x2,float z2,int segNbr, float[] transf, Ground ground,float offset) {
		float dx=(x2-x1)/segNbr;
		float dz=(z2-z1)/segNbr;
		float x= x1;
		float z= z1;
		for (int i=0;i<=segNbr;i++) {
			createVertexWorldCoord(gl,x,z,transf,ground,offset);
			x+=dx;
			z+=dz;
		}		
	}
	
	private void createVertexWorldCoord(GL2 gl, float x, float z, float[] transf, Ground ground,float offset) {
		float x1=   x*transf[0] + z*transf[8]  + transf[12];
		float z1= - x*transf[2] - z*transf[10] - transf[14];
		gl.glVertex3f( x1,ground.getY(x1, z1)+offset,-z1);
	}
	
	public void drawPickable(GL2 gl, float[] transf, Ground ground) {

		if (selType==SELCIRCLE) {
			
			gl.glBegin (GL2.GL_TRIANGLE_FAN );

			for (int i=0;i<circleSegNbr;i++) {
				float x0=(float) (centerX+Math.max(scaleX, scaleZ)*Math.cos(i*circleStepAng));
				float z0=(float) (centerZ+Math.max(scaleX, scaleZ)*Math.sin(i*circleStepAng));
				createVertexWorldCoord(gl,x0,z0,transf,ground,0f);
			}

			gl.glEnd();

		}
		else {
			
			gl.glBegin (GL2.GL_TRIANGLE_FAN );
			
			createVertexWorldCoord(gl,centerX+scaleX, centerZ-scaleZ,transf,ground,0f);
			createVertexWorldCoord(gl,centerX-scaleX, centerZ-scaleZ,transf,ground,0f);
			createVertexWorldCoord(gl,centerX-scaleX, centerZ+scaleZ,transf,ground,0f);
			createVertexWorldCoord(gl,centerX+scaleX, centerZ+scaleZ,transf,ground,0f);
			
			gl.glEnd();
			
		}
			
	}
	
	

}
