package fr.dow.mapviewer.display.joglobject.joglentity;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.util.HashSet;

import javax.media.opengl.GL2;

import fr.dow.gamedata.filesystem.DowMod;

import fr.dow.gamedata.rgdattribute.AttributeString;
import fr.dow.gamedata.rgdattribute.AttributeTable;
import fr.dow.gamedata.rgdattribute.RgdAttribute;

import fr.dow.gamedata.whemodel.LinkedAnim;
import fr.dow.gamedata.whemodel.WheModel;

import fr.dow.gamedata.whmmodel.SubModelWHM;
import fr.dow.gamedata.whmmodel.WhmModel;
import fr.dow.gamedata.whmmodel.anim.Anim;

public class JoglEntity {

	private static final String defaultEntityModel="missingmodel";
	
	public String baseName;

	private String blueprint;
	
	public JoglSelUI joglSelUI;
	
	public JoglSubModel[] joglSubModels;
	
	public JoglEntityTextures entityTextures;
	
	public HashSet<String> hiddenMeshes;
	
	public Integer genListTextured;
	public Integer genListRaw;

	public String getBlueprint() {
		return blueprint;
	}
	
	public static JoglEntity loadRgd(GL2 gl, DowMod modData, String blueprint) {

		JoglEntity joglEntity=null;
		
		System.out.println("Loading Entity: " + blueprint);
		
		try {
		
		//
		// load attribute file (rgd) => get model file names (whm+whe)
		//
		String rgdFileName=blueprint.substring(5).replace('\\','/');
		AttributeTable entityAttr=RgdAttribute.readRGD(new ByteArrayInputStream(modData.getFileData(rgdFileName))); 
		AttributeTable blueprintAttr=(AttributeTable) entityAttr.subData("entity_blueprint_ext");
		AttributeString animatorAttr=(AttributeString) blueprintAttr.subData("animator");
		
		joglEntity=loadWhm(gl,modData,animatorAttr.data());
		
		} catch (Exception e) {
			System.err.println("Warning - could not load "+ blueprint + " - Switching to default model = " + defaultEntityModel);
			
			try { 
				joglEntity=loadWhm(gl,modData,defaultEntityModel);
			}
			catch (Exception e1) {
				throw new RuntimeException("Error - could not load "+ defaultEntityModel); 
			}
		}				
		
		joglEntity.blueprint=blueprint;
		return joglEntity;
	}

	private static JoglEntity loadWhm(GL2 gl, DowMod modData, String filename) throws IOException {

		String baseFilename=filename.toLowerCase().replace('\\','/');
		
		JoglEntity joglEntity=new JoglEntity();
		
		joglEntity.baseName=baseFilename;
		
		//
		// load whm file (model description)
		//
		WhmModel whmModel = new WhmModel("art/ebps/"+baseFilename+".whm",modData);
		
		//
		// load textures
		//
		joglEntity.entityTextures=new JoglEntityTextures(gl, whmModel, modData);
		
		//
		// if whe available,
		// =>init visibility 
		// =>get selection borders 
		//
		joglEntity.readWHE("art/ebps/"+baseFilename+".whe", whmModel, modData);			
		
		if (joglEntity.joglSelUI==null) {
			joglEntity.joglSelUI=new JoglSelUI(whmModel._bvol);
			//System.out.println("NO WHE DATA for " + blueprint);
		}
		
		//
		// create submodels
		//
		joglEntity.joglSubModels=new JoglSubModel[whmModel._subModels.size()];
		int joglSubModelIndex=0;
		for (SubModelWHM subModel : whmModel._subModels.values()) joglEntity.joglSubModels[joglSubModelIndex++]=new JoglSubModel(gl,subModel,joglEntity.entityTextures);
		
		//
		// create genlists
		//
		joglEntity.genListTextured = gl.glGenLists(1);
		joglEntity.genListRaw = gl.glGenLists(1);

		//
		// Fill genlists
		//
		joglEntity.updateGenList(gl);
		
		return joglEntity;
	}
	
	public void updateGenList(GL2 gl) {

		//
		// store invisible list
		//
		//for (JoglSubModel joglSubModel : joglSubModels)
		//	if (!hiddenMeshes.contains(joglSubModel.name)) 
				
		
		//
		// update textured list
		//
		
		gl.glNewList( genListTextured, GL2.GL_COMPILE );
		
		gl.glEnable(GL2.GL_BLEND);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		
		gl.glEnable(GL2.GL_ALPHA_TEST);
		gl.glAlphaFunc(GL2.GL_GREATER, 0.0f);
		
		for (JoglSubModel joglSubModel : joglSubModels)
			if (!hiddenMeshes.contains(joglSubModel.name)) 
				joglSubModel.displayTextured(gl);

		gl.glDisable(GL2.GL_BLEND);
		gl.glDisable(GL2.GL_ALPHA_TEST);
		
		gl.glEndList();
		
		//
		// update raw list (no texture)
		//
		
		gl.glNewList( genListRaw, GL2.GL_COMPILE );
		
		for (JoglSubModel joglSubModel : joglSubModels)
			if (!hiddenMeshes.contains(joglSubModel.name)) 
				joglSubModel.displayRaw(gl);
		
		gl.glEndList();
		
	}
	
	//
	// update display for entity
	// 
	public void displayTextured(GL2 gl) {
		gl.glCallList(genListTextured);
	}
	
	public void displayRaw(GL2 gl) {
		gl.glCallList(genListRaw);
	}
	
	//
	// Delete data from GL memory
	//
	public void clearFromMemory(GL2 gl) {
		for (JoglSubModel joglSubModel : joglSubModels)
				joglSubModel.clearFromMemory(gl);
		
		gl.glDeleteLists(genListTextured, 1);
		genListTextured=null;
		gl.glDeleteLists(genListRaw, 1);
		genListRaw=null;
	}
	//
	// fills a table with all parts names that are to be set to invisible on start
	//
	// visibility is recovered from anim parameters
	//
	private void readWHE(String wheFilename, WhmModel whmModel, DowMod modData) throws IOException{
		
		String animName=null;
		String animSource=null;
		
		hiddenMeshes=new HashSet<String>();
		
		//extract whe
		byte[] wheData=modData.getFileData(wheFilename);
		
		// exit if whe not found/accessible
		if (wheData==null) return;
			
		// read whe data
		WheModel wheModel = new WheModel(new ByteArrayInputStream(wheData));
		
		// extract selection data
		joglSelUI=new JoglSelUI(wheModel.selUI);
		
		// extract anim name from whe file
		animName=wheModel.getIdleAnimation();
		
		// exit if animName is not defined
		if (animName == null) return;
		
		// check if anim is linked => follow link
		LinkedAnim linkedAnim=wheModel.linkedAnims.get(animName.toLowerCase());
		if (linkedAnim!=null) {
			animName=linkedAnim.animName;
			animSource=linkedAnim.sourceModel;
		}

		//
		// get anim from whm
		//
		Anim anim=null;
		if (animSource==null) anim=whmModel._anims.get(animName.toLowerCase());
		else anim=WhmModel.findAnim(animName,animSource,modData);

		// exit if no anim found
		if (anim == null) return;
			
		//
		// extract visibility data from anim
		//
		//System.out.println("	=> Extracting visibility from anim ="+animName);	
		HashSet<String>	invisibleMeshes=anim.getInvisibleMeshes();
			
		// return empty if all models are included in anim (nothing to show)
		if (invisibleMeshes.containsAll(whmModel._subModels.keySet())) return;

		// send back visibility data
		hiddenMeshes=invisibleMeshes;
		
	}
		
}
