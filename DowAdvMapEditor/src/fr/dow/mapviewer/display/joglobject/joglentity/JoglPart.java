package fr.dow.mapviewer.display.joglobject.joglentity;

import javax.media.opengl.GL2;

public class JoglPart {
	
	public int TextureID;
	public Integer genList;

	public JoglPart(GL2 gl, int TextureID, int[] vertexIndexList, float[][] vpos, float[][] npos, float[][] tpos ) {
		
		// store texture ID
		this.TextureID=TextureID;
		
		// create genList
		genList = gl.glGenLists(1);
		gl.glNewList(genList, GL2.GL_COMPILE );
		
    	gl.glBegin(GL2.GL_TRIANGLES);
		for (int vertexIndex : vertexIndexList) {
			gl.glNormal3f( npos[vertexIndex][0],  npos[vertexIndex][1], npos[vertexIndex][2]);
			gl.glMultiTexCoord2f(GL2.GL_TEXTURE0, tpos[vertexIndex][0], tpos[vertexIndex][1]);
			gl.glVertex3f( vpos[vertexIndex][0],  vpos[vertexIndex][1], vpos[vertexIndex][2]);
		}
        gl.glEnd ();

        gl.glEndList();
        
	}
	
	public void display(GL2 gl) {
		gl.glCallList(genList);
	}

	public void clearFromMemory(GL2 gl) {
		gl.glDeleteLists(genList, 1);
		genList=null;
	}
	
}
