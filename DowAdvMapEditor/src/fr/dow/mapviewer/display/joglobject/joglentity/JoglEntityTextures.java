package fr.dow.mapviewer.display.joglobject.joglentity;

import java.util.LinkedHashMap;

import javax.media.opengl.GL2;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.gamedata.texture.DdsTexture;
import fr.dow.gamedata.texture.RshTexture;
import fr.dow.gamedata.texture.TgaTexture;
import fr.dow.gamedata.whmmodel.WhmModel;

public class JoglEntityTextures {
	
	public LinkedHashMap<String, Integer> textureList;
	
	public JoglEntityTextures(GL2 gl, WhmModel whmModel, DowMod modData) {
		
		textureList=new LinkedHashMap<String, Integer>();
		
		//
		// extract indirect textures from whm model
		//
		// Warning: assumes that there is only one texture in an rsh file for SSHR indirect texture
		//
		for (RelicChunk SSHR : whmModel.linkedShaderChunks) textureList.put(SSHR.get_name().trim(), loadTexture(gl, ((RelicChunkDATA) SSHR).dataGetString(), modData));

		//
		// extract direct textures from whm model
		//
		// Warning: assumes that 1st shader allways contain the texture to render
		//
		for (RelicChunk SHDR : whmModel.includedShaderChunks) {
			RelicChunkDATA CHAN=((RelicChunkFOLD) SHDR).subChunkDATA("CHAN");
			CHAN.dataPosition(12);
			String textureName=CHAN.dataGetString();
			
			if (textureName.length()==0) continue; //skip if no texture is specified
			
			RelicChunkFOLD TXTR=null;
			for (RelicChunk tmpTXTR : whmModel.textureIncludedChunks) 
				if (textureName.equalsIgnoreCase(tmpTXTR.get_name().trim())) {
					TXTR=(RelicChunkFOLD) tmpTXTR; break;
				}
			
			if (TXTR==null) { System.err.println("EntityTextures, could not find TXTR for "+textureName); continue; }
			
			textureList.put(SHDR.get_name().trim(), RshTexture.loadID(gl,TXTR));
		}
		
		//
		// Init textures
		//
		gl.glEnable(GL2.GL_TEXTURE_2D);
		for (int textureID : textureList.values()) 
			if (textureID!=0) {
				gl.glBindTexture(GL2.GL_TEXTURE_2D, textureID);
				gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_DECAL);
				gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
				gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT);
		        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR_MIPMAP_LINEAR);
		        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
			}
		gl.glDisable(GL2.GL_TEXTURE_2D);		
		
		
	}

	
	public int getTextureID(String textureName) {
		try {
			return textureList.get(textureName);
		}
		catch (NullPointerException e) {
			System.err.println("EntityTextures.getTextureID - could not find textureName "+textureName);
			return 0;
		}
	}
	
	private int loadTexture(GL2 gl, String texFilename, DowMod modData) {
		
		int TextureID=0;
		
		TextureID=RshTexture.loadID(gl,texFilename+".rsh", modData);
		if (TextureID!=0) return TextureID;
		
		TextureID=TgaTexture.loadID(texFilename+".tga", modData);
		if (TextureID!=0) return TextureID;
		
		TextureID=DdsTexture.loadID(texFilename+".dds", modData);
		if (TextureID!=0) return TextureID;
		
		return 0;
	}



}
