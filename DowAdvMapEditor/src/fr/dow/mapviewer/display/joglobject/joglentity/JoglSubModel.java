package fr.dow.mapviewer.display.joglobject.joglentity;

import javax.media.opengl.GL2;

import fr.dow.gamedata.whmmodel.SubModelWHM;

public class JoglSubModel {

	public Integer genListTextured;
	public Integer genListRaw;
	
	public String name;
	
	public JoglPart[] partList;
	
	public JoglSubModel(GL2 gl, SubModelWHM subModel, JoglEntityTextures entityTextures) {
		
		name=subModel.partName.trim().toLowerCase();
		
		//
		// init submodels
		//
		
		float[][] vpos=subModel.vertexPos;
		float[][] npos=subModel.vertexNormal;
		float[][] tpos=subModel.texCoord;
		
		int nParts=subModel.parts.length;
		
		partList=new JoglPart[nParts];
		for (int i=0;i<nParts;i++) {
			partList[i]=new JoglPart(gl,entityTextures.getTextureID(subModel.parts[i].textureName),subModel.parts[i].vertexIndexList, vpos,npos,tpos);
		}

		
		//
		// create raw display list (no texture)
		//
		
		genListRaw = gl.glGenLists(1);
		gl.glNewList(genListRaw, GL2.GL_COMPILE );
		for (JoglPart part : partList) part.display(gl);
        gl.glEndList();		
		
		//
		// create textured display list
		//
			
		genListTextured = gl.glGenLists(1);
		gl.glNewList(genListTextured, GL2.GL_COMPILE );

		int currentTexture=partList[0].TextureID;
		
		if (currentTexture!=0) {
			gl.glEnable(GL2.GL_TEXTURE_2D);
			gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);
			gl.glBindTexture(GL2.GL_TEXTURE_2D, currentTexture);
		}
		
		for (JoglPart part : partList) {
			if (currentTexture!=part.TextureID)
				if (part.TextureID==0) {
					gl.glDisable(GL2.GL_TEXTURE_2D);
					currentTexture=0;
				}
				else if (currentTexture==0) {
					gl.glEnable(GL2.GL_TEXTURE_2D);
					gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);
					currentTexture=part.TextureID;
					gl.glBindTexture(GL2.GL_TEXTURE_2D, currentTexture);
				}
				else {
					currentTexture=part.TextureID;
					gl.glBindTexture(GL2.GL_TEXTURE_2D, currentTexture);					
				}
			
			part.display(gl);
		}
		
		if (currentTexture!=0) {
			gl.glDisable(GL2.GL_TEXTURE_2D);
		}
		
        gl.glEndList();		
		
	}
	
	public void displayTextured(GL2 gl) {
		gl.glCallList(genListTextured);
	}
	
	public void displayRaw(GL2 gl) {
		gl.glCallList(genListRaw);
	}

	public void clearFromMemory(GL2 gl) {
		for (JoglPart part : partList)
			part.clearFromMemory(gl);
	
		gl.glDeleteLists(genListTextured, 1);
		genListTextured=null;
		gl.glDeleteLists(genListRaw, 1);
		genListRaw=null;
	}
	
}
