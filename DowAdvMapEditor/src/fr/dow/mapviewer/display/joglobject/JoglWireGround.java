package fr.dow.mapviewer.display.joglobject;

import javax.media.opengl.GL2;

import fr.dow.gamedata.sgbmap.terrain.Impass;
import fr.dow.mapviewer.display.renderer.MapViewerRenderer.DisplayEvent;
import fr.dow.mapviewer.display.renderer.MapViewerRenderer.DisplayListener;

public class JoglWireGround implements DisplayListener {

	private float[][] mapY;
	private float[] mapX;
	private float[] mapZ;
	private float scale;
	
	private int i1;
	private int j1;
	private int i2;
	private int j2;
	
	public JoglWireGround(float[][] mapY, float[] mapX, float[] mapZ,float scale ) {
		this.mapY=mapY;
		this.mapX=mapX;
		this.mapZ=mapZ;
		this.scale=scale;
	}
	
	public synchronized void setDisplayWindow(int i1, int j1, int i2, int j2) {
		this.i1=Math.max(i1, 0);
		this.j1=Math.max(j1, 0);
		this.i2=Math.min(i2,mapX.length);
		this.j2=Math.min(j2,mapZ.length);
	}
	
	@Override
	public synchronized void display(DisplayEvent evt) {
		
		if (mapY==null) return;
		
		if ( (i1==i2) | (j1==j2) ) return;
		
		GL2 gl=evt.gl;
		
		gl.glColor4f(0f, 1f, 0f, 1f);
		
        for (int i=i1;i<i2;i++)
            for (int j=j1;j<j2;j++)
            {
            	gl.glBegin(GL2.GL_LINE_LOOP);
            	
            	gl.glVertex3f(mapX[i], mapY[i][j], -mapZ[j]);
            	
            	gl.glVertex3f(mapX[i+1], mapY[i+1][j], -mapZ[j]);
            	
            	gl.glVertex3f(mapX[i+1], mapY[i+1][j+1], -mapZ[j+1]);
                
            	gl.glEnd();
            	
            }
		
    	gl.glBegin(GL2.GL_LINE_STRIP);
        for (int i=i1;i<=i2;i++) gl.glVertex3f(mapX[i], mapY[i][j2], -mapZ[j2]);
        gl.glEnd();
        
    	gl.glBegin(GL2.GL_LINE_STRIP);
        for (int j=j1;j<=j2;j++) gl.glVertex3f(mapX[i1], mapY[i1][j], -mapZ[j]);
        gl.glEnd();

		boolean[][] pass= Impass.calcPassAllowance(mapY,scale);

		gl.glColor4f(1f, 0f, 0f, 0.4f);
		gl.glEnable(GL2.GL_BLEND);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

        for (int i=i1;i<i2;i++)
            for (int j=j1;j<j2;j++)
            	if (!pass[i][j])
		            {
		            	gl.glBegin(GL2.GL_TRIANGLE_STRIP);
		            	
		            	gl.glVertex3f(mapX[i+1], mapY[i+1][j], -mapZ[j]);
		            	gl.glVertex3f(mapX[i], mapY[i][j], -mapZ[j]);
		            	gl.glVertex3f(mapX[i+1], mapY[i+1][j+1], -mapZ[j+1]);
		            	gl.glVertex3f(mapX[i], mapY[i][j+1], -mapZ[j+1]);
		            	
		            	gl.glEnd();
		            	
		            }
        
		gl.glDisable(GL2.GL_BLEND);
		
	}
	
}
