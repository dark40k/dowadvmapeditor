package fr.dow.mapviewer.display.joglobject;

import javax.media.opengl.GL2;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.mapviewer.display.params.MapDisPars;

public class JoglWorld {
	
	public MapDisPars mapDisplayParams;
	
	public JoglTerrain joglTerrain ;
	public JoglMapEntities joglMapEntities;
	public JoglMarkers joglMarkers;

	public JoglWorld(SgbMap map, DowMod modData, MapDisPars mapDisplayParams) {
		this.mapDisplayParams=mapDisplayParams;
	}
	
	public void init(GL2 gl, SgbMap map, DowMod modData) {
		joglTerrain = new JoglTerrain(gl, map.terrain, modData, mapDisplayParams.terrainDisplayParams);
		joglMapEntities = new JoglMapEntities(gl, map, modData, mapDisplayParams);
		joglMarkers = new JoglMarkers(gl, map, mapDisplayParams.markerDisPars);
	}
	
	public void draw(GL2 gl, SgbMap map, DowMod modData) {
		
        joglTerrain.display1(gl, map.terrain, modData);	// display terrain     
		
        joglMapEntities.display(gl, map, modData);	 // display entities
		
       	joglMarkers.display(gl,map.markers,map.terrain.ground);	// display markers
		
       	joglTerrain.display2(gl);	// display water 
		
	}

}
