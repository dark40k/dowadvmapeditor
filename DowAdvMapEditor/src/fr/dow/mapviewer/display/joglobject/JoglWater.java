package fr.dow.mapviewer.display.joglobject;

import javax.media.opengl.GL2;
import javax.media.opengl.GLException;

import com.jogamp.opengl.util.texture.Texture;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.terrain.Terrain;
import fr.dow.gamedata.texture.TgaTexture;

public class JoglWater {
	
	private Terrain _terrain;
	
	private int _waterTex;
	
	private final String _waterTexFile="art/scenarios/textures/water/genericwaves/anim_00000.tga";
	private final float _waterLevel=4.5f;
	
	private int _genList;

	public JoglWater(GL2 gl, Terrain terrain, DowMod modData) throws GLException {
		_terrain=terrain;

        //
        // Chargement de la texture pour l'eau
        //
		System.out.println("Loading Texture : " + _waterTexFile);
		
		Texture waterTexture=TgaTexture.load(_waterTexFile, modData);
		
		if (waterTexture!=null) {
			_waterTex=waterTexture.getTextureObject();
			gl.glBindTexture(GL2.GL_TEXTURE_2D, _waterTex);
			gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_DECAL);
			gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
			gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT);
	        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
	        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
		}
		else _waterTex=0;
		
		//
		// get genlist for water
		//
		_genList = gl.glGenLists(1);
		
		resetTerrain(gl,terrain);
	}
	
	public void display(GL2 gl){
		gl.glCallList(_genList);
	}

	public void resetTerrain(GL2 gl, Terrain terrain) {
		
		//
        // local data for water params
        //
        
		float texCoef=_terrain.terrainParams.get_waveRepeat();
		float red=_terrain.terrainParams.get_waveRed()/255f;
		float green=_terrain.terrainParams.get_waveGreen()/255f;
		float blue=_terrain.terrainParams.get_waveBlue()/255f;
		float alpha=_terrain.terrainParams.get_waveAlpha()/255f;

		//
		// Data map mesh
		//
		
		float mapX[]=_terrain.ground.heightMap.get_heightGrid_Xaxis();
		float[] waterTexX=new float[mapX.length];
		for (int i=0;i<waterTexX.length;i++)  waterTexX[i]=mapX[i]*texCoef;
		
		float mapZ[]=_terrain.ground.heightMap.get_heightGrid_Zaxis();
		float[] waterTexZ=new float[mapZ.length];
		for (int i=0;i<waterTexZ.length;i++)  waterTexZ[i]=mapZ[i]*texCoef;

		//
		// Start genlist for water
		//
		gl.glNewList( _genList, GL2.GL_COMPILE );
        
		gl.glEnable(GL2.GL_BLEND);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

		//gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_MODULATE);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, _waterTex);
        
		for (int i=1;i<mapX.length;i++)
			for (int j=1;j<mapZ.length;j++) {
				
            	gl.glBegin(GL2.GL_TRIANGLE_STRIP);
            	
            	gl.glColor4f(red, green, blue, alpha);
            	gl.glTexCoord2f(waterTexX[i-1], waterTexZ[j-1]);
            	gl.glVertex3f(mapX[i-1], _waterLevel, -mapZ[j-1]);
            	
            	gl.glColor4f(red, green, blue, alpha);
            	gl.glTexCoord2f(waterTexX[i-1], waterTexZ[j]);
            	gl.glVertex3f(mapX[i-1], _waterLevel, -mapZ[j]);
            	
            	gl.glColor4f(red, green, blue, alpha);
            	gl.glTexCoord2f(waterTexX[i], waterTexZ[j-1]);
            	gl.glVertex3f(mapX[i], _waterLevel, -mapZ[j-1]);
            	
            	gl.glColor4f(red, green, blue, alpha);
            	gl.glTexCoord2f(waterTexX[i], waterTexZ[j]);
            	gl.glVertex3f(mapX[i], _waterLevel, -mapZ[j]);
                
            	gl.glEnd();
				
			}
		
		gl.glDisable(GL2.GL_TEXTURE_2D);
		gl.glDisable(GL2.GL_BLEND);
		
        gl.glEndList();
        
	}
	
}
