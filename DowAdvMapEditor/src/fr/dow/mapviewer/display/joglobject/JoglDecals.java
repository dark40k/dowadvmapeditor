package fr.dow.mapviewer.display.joglobject;

import java.util.LinkedHashMap;

import javax.media.opengl.GL2;
import javax.media.opengl.GLException;

import com.jogamp.opengl.util.texture.Texture;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.terrain.Decal;
import fr.dow.gamedata.sgbmap.terrain.DecalFileLinks;
import fr.dow.gamedata.sgbmap.terrain.Terrain;
import fr.dow.gamedata.texture.DdsTexture;
import fr.dow.gamedata.texture.TgaTexture;

public class JoglDecals {

	private final static int selBorderSegNbr=10;
	
	private LinkedHashMap<Integer,DecalSelectionData> decalsSelectionData=new LinkedHashMap<Integer,DecalSelectionData>(); 
	
	private Terrain terrain;
	
	private int _genList; // pointer sur la display list JOGL
	
	private LinkedHashMap<Integer,Texture> _decalTex=new LinkedHashMap<Integer,Texture>(); // key=> pointer, data=>textureID
	
	public JoglDecals(GL2 gl, Terrain terrain, DowMod modData) throws GLException {
		
		_genList = gl.glGenLists(1);
		this.terrain=terrain;
		
		resetTerrain(gl,terrain,modData);
    		
	}

	public void display(GL2 gl){
		gl.glCallList(_genList);
	}	
	
	private void updateDecals(GL2 gl,DecalFileLinks mapDecalFiles,DowMod modData) {
		
		for (Integer decalPointer : mapDecalFiles.keySet()) {

			//check if decal ptr already exist
			if (_decalTex.containsKey(decalPointer)) continue;
			
			//load decal
			String decalFilename=mapDecalFiles.get(decalPointer);
			System.out.println("Loading Decal: " + decalPointer+ " => " + decalFilename);
			Texture newDecalTex=loadTexture(gl,decalFilename, modData);
			
			if (newDecalTex==null) {
				System.err.println("ERROR : Texture not found =>"+decalFilename);
				newDecalTex=loadTexture(gl,"art/decals/baddecal", modData);
			}
			
			gl.glBindTexture(GL2.GL_TEXTURE_2D, newDecalTex.getTextureObject());
			gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_DECAL);
			gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP_TO_BORDER);
			gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP_TO_BORDER);
	        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR_MIPMAP_LINEAR);
	        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);

			_decalTex.put(decalPointer, newDecalTex);
			
		}
		
	}
	
	private Texture loadTexture(GL2 gl, String texFilename, DowMod modData) {
		
		Texture newTex=null;
		
		newTex=TgaTexture.load(texFilename+".tga", modData);
		if (newTex!=null) return newTex;
		
		newTex=DdsTexture.load(texFilename+".dds", modData);
		if (newTex!=null) return newTex;
		
		return null;
	}

	public void resetTerrain(GL2 gl,Terrain terrain,DowMod modData) {
		
		//
		//update decal list
		//
		this.terrain=terrain;
		updateDecals(gl,terrain.decalFileLinks,modData);

		//
		// Data map mesh
		//
		
		float mapX[]=terrain.ground.heightMap.get_heightGrid_Xaxis();
		float mapZ[]=terrain.ground.heightMap.get_heightGrid_Zaxis();
		float mapY[][]=terrain.ground.heightMap.get_heightGrid();
		
		//
		// Start genlist
		//
		gl.glNewList( _genList, GL2.GL_COMPILE );
		
		//
		// prepare for decal
		//
		gl.glEnable(GL2.GL_BLEND);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		
		//
		// Set texture unit 0 for decal
		//
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);

		gl.glMatrixMode(GL2.GL_TEXTURE);

		for (Integer decalID : terrain.decals.keySet()) {

			Decal decal=terrain.decals.get(decalID);
			Texture decalTex=_decalTex.get(decal.get_decalPtr());
			if (decalTex==null) continue;
			
			// facteur pour texture
			float sx=1f/decalTex.getWidth()/decal.get_size();
			float sz=1f/decalTex.getHeight()/decal.get_size();
			
			float xc=decal.get_x();
			float zc=decal.get_z();
			
			float cos=(float) Math.cos(decal.get_angle());
			float sin=(float) Math.sin(-decal.get_angle());
			
			//
			// stockage donnees pour selection
			//
			DecalSelectionData tmpDecalSel=new DecalSelectionData();
			tmpDecalSel.cx=xc;
			tmpDecalSel.cz=zc;
			tmpDecalSel.dx1=0.5f*(cos/sx+sin/sz);
			tmpDecalSel.dz1=0.5f*(-sin/sx+cos/sz);
			tmpDecalSel.dx2=0.5f*(cos/sx-sin/sz);
			tmpDecalSel.dz2=0.5f*(-sin/sx-cos/sz);
			
			decalsSelectionData.put(decalID,tmpDecalSel);
			
			//
			// generation liste
			//
			float[] transf={
					cos*sx, sin*sz, 0f, 0f,
					-sin*sx, cos*sz, 0f, 0f,
					0f, 0f, 1f, 0f,
					0.5f+sx*(-xc*cos+zc*sin), 0.5f+sz*(-xc*sin-zc*cos), 0f, 1f };
			
			gl.glBindTexture(GL2.GL_TEXTURE_2D, decalTex.getTextureObject());
			
			gl.glPushMatrix();
			
			gl.glLoadMatrixf(transf, 0);
			
			float xdi=(Math.abs(cos/sx)+Math.abs(sin/sz)) / 2f / terrain.ground.groundData.get_cellsize(); // 1/2 max size of texture in X direction, index unit
			float zdj=(Math.abs(cos/sz)+Math.abs(sin/sx)) / 2f / terrain.ground.groundData.get_cellsize(); // 1/2 max size of texture in X direction, index unit
			float xci=terrain.ground.groundData.posToIndexX(xc); // texture center, index unit
			float zcj=terrain.ground.groundData.posToIndexZ(zc); // texture center, index unit
			
			int imin=(int) Math.max(Math.floor(xci-xdi),0f);
			int imax=(int) Math.min(Math.ceil(xci+xdi),mapX.length-1);
			int jmin=(int) Math.max(Math.floor(zcj-zdj),0);
			int jmax=(int) Math.min(Math.ceil(zcj+zdj),mapZ.length-1);
			
	        // create vertex table
	        for (int i=imin;i<imax;i++)
	            for (int j=jmin;j<jmax;j++)
	            {
	            	
	            	gl.glBegin(GL2.GL_TRIANGLE_STRIP);
	            	
	            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE0, mapX[i+1], mapZ[j]);
	            	gl.glVertex3f(mapX[i+1], mapY[i+1][j], -mapZ[j]);
	            	
	            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE0, mapX[i], mapZ[j]);
	            	gl.glVertex3f(mapX[i], mapY[i][j], -mapZ[j]);
	            	
	            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE0, mapX[i+1], mapZ[j+1]);
	            	gl.glVertex3f(mapX[i+1], mapY[i+1][j+1], -mapZ[j+1]);
	                
	            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE0, mapX[i], mapZ[j+1]);
	            	gl.glVertex3f(mapX[i], mapY[i][j+1], -mapZ[j+1]);
	            	
	            	gl.glEnd();
	            }
	        
			gl.glPopMatrix();
			
		}
		
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		
		gl.glDisable(GL2.GL_TEXTURE_2D);
		gl.glDisable(GL2.GL_BLEND);
		
        gl.glEndList();		
	}

	public class DecalSelectionData {
		float cx;
		float cz;
		float dx1;
		float dz1;
		float dx2;
		float dz2;
	}
	
	private void drawContour(GL2 gl, DecalSelectionData decData, float offset) {
		gl.glBegin (GL2.GL_LINE_STRIP);
		drawLineStrip(gl, decData.cx+decData.dx1, decData.cz+decData.dz1, decData.cx+decData.dx2, decData.cz+decData.dz2, selBorderSegNbr,offset);
		drawLineStrip(gl, decData.cx+decData.dx2, decData.cz+decData.dz2, decData.cx-decData.dx1, decData.cz-decData.dz1, selBorderSegNbr,offset);
		drawLineStrip(gl, decData.cx-decData.dx1, decData.cz-decData.dz1, decData.cx-decData.dx2, decData.cz-decData.dz2, selBorderSegNbr,offset);
		drawLineStrip(gl, decData.cx-decData.dx2, decData.cz-decData.dz2, decData.cx+decData.dx1, decData.cz+decData.dz1, selBorderSegNbr,offset);
		gl.glEnd();
	}
	
	private void drawLineStrip(GL2 gl, float x1,float z1,float x2,float z2,int segNbr,float offset) {
		float dx=(x2-x1)/segNbr;
		float dz=(z2-z1)/segNbr;
		float x=x1;
		float z=z1;
		for (int i=0;i<=segNbr;i++) {
			gl.glVertex3f( x,terrain.ground.getY(x, z)+offset,-z);
			x+=dx;
			z+=dz;
		}
	}

	public void drawPicked(GL2 gl, int id,float offset) {
		drawContour(gl, decalsSelectionData.get(id),offset);
	}
	
	public void drawPickable(GL2 gl) {
		
		for ( Integer id : decalsSelectionData.keySet()) {
			
			DecalSelectionData decData=decalsSelectionData.get(id);
			
			gl.glPushName(id);
			
			float x0=decData.cx+decData.dx1;
			float z0=decData.cz+decData.dz1;
			float y0=terrain.ground.getY(x0, z0);

			float x1=decData.cx+decData.dx2;
			float z1=decData.cz+decData.dz2;
			float y1=terrain.ground.getY(x1, z1);
			
			float x2=decData.cx-decData.dx1;
			float z2=decData.cz-decData.dz1;
			float y2=terrain.ground.getY(x2, z2);
			
			float x3=decData.cx-decData.dx2;
			float z3=decData.cz-decData.dz2;
			float y3=terrain.ground.getY(x3, z3);
			
        	gl.glBegin(GL2.GL_TRIANGLE_STRIP);
        	gl.glVertex3f(x0,y0,-z0);
        	gl.glVertex3f(x1,y1,-z1);
        	gl.glVertex3f(x3,y3,-z3);
        	gl.glVertex3f(x2,y2,-z2);
        	gl.glEnd();
        	
        	gl.glPopName();
        	
		}
		
	}
	
}
