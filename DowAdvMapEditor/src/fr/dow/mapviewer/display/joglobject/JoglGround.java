package fr.dow.mapviewer.display.joglobject;

import java.util.LinkedHashMap;

import javax.media.opengl.GL2;
import javax.media.opengl.GLException;

import com.jogamp.opengl.util.texture.Texture;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.terrain.GroundTexture;
import fr.dow.gamedata.sgbmap.terrain.Terrain;
import fr.dow.gamedata.sgbmap.terrain.TexTileFileLinks;
import fr.dow.gamedata.texture.DdsTexture;
import fr.dow.gamedata.texture.TgaTexture;

public class JoglGround {
	
	private int _genList; // pointer sur la display list JOGL

	private int _groundTex;
	
	private LinkedHashMap<Integer,Texture> _tileTex=new LinkedHashMap<Integer,Texture>(); // key=> pointer, data=>textureID
	
	public JoglGround(GL2 gl, JoglTiles joglTiles, Terrain terrain, DowMod modData) throws GLException {
		
		// Recuperation de la reference pour la texture de base 
		int[] textureId = new int[1];
		gl.glGenTextures( 1, textureId, 0 );
		_groundTex=textureId[0];
		
		// get genList nbr
		_genList = gl.glGenLists(1);

		// update display genlist
		resetTerrain(gl,joglTiles,terrain,modData);
    		
	}

	private void updateTexTiles(GL2 gl, TexTileFileLinks texTileFileLinks, DowMod modData) {
		
		for (Integer texPointer : texTileFileLinks.keySet()) {

			// check if texture key is not already defined (skip loading)
			if (_tileTex.containsKey(texPointer)) continue;
			
			String texFilename=texTileFileLinks.get(texPointer);
			
			System.out.println("Loading Tile: " + texPointer+ " => " + texFilename);
			Texture newtileTex=loadTexture(gl,texFilename, modData);
			
			if (newtileTex==null) {
				System.err.println("ERROR : Texture not found =>"+texFilename);
				continue;
			}
			
			gl.glBindTexture(GL2.GL_TEXTURE_2D, newtileTex.getTextureObject());
			gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_DECAL);
			gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
			gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT);
	        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR_MIPMAP_LINEAR);
	        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);

			_tileTex.put(texPointer, newtileTex);
			
		}
		
	}
	
	private void updateTexGround(GL2 gl, GroundTexture groundTexture) {
		
		System.out.println("Loading Ground Texture from memory");
		
		gl.glBindTexture(GL2.GL_TEXTURE_2D, _groundTex);
		gl.glTexImage2D(GL2.GL_TEXTURE_2D, 0, GL2.GL_RGBA, groundTexture.getWidth(), groundTexture.getHeight(), 0, GL2.GL_BGRA, GL2.GL_UNSIGNED_BYTE, groundTexture.getTexture());
		
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_DECAL);
		gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
		gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
		
	}
	
	public void resetTerrain(GL2 gl, JoglTiles joglTiles, Terrain terrain, DowMod modData) {

		updateTexGround(gl,terrain.ground.groundTexture);
		
		updateTexTiles(gl,terrain.ground.texTileFileLinks,modData);
        
		//
		// Start updating genList
		//
		gl.glNewList( _genList, GL2.GL_COMPILE );
		
		// Set texture unit 0 for ground texture
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_DECAL);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, _groundTex);
		
		// Set texture unit 1 for tile texture and leave focus on it (to allow changing texture on the fly)
		boolean tileTextureStatus=true;
		gl.glActiveTexture(GL2.GL_TEXTURE1);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_DECAL);
		
		// render ground
		for (int tileX=0; tileX<terrain.ground.texTiles.get_texTileNX(); tileX++)
			for (int tileZ=0; tileZ<terrain.ground.texTiles.get_texTileNZ(); tileZ++) {

				// change texture on the fly (focus is on Texture unit 1)
				Texture tileTex=_tileTex.get(terrain.ground.texTiles.get_texTilePtr(tileX, tileZ));

				if (tileTex!=null) {
					if (!tileTextureStatus) {
						gl.glEnable(GL2.GL_TEXTURE_2D);
						gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_DECAL);
						tileTextureStatus=true;
					}
					gl.glBindTexture(GL2.GL_TEXTURE_2D, tileTex.getTextureObject());
				}
				else if (tileTextureStatus) {
					gl.glDisable(GL2.GL_TEXTURE_2D);
					tileTextureStatus=false;
				}
				
				joglTiles.display(gl,tileX,tileZ);
			}
		
		if (tileTextureStatus) gl.glDisable(GL2.GL_TEXTURE_2D);
		
		gl.glActiveTexture(GL2.GL_TEXTURE0);
		gl.glDisable(GL2.GL_TEXTURE_2D);
		
		//
		// End of genList
		//
        gl.glEndList();	
        
	}
	
	public void display(GL2 gl){
		
		gl.glCallList(_genList);
		
	}
	
	private Texture loadTexture(GL2 gl, String texFilename, DowMod modData) {
		
		Texture newTex=null;
		
		newTex=TgaTexture.load(texFilename+".tga", modData);
		if (newTex!=null) return newTex;
		
		newTex=DdsTexture.load(texFilename+".dds", modData);
		if (newTex!=null) return newTex;
		
		return null;
	}

	
}
