package fr.dow.mapviewer.display.joglobject;

import javax.media.opengl.GL2;

import fr.dow.gamedata.sgbmap.terrain.Terrain;

public class JoglTiles {
	
	private Terrain terrain;
	
	private int[][] tileGenList;
	private int NX=0;
	private int NZ=0;
	
	public JoglTiles(GL2 gl, Terrain terrain) {
		this.terrain=terrain;
		resetTerrain(gl);
	}

	public void resetTerrain(GL2 gl) {
		
		if ((terrain.ground.texTiles.get_texTileNX()!=NX)||(terrain.ground.texTiles.get_texTileNZ()!=NZ)) {
			for (int tileX=0; tileX<NX; tileX++)
				for (int tileZ=0; tileZ<NZ; tileZ++)
					gl.glDeleteLists(tileGenList[tileX][tileZ],1);
			
			NX=terrain.ground.texTiles.get_texTileNX();
			NZ=terrain.ground.texTiles.get_texTileNZ();
			tileGenList=new int[NX][NZ];
		}
		
		TilesGrid tilesGrid=new TilesGrid(terrain);
		for (int tileX=0; tileX<NX; tileX++)
			for (int tileZ=0; tileZ<NZ; tileZ++) {
				tileGenList[tileX][tileZ]=newTile(gl,tileX,tileZ,tilesGrid);
				// isSelected[tileX][tileZ]=true; // used for debug
			}
		
	}
	
	public void display(GL2 gl, int tileX,int tileZ){
		gl.glCallList(tileGenList[tileX][tileZ]);
	}

	private int newTile(GL2 gl,int tileX,int tileZ, TilesGrid tilesGrid) {
		
		int genList = gl.glGenLists(1);
		
		gl.glNewList( genList, GL2.GL_COMPILE );
		
		int imin=tileX*tilesGrid.tilesize;
		int jmin=tileZ*tilesGrid.tilesize;
		
        // create vertex table
        for (int i0=0;i0<tilesGrid.tilesize;i0++)
            for (int j0=0;j0<tilesGrid.tilesize;j0++)
            {
            	int i1=i0+imin;
            	int j1=j0+jmin;
            	
            	gl.glBegin(GL2.GL_TRIANGLE_STRIP);
            	
            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE0, tilesGrid.texS1[i1+1], tilesGrid.texT1[j1]);
            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE1, tilesGrid.texS2[i0+1], tilesGrid.texT2[j0]);
            	gl.glVertex3f(tilesGrid.mapX[i1+1], tilesGrid.mapY[i1+1][j1], -tilesGrid.mapZ[j1]);
            	
            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE0, tilesGrid.texS1[i1], tilesGrid.texT1[j1]);
            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE1, tilesGrid.texS2[i0], tilesGrid.texT2[j0]);
            	gl.glVertex3f(tilesGrid.mapX[i1], tilesGrid.mapY[i1][j1], -tilesGrid.mapZ[j1]);
            	
            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE0, tilesGrid.texS1[i1+1], tilesGrid.texT1[j1+1]);
            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE1, tilesGrid.texS2[i0+1], tilesGrid.texT2[j0+1]);
            	gl.glVertex3f(tilesGrid.mapX[i1+1], tilesGrid.mapY[i1+1][j1+1], -tilesGrid.mapZ[j1+1]);
                
            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE0, tilesGrid.texS1[i1], tilesGrid.texT1[j1+1]);
            	gl.glMultiTexCoord2f(GL2.GL_TEXTURE1, tilesGrid.texS2[i0], tilesGrid.texT2[j0+1]);
            	gl.glVertex3f(tilesGrid.mapX[i1], tilesGrid.mapY[i1][j1+1], -tilesGrid.mapZ[j1+1]);
            	
            	gl.glEnd();
            }
        
        gl.glEndList();
		
        return genList;
	}
	
	private static class TilesGrid {
		
		int tilesize;
		float mapX[];
		float mapZ[];
		float mapY[][];
		float[] texS1;
		float[] texT1;
		float[] texS2;
		float[] texT2;
		
		public TilesGrid(Terrain terrain) {
			
			tilesize=terrain.ground.groundData.get_texTileSize();
			
			mapX=terrain.ground.heightMap.get_heightGrid_Xaxis();
			mapZ=terrain.ground.heightMap.get_heightGrid_Zaxis();
			mapY=terrain.ground.heightMap.get_heightGrid();
			
			texS1=rampelin(mapX.length,0,1);
			texT1=rampelin(mapZ.length,0,1);

			texS2=rampelin(tilesize+1,0,terrain.ground.groundData.get_texTileRepeat());
			texT2=rampelin(tilesize+1,0,terrain.ground.groundData.get_texTileRepeat());
			
		}
		
		private float[] rampelin(int n, float Y0,float Y1) {
			float[] Y=new float[n];
			float a=(Y1-Y0)/(n-1);
			for (int i=0; i<n; i++) Y[i]=a*i+Y0;
			return Y;
		}
		
	}

	public void drawPickable(GL2 gl) {
		
		for (int tileX=0; tileX<NX; tileX++)
			for (int tileZ=0; tileZ<NZ; tileZ++) {
					
				float[][] heightGrid=terrain.ground.heightMap.get_heightGrid();
				float[] xGrid=terrain.ground.heightMap.get_heightGrid_Xaxis();
				float[] zGrid=terrain.ground.heightMap.get_heightGrid_Zaxis();
				
				int tileSize=terrain.ground.groundData.get_texTileSize();
				
				int imin=tileX*tileSize+1;
				int imax=imin+tileSize-2;
				
				int jmin=tileZ*tileSize+1;
				int jmax=jmin+tileSize-2;
				
				if (imin<0) continue;
				if (jmin<0) continue;
				if (imax>xGrid.length) continue;
				if (jmax>zGrid.length) continue;
				
				gl.glPushName(getId(tileX,tileZ));
			
				gl.glBegin (GL2.GL_TRIANGLE_FAN);

				gl.glVertex3f( xGrid[imin],heightGrid[imin][jmin],-zGrid[jmin]);
				gl.glVertex3f( xGrid[imin],heightGrid[imin][jmax],-zGrid[jmax]);
				gl.glVertex3f( xGrid[imax],heightGrid[imax][jmax],-zGrid[jmax]);
				gl.glVertex3f( xGrid[imax],heightGrid[imax][jmin],-zGrid[jmin]);

				gl.glEnd();
					
				gl.glPopName();
        	
		}
		
	}

	public void drawPicked(GL2 gl, int id, float offset) {
		drawPicked(gl,getXfromId(id),getZfromId(id),offset); 
	}
	
	public void drawPicked(GL2 gl, int tileX, int tileZ, float offset) {
			
		float[][] heightGrid=terrain.ground.heightMap.get_heightGrid();
		float[] xGrid=terrain.ground.heightMap.get_heightGrid_Xaxis();
		float[] zGrid=terrain.ground.heightMap.get_heightGrid_Zaxis();
		
		int tileSize=terrain.ground.groundData.get_texTileSize();
		
		int imin=tileX*tileSize+1;
		int imax=imin+tileSize-2;
		
		int jmin=tileZ*tileSize+1;
		int jmax=jmin+tileSize-2;
		
		if (imin<0) return;
		if (jmin<0) return;
		if (imax>xGrid.length) return;
		if (jmax>zGrid.length) return;
		
		gl.glBegin (GL2.GL_LINE_STRIP);

		for (int i=imin;i<=imax;i++) {
			gl.glVertex3f( xGrid[i],heightGrid[i][jmin]+offset,-zGrid[jmin]);
		}
		for (int j=jmin;j<=jmax;j++) {
			gl.glVertex3f( xGrid[imax],heightGrid[imax][j]+offset,-zGrid[j]);
		}
		for (int i=imax;i>=imin;i--) {
			gl.glVertex3f( xGrid[i],heightGrid[i][jmax]+offset,-zGrid[jmax]);
		}
		for (int j=jmax;j>=jmin;j--) {
			gl.glVertex3f( xGrid[imin],heightGrid[imin][j]+offset,-zGrid[j]);
		}
		gl.glEnd();
		
	}
	
	private int getId(int i,int j) {
		return i+j*NX;
	}
	
	private int getXfromId(int id) {
		return id%NX;
	}
	
	private int getZfromId(int id) {
		return id/NX;
	}
	
}
