package fr.dow.mapviewer.display.joglobject;

import javax.media.opengl.GL2;

import com.jogamp.opengl.util.gl2.GLUT;

import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.gamedata.sgbmap.terrain.Ground;
import fr.dow.gamedata.sgbmap.units.Marker;
import fr.dow.gamedata.sgbmap.units.Markers;
import fr.dow.mapviewer.display.params.MarkersDisPars;

public class JoglMarkers implements MarkersDisPars.UpdateListener {

	private MarkersDisPars markersDisPars;
	
	private Markers localMarkersCopy;
	
	private int _genList;
	
	private static final float circleOffset=0.01f;
	private static final int circleSegNbr=32;
	private static final double circleStepAng=-2*Math.PI/circleSegNbr;
	
	private static final float centerCircleRadius=1f;
	private static final float centerCircleOffset=0.1f;
	private static final int centerCircleSegNbr=8;
	private static final double centerCircleStepAng=-2*Math.PI/centerCircleSegNbr;
	
	private static final float selectionCircleRadius=1.2f;
	private static final int selectionCircleSegNbr=8;
	private static final double selectionCircleStepAng=-2*Math.PI/selectionCircleSegNbr;
	
	private static final float textOffset=1f;
	
	private GLUT glut = new GLUT();

	//
	// Update level management
	//
	
	private int updateLevel=0;
	@Override
	public void changeUpdateLevel(MarkersDisPars.UpdateEvent evt) {
		if (evt.getLevel()>updateLevel) updateLevel=evt.getLevel();
	}
	
	//
	//
	//
	
	public JoglMarkers(GL2 gl, SgbMap sgbMap, MarkersDisPars markersDisPars) {
		
		this.markersDisPars=markersDisPars;
		
		_genList = gl.glGenLists(1);
		
		updateGenList(gl, sgbMap.markers, sgbMap.terrain.ground);
		
		markersDisPars.addUpdateListener(this);
		
	}

	
	public void display(GL2 gl, Markers markers, Ground ground) {
		if (updateLevel>0) {
			updateGenList(gl,markers,ground);
			updateLevel=0;
		}
		
		gl.glCallList(_genList);
		
		if (markersDisPars.showMarkers && markersDisPars.showTitle) {
			
	        gl.glDisable(GL2.GL_LIGHTING);
	        
			gl.glColor4f(markersDisPars.textColor.getRed()/255f, markersDisPars.textColor.getGreen()/255f, markersDisPars.textColor.getBlue()/255f, 1f);
			
			for (Marker marker : localMarkersCopy.values()) {
				float x=marker.get_x();
				float y=marker.get_y();
				float z=marker.get_z();
				
				gl.glRasterPos3f(x,y+textOffset,-z);
				glut.glutBitmapString(GLUT.BITMAP_8_BY_13, marker.get_name() );
				
			}
			
	        gl.glEnable(GL2.GL_LIGHTING);
		}
	}

	public void updateGenList(GL2 gl, Markers markers, Ground ground) {

		localMarkersCopy=(Markers)markers.clone();
		
		gl.glNewList( _genList, GL2.GL_COMPILE );

		if (markersDisPars.showMarkers) {

			gl.glDisable(GL2.GL_LIGHTING);
			gl.glEnable(GL2.GL_BLEND);

			for (Marker marker : localMarkersCopy.values()) {
				float x=marker.get_x();
				float z=marker.get_z();

				gl.glColor4f(marker.get_markertype().getRed(), marker.get_markertype().getGreen(), marker.get_markertype().getBlue(), 0.5f);

				gl.glBegin (GL2.GL_TRIANGLE_FAN);

				for (int i=0;i<centerCircleSegNbr;i++) {
					float x1=(float) (x+centerCircleRadius*Math.cos(i*centerCircleStepAng));
					float z1=(float) (z+centerCircleRadius*Math.sin(i*centerCircleStepAng));
					float y1=ground.getY(x1,z1);
					gl.glVertex3f( x1,y1+centerCircleOffset,-z1);
				}

				gl.glEnd();

				gl.glColor4f(marker.get_markertype().getRed(), marker.get_markertype().getGreen(), marker.get_markertype().getBlue(), 1f);

				gl.glBegin (GL2.GL_LINE_LOOP);

				for (int i=0;i<centerCircleSegNbr;i++) {
					float x1=(float) (x+centerCircleRadius*Math.cos(i*centerCircleStepAng));
					float z1=(float) (z+centerCircleRadius*Math.sin(i*centerCircleStepAng));
					float y1=ground.getY(x1,z1);
					gl.glVertex3f( x1,y1+centerCircleOffset,-z1);
				}

				gl.glEnd();

			}

			if (markersDisPars.showProxRadius) {

				gl.glColor4f(markersDisPars.textColor.getRed()/255f, markersDisPars.textColor.getGreen()/255f, markersDisPars.textColor.getBlue()/255f, 1f);

				for (Marker marker : localMarkersCopy.values()) {
					float x=marker.get_x();
					float z=marker.get_z();
					float r=marker.get_proximity();

					gl.glBegin (GL2.GL_LINE_LOOP);

					for (int i=0;i<circleSegNbr;i++) {
						float x1=(float) (x+r*Math.cos(i*circleStepAng));
						float z1=(float) (z+r*Math.sin(i*circleStepAng));
						float y1=ground.getY(x1,z1);
						gl.glVertex3f( x1,y1+circleOffset,-z1);
					}

					gl.glEnd();

				}
			}
			
			gl.glEnable(GL2.GL_LIGHTING);
			gl.glDisable(GL2.GL_BLEND);

		}
		
		gl.glEndList();

	}

	public void drawPickable(GL2 gl, Ground ground) {
		for (Integer id : localMarkersCopy.keySet()) {
			Marker marker=localMarkersCopy.get(id);
			
			float x=marker.get_x();
			float z=marker.get_z();

			gl.glPushName(id);
			
			gl.glBegin (GL2.GL_TRIANGLE_FAN);

			for (int i=0;i<centerCircleSegNbr;i++) {
				float x1=(float) (x+selectionCircleRadius*Math.cos(i*selectionCircleStepAng));
				float z1=(float) (z+selectionCircleRadius*Math.sin(i*selectionCircleStepAng));
				float y1=ground.getY(x1,z1);
				gl.glVertex3f( x1,y1,-z1);
			}

			gl.glEnd();
			
			gl.glPopName();
			
		}
	}

	public void drawPicked(GL2 gl, Integer id, Ground ground, float offset) {
		
		Marker marker=localMarkersCopy.get(id);
		
		float x=marker.get_x();
		float z=marker.get_z();
		
		gl.glBegin (GL2.GL_LINE_LOOP);

		for (int i=0;i<selectionCircleSegNbr;i++) {
			float x1=(float) (x+selectionCircleRadius*Math.cos(i*selectionCircleStepAng));
			float z1=(float) (z+selectionCircleRadius*Math.sin(i*selectionCircleStepAng));
			float y1=ground.getY(x1,z1);
			gl.glVertex3f( x1,y1+offset,-z1);
		}

		gl.glEnd();
		
		
	}

	
	
}
