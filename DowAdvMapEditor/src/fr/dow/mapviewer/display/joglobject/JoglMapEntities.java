package fr.dow.mapviewer.display.joglobject;

import java.util.Vector;

import javax.media.opengl.GL2;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.gamedata.sgbmap.terrain.Ground;
import fr.dow.gamedata.sgbmap.units.Entities;
import fr.dow.gamedata.sgbmap.units.Entity;
import fr.dow.mapviewer.display.joglobject.joglentity.JoglEntity;
import fr.dow.mapviewer.display.params.EntitiesDisPars;
import fr.dow.mapviewer.display.params.MapDisPars;
import fr.dow.mapviewer.display.params.BaseDisPars;
import fr.dow.mapviewer.display.params.PlayersDisPars;

public class JoglMapEntities implements BaseDisPars.UpdateListener {

	private EntitiesDisPars entitiesDisPars;
	private PlayersDisPars playersDisPars;

	public Vector<JoglEntity> joglEntities;	
	
	private int _genList; // pointer sur la display list JOGL

	//
	// Update level management
	//
	
	private int entitiesUpdateLevel=0;
	
	@Override
	public void changeUpdateLevel(BaseDisPars.UpdateEvent evt) {
		if (evt.getLevel()>entitiesUpdateLevel) 
			entitiesUpdateLevel=evt.getLevel();
	}
	
	//
	//
	//
	
	public JoglMapEntities(GL2 gl, SgbMap map, DowMod modData, MapDisPars mapDisPars) {
		
		this.playersDisPars=mapDisPars.playerDisplayParams;
		this.entitiesDisPars=mapDisPars.entitiesDisplayParams;
		
    	//create OpenGL Entities objects
		joglEntities=new Vector<JoglEntity>();
				
		// create genList
		_genList = gl.glGenLists(1);
		
		// create entitites and update display
		updateMapEntities(gl,map,modData);
		
		this.playersDisPars.addUpdateListener(this);
		this.entitiesDisPars.addUpdateListener(this);
	}

	public void display(GL2 gl, SgbMap map, DowMod modData) {
		
        if (entitiesUpdateLevel>0) {
        	updateMapEntities(gl,map,modData);
        	entitiesUpdateLevel=0;
        }
        
		gl.glCallList(_genList);
	}

	private void updateMapEntities(GL2 gl, SgbMap map, DowMod modData) {
		
		// loads unloaded joglentities
		// replace changed joglentities
		// update current joglentities
		// remove useless joglentities
		for (int i=0;i<map.entityBlueprints.size();i++) {
			if (i>=joglEntities.size()) {
				loadEntityBlueprint(gl, i, map, modData);
			} else if (!map.entityBlueprints.get(i).contentEquals(joglEntities.get(i).getBlueprint())) {
				clearEntityBlueprint(gl,i);
				loadEntityBlueprint(gl, i, map, modData);
			} else {
				joglEntities.get(i).updateGenList(gl);
			}
		}
		
		if (joglEntities.size()>map.entityBlueprints.size()) {
			for (int i=map.entityBlueprints.size(); i<joglEntities.size(); i++) 
				if (joglEntities.get(i)!=null)
					clearEntityBlueprint(gl, i);
			joglEntities.setSize(map.entityBlueprints.size());
		}
		
		// update genList
		gl.glNewList( _genList, GL2.GL_COMPILE );
		
		for (Entity entity : map.entities.values()) {
			
			PlayersDisPars.PlayerData playerData=playersDisPars.players.get(entity.getPlayerID());
			
			if (playerData.isVisible) {

				JoglEntity joglEntity=joglEntities.get(entity.getBlueprintIndex());
				
				if (joglEntity!=null) {
					
					gl.glPushMatrix();
					gl.glMultMatrixf(entity.getTransf(), 0);
					
					gl.glColor3f(playerData.red, playerData.green, playerData.blue);

					if (playerData.isTextured) joglEntity.displayTextured(gl);
					else joglEntity.displayRaw(gl);
					
					gl.glPopMatrix();
					
				} else {
					System.err.println("Warning : no JoglEntity for index : " + entity.getBlueprintIndex());
				}
				
			}
		}
		
        gl.glEndList();
		
	}

	private void loadEntityBlueprint(GL2 gl, int i, SgbMap map, DowMod modData ) {
		if (joglEntities.size()<=i) joglEntities.setSize(i+1);
		JoglEntity newJoglEntity=JoglEntity.loadRgd(gl, modData, map.entityBlueprints.get(i));
		joglEntities.set(i,newJoglEntity);
	}
	
	private void clearEntityBlueprint(GL2 gl, int i) {
		joglEntities.get(i).clearFromMemory(gl);
		joglEntities.set(i, null);
	}
	
	public void drawPickable(GL2 gl,Entities mapEntities, Ground ground) {
		for (Entity entity : mapEntities.values()) 
		{
			gl.glPushName(entity.getId());
			
			JoglEntity joglEntity=joglEntities.get(entity.getBlueprintIndex());
			joglEntity.joglSelUI.drawPickable(gl,entity.getTransf(),ground);
			
        	gl.glPopName();
		}
	}
	
	public void drawPicked(GL2 gl,int id, Entities mapEntities, Ground ground, float offset) {
		Entity entity=mapEntities.get(id);
		JoglEntity joglEntity=joglEntities.get(entity.getBlueprintIndex());
		joglEntity.joglSelUI.displaySelected(gl,entity.getTransf(),ground,offset);
	}
	
	public void displaySelected(GL2 gl, Entities mapEntities, Ground ground,float offset) {
		for (Entity entity : mapEntities.values()) 
			{
				JoglEntity joglEntity=joglEntities.get(entity.getBlueprintIndex());
				joglEntity.joglSelUI.displaySelected(gl,entity.getTransf(),ground,offset);
			}
	}
	
}
