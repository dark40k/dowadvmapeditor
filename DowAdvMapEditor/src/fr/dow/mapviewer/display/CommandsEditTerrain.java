package fr.dow.mapviewer.display;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.gamedata.sgbmap.terrain.PathSectorMap;
import fr.dow.gamedata.sgbmap.units.MarkerType;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.editdecals.EditDecalsDialog;
import fr.dow.mapeditor.dialogs.editmarkers.EditMarkersDialog;
import fr.dow.mapeditor.dialogs.edittiles.EditTilesDialog;
import fr.dow.mapeditor.dialogs.heightmap.EditHeightMapDialog;
import fr.dow.mapeditor.dialogs.smoother.SmootherDialog;

public class CommandsEditTerrain {

	MapViewer mapViewer;

	public CommandsEditTerrain(MapViewer mapViewer) {
		this.mapViewer=mapViewer;
	}
	
	//
	// Decals
	//
	
	public void editDecals(MapEditor mapEditor) {
		EditDecalsDialog.showDialog(mapEditor, mapViewer);
	}
	
	public void doChangeDecalBP(int decalID,String newDecalBP) {
		mapViewer.map.terrain.changeDecalBP(decalID,newDecalBP);
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
	}

	public void doChangeDecalPos(int decalID, float x, float z, float size,float angle) {
		mapViewer.map.terrain.changeDecalPos(decalID,x,z,size,angle);
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
	}

	public void doDecalDelete(int decalID) {
		mapViewer.map.terrain.decalDelete(decalID);
		mapViewer.mapRenderer.currentSelection.decals.remove(decalID);
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
	}

	public void doDecalDeleteAll() {
		mapViewer.map.terrain.decalDeleteAll();
		mapViewer.mapRenderer.currentSelection.decals.clear();
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
	}

	public Integer doDecalCreate(String decalBP, float x, float z, float size,float angle) {
		Integer decalID=mapViewer.map.terrain.decalCreate(decalBP,x,z,size,angle);
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
		return decalID;
	}
	
	public void doDecalUpdate(int decalID, String decalBP, float x, float z, float size,float angle) {
		mapViewer.map.terrain.decalUpdate(decalID,decalBP,x,z,size,angle);
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
	}
	
	//
	// Tiles
	//
	
	public void editTiles(MapEditor mapEditor) {
		EditTilesDialog.showDialog(mapEditor, mapViewer);
	}
	
	public void doChangeTileBP(Integer tileID, String newTileBP) {
		mapViewer.map.terrain.ground.changeTileBP(tileID,newTileBP);
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
	}

	//
	// Markers
	//
	
	public void editMarkers(MapEditor mapEditor) {
		EditMarkersDialog.showDialog(mapEditor, mapViewer);
		
	}
	
	public void doMarkerDelete(int markerID) {
		mapViewer.map.markers.markerDelete(markerID);
		mapViewer.mapRenderer.currentSelection.markers.remove(markerID);
		mapViewer.mapRenderer.mapDisplayParams.markerDisPars.fireUpdateEvent(2);
	}

	public void doChangeMarkerName(Integer id, String name) {
		mapViewer.map.markers.get(id).set_name(name);
		mapViewer.mapRenderer.mapDisplayParams.markerDisPars.fireUpdateEvent(1);
	}

	public void doChangeMarkerPos(Integer id, Float x, Float y, Float z) {
		mapViewer.map.markers.get(id).set_position(x,y,z);
		mapViewer.mapRenderer.mapDisplayParams.markerDisPars.fireUpdateEvent(1);
	}

	public void doChangeMarkerProximity(Integer id, Float proximity) {
		mapViewer.map.markers.get(id).set_proximity(proximity);
		mapViewer.mapRenderer.mapDisplayParams.markerDisPars.fireUpdateEvent(1);
	}
	
	public void doChangeMarkerCustomNumber(Integer id, Float customNumber) {
		mapViewer.map.markers.get(id).set_customNumber(customNumber);
		mapViewer.mapRenderer.mapDisplayParams.markerDisPars.fireUpdateEvent(1);
	}

	public void doChangeMarkerCustomString(Integer id, String customString) {
		mapViewer.map.markers.get(id).set_customString(customString);		
		mapViewer.mapRenderer.mapDisplayParams.markerDisPars.fireUpdateEvent(1);
	}

	public void doMarkerChangeType(Integer id, MarkerType markerType) {
		mapViewer.map.markers.get(id).set_type(markerType);		
		mapViewer.mapRenderer.mapDisplayParams.markerDisPars.fireUpdateEvent(2);
	}

	//
	// Ground
	//
	
	public void editHeightMap(MapEditor mapEditor) {
		EditHeightMapDialog.showDialog(mapEditor, mapViewer);
	}

	public void smoothHeightMap(MapEditor mapEditor) {
		SmootherDialog.showDialog(mapEditor, mapViewer);
	}

	public void importPASM(MapEditor mapEditor) {
	
		// get import map File
		File importMapFile=mapEditor.getMapFileUsingDialog();
        if (importMapFile == null) return; 

    	// check tga and mod files
        File importTgaFile = mapEditor.getTgaFile(importMapFile);
        if (importTgaFile == null) return;
        
        SgbMap importMap;
		try {
			importMap = new SgbMap(new DowFileVersion(importMapFile,null), new DowFileVersion(importTgaFile,null));
		}
		catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Could not load map","Error",JOptionPane.ERROR_MESSAGE);
			return;
		}
        
		// check map size matches current map
		if ( (importMap.terrain.ground.groundData.get_cellnbrX() != mapViewer.map.terrain.ground.groundData.get_cellnbrX()) || 
				(importMap.terrain.ground.groundData.get_cellnbrZ() != mapViewer.map.terrain.ground.groundData.get_cellnbrZ()) ){
			JOptionPane.showMessageDialog(null, "Cannot import PASM : map sizes are different","Error",JOptionPane.ERROR_MESSAGE);
			return;			
		}
		
		mapViewer.map.terrain.pathFindingInformation.pathSectorMap=(PathSectorMap) importMap.terrain.pathFindingInformation.pathSectorMap.clone();
		
		mapViewer.mapRenderer.mapDisplayParams.terrainDisplayParams.fireUpdateEvent(2);
		
	}


}
