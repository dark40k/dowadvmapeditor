package fr.dow.mapviewer.display;

import fr.dow.gamedata.sgbmap.units.PlayerID;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapeditor.dialogs.editentities.EditEntitiesDialog;
import fr.dow.mapeditor.dialogs.editgroups.EditEgroupsDialog;
import fr.dow.mapeditor.dialogs.editgroups.EditSgroupsDialog;
import fr.dow.mapeditor.dialogs.editsquads.EditSquadsDialog;
import fr.dow.mapeditor.dialogs.rotateentities.RotateEntitiesDialog;

public class CommandsGroups {
	
	MapViewer mapViewer;
	
	public CommandsGroups(MapViewer mapViewer) {
		this.mapViewer=mapViewer;
	}

	public void editSGroups(MapEditor mapEditor) {
		EditSgroupsDialog.showDialog(mapEditor, mapViewer);
	}

	public void editEGroups(MapEditor mapEditor) {
		EditEgroupsDialog.showDialog(mapEditor, mapViewer);
	}

	public void editEntities(MapEditor mapEditor) {
		EditEntitiesDialog.showDialog(mapEditor, mapViewer);
	}

	public void RotateEntities(MapEditor mapEditor) {
		RotateEntitiesDialog.showDialog(mapEditor, mapViewer);
	}

	public void editSquads(MapEditor mapEditor) {
		EditSquadsDialog.showDialog(mapEditor, mapViewer);
	}

	public void doEntitiesDeleteAll(boolean includeSquads) {
		mapViewer.mapRenderer.currentSelection.entities.clear();
		mapViewer.map.deleteEntities(includeSquads);
		mapViewer.mapRenderer.mapDisplayParams.entitiesDisplayParams.fireUpdateEvent(1);
	}

	public void doEntityUpdate(Integer id, String entityBP, PlayerID playerID, Float x, Float y, Float z, float headingRad, float pitchRad,
			float rollRad, Double sX, Double sY, Double sZ) {
		mapViewer.map.entityUpdate(	id, entityBP, playerID, x, y, z, headingRad, pitchRad, rollRad, sX, sY, sZ);
		mapViewer.mapRenderer.mapDisplayParams.entitiesDisplayParams.fireUpdateEvent(1);
	}

	public void deleteEntity(Integer id) {
		mapViewer.getRenderer().currentSelection.entities.remove(id);
		mapViewer.getMap().entities.remove(id);
		mapViewer.mapRenderer.mapDisplayParams.entitiesDisplayParams.fireUpdateEvent(1);
	}

	public void doEntityChangeOwner(Integer id, PlayerID playerID) {
		mapViewer.getMap().entities.get(id).setPlayerID(playerID);
		mapViewer.mapRenderer.mapDisplayParams.entitiesDisplayParams.fireUpdateEvent(1);
	}
	

}
