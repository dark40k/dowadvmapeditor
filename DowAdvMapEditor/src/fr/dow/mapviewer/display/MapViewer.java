package fr.dow.mapviewer.display;

import java.awt.BorderLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.IOException;

import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.swing.JInternalFrame;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.jogamp.opengl.util.FPSAnimator;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.mapeditor.MapEditor;
import fr.dow.mapviewer.display.renderer.MapViewerRenderer;


public class MapViewer extends JInternalFrame implements InternalFrameListener,ComponentListener {

	// Window launch params
	private static int openFrameCount = 0;
	private static final int xOffset = 30, yOffset = 30;

	// Graphic objects
	MapEditor mapEditor;
	GLCanvas glCanvas;
	FPSAnimator animator;
	MapViewerRenderer mapRenderer;

	// Map and Mod data
	SgbMap map;
	DowMod modData;
	
	// Commands
	public CommandsFile commandsFile=new CommandsFile(this);
	public CommandsSelection commandsSelection=new CommandsSelection(this);
	public CommandsEditMap commandsEditMap=new CommandsEditMap(this);
	public CommandsEditTerrain commandsEditTerrain=new CommandsEditTerrain(this);
	public CommandsView commandsView=new CommandsView(this);
	public CommandsGroups commandsGroups=new CommandsGroups(this);
	public CommandsAbout commandsAbout=new CommandsAbout(this);
	
	// Constructor
	public MapViewer(MapEditor mapEditor, DowFileVersion dowSgbFile,DowFileVersion dowTgaFile, DowMod dowMod) throws IOException {

		super("Loading Map : " + dowSgbFile.getGlobalPath(), 
				true, //resizable
				true, //closable
				true, //maximizable
				true);//iconifiable

		// Store parent
		this.mapEditor=mapEditor;
		
		// Store Mod
		modData = dowMod;
		
		// Load Map
		System.out.println("Loading Map file : " + dowSgbFile.getGlobalPath());
		map = new SgbMap(dowSgbFile,dowTgaFile);

		if (dowSgbFile.getFile()!=null) 
			updateTitle();
		else if (dowSgbFile.getDowFile()!=null)
			this.setTitle(modData.getModFile().getName() + " / (Sga Loaded) " + dowSgbFile.getDowFile().getName());
		
		//set window params
		System.out.println("Init graphics");			
		setSize(500,500);
		setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
		getContentPane().setLayout( new BorderLayout() );
		
		//Init OpenGL Canvas
        GLProfile glprofile = GLProfile.getDefault();
        GLCapabilities glcapabilities = new GLCapabilities( glprofile );
		glCanvas = new GLCanvas(glcapabilities);
		glCanvas.setIgnoreRepaint( true );
		this.getContentPane().add( glCanvas, BorderLayout.CENTER );
		
		//Init renderer
		mapRenderer=new MapViewerRenderer(glCanvas,map,modData);

		//Init animator
		System.out.println("Starting animation");	
		animator = new FPSAnimator( glCanvas, 25 );
		//animator.setRunAsFastAsPossible(false);
		animator.start(); 
		
		//Add event listener
		this.addInternalFrameListener(this);
		this.addComponentListener(this);
		
        // Launch display
		this.revalidate();

	}

	public MapViewerRenderer getRenderer() {
		return mapRenderer;
	}
	
	public SgbMap getMap(){
		return map;
	}

	public DowMod getMod() {
		return modData;
	}
	
	public void updateTitle() {
		setTitle(map.mapData.get_mapName() + " | Mod= " + modData.getModFile().getName() + " | File= " + map.mapFile.getAbsolutePath());
	}

	@Override
	public void internalFrameActivated(InternalFrameEvent arg0) {
		mapEditor.toolBar.setLinkedMapViewer(this);
	}

	@Override
	public void internalFrameClosed(InternalFrameEvent arg0) {
		mapEditor.toolBar.removeLinkedMapViewer(this);
	}

	@Override
	public void internalFrameClosing(InternalFrameEvent arg0) {
	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent arg0) {
		mapEditor.toolBar.removeLinkedMapViewer(this);
	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent arg0) {
	}

	@Override
	public void internalFrameIconified(InternalFrameEvent arg0) {
	}

	@Override
	public void internalFrameOpened(InternalFrameEvent arg0) {
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		glCanvas.requestFocusInWindow();
	}
	
}
