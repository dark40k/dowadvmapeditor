package fr.dow.mapviewer.display.selection;

import java.util.HashSet;

import fr.dow.mapviewer.display.selection.SelFilter.FilterType;

public class SelectionData implements Cloneable {

	public HashSet<Integer> tiles = new HashSet<Integer>();
	public HashSet<Integer> decals = new HashSet<Integer>();
	public HashSet<Integer> entities = new HashSet<Integer>();
	public HashSet<Integer> markers = new HashSet<Integer>();
	public SelArea area = new SelArea();
	
	public void addAll(SelFilter filter, Selection selection) {
		if (filter.isNotFiltered(FilterType.DECAL)) decals.addAll(selection.decals);
		if (filter.isNotFiltered(FilterType.ENTITY)) entities.addAll(selection.entities);
		if (filter.isNotFiltered(FilterType.TILE)) tiles.addAll(selection.tiles);
		if (filter.isNotFiltered(FilterType.MARKER)) markers.addAll(selection.markers);
		if (filter.isNotFiltered(FilterType.GROUNDAREA)) area.setArea(selection.area);
	}
	
	public boolean containsAll(SelFilter filter, Selection selection) {
		if (filter.isNotFiltered(FilterType.DECAL)) if (!decals.containsAll(selection.decals)) return false;
		if (filter.isNotFiltered(FilterType.ENTITY)) if (!entities.containsAll(selection.entities)) return false;
		if (filter.isNotFiltered(FilterType.TILE)) if (!tiles.containsAll(selection.tiles)) return false;
		if (filter.isNotFiltered(FilterType.MARKER)) if (!markers.containsAll(selection.markers)) return false;
		if (filter.isNotFiltered(FilterType.GROUNDAREA)) return false;
		return true;
	}
	
	public void removeAll(SelFilter filter, Selection selection) {
		if (filter.isNotFiltered(FilterType.DECAL)) decals.removeAll(selection.decals);
		if (filter.isNotFiltered(FilterType.ENTITY)) entities.removeAll(selection.entities);
		if (filter.isNotFiltered(FilterType.TILE)) tiles.removeAll(selection.tiles);
		if (filter.isNotFiltered(FilterType.MARKER)) markers.removeAll(selection.markers);
	}
	
	public void clearAll() {
		decals.clear();
		entities.clear();
		tiles.clear();
		markers.clear();
		area.clear();
	}
	
	public void clear(SelFilter filter) {
		if (filter.isNotFiltered(FilterType.DECAL)) decals.clear();
		if (filter.isNotFiltered(FilterType.ENTITY)) entities.clear();
		if (filter.isNotFiltered(FilterType.TILE)) tiles.clear();
		if (filter.isNotFiltered(FilterType.MARKER)) markers.clear();
		if (filter.isNotFiltered(FilterType.GROUNDAREA)) area.clear();
	}
	
	@SuppressWarnings("unchecked") // no control when casting after cloning
	public Object clone() {
		
		Selection o = null;
		try {
			o = (Selection)super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		
		// Note: Integer is immutable object, no need reassign after for cloning.
		o.tiles=(HashSet<Integer>)o.tiles.clone();
		o.decals=(HashSet<Integer>)o.decals.clone();
		o.entities=(HashSet<Integer>)o.entities.clone();
		o.markers=(HashSet<Integer>)o.markers.clone();
		o.area=(SelArea) o.area.clone();
		
		return o;
	}
	
}
