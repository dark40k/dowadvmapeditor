package fr.dow.mapviewer.display.selection;

import java.util.EventListener;
import java.util.EventObject;

import javax.swing.event.EventListenerList;

public class SelFilter {
	
	public enum FilterType {
		NONE(0), TILE(1), DECAL(2), ENTITY(4), MARKER(8), GROUNDAREA(16), ALL(~0);
		private final int filter;
		private FilterType(int i) { this.filter=i; }
		public int getInt() { return filter;}
	}
	
	private int filter;
	private int lock;
	
	public SelFilter(FilterType filterType) {
		filter=filterType.getInt();
		lock=FilterType.NONE.getInt();
	}
	
	public SelFilter(SelFilter selFilter) {
		filter=selFilter.filter;
		lock=selFilter.lock;
	}
	
	public void setFilter(FilterType filterType, boolean filterStatus) {
		if (filterStatus!=isNotFiltered(filterType)) {
			if (filterStatus) {
				add(filterType);
			} else {
				remove(filterType);
			}
			fireChangeFilterEvent();
		}
	}
	
	public void set(SelFilter selFilter) {
		this.filter=selFilter.filter;
		this.lock=selFilter.lock;
		fireChangeFilterEvent();
	}
	
	public void set(FilterType filterType) {
		if (isLocked(filterType)) return;		
		this.filter=filterType.getInt();
		fireChangeFilterEvent();
	}
	
	public void add(FilterType filterType) {
		if (isLocked(filterType)) return;		
		filter=(filter | filterType.getInt());
		fireChangeFilterEvent();
	}
	
	public void remove(FilterType filterType) {
		filter=(filter & ~filterType.getInt());
		fireChangeFilterEvent();
	}
	
	public boolean isFiltered(FilterType filterType) {
		return (filter & filterType.getInt())==0;
	}
	
	public boolean isNotFiltered(FilterType filterType) {
		return (filter & filterType.getInt())!=0;
	}
	
	//
	// locking commands
	//
	public void unlockAll() {
		lock=FilterType.NONE.getInt();
		fireChangeFilterEvent();
	}
	
	public void lock(FilterType filterType) {
		lock=(lock | filterType.getInt());
		if (!isFiltered(filterType)) { remove(filterType); }
		fireChangeFilterEvent();
	}
	
	public void lockAllExcept(FilterType filterType) {
		lock=~filterType.getInt();
		filter=filterType.getInt();
		fireChangeFilterEvent();
	}
	
	public boolean isLocked(FilterType filterType) {
		return (lock & filterType.getInt())!=0;
	}
	
	//
	// Change event
	//
	
	private EventListenerList listenerList = new EventListenerList();

	public class ChangeFilterEvent extends EventObject {
		public ChangeFilterEvent(Object source) {
			super(source);
		}
	}

	public interface ChangeFilterListener extends EventListener {
		public void changeFilter(ChangeFilterEvent evt);
	}

	public void addChangeFilterListener(ChangeFilterListener listener) {
		listenerList.add(ChangeFilterListener.class, listener);
	}
	public void removeChangeFilterListener(ChangeFilterListener listener) {
		listenerList.remove(ChangeFilterListener.class, listener);
	}
	void fireChangeFilterEvent() {
		ChangeFilterEvent evt=new ChangeFilterEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == ChangeFilterListener.class) {
				((ChangeFilterListener) listeners[i+1]).changeFilter(evt);
			}
		}
	}

	
	
	
}
