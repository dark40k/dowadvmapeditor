package fr.dow.mapviewer.display.selection;

import java.util.EventListener;
import java.util.EventObject;

import javax.media.opengl.GL2;
import javax.swing.event.EventListenerList;

import com.jogamp.opengl.util.gl2.GLUT;

import fr.dow.gamedata.sgbmap.SgbMap;
import fr.dow.gamedata.sgbmap.terrain.HeightMap;

public class SelArea implements Cloneable {
	
    private GLUT glut = new GLUT();
    
	public enum CornerDef { NORTHEAST,NORTHWEST,SOUTHWEST,SOUTHEAST; }
	
	public int i1;
	public int j1;
	public int i2;
	public int j2;
	
	public SelArea() {
		clear();
	}

	public void setArea(int i1,int j1,int i2,int j2) {
		this.i1=Math.min(i1,i2);
		this.j1=Math.min(j1,j2);
		this.i2=Math.max(i1,i2);
		this.j2=Math.max(j1,j2);
		fireChangeSelAreaEvent();
	}
	
	public void setArea(SelArea selArea) {
		this.i1=selArea.i1;
		this.i2=selArea.i2;
		this.j1=selArea.j1;
		this.j2=selArea.j2;
		fireChangeSelAreaEvent();
	}
	
	public void clear() {
		i1=0;
		j1=0;
		i2=0;
		j2=0;
		fireChangeSelAreaEvent();
	}
	
	public void drawPicked(GL2 gl, SgbMap map, float offset) {
		
		float[][] heightGrid=map.terrain.ground.heightMap.get_heightGrid();
		float[] xGrid=map.terrain.ground.heightMap.get_heightGrid_Xaxis();
		float[] zGrid=map.terrain.ground.heightMap.get_heightGrid_Zaxis();
	
		int imin=forceBorder(i1,0,xGrid.length-1);
		int imax=forceBorder(i2,0,xGrid.length-1);
		int jmin=forceBorder(j1,0,zGrid.length-1);
		int jmax=forceBorder(j2,0,zGrid.length-1);
		
		gl.glBegin (GL2.GL_LINE_STRIP);

		for (int i=imin;i<=imax;i++) {
			gl.glVertex3f( xGrid[i],heightGrid[i][jmin]+offset,-zGrid[jmin]);
		}
		for (int j=jmin;j<=jmax;j++) {
			gl.glVertex3f( xGrid[imax],heightGrid[imax][j]+offset,-zGrid[j]);
		}
		for (int i=imax;i>=imin;i--) {
			gl.glVertex3f( xGrid[i],heightGrid[i][jmax]+offset,-zGrid[jmax]);
		}
		for (int j=jmax;j>=jmin;j--) {
			gl.glVertex3f( xGrid[imin],heightGrid[imin][j]+offset,-zGrid[j]);
		}
		gl.glEnd();
		
		gl.glRasterPos3f(xGrid[imin],heightGrid[imin][jmin]+offset,-zGrid[jmin]);
		glut.glutBitmapString(GLUT.BITMAP_8_BY_13, "SW" );
		gl.glRasterPos3f(xGrid[imax],heightGrid[imax][jmin]+offset,-zGrid[jmin]);
		glut.glutBitmapString(GLUT.BITMAP_8_BY_13, "SE" );
		gl.glRasterPos3f(xGrid[imin],heightGrid[imin][jmax]+offset,-zGrid[jmax]);
		glut.glutBitmapString(GLUT.BITMAP_8_BY_13, "NW" );
		gl.glRasterPos3f(xGrid[imax],heightGrid[imax][jmax]+offset,-zGrid[jmax]);
		glut.glutBitmapString(GLUT.BITMAP_8_BY_13, "NE" );
		
	}
	
	private static int forceBorder(int i, int imin, int imax) {
		if (i<imin) return imin;
		if (i>imax) return imax;
		return i;
	}
	
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	public int getCornerI(CornerDef corner) {
		switch (corner) {
			case NORTHEAST : return i2;
			case NORTHWEST : return i1;
			case SOUTHWEST : return i1;
			case SOUTHEAST : return i2;
		}
		return -1;
	}
	
	public int getCornerJ(CornerDef corner) {
		switch (corner) {
			case NORTHEAST : return j2;
			case NORTHWEST : return j2;
			case SOUTHWEST : return j1;
			case SOUTHEAST : return j1;
		}
		return -1;
	}

	public void moveCorner(CornerDef corner, int di, int dj, HeightMap heightMap) {
		switch (corner) {
			case NORTHEAST : i2+=di; j2+=dj; break; 
			case NORTHWEST : i1+=di; j2+=dj; break; 
			case SOUTHWEST : i1+=di; j1+=dj; break; 
			case SOUTHEAST : i2+=di; j1+=dj; break; 
		}
		forceInsideMap(heightMap);
	}
	
	public void forceInsideMap(HeightMap heightmap) {
		
		i1=Math.min(i1,i2);
		j1=Math.min(j1,j2);
		i2=Math.max(i1,i2);
		j2=Math.max(j1,j2);
		
		i1=Math.max(i1,0);
		j1=Math.max(j1,0);
		i2=Math.min(i2, heightmap.get_heightGrid_Xaxis().length-1);
		j2=Math.min(j2, heightmap.get_heightGrid_Zaxis().length-1);
		
		fireChangeSelAreaEvent();
	}
	
	//
	// Change event
	//
	
	private EventListenerList listenerList = new EventListenerList();

	public class ChangeSelAreaEvent extends EventObject {
		public ChangeSelAreaEvent(Object source) {
			super(source);
		}
	}

	public interface ChangeSelAreaListener extends EventListener {
		public void changeSelArea(ChangeSelAreaEvent evt);
	}

	public void addChangeSelAreaListener(ChangeSelAreaListener listener) {
		listenerList.add(ChangeSelAreaListener.class, listener);
	}
	public void removeChangeSelAreaListener(ChangeSelAreaListener listener) {
		listenerList.remove(ChangeSelAreaListener.class, listener);
	}
	void fireChangeSelAreaEvent() {
		ChangeSelAreaEvent evt=new ChangeSelAreaEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == ChangeSelAreaListener.class) {
				((ChangeSelAreaListener) listeners[i+1]).changeSelArea(evt);
			}
		}
	}
	
}
