package fr.dow.mapviewer.display.renderer;

public class ScreenPosition {
	public int x;
	public int y;
	
	public ScreenPosition(int x,int y) {
		this.x=x;
		this.y=y;
	}
}
