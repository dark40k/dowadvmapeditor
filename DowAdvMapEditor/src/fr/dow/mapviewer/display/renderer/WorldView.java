package fr.dow.mapviewer.display.renderer;

public class WorldView implements Cloneable {
	
	private final float PI_180 = (float) (Math.PI / 180.0);
	
	public float rotX;
	public float rotY;

	public float posX;
	public float posY;
	public float posZ;

	public static WorldView newDefault() {
		WorldView newWorldView=new WorldView();
		newWorldView.rotX=-45;
		newWorldView.rotY=-45;
		newWorldView.posX=100;
		newWorldView.posY=150;
		newWorldView.posZ=100;
		return newWorldView;		
	}
	
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	// return a moved view based on current orientation
	public WorldView copyZoomed(float disp) {
		WorldView newWorldView=(WorldView)clone();
		newWorldView.posX += (float) Math.sin(-newWorldView.rotY * PI_180) * (float) Math.cos(-newWorldView.rotX * PI_180) * disp;
		newWorldView.posY += (float) Math.sin(-newWorldView.rotX * PI_180) * disp;
		newWorldView.posZ += (float) Math.cos(-newWorldView.rotY * PI_180) * (float) Math.cos(-newWorldView.rotX * PI_180) * disp;
		return newWorldView;
	}
	
	// return a moved view based on current orientation
	public WorldView copyMoved(float right,float up) {
		WorldView newWorldView=(WorldView)clone();
		newWorldView.posX -= -newWorldView.sinRotY() * up + newWorldView.cosRotY() * right ;
		newWorldView.posZ -=  newWorldView.cosRotY() * up + newWorldView.sinRotY() * right ;	
		return newWorldView;
	}
	
	// return a moved view based on current orientation
	public WorldView copyMovedAbs(float east,float north) {
		WorldView newWorldView=(WorldView)clone();
		newWorldView.posX += east;
		newWorldView.posZ += north;	
		return newWorldView;
	}
	
	
	// return a rotated view while keeping center of view (view projection height=0)
	// Warning: rotation may be slightly different to avoid forbidden view (horizontal)
	public WorldView copyRotated(float deltaRotX, float deltaRotY) {
		return copyWithNewViewAngle(rotX+deltaRotX,rotY+deltaRotY);
	}
	
	// return a rotated view while keeping center of view (view projection height=0)
	// Warning: rotation may be slightly different to avoid forbidden view (horizontal)
	public WorldView copyWithNewViewAngle(float newRotX, float newRotY) {
		
		WorldView newWorldView=(WorldView)clone();
		
		newWorldView.rotX=newRotX;
		newWorldView.rotY=newRotY;

		newWorldView.forceValidView();
		
		float dist=posY/sinRotX(); // distance au point de vis�e (y=0)
		float dist1=dist * cosRotX(); // ancienne distance a plat au point de vis�e
		float dist2=dist * newWorldView.cosRotX(); // nouvelle distance a plat au point de vis�e
		
		newWorldView.posX -= dist1*sinRotY() - dist2*newWorldView.sinRotY();
		newWorldView.posY = dist*newWorldView.sinRotX();
		newWorldView.posZ += dist1*cosRotY() - dist2*newWorldView.cosRotY();
		
		return newWorldView;
		
	}
	
	public void forceValidView() {
		while (rotX>=360) rotX-=360;
		while (rotX<0) rotX+=360;
		while (rotY>=360) rotY-=360;
		while (rotY<0) rotY+=360;
		if (rotX<0.5f) rotX=0.5f;
	}
	
	public String toString() {
		return posX+" "+posY+" "+posZ+" "+rotX+" "+rotY;
	}
	
	public float cosRotX() {
		return (float) Math.cos(rotX * PI_180);
	}
	
	public float sinRotX() {
		return (float) Math.sin(rotX * PI_180);
	}
	
	public float cosRotY() {
		return (float) Math.cos(rotY * PI_180);
	}
	
	public float sinRotY() {
		return (float) Math.sin(rotY * PI_180);
	}
		
}
