package fr.dow.mapviewer.display.renderer;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;
import java.util.EventListener;
import java.util.EventObject;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLException;
import javax.media.opengl.glu.gl2.GLUgl2;
import javax.swing.event.EventListenerList;

import com.jogamp.opengl.util.awt.Screenshot;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.sgbmap.SgbMap;

import fr.dow.mapviewer.display.joglobject.JoglSelection;
import fr.dow.mapviewer.display.joglobject.JoglWorld;
import fr.dow.mapviewer.display.params.MapDisPars;
import fr.dow.mapviewer.display.selection.SelFilter;
import fr.dow.mapviewer.display.selection.Selection;
import fr.dow.mapviewer.display.selection.SelFilter.FilterType;

@SuppressWarnings("deprecation")
public class MapViewerRenderer implements GLEventListener,MouseListener, MouseMotionListener,KeyListener {

    private static final float drawSelOffset = 0.2f;
	private static final float drawMouseOverOffset = 0.0f;

	private GLUgl2 glu = new GLUgl2();

    private SgbMap map;
	private DowMod modData;    
	
    public JoglWorld joglWorld;
	
	public MapDisPars mapDisplayParams=new MapDisPars();
	
	public SelFilter curFilter=new SelFilter(FilterType.NONE);
	public Selection currentSelection=new Selection(); 
	
	private boolean shiftKey=false;
	private Selection mouseOver=new Selection();
	
	public Mouse3DMoveHandler mouse3DMoveHandler;
	
	public ScreenPosition mScreenPos0=null; // not null means that selection is in rectangle mode
	public ScreenPosition mScreenPos;
	
	public WorldPosition mWorldPos;
	
	private File screenshotFile=null;
	private int screenshotWidth;
	private int screenshotHeight;

	
	//
	// general methods
	//
	
    public MapViewerRenderer(GLCanvas glCanvas, SgbMap map, DowMod modData) {
    	
    	this.map=map;
    	this.modData=modData;
    	
		mouse3DMoveHandler=new Mouse3DMoveHandler(glCanvas);
    	
        glCanvas.addGLEventListener(this);
        
        glCanvas.addMouseListener(this);
        glCanvas.addMouseMotionListener(this);
        glCanvas.addKeyListener(this);

        
    	joglWorld=new JoglWorld(map, modData, mapDisplayParams);
    	
    }
    
	public void RequireScreenShot(File imageFile, int imageWidth, int imageHeight) {
		screenshotFile=imageFile;
		screenshotWidth=imageWidth;
		screenshotHeight=imageHeight;
	}

	private void saveScreenshot() {
		try {
			Screenshot.writeToTargaFile(screenshotFile, screenshotWidth, screenshotHeight);
		} catch (GLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		screenshotFile=null;
    }
    
	private void applySelection() {
		
		Selection tmpSelection=mouseOver;
		
		if (shiftKey) {
			if (currentSelection.containsAll(curFilter,tmpSelection)) {
				currentSelection.removeAll(curFilter,tmpSelection);
			} else {
				currentSelection.addAll(curFilter,tmpSelection);
			}
		}
		else {
			currentSelection.clear(curFilter);
			currentSelection.addAll(curFilter,tmpSelection);
		}
		currentSelection.fireSelectionChangeEvent();
	}
	
	private void drawRectangle(GL2 gl, ScreenPosition pos1, ScreenPosition pos2) {
		
        gl.glMatrixMode(GL2.GL_PROJECTION); 
        gl.glPushMatrix();
        gl.glLoadIdentity();
        
		int[] viewport=new int[4];
        gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);
        gl.glOrtho(0, viewport[2], viewport[3], 0, 1, -1); // switch to ortho mode
        

        gl.glMatrixMode(GL2.GL_MODELVIEW); 
        gl.glPushMatrix();
        gl.glLoadIdentity();
        
		gl.glBegin (GL2.GL_LINE_LOOP);
		gl.glVertex3f( pos1.x, pos1.y, 0f);
		gl.glVertex3f( pos1.x, pos2.y, 0f);
		gl.glVertex3f( pos2.x, pos2.y, 0f);
		gl.glVertex3f( pos2.x, pos1.y, 0f);
		gl.glEnd();
		
        gl.glMatrixMode(GL2.GL_PROJECTION); 
        gl.glPopMatrix();
        gl.glMatrixMode(GL2.GL_MODELVIEW); 
        gl.glPopMatrix();
		
	}
	
	//
	// OpenGL interface implementation
	//
    
	public void init(GLAutoDrawable drawable) throws GLException {
		
        GL2 gl = drawable.getGL().getGL2();

		// create OpenGL terrain link object
       	joglWorld.init(gl, map, modData );
       	
        //
        // Init OpenGL parameters
        //
        gl.glEnable(GL2.GL_CULL_FACE);
        gl.glEnable(GL2.GL_COLOR_MATERIAL) ;
        gl.glFrontFace(GL2.GL_CW);
       
        //
        // set drawing primitives parameters
        //
        gl.glPointSize(4.0f);
        
        //
        // low graphic quality : better performances and result looks no worse.
        //
        gl.glShadeModel(GL2.GL_FLAT); //Enables Smooth Color Shading
        //gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST); // Really Nice Perspective Calculations
        
        gl.glEnable(GL2.GL_DEPTH_TEST); //Enables Depth Testing
        gl.glClearDepth(1.0); //Enables Clearing Of The Depth Buffer
        gl.glDepthFunc(GL2.GL_LEQUAL); //The Type Of Depth Test To Do
        
        //
        // Set lighting
        //
        gl.glEnable(GL2.GL_LIGHTING);
        float pos[] = { 5.0f, 5.0f, 10.0f, 0.0f };
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, pos, 0);
        gl.glEnable(GL2.GL_LIGHT0);
        
        //
        // Set default material and background color
        //
        gl.glColorMaterial ( GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE ) ;
        gl.glClearColor(0.71f, 0.76f, 0.53f, 0.0f);              

        
    }

    public void display(GLAutoDrawable drawable) {
    	
    	// update display source informations
        GL2 gl = drawable.getGL().getGL2();

        // Clear display
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT); //Clear The Screen And The Depth Buffer
        gl.glEnable(GL2.GL_DEPTH_TEST); //Enables Depth Testing
		gl.glEnable(GL2.GL_LIGHTING);
        
        // Set view direction
        mouse3DMoveHandler.setViewDirection(gl);
         
		// Draw scene
       	joglWorld.draw(gl,map,modData);
       	
		// capture screenshot if needed
       	if (screenshotFile!=null) saveScreenshot();
       	
       	// switch to line mode for additional display (no more shadings, depth test,...)
       	gl.glDisable(GL2.GL_LIGHTING);
        gl.glDisable(GL2.GL_DEPTH_TEST);
        
        // detect mouse position on ground (requires that scene has been drawn)
       	mWorldPos=WorldPosition.getNewPosition(gl,mScreenPos);
        
       	// detect entities below mouse
		if (mScreenPos!=null) 
			mouseOver = JoglSelection.getMousePointOver(gl, joglWorld, map, curFilter, mScreenPos, mScreenPos0);
		else 
			mouseOver.clearAll();
		
		//display additional extensions
		fireDisplayEvent(gl, mouseOver, mScreenPos, mScreenPos0);

		// Selection display
        JoglSelection.drawSelected(gl, currentSelection, curFilter, mapDisplayParams.selDisPars.selected, joglWorld, map, drawSelOffset);
       	
       	// mouse over display
        JoglSelection.drawSelected(gl, mouseOver, curFilter, mapDisplayParams.selDisPars.over, joglWorld, map, drawMouseOverOffset);
		
        // display color for mouse position and selection rectangle
		gl.glColor4f(
				mapDisplayParams.selDisPars.mouseArea.getRed()/255f, 
				mapDisplayParams.selDisPars.mouseArea.getGreen()/255f, 
				mapDisplayParams.selDisPars.mouseArea.getBlue()/255f,
				1.0f);
		
		if (mScreenPos0!=null) drawRectangle(gl, mScreenPos0, mScreenPos);
		else 
			if (mWorldPos!=null) {
				gl.glBegin (GL2.GL_POINTS);
				gl.glVertex3f( mWorldPos.x,  mWorldPos.y, -mWorldPos.z);
				gl.glEnd ();
			}
		
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

    public void reshape(GLAutoDrawable drawable,
                        int xstart,
                        int ystart,
                        int width,
                        int height) {
        GL2 gl = drawable.getGL().getGL2();

        height = (height == 0) ? 1 : height;

        gl.glViewport(0, 0, width, height);
        
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45, (float) width / height, 0.1, 100000);
        
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void displayChanged(GLAutoDrawable drawable,
                               boolean modeChanged,
                               boolean deviceChanged) {
    }

    public void dispose(GLAutoDrawable drawable) { }

	//
	// Mouse interface implementation
	//
    
    
	public void mouseClicked(MouseEvent e) {}

	public void mousePressed(MouseEvent e) {
		if ( ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK)!=0)  && (mScreenPos0==null))
			mScreenPos0=new ScreenPosition(e.getX(),e.getY());
	}

	public void mouseReleased(MouseEvent e) {
		if (((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK)==0) && (mScreenPos0!=null)) {
			applySelection();
			mScreenPos0=null;
		}
	}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}

	public void mouseDragged(MouseEvent e) {
		if (mScreenPos0==null) {
			if ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK)!=0) {
				mScreenPos0=mScreenPos;
			}
		}
		mScreenPos=new ScreenPosition(e.getX(),e.getY());
	}

	public void mouseMoved(MouseEvent e) {
		mScreenPos=new ScreenPosition(e.getX(),e.getY());
	}
 
	//
	// Keyboard interface implementation
	//
	
	public void keyPressed(KeyEvent arg0) {
		shiftKey=arg0.isShiftDown();
	}

	public void keyReleased(KeyEvent arg0) {
		shiftKey=arg0.isShiftDown();
	}

	public void keyTyped(KeyEvent arg0) {
	}
	
	//
	// display Event
	// 
	
	protected EventListenerList displayListenerList = new EventListenerList();

	public class DisplayEvent extends EventObject {
		public GL2 gl;
		public Selection mouseOver;
		public ScreenPosition screenPos;
		public ScreenPosition screenPos0;
		public DisplayEvent(Object source, GL2 gl, Selection mouseOver, ScreenPosition screenPos, ScreenPosition screenPos0) {
			super(source);
			this.gl=gl;
			this.mouseOver=mouseOver;
			this.screenPos=screenPos;
			this.screenPos0=screenPos0;
		}
	}

	public interface DisplayListener extends EventListener {
		public void display(DisplayEvent evt);
	}

	public void addDisplayListener(DisplayListener listener) {
		displayListenerList.add(DisplayListener.class, listener);
	}
	
	public void removeDisplayListener(DisplayListener listener) {
		displayListenerList.remove(DisplayListener.class, listener);
	}
	
	protected void fireDisplayEvent(GL2 gl, Selection mouseOver, ScreenPosition screenPos, ScreenPosition screenPos0) {
		DisplayEvent evt=new DisplayEvent(this,gl,mouseOver,screenPos,screenPos0);
		Object[] listeners = displayListenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == DisplayListener.class) {
				((DisplayListener) listeners[i+1]).display(evt);
			}
		}
	}
	
	
	
	
}    
