package fr.dow.mapviewer.display.renderer;

import java.nio.ByteBuffer;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.gl2.GLUgl2;

import com.jogamp.common.nio.Buffers;

public class WorldPosition {

	public float x;
	public float y;
	public float z;
	
	private static GLUgl2 glu = new GLUgl2();
	
	public static WorldPosition getNewPosition(GL2 gl, ScreenPosition screenPos) {

		if (screenPos==null) return null;
		
		int viewport[] = new int[4];
		double mvmatrix[] = new double[16];
		double projmatrix[] = new double[16];
		double position[]=new double[3];;
		ByteBuffer buffer = Buffers.newDirectByteBuffer(4); 
		
		gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);
        gl.glGetDoublev(GL2.GL_MODELVIEW_MATRIX, mvmatrix, 0);
        gl.glGetDoublev(GL2.GL_PROJECTION_MATRIX, projmatrix, 0);

		int y1 = viewport[3] - screenPos.y;
		
		buffer.clear();
		gl.glReadPixels( screenPos.x, y1, 1, 1, GL2.GL_DEPTH_COMPONENT, GL2.GL_FLOAT, buffer );
		float depth=buffer.getFloat();
		
		if ( (depth==1.0f) || (depth==0.0f)) return null;

		glu.gluUnProject(screenPos.x, y1, depth, 
	              mvmatrix, 0,
	              projmatrix, 0, 
	              viewport, 0, 
	              position, 0);
		
		WorldPosition newPos=new WorldPosition();
		
		newPos.x=(float) position[0];
		newPos.y=(float) position[1];
		newPos.z=(float) -position[2];
        
		return newPos; 
	}
	
}
