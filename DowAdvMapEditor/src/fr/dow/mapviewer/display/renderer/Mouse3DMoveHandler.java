package fr.dow.mapviewer.display.renderer;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.media.opengl.GL2;
import javax.media.opengl.awt.GLCanvas;

public class Mouse3DMoveHandler implements MouseListener, MouseMotionListener, MouseWheelListener,KeyListener {

	private static final int KEY_CODE_RESET_WORLD_VIEW=8;
	private static final int KEY_CODE_UP=38;
	private static final int KEY_CODE_DOWN=40;
	private static final int KEY_CODE_LEFT=37;
	private static final int KEY_CODE_RIGHT=39;
	
	private WorldView worldView=WorldView.newDefault();
	
	private ScreenPosition prevMousePosMove = null;
	private ScreenPosition prevMousePosRot = null;
	private ScreenPosition prevMousePosZoom = null;
	
	private boolean altKey;
	private Long lastTime;

	public Mouse3DMoveHandler(GLCanvas glCanvas) {
        glCanvas.addMouseListener(this);
        glCanvas.addMouseMotionListener(this);
        glCanvas.addMouseWheelListener(this);
        glCanvas.addKeyListener(this);
	}

	public void setViewDirection(GL2 gl) {
        WorldView tmpView=worldView;
        gl.glLoadIdentity();
		gl.glRotatef(-tmpView.rotX, 1.0f, 0.0f, 0.0f);
		gl.glRotatef(tmpView.rotY, 0.0f, 1.0f, 0.0f);
		gl.glTranslatef(-tmpView.posX, -tmpView.posY, -tmpView.posZ);	    
	}

	public WorldView getView() {
		return (WorldView) worldView.clone();
	}
	
	public void setView(WorldView newWorldView) {
		worldView=newWorldView;
	}
	
	public void setView(float x,float y, float z, float lookdown, float heading) {

		WorldView newWorldView=new WorldView();
		
		newWorldView.posX=x;
		newWorldView.posY=y;
		newWorldView.posZ=z;
		newWorldView.rotX=lookdown;
		newWorldView.rotY=heading;
		
		newWorldView.forceValidView();
		
		worldView=newWorldView;
	}
	
	//
	// mouse events management
	//

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}

	public void mousePressed(MouseEvent e) {}

	public void mouseReleased(MouseEvent e) {
		
		if ((e.getModifiersEx() & MouseEvent.BUTTON2_DOWN_MASK)==0) 
			if (prevMousePosMove!=null)
				prevMousePosMove=null;
			
		
		if ((e.getModifiersEx() & (MouseEvent.BUTTON1_DOWN_MASK+MouseEvent.BUTTON3_DOWN_MASK))==0) 
			if (prevMousePosZoom!=null)
				prevMousePosZoom=null;
	}

	public void mouseClicked(MouseEvent e) {}

	public void mouseDragged(MouseEvent e) {
		
		int x = e.getX();
		int y = e.getY();
		Dimension size = e.getComponent().getSize();

		if ((e.getModifiersEx() & MouseEvent.BUTTON2_DOWN_MASK)!=0) { 
			
			if (prevMousePosMove==null) prevMousePosMove=new ScreenPosition(x,y);  
			
			float deltaX = 512.0f * ( (float)(x-prevMousePosMove.x) / (float)size.width );
			float deltaY = 512.0f * ( (float)(y-prevMousePosMove.y) / (float)size.height );
			worldView=worldView.copyMoved(deltaX, deltaY);
			
			prevMousePosMove.x=x;
			prevMousePosMove.y=y;
			return;
		}
		
		if (altKey && ((e.getModifiersEx() & (MouseEvent.BUTTON1_DOWN_MASK+MouseEvent.BUTTON3_DOWN_MASK))!=0)) {
			
			if (prevMousePosZoom==null) prevMousePosZoom=new ScreenPosition(x,y);  
			
			float disp = 512.0f * ( (float)(y-prevMousePosZoom.y) / (float)size.height );
			worldView=worldView.copyZoomed(disp);
			
			prevMousePosZoom.x=x;
			prevMousePosZoom.y=y;
			return;
		}
		
	}

	public void mouseMoved(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		Dimension size = e.getComponent().getSize();

		if (altKey) {
			
			if (prevMousePosRot==null) prevMousePosRot=new ScreenPosition(x,y);  
			
			float deltaX = -360.0f * ( (float)(y-prevMousePosRot.y) / (float)size.height );
			float deltaY =  360.0f * ( (float)(x-prevMousePosRot.x) / (float)size.width );
			worldView=worldView.copyRotated(deltaX,deltaY);
			
			prevMousePosRot.x=x;
			prevMousePosRot.y=y;
			return;
		}
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		float disp= (float)(e.getWheelRotation() * 2.5f * Math.sqrt(Math.abs(worldView.posY)));
		worldView=worldView.copyZoomed(disp);
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode()==KEY_CODE_RESET_WORLD_VIEW) worldView=WorldView.newDefault();
		
		switch (e.getKeyCode()) {
			case KEY_CODE_UP : worldView=worldView.copyMoved(0f,0.2f*Math.abs(worldView.posY)); break;
			case KEY_CODE_DOWN : worldView=worldView.copyMoved(0f,-0.2f*Math.abs(worldView.posY)); break;
			case KEY_CODE_LEFT : worldView=worldView.copyMoved(0.2f*Math.abs(worldView.posY),0f); break;
			case KEY_CODE_RIGHT : worldView=worldView.copyMoved(-0.2f*Math.abs(worldView.posY),0f); break;
			default : if (lastTime!=null) lastTime=null;
				
		}
		
		altKey=e.isAltDown();

	}

	public void keyReleased(KeyEvent e) {
		altKey=e.isAltDown();
		if (!altKey) { 
			if (prevMousePosRot!=null) prevMousePosRot=null;
			if (prevMousePosZoom!=null) prevMousePosZoom=null;
		}
	}

	public void keyTyped(KeyEvent e) {}

}
